//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_OBJECTTYPES_H
#define MARIONETTE_OBJECTTYPES_H

#include <utility>
#include <type_traits>

#include "MarionetteBase.h"
#include "CollectionHelpers.h"
#include "ObjectHelpers.h"

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

namespace Marionette
{
  namespace Collections
  {
    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES owning_object_holder_individual
      {
        static_assert(Utility::always_false<T>, "Please initialize the Object properly with a TypeHolder of properties!");
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES owning_object_holder_individual_aggregator
      {
        static_assert(Utility::always_false<T>, "Please initialize the Object properly with a TypeHolder of properties!");
      };

#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wunsafe-buffer-usage"
#endif
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class... PropExs>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES owning_object_holder_individual<Utility::TypeHolder<T, PropExs...>>
      {
       private:

        static constexpr PropertiesIndexer total_size = (PropExs::extent + ...);

       protected:

        T m_arr[total_size];

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const T &
        get_object(const PropertiesIndexer idx) const
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(idx >= 0 && idx < total_size);
#endif

          return m_arr[idx];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr T &
        get_object(const PropertiesIndexer idx)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(idx >= 0 && idx < total_size);
#endif

          return m_arr[idx];
        }
      };
#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic pop
#endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Ts>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES owning_object_holder_individual_aggregator<Utility::TypeHolder<Ts...>> :
        public owning_object_holder_individual<Ts>...
      {
       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class T, class... PropExs>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer get_base_offset(Utility::TypeHolder<T, PropExs...>)
        {
          PropertiesIndexer ret = 0;

          bool found = false;

          (((found = found || std::is_same_v<Property, typename PropExs::Property>), ret += PropExs::extent * (!found)), ...);

          return ret * found;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class T, class... PropExs>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer get_property_extent(Utility::TypeHolder<T, PropExs...>)
        {
          return ((PropExs::extent * std::is_same_v<Property, typename PropExs::Property>) +...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto find_correct_type_holder()
        {
          return Utility::get_one_type_or_invalid(
            (Utility::TypeHolder<Utility::InvalidType> {} | ... |
             std::conditional_t<Ts::template contains<typename Property::Type>(), Utility::TypeHolder<Ts>, Utility::TypeHolder<Utility::InvalidType>> {}));
        }

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto & get_object(const PropertiesIndexer mult_arr_idx) const
        {
          constexpr auto this_type_holder = find_correct_type_holder<Property>();

          static_assert(!std::is_same_v<std::decay_t<decltype(this_type_holder)>, Utility::InvalidType>, "'get_object' must be called with a valid property!");

          static_assert(this_type_holder.number() > 1, "'get_object' must be called with a valid property!");
          //In case the collection does not contain the property,
          //find_correct_type_holder will return an empty type holder.
          //This should never happen due to the way this is handled
          //by the Object class itself, which checks for a valid property.

          constexpr PropertiesIndexer base_index = get_base_offset<Property>(this_type_holder);

          [[maybe_unused]] constexpr PropertiesIndexer maximum_extent = get_property_extent<Property>(this_type_holder);

#if MARIONETTE_USE_ASSERTIONS
          assert(mult_arr_idx >= 0 && mult_arr_idx < maximum_extent);
#endif

          return owning_object_holder_individual<std::decay_t<decltype(this_type_holder)>>::get_object(base_index + mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & get_object(const PropertiesIndexer mult_arr_idx)
        {
          constexpr auto this_type_holder = find_correct_type_holder<Property>();

          static_assert(this_type_holder.number() > 1, "'get_object' must be called with a valid property!");
          //In case the collection does not contain the property,
          //find_correct_type_holder will return an empty type holder.
          //This should never happen due to the way this is handled
          //by the Object class itself, which checks for a valid property.

          constexpr PropertiesIndexer base_index = get_base_offset<Property>(this_type_holder);

          [[maybe_unused]] constexpr PropertiesIndexer maximum_extent = get_property_extent<Property>(this_type_holder);

#if MARIONETTE_USE_ASSERTIONS
          assert(mult_arr_idx >= 0 && mult_arr_idx < maximum_extent);
#endif

          return owning_object_holder_individual<std::decay_t<decltype(this_type_holder)>>::get_object(base_index + mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto * get_object_pointer(const PropertiesIndexer mult_arr_idx) const
        {
          return &this->template get_object<Property>(mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto * get_object_pointer(const PropertiesIndexer mult_arr_idx)
        {
          return &this->template get_object<Property>(mult_arr_idx);
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES owning_object_holder_jv
      {
        static_assert(Utility::always_false<T>, "Please initialize the Object properly with a TypeHolder of properties!");
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES owning_object_holder_jv_aggregator
      {
        static_assert(Utility::always_false<T>, "Please initialize the Object properly with a TypeHolder of properties!");
      };

#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wunsafe-buffer-usage"
#endif
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class JVProp, PropertiesIndexer extent, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES owning_object_holder_jv<InterfaceDescription::PropertyAndExtent<JVProp, extent>, Layout>
      {
       protected:

        Collection<Layout, typename JVProp::Properties> m_coll[extent];

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto &
        get_jagged_vector_underlying_collection(const PropertiesIndexer idx) const
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(idx >= 0 && idx < extent);
#endif

          return m_coll[idx];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto &
        get_jagged_vector_underlying_collection(const PropertiesIndexer idx)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(idx >= 0 && idx < extent);
#endif

          return m_coll[idx];
        }
      };

#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic pop
#endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Ts, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES owning_object_holder_jv_aggregator<Utility::TypeHolder<Ts...>, Layout> :
        public owning_object_holder_jv<Ts, Layout>...
      {
       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto find_property_and_extent()
        {
          constexpr auto pre_ret = (Utility::TypeHolder<Utility::InvalidType> {} & ... &
                                    Utility::TypeHolder<std::conditional_t<std::is_same_v<typename Ts::Property, Property>, Ts, Utility::InvalidType>> {});

          return Utility::get_one_type_or_invalid(pre_ret);
        }

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class MetaInfo>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
        get_jagged_vector_underlying_collection(const PropertiesIndexer mult_arr_idx = 0) const
        {
          static_assert(MetaInfo::extent == 1, "Jagged vector objects should not be created in an unspecified access!");

          constexpr auto prop_ex = find_property_and_extent<Property>();

          return owning_object_holder_jv<std::decay_t<decltype(prop_ex)>, Layout>::get_jagged_vector_underlying_collection(mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class MetaInfo>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
        get_jagged_vector_underlying_collection(const PropertiesIndexer mult_arr_idx = 0)
        {
          static_assert(MetaInfo::extent == 1, "Jagged vector objects should not be created in an unspecified access!");

          constexpr auto prop_ex = find_property_and_extent<Property>();

          return owning_object_holder_jv<std::decay_t<decltype(prop_ex)>, Layout>::get_jagged_vector_underlying_collection(mult_arr_idx);
        }
      };
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class SizeType, class DifferenceType, class JaggedVectorLayoutType>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES OwningObject
    {
      using OriginalLayout = OwningObject;

      using size_type       = SizeType;
      using difference_type = DifferenceType;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
      interface_properties()
      {
        return InterfaceProperties {AccessProperties::Full, ResizeProperties::Full, MutabilityProperties::Full};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool object_is_reference()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_write_proxy()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool stores_multi_array_index()
      {
        return false;
      }

      template <class Property, class MetaInfo>
      using JaggedVectorCollectionType = Collection<JaggedVectorLayoutType, typename Property::Properties, MetaInfo>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Types>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES object_holder :
        public impl::owning_object_holder_individual_aggregator<
          typename InterfaceDescription::InterfaceInformation<Types>::template flattened_properties_by_type<true, false, false, true>>,
        public impl::owning_object_holder_jv_aggregator<
          typename InterfaceDescription::InterfaceInformation<Types>::template flattened_jagged_vector<true, false, true>,
          JaggedVectorLayoutType>
      {
        friend struct Utility::FriendshipProvider;

        template <class A, class B, class C>
        friend struct Object;

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr PropertiesIndexer
        get_multi_array_index() const
        {
          return 0;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type get_proxied_index() const
        {
          return 0;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
        update_multi_array_index(const PropertiesIndexer) const
        {
        }
      };
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    namespace impl
    {
#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
      __pragma("nv_diag_suppress 20012")
  #else
      _Pragma("nv_diag_suppress 20012")
  #endif
#endif
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class PtrType, class SizeType, bool has_index, bool has_multi_array_index>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ProxyObjectHolderBase;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class PtrType, class SizeType>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ProxyObjectHolderBase<PtrType, SizeType, false, false>
      {
       protected:

        PtrType m_ptr;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ProxyObjectHolderBase(const PtrType & ptr): m_ptr(ptr)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ProxyObjectHolderBase() = default;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
        update_multi_array_index(const PropertiesIndexer) const
        {
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class PtrType, class SizeType>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ProxyObjectHolderBase<PtrType, SizeType, false, true>
      {
       protected:

        PtrType           m_ptr      = {};
        PropertiesIndexer m_marr_idx = 0;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ProxyObjectHolderBase(const PtrType & ptr, const PropertiesIndexer & marr_idx):
          m_ptr(ptr),
          m_marr_idx(marr_idx)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ProxyObjectHolderBase() = default;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class PtrType, class SizeType>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ProxyObjectHolderBase<PtrType, SizeType, true, false>
      {
       protected:

        PtrType  m_ptr = {};
        SizeType m_idx = 0;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ProxyObjectHolderBase(const PtrType & ptr, const SizeType & idx): m_ptr(ptr), m_idx(idx)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ProxyObjectHolderBase() = default;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class PtrType, class SizeType>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ProxyObjectHolderBase<PtrType, SizeType, true, true>
      {
       protected:

        PtrType           m_ptr      = {};
        PropertiesIndexer m_marr_idx = 0;
        SizeType          m_idx      = 0;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ProxyObjectHolderBase(const PtrType &           ptr,
                                                                                              const PropertiesIndexer & marr_idx,
                                                                                              const SizeType &          idx):
          m_ptr(ptr),
          m_marr_idx(marr_idx),
          m_idx(idx)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ProxyObjectHolderBase() = default;
      };

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
      __pragma("nv_diag_default 20012")
  #else
      _Pragma("nv_diag_default 20012")
  #endif
#endif

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto proxy_object_is_to_owning_object(T)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SzTp, class DfTp, class Layout>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto proxy_object_is_to_owning_object(OwningObject<SzTp, DfTp, Layout>)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_collection_layout_original_type(T, Args &&...)
      {
        return T {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Ret = typename T::OriginalLayoutType>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_collection_layout_original_type(T)
      {
        return Ret {};
      }

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class PtrType, bool force_constant, bool with_multi_array_index>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ProxyObject
    {
     private:

      using ProxiedType = std::decay_t<decltype(*(std::declval<PtrType>()))>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool proxies_another_object()
      {
        return decltype(impl::proxy_object_is_to_owning_object(OriginalLayout {}))::value;
      }

     public:

      using OriginalLayout = Layout;

      template <bool other_fc>
      using constant_type = ProxyObject<OriginalLayout, PtrType, other_fc || force_constant, with_multi_array_index>;

      template <bool other_fc>
      using with_multi_array_type = ProxyObject<OriginalLayout, PtrType, other_fc || force_constant, true>;

      using size_type       = typename OriginalLayout::size_type;
      using difference_type = typename OriginalLayout::difference_type;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
      interface_properties()
      {
        return OriginalLayout::interface_properties() & InterfaceProperties {AccessProperties::Full,
                                                                             force_constant ? ResizeProperties::NoResize : ResizeProperties::Full,
                                                                             force_constant ? MutabilityProperties::NoModify : MutabilityProperties::Full};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool object_is_reference()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_write_proxy()
      {
        return proxies_another_object();
        //The "another object" being proxied is a WriteProxy, by construction.
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool stores_multi_array_index()
      {
        return with_multi_array_index;
      }

     protected:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class MetaInfo, bool is_object_proxy>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES JaggedVectorCollectionTypeGetterHelper;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES JaggedVectorCollectionTypeGetterHelper<Property, MetaInfo, true>
      {
        using Type = typename OriginalLayout::template JaggedVectorCollectionType<Property, MetaInfo>;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES JaggedVectorCollectionTypeGetterHelper<Property, MetaInfo, false>
      {
        using LayoutType =
          LayoutTypes::Internal::ViewLayout<OriginalLayout, PtrType, ResizeProperties::Full, MutabilityProperties::Full, stores_multi_array_index()>;

        using Type = Collection<LayoutType,
                                typename Property::Properties::template append<InterfaceDescription::JaggedVectorSizeProperty<Property>>,
                                typename MetaInfo::template override_size_tag<InterfaceDescription::JaggedVectorSizeProperty<Property>>>;
      };

     public:

      template <class Property, class MetaInfo>
      using JaggedVectorCollectionType = typename JaggedVectorCollectionTypeGetterHelper<Property, MetaInfo, proxies_another_object()>::Type;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Types>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES object_holder :
        impl::ProxyObjectHolderBase<PtrType, size_type, !proxies_another_object(), with_multi_array_index>
      {
       private:

        using Base = impl::ProxyObjectHolderBase<PtrType, size_type, !proxies_another_object(), with_multi_array_index>;

       public:

        using Base::Base;

        friend struct Utility::FriendshipProvider;

        template <class A, class B, class C>
        friend struct Object;

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto & get_proxied_pointer() const
        {
          return this->m_ptr;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & get_proxied_pointer()
        {
          return this->m_ptr;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_proxied_index() const
        {
          if constexpr (proxies_another_object())
            {
              return size_type {0};
            }
          else
            {
              return static_cast<const size_type &>(this->m_idx);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_proxied_index()
        {
          if constexpr (proxies_another_object())
            {
              return size_type {0};
            }
          else
            {
              return static_cast<size_type &>(this->m_idx);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
        get_multi_array_index() const
        {
          if constexpr (with_multi_array_index)
            {
              return static_cast<const PropertiesIndexer &>(this->m_marr_idx);
            }
          else
            {
              return PropertiesIndexer {0};
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array_index()
        {
          if constexpr (with_multi_array_index)
            {
              return static_cast<PropertiesIndexer &>(this->m_marr_idx);
            }
          else
            {
              return PropertiesIndexer {0};
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
        update_multi_array_index([[maybe_unused]] const PropertiesIndexer new_index)
        {
          if constexpr (with_multi_array_index)
            {
              this->get_multi_array_index() = new_index;
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class MetaInfo, class disabler = std::enable_if<!proxies_another_object()>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto
        build_proxy_jagged_vector_outer_collection([[maybe_unused]] const PropertiesIndexer mult_arr_idx) const
        {
          using NewLayoutType = typename JaggedVectorCollectionTypeGetterHelper<Property, MetaInfo, false>::LayoutType;

          if constexpr (stores_multi_array_index())
            {
              return Collection<NewLayoutType, Utility::TypeHolder<Property>, typename MetaInfo::template override_extent<1>> {
                this->m_ptr,
                mult_arr_idx};
            }
          else
            {
              return Collection<NewLayoutType, Utility::TypeHolder<Property>, typename MetaInfo::template override_extent<1>> {
                this->m_ptr};
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class MetaInfo, class disabler = std::enable_if<!proxies_another_object()>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto
        build_proxy_jagged_vector_outer_collection([[maybe_unused]] const PropertiesIndexer mult_arr_idx)
        {
          using NewLayoutType = typename JaggedVectorCollectionTypeGetterHelper<Property, MetaInfo, false>::LayoutType;

          if constexpr (stores_multi_array_index())
            {
              return Collection<NewLayoutType, Utility::TypeHolder<Property>, typename MetaInfo::template override_extent<1>> {
                this->m_ptr,
                mult_arr_idx};
            }
          else
            {
              return Collection<NewLayoutType, Utility::TypeHolder<Property>, typename MetaInfo::template override_extent<1>> {
                this->m_ptr};
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class MetaInfo>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
        get_jagged_vector_underlying_collection(const PropertiesIndexer mult_arr_idx) const
        {
          if constexpr (ProxyObject::proxies_another_object())
            {
              return Utility::FriendshipProvider::template get_jagged_vector_underlying_collection<Property, MetaInfo>(*(this->m_ptr), mult_arr_idx);
            }
          else
            {
              return this->template build_proxy_jagged_vector_outer_collection<Property, MetaInfo>(mult_arr_idx).underlying_collection();
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class MetaInfo>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
        get_jagged_vector_underlying_collection(const PropertiesIndexer mult_arr_idx)
        {
          if constexpr (ProxyObject::proxies_another_object())
            {
              return Utility::FriendshipProvider::template get_jagged_vector_underlying_collection<Property, MetaInfo>(*(this->m_ptr), mult_arr_idx);
            }
          else
            {
              return this->template build_proxy_jagged_vector_outer_collection<Property, MetaInfo>(mult_arr_idx).underlying_collection();
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object(const PropertiesIndexer mult_arr_idx) const
        {
          if constexpr (ProxyObject::proxies_another_object())
            {
              return Utility::FriendshipProvider::template get_object<Property>(*(this->m_ptr), mult_arr_idx);
            }
          else
            {
              return Utility::FriendshipProvider::template get_array<Property>(*(this->m_ptr), mult_arr_idx, this->m_idx);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object(const PropertiesIndexer mult_arr_idx)
        {
          if constexpr (ProxyObject::proxies_another_object())
            {
              return Utility::FriendshipProvider::template get_object<Property>(*(this->m_ptr), mult_arr_idx);
            }
          else
            {
              return Utility::FriendshipProvider::template get_array<Property>(*(this->m_ptr), mult_arr_idx, this->m_idx);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object_pointer(const PropertiesIndexer mult_arr_idx) const
        {
          if constexpr (ProxyObject::proxies_another_object())
            {
              return Utility::FriendshipProvider::template get_object_pointer<Property>(*(this->m_ptr), mult_arr_idx);
            }
          else
            {
              return Utility::FriendshipProvider::template get_array_pointer<Property>(*(this->m_ptr), mult_arr_idx, this->m_idx);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object_pointer(const PropertiesIndexer mult_arr_idx)
        {
          if constexpr (ProxyObject::proxies_another_object())
            {
              return Utility::FriendshipProvider::template get_object_pointer<Property>(*(this->m_ptr), mult_arr_idx);
            }
          else
            {
              return Utility::FriendshipProvider::template get_array_pointer<Property>(*(this->m_ptr), mult_arr_idx, this->m_idx);
            }
        }
      };
    };

  }

}

#endif
