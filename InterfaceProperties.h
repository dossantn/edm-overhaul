//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_INTERFACEPROPERTIES_H
#define MARIONETTE_INTERFACEPROPERTIES_H

namespace Marionette
{

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr AccessProperties
  operator&(const AccessProperties a, const AccessProperties b)
  {
    return (a > b ? b : a);
  }

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr AccessProperties
  operator|(const AccessProperties a, const AccessProperties b)
  {
    return (a > b ? a : b);
  }

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr AccessProperties
  operator-(const AccessProperties a)
  {
    if (a == AccessProperties::Invalid)
      {
        return a;
      }
    else
      {
        return static_cast<AccessProperties>(static_cast<PropertiesIndexer>(a) - 1);
      }
  }

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ResizeProperties
  operator&(const ResizeProperties a, const ResizeProperties b)
  {
    return (a > b ? b : a);
  }

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ResizeProperties
  operator|(const ResizeProperties a, const ResizeProperties b)
  {
    return (a > b ? a : b);
  }

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ResizeProperties
  operator-(const ResizeProperties a)
  {
    if (a == ResizeProperties::Invalid)
      {
        return a;
      }
    else
      {
        return static_cast<ResizeProperties>(static_cast<PropertiesIndexer>(a) - 1);
      }
  }

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr MutabilityProperties
  operator&(const MutabilityProperties a, const MutabilityProperties b)
  {
    return (a > b ? b : a);
  }

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr MutabilityProperties
  operator|(const MutabilityProperties a, const MutabilityProperties b)
  {
    return (a > b ? a : b);
  }

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr MutabilityProperties
  operator-(const MutabilityProperties a)
  {
    if (a == MutabilityProperties::Invalid)
      {
        return a;
      }
    else
      {
        return static_cast<MutabilityProperties>(static_cast<PropertiesIndexer>(a) - 1);
      }
  }

  MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES InterfaceProperties
  {
    AccessProperties     access     = AccessProperties::Invalid;
    ResizeProperties     resize     = ResizeProperties::Invalid;
    MutabilityProperties mutability = MutabilityProperties::Invalid;

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr InterfaceProperties
    operator&(const InterfaceProperties & other) const
    {
      return InterfaceProperties {access & other.access, resize & other.resize, mutability & other.mutability};
    }

    //--------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool valid() const
    {
      return access > AccessProperties::Invalid && resize > ResizeProperties::Invalid && mutability > MutabilityProperties::Invalid;
    }

    //--------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_refer_to_const_subcollection() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::NoModify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool
    can_refer_to_modifiable_subcollection() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::Modify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_refer_to_const_object() const
    {
      return access >= AccessProperties::GetContents && mutability >= MutabilityProperties::NoModify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_refer_to_modifiable_object() const
    {
      return access >= AccessProperties::GetContents && mutability >= MutabilityProperties::Modify;
    }

    //--------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_copy_into_collection() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::Modify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_copy_out_of_collection() const
    {
      return access >= AccessProperties::GetAddress;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_copy_into_object() const
    {
      return can_copy_into_collection() && can_refer_to_modifiable_object();
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_copy_out_of_object() const
    {
      return can_copy_out_of_collection() && can_refer_to_const_object();
    }

    //--------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_move_into_collection() const
    {
      return access >= AccessProperties::GetAddress && resize >= ResizeProperties::ResizePartial && mutability >= MutabilityProperties::Modify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_move_out_of_collection() const
    {
      return access >= AccessProperties::GetAddress && resize >= ResizeProperties::Move && mutability >= MutabilityProperties::Modify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_move_into_object() const
    {
      return access >= AccessProperties::GetContents && resize >= ResizeProperties::ResizePartial && mutability >= MutabilityProperties::Modify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_move_out_of_object() const
    {
      return access >= AccessProperties::GetContents && resize >= ResizeProperties::ResizePartial && mutability >= MutabilityProperties::Modify;
    }

    //--------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_refer_to_const_array() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::NoModify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_refer_to_modifiable_array() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::Modify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_resize_array() const
    {
      return can_refer_to_modifiable_array() && resize >= ResizeProperties::ResizePartial;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_read_sizes() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::NoModify;
    }

    //--------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_resize_partially() const
    {
      return can_resize_array() && resize >= ResizeProperties::ResizePartial;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_resize_fully() const
    {
      return can_resize_array() && resize >= ResizeProperties::Resize;
    }

    //--------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_resize_jagged_vector_object() const
    {
      return can_refer_to_modifiable_object() && resize >= ResizeProperties::ResizePartial;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool
    can_get_jagged_vector_collection_size() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::NoModify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool
    can_set_jagged_vector_collection_size() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::Modify && resize >= ResizeProperties::ResizePartial;
    }

    //--------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_copy_from_array() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::NoModify;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_copy_to_array() const
    {
      return access >= AccessProperties::GetAddress && mutability >= MutabilityProperties::Modify;
    }

    //--------------------------------------------------------------------------------------------------
  };

}

#endif
