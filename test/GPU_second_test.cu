#include <iostream>
#include <vector>
#include <string>

#define NDEBUG

#include "Marionette.h"

static constexpr unsigned int fixed_size = 1024;

static constexpr unsigned int group_size = 4;

static constexpr unsigned int log_2_group_size = 2;

MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(energy, Energy, fixed_size, float);

MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(time, Time, fixed_size, float);

MARIONETTE_DECLARE_SUBCOLLECTION_PROPERTY_SIZED(values, Values, fixed_size, Energy, Time);

MARIONETTE_DECLARE_SIMPLE_PROPERTY_ARRAY_PROPERTY_SIZED(count, Count, fixed_size, group_size, unsigned int);

struct InterfaceAdder : Marionette::InterfaceDescription::NoObject
{
  template <class F, class L>
  struct CollectionFunctions
  {
    constexpr void process_one(int i, unsigned int x)
    {
      F * dhis = static_cast<F *>(this);

      dhis->values().setTime(i, x & 0xFFFCu);
      dhis->values().setEnergy(i, x & 0xFFF0u);

      unsigned int mask = 0xFFu;

      for (unsigned int group = 0; group < group_size; ++group)
        {
          const unsigned int real_group             = (x & mask) >> (group * log_2_group_size);
          dhis->count()[real_group % group_size][i] = (x & mask);
          mask <<= log_2_group_size;
        }
    }
  };
};

struct Alternative
{
  int size = 0;

  struct inner
  {
    float energy[fixed_size];
    float time[fixed_size];
  } values;

  unsigned int count[group_size][fixed_size];

  constexpr void process_one(int i, unsigned int x)
  {
    values.time[i]   = x & 0xFFFCu;
    values.energy[i] = x & 0xFFF0u;

    unsigned int mask = 0xFFu;

    for (unsigned int group = 0; group < group_size; ++group)
      {
        const unsigned int real_group     = (x & mask) >> (group * log_2_group_size);
        count[real_group % group_size][i] = x & mask;
        mask <<= log_2_group_size;
      }
  }
};

using ExampleType   = Marionette::InterfaceDescription::PropertyList<Values, Count, InterfaceAdder>;
using OurCollection = Marionette::Collections::Collection<Marionette::LayoutTypes::LocalStruct<int, int>, ExampleType>;

#include <cstdlib>

__global__ void test_handwritten(Alternative * alt, unsigned int * arr)
{
  const int index = blockIdx.x * blockDim.x + threadIdx.x;

  if (index < alt->size)
    {
      alt->process_one(index, arr[index]);
    }
}

__global__ void test_normal(OurCollection * coll, unsigned int * arr)
{
  const int index = blockIdx.x * blockDim.x + threadIdx.x;

  if (index < coll->size())
    {
      coll->process_one(index, arr[index]);
    }
}

void print(const OurCollection & c)
{
  std::cout << c.size() << ":\n";
  for (auto && v: c)
    {
      std::cout << v.values().energy() << " " << v.values().time();
      for (unsigned int i = 0; i < group_size; ++i)
        {
          std::cout << " " << v.count()[i];
        }
      std::cout << "\n";
    }
  std::cout << std::flush;
}

void print(const Alternative & alt)
{
  std::cout << alt.size << ":\n";
  for (int i = 0; i < alt.size; ++i)
    {
      std::cout << alt.values.energy[i] << " " << alt.values.time[i];
      for (unsigned int group = 0; group < group_size; ++group)
        {
          std::cout << " " << alt.count[group][i];
        }
      std::cout << "\n";
    }
  std::cout << std::flush;
}

int main()
{
  std::srand(42);

  std::cout << sizeof(OurCollection) << " <- " << sizeof(Alternative) << "\n-----------------------------------\n" << std::endl;

  OurCollection coll;

  std::memset(&coll, 0, sizeof(OurCollection));

  coll.resize(20);

  Alternative alt;

  std::memset(&alt, 0, sizeof(Alternative));

  alt.size = 20;

  print(alt);

  std::cout << "\n...\n";

  print(coll);

  std::vector<unsigned int> array(20);

  for (auto & v: array)
    {
      v = std::rand();
    }

  OurCollection * gpu_coll = nullptr;
  Alternative *   gpu_alt  = nullptr;
  unsigned int *  gpu_arr  = nullptr;

  cudaMalloc(&gpu_alt, sizeof(Alternative));
  cudaMalloc(&gpu_coll, sizeof(OurCollection));
  cudaMalloc(&gpu_arr, sizeof(unsigned int) * array.size());

  cudaMemcpy(gpu_alt, &alt, sizeof(Alternative), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_coll, &coll, sizeof(OurCollection), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_arr, array.data(), sizeof(unsigned int) * array.size(), cudaMemcpyHostToDevice);

  test_handwritten<<<4, 64>>>(gpu_alt, gpu_arr);
  test_normal<<<4, 64>>>(gpu_coll, gpu_arr);

  cudaMemcpy(&alt, gpu_alt, sizeof(Alternative), cudaMemcpyDeviceToHost);
  cudaMemcpy(&coll, gpu_coll, sizeof(OurCollection), cudaMemcpyDeviceToHost);

  std::cout << "\n-----------------------------------\n";

  print(alt);

  std::cout << "\n...\n";

  print(coll);

  cudaFree(gpu_alt);
  cudaFree(gpu_coll);
  cudaFree(gpu_arr);

  std::cout << "DONE!" << std::endl;
  
  return 0;
}
