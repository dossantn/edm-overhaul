#include <iostream>
#include <vector>
#include <string>

#include "Marionette.h"

struct Comparator : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    template <class Other, class Dummy1 = decltype(std::declval<Other>().a()), class Dummy2 = decltype(std::declval<Other>().b())>
    friend constexpr bool operator==(const F & f, const Other & o)
    {
      return int(f.a()) == int(o.a()) && int(f.b()) == int(o.b());
    }

    friend std::ostream & operator<<(std::ostream & s, const F & of)
    {
      s << "(" << of.a() << " " << of.b() << ")";

      return s;
    }
  };
};

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(a, A, int);
MARIONETTE_DECLARE_PER_ITEM_PROPERTY(b, B, int);

using Types         = Marionette::InterfaceDescription::PropertyList<A, B, Comparator>;
using OurCollection = Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, Types>;
using OurObject     = Marionette::Collections::Object<Marionette::Collections::OwningObject<>, Types>;

using VecType  = OurCollection;
using ItemType = OurObject;

template <class VecLike>
void print(const VecLike & vl, const std::string & pref = "")
{
  std::cout << pref << (pref.size() > 0 ? ": " : "");
  for (const auto & v: vl)
    {
      std::cout << "(" << v.a() << ", " << v.b() << ") | ";
    }
  std::cout << std::endl;
}

template <class Vec1, class Vec2>
bool check_differences(const Vec1 & v1, const Vec2 & v2)
{
  auto it1 = v1.begin();
  auto it2 = v2.begin();
  for (; it1 != v1.end() && it2 != v2.end(); ++it1, ++it2)
    {
      if (!(*it1 == *it2))
        {
          return true;
        }
    }
  return !(it1 == v1.end() && it2 == v2.end());
}

template <class Vec1, class Vec2>
void test(const Vec1 & v1, const Vec2 & v2, const std::string & test_name)
{
  std::cout << test_name << ":\n";
  if (check_differences(v1, v2))
    {
      print(v1, "      wanted");
      print(v2, "         got");
    }
  else
    {
      std::cout << "     passed!" << std::endl;
    }
  std::cout << "------------------------------------------" << std::endl;
}

template <class T>
void do_test(T)
{
  test(T::template create<std::vector<ItemType>>(), T::template create<VecType>(), T::name());
}

struct Test1
{
  static std::string name()
  {
    return "001";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    return v;
  }
};

struct Test2
{
  static std::string name()
  {
    return "002";
  }

  template <class T>
  static T create()
  {
    T                     v(5, ItemType {1, 1});
    std::vector<ItemType> other(10, {4, 4});
    v = T(other);
    return v;
  }
};

struct Test3
{
  static std::string name()
  {
    return "003";
  }

  template <class T>
  static T create()
  {
    T       v(5, ItemType {1, 1});
    VecType other(10, {4, 4});
    v = static_cast<T>(other);
    return v;
  }
};

struct Test4
{
  static std::string name()
  {
    return "004";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.assign(8, ItemType {9, 9});
    return v;
  }
};

struct Test5
{
  static std::string name()
  {
    return "005";
  }

  template <class T>
  static T create()
  {
    T                     v(5, ItemType {1, 1});
    std::vector<ItemType> other({
      {1, 1},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    v.assign(other.begin() + 1, other.end() - 1);
    return v;
  }
};

struct Test6
{
  static std::string name()
  {
    return "006";
  }

  template <class T>
  static T create()
  {
    T       v(5, ItemType {1, 1});
    VecType other({
      {1, 1},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    v.assign(other.begin() + 1, other.end() - 1);
    return v;
  }
};

struct Test7
{
  static std::string name()
  {
    return "007";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.assign(7, v[3]);
    return v;
  }
};

struct Test8
{
  static std::string name()
  {
    return "008";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.assign({
      {1, 1},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    return v;
  }
};

struct Test9
{
  static std::string name()
  {
    return "009";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.clear();
    return v;
  }
};

struct Test10
{
  static std::string name()
  {
    return "010";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.insert(v.begin(), ItemType {4, 4});
    v.insert(v.end(), ItemType {8, 8});
    v.insert(v.begin() + 4, ItemType {3, 3});
    v.insert(v.end() - 2, ItemType {7, 7});
    return v;
  }
};

struct Test11
{
  static std::string name()
  {
    return "011";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.insert(v.begin() + 1, 3, ItemType {4, 4});
    v.insert(v.begin(), 3, ItemType {0, 0});
    v.insert(v.end(), 3, ItemType {9, 9});
    return v;
  }
};

struct Test12
{
  static std::string name()
  {
    return "012";
  }

  template <class T>
  static T create()
  {
    T v({
      {1, 1},
      {2, 2},
      {3, 3},
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v.insert(v.begin() + 4, 3, v[5]);
    return v;
  }
};

struct Test13
{
  static std::string name()
  {
    return "013";
  }

  template <class T>
  static T create()
  {
    T       v(5, ItemType {1, 1});
    VecType other({
      {1, 0},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    v.insert(v.begin() + 2, other.begin() + 1, other.end() - 1);
    return v;
  }
};

struct Test14
{
  static std::string name()
  {
    return "014";
  }

  template <class T>
  static T create()
  {
    T                     v(5, ItemType {1, 1});
    std::vector<ItemType> other({
      {1, 0},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    v.insert(v.begin() + 2, other.begin() + 1, other.end() - 1);
    return v;
  }
};

struct Test15
{
  static std::string name()
  {
    return "015";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.emplace(v.begin() + 2, 5, 6);
    return v;
  }
};

struct Test16
{
  static std::string name()
  {
    return "016";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.emplace(v.end(), 5, 6);
    return v;
  }
};

struct Test17
{
  static std::string name()
  {
    return "017";
  }

  template <class T>
  static T create()
  {
    T v({
      {1, 1},
      {2, 2},
      {3, 3},
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v.erase(v.begin());
    return v;
  }
};

struct Test18
{
  static std::string name()
  {
    return "018";
  }

  template <class T>
  static T create()
  {
    T v({
      {1, 1},
      {2, 2},
      {3, 3},
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v.erase(v.begin() + 1);
    return v;
  }
};

struct Test19
{
  static std::string name()
  {
    return "019";
  }

  template <class T>
  static T create()
  {
    T v({
      {1, 1},
      {2, 2},
      {3, 3},
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v.erase(v.end() - 1);
    return v;
  }
};

struct Test20
{
  static std::string name()
  {
    return "020";
  }

  template <class T>
  static T create()
  {
    T v({
      {1, 1},
      {2, 2},
      {3, 3},
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v.erase(v.begin(), v.end());
    return v;
  }
};

struct Test21
{
  static std::string name()
  {
    return "021";
  }

  template <class T>
  static T create()
  {
    T v({
      {1, 1},
      {2, 2},
      {3, 3},
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v.erase(v.begin() + 1, v.end() - 1);
    return v;
  }
};

struct Test22
{
  static std::string name()
  {
    return "022";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.push_back({5, 5});
    return v;
  }
};

struct Test23
{
  static std::string name()
  {
    return "023";
  }

  template <class T>
  static T create()
  {
    T v({
      {1, 1},
      {2, 2},
      {3, 3},
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v.push_back(v[2]);
    v.push_back(v[2]);
    return v;
  }
};

struct Test24
{
  static std::string name()
  {
    return "024";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.emplace_back(5, 5);
    return v;
  }
};

struct Test25
{
  static std::string name()
  {
    return "025";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.pop_back();
    return v;
  }
};

struct Test26
{
  static std::string name()
  {
    return "026";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.resize(3);
    return v;
  }
};

struct Test27
{
  static std::string name()
  {
    return "027";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.resize(7);
    return v;
  }
};

struct Test28
{
  static std::string name()
  {
    return "028";
  }

  template <class T>
  static T create()
  {
    T v(5, ItemType {1, 1});
    v.resize(9, {3, 3});
    return v;
  }
};

struct Test29
{
  static std::string name()
  {
    return "029";
  }

  template <class T>
  static T create()
  {
    T v;
    v.resize(9, {3, 3});
    return v;
  }
};

struct Test30
{
  static std::string name()
  {
    return "030";
  }

  template <class T>
  static T create()
  {
    std::vector<ItemType> v({
      {1, 1},
      {2, 2},
      {3, 3},
      {4, 4},
      {5, 5},
      {6, 6}
    });
    return T(v);
  }
};

struct Test31
{
  static std::string name()
  {
    return "031";
  }

  template <class T>
  static T create()
  {
    VecType v({
      {1, 1},
      {2, 2},
      {3, 3},
      {4, 4},
      {5, 5},
      {6, 6}
    });
    return T(v);
  }
};

struct Test32
{
  static std::string name()
  {
    return "032";
  }

  template <class T>
  static T create()
  {
    T                     v(5, {1, 1});
    std::vector<ItemType> other({
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v = static_cast<T>(other);
    return v;
  }
};

struct Test33
{
  static std::string name()
  {
    return "033";
  }

  template <class T>
  static T create()
  {
    T       v(5, {1, 1});
    VecType other({
      {4, 4},
      {5, 5},
      {6, 6}
    });

    v = static_cast<T>(other);

    return v;
  }
};

struct Test34
{
  static std::string name()
  {
    return "034";
  }

  template <class T>
  static T create()
  {
    T       v(5, ItemType {1, 1});
    VecType other({
      {1, 0},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    v.insert(v.begin() + 2,
             {
               {99, 99},
               {88, 88},
               {77, 77}
    });
    return v;
  }
};

template <class... Tests>
void do_tests(Tests... ts)
{
  (do_test(ts), ...);
}

int main()
{
  do_tests(Test1 {},
           Test2 {},
           Test3 {},
           Test4 {},
           Test5 {},
           Test6 {},
           Test7 {},
           Test8 {},
           Test9 {},
           Test10 {},
           Test11 {},
           Test12 {},
           Test13 {},
           Test14 {},
           Test15 {},
           Test16 {},
           Test17 {},
           Test18 {},
           Test19 {},
           Test20 {},
           Test21 {},
           Test22 {},
           Test23 {},
           Test24 {},
           Test25 {},
           Test26 {},
           Test27 {},
           Test28 {},
           Test29 {},
           Test30 {},
           Test31 {},
           Test32 {},
           Test33 {},
           Test34 {});

  //Test 27 may fail since base resize in the SoA container just changes the size,
  //does not default-construct every element.
  //(For what we do, and given that we want to work with POD-like stuff,
  // it's simpler/more efficient this way.)

  std::cout << "DONE!" << std::endl;
  
  return 0;
}
