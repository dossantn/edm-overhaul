#include <iostream>
#include <vector>
#include <string>
#include <cstdio>

#include "Marionette.h"

static constexpr Marionette::PropertiesIndexer num_groups = 4;

MARIONETTE_DECLARE_SIMPLE_PROPERTY_ARRAY_PROPERTY(inner, Inner, num_groups, unsigned int);

struct Printer : Marionette::InterfaceDescription::NoObject
{
  template <class Final, class Layout>
  struct ObjectFunctions
  {
    friend std::ostream & operator<<(std::ostream & s, const Final & of)
    {
      s << "(";

      for (Marionette::PropertiesIndexer i = 0; i < num_groups; ++i)
        {
          if (i > 0)
            {
              s << " ";
            }
          s << of[i];
        }

      s << ")";

      return s;
    }
  };
};

MARIONETTE_DECLARE_PROPERTY_ARRAY_PROPERTY(outer, Outer, num_groups, Printer, Inner);

using OurTypes = Marionette::InterfaceDescription::PropertyList<Printer, Outer>;

using OurCollectionHolder = Marionette::Collections::CollectionHolder<Marionette::LayoutTypes::StandardVectorPerItem, OurTypes>;

using OurCollection = Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, OurTypes>;

int main()
{
  using Marionette::Utility::array_initialize;
  using Marionette::Utility::ArrayInitialize;

  std::vector<unsigned int> v1(15, 1), v2(17, 2), v3(14, 3), v4(9, 4);

  const int coll_size = 11;

  OurCollection coll(coll_size);

  auto print_all = [](auto && c, bool skip_line = false) {
    for (auto && x: c)
      {
        std::cout << x << std::endl;
      }

    if (skip_line)
      {
        std::cout << "\n";
      }
    else
      {
        std::cout << "--------------------------------------------------------------" << std::endl;
      }
  };

  for (int i = 0; i < num_groups; ++i)
    {
      for (int j = 0; j < num_groups; ++j)
        {
          for (int k = 0; k < coll_size; ++k)
            {
              coll[i][j][k] = static_cast<unsigned int>(i * num_groups * coll_size + j * coll_size + k);
            }
        }
    }

  print_all(coll);

  coll.resize(16);

  print_all(coll);
  
  print_all(coll[0], true);
  print_all(coll[1], true);
  print_all(coll[2], true);
  print_all(coll[3]);
  print_all(coll[1][2]);
  
  std::cout << "0:" << std::endl;
  
  Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, Marionette::InterfaceDescription::PropertyList<Printer, Inner>> other1 =
    coll[0];
    
  std::cout << Marionette::Utility::FriendshipProvider::get_multi_array_index(other1) << " / " << other1.extent() << " | " << other1.size() << "\n1:" << std::endl;
  
  Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, Marionette::InterfaceDescription::PropertyList<Printer, Inner>> other2 = coll[1];
    
  std::cout << Marionette::Utility::FriendshipProvider::get_multi_array_index(other2) << " / " << other2.extent() << " | " << other2.size() << "\n2:" << std::endl;
  
  Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, Marionette::InterfaceDescription::PropertyList<Printer, Inner>> other3 =
    coll[2];
    
  std::cout << Marionette::Utility::FriendshipProvider::get_multi_array_index(other3) << " / " << other3.extent() << " | " << other3.size() << "\n3:" << std::endl;
  
  Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, Marionette::InterfaceDescription::PropertyList<Printer, Inner>> other4 =
    coll[3];
    
  std::cout << Marionette::Utility::FriendshipProvider::get_multi_array_index(other4) << " / " << other4.extent() << " | " << other4.size() << "\nEND" << std::endl;

  print_all(other1, true);
  print_all(other2, true);
  print_all(other3, true);
  print_all(other4);

  print_all(coll);

  for (int i = 0; i < num_groups; ++i)
    {
      for (int j = 0; j < num_groups; ++j)
        {
          const int num = (i + j + 1) % num_groups;
          switch (num)
            {
              default:
              case 0:
                coll[i][j] = v1;
                break;
              case 1:
                coll[i][j] = v2;
                break;
              case 2:
                coll[i][j] = v3;
                break;
              case 3:
                coll[i][j] = v4;
                break;
            }
        }
    }
  print_all(coll);

  coll[0] = array_initialize<ArrayInitialize::Wrap>(v1, v3);
  coll[1] = array_initialize<ArrayInitialize::Wrap>(v2, v4);
  coll[2] = array_initialize<ArrayInitialize::RepeatLast>(v2, v4);
  coll[3] = array_initialize(v2, v1, v3);

  print_all(coll);

  std::cout << "DONE!" << std::endl;
  
  return 0;
}
