#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <utility>

#include "Marionette.h"

struct Object
{
  struct SubObject
  {
    int m_a = -1, m_b = -1;

    const int & a() const
    {
      return m_a;
    }

    int & a()
    {
      return m_a;
    }

    const int & b() const
    {
      return m_b;
    }

    int & b()
    {
      return m_b;
    }

    template <class Other>
    friend constexpr bool operator==(const SubObject & s, const Other & o)
    {
      return s.a() == o.a() && s.b() == o.b();
    }

    friend std::ostream & operator<<(std::ostream & os, const SubObject & so)
    {
      return os << "{" << std::setw(3) << so.a() << " " << std::setw(3) << so.b() << "}";
    }

    template <class T,
              class disabler = std::enable_if_t<std::is_constructible_v<int, decltype(std::declval<const T &>().a())> &&
                                                std::is_constructible_v<int, decltype(std::declval<T &>().b())>>>
    constexpr SubObject(const T & t): m_a(t.a()), m_b(t.b())
    {
    }

    constexpr SubObject(const int a, const int b): m_a(a), m_b(b)
    {
    }

    constexpr SubObject() = default;
  };

  std::vector<SubObject> m_v;

  const std::vector<SubObject> & v() const
  {
    return m_v;
  }

  std::vector<SubObject> & v()
  {
    return m_v;
  }

  int m_c = -1;

  const int & c() const
  {
    return m_c;
  }

  int & c()
  {
    return m_c;
  }

  template <class T,
            class disabler = std::enable_if_t<std::is_constructible_v<std::vector<SubObject>, decltype(std::declval<const T &>().v())> &&
                                              std::is_constructible_v<int, decltype(std::declval<const T &>().c())>>>
  constexpr Object(const T & t): m_v(t.v()), m_c(t.c())
  {
  }

  template <class Vec, class disabler = std::enable_if_t<std::is_constructible_v<std::vector<SubObject>, const Vec &>>>
  constexpr Object(const Vec & v, const int c): m_v(v), m_c(c)
  {
  }

  Object() = default;

  friend std::ostream & operator<<(std::ostream & os, const Object & o)
  {
    os << "(" << o.c() << ": ";

    for (auto && so: o.v())
      {
        os << " " << so;
      }

    return os << ")";
  }

  template <class Other>
  friend constexpr bool operator==(const Object & obj, const Other & oth)
  {
    if (obj.v().size() != static_cast<size_t>(oth.v().size()))
      {
        return false;
      }

    for (size_t i = 0; i < obj.v().size(); ++i)
      {
        if (!(obj.v()[i] == oth.v()[i]))
          {
            return false;
          }
      }

    return obj.c() == oth.c();
  }
};

struct SubObjectInterface : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    F & operator=(const Object::SubObject & other)
    {
      auto & dhis = static_cast<F &>(*this);
      dhis.a()    = other.a();
      dhis.b()    = other.b();
      return dhis;
    }

    template <class Other>
    friend constexpr bool operator==(const F & f, const Other & o)
    {
      return f.a() == o.a() && f.b() == o.b();
    }

    friend std::ostream & operator<<(std::ostream & os, const F & f)
    {
      return os << "{" << std::setw(3) << f.a() << " " << std::setw(3) << f.b() << "}";
    }
  };
};

struct ObjectInterface : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    F & operator=(const Object & other)
    {
      auto & dhis = static_cast<F &>(*this);
      dhis.v()    = other.v();
      dhis.c()    = other.c();
      return dhis;
    }

    template <class Other>
    friend constexpr bool operator==(const F & obj, const Other & oth)
    {
      if (obj.v().size() != static_cast<typename Layout::size_type>(oth.v().size()))
        {
          return false;
        }

      for (typename Layout::size_type i = 0; i < obj.v().size(); ++i)
        {
          if (!(obj.v()[i] == oth.v()[i]))
            {
              return false;
            }
        }

      return obj.c() == oth.c();
    }

    friend std::ostream & operator<<(std::ostream & os, const F & f)
    {
      os << "(" << f.c() << ": ";

      for (auto && so: f.v())
        {
          os << " " << so;
        }

      return os << ")";
    }
  };
};

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(a, A, int);
MARIONETTE_DECLARE_PER_ITEM_PROPERTY(b, B, int);
MARIONETTE_DECLARE_PER_ITEM_PROPERTY(c, C, int);
MARIONETTE_DECLARE_JAGGED_VECTOR_PROPERTY(v, V, size_t, A, B, SubObjectInterface);

using OurTypes = Marionette::InterfaceDescription::PropertyList<V, C, ObjectInterface>;

using OurCollection = Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, OurTypes>;

using OurObject = Marionette::Collections::Object<Marionette::Collections::OwningObject<>, OurTypes>;

using ItemType = Object::SubObject;
using VecType  = std::vector<Object::SubObject>;

template <class VecLike>
void print(const VecLike & vl, const std::string & pref = "")
{
  std::cout << pref << (pref.size() > 0 ? ": " : "");
  for (const auto & v: vl)
    {
      std::cout << "[" << v << "] | ";
    }
  std::cout << std::endl;
}

template <class Vec1, class Vec2>
bool check_differences(const Vec1 & v1, const Vec2 & v2)
{
  auto it1 = v1.begin();
  auto it2 = v2.begin();
  for (; it1 != v1.end() && it2 != v2.end(); ++it1, ++it2)
    {
      if (!(*it1 == *it2))
        {
          return true;
        }
    }
  return !(it1 == v1.end() && it2 == v2.end());
}

template <class Vec1, class Vec2>
void test(const Vec1 & v1, const Vec2 & v2, const std::string & test_name, const bool force_print = false)
{
  std::cout << test_name << ":\n";
  if (check_differences(v1, v2) || force_print)
    {
      print(v1, "      wanted");
      print(v2, "         got");
    }
  else
    {
      std::cout << "     passed!" << std::endl;
    }
  std::cout << "------------------------------------------" << std::endl;
}

template <class T>
void do_test(T)
{
  std::vector<Object> v(5);
  OurCollection       c(5);

  int count = 0;

  for (auto && x: v)
    {
      x.v().resize(5, {-int(x.v().size()) - 1, -int(x.v().size()) - 1});
      x.c() = ++count;
    }

  count = 0;

  for (auto && x: c)
    {
      x.v().resize(5, {-int(x.v().size()) - 1, -int(x.v().size()) - 1});
      x.c() = ++count;
    }

  for (unsigned i = 0; i < 5; ++i)
    {
      T::operate_on(v[i].v());
      T::operate_on(c[i].v());
      test(v, c, T::name() + "_" + std::to_string(i + 1));
    }
}

struct Test2
{
  static std::string name()
  {
    return "002";
  }

  template <class T>
  static void operate_on(T && v)
  {
    std::vector<ItemType> other(10, {4, 4});
    v = other;
  }
};

struct Test3
{
  static std::string name()
  {
    return "003";
  }

  template <class T>
  static void operate_on(T && v)
  {
    VecType other(10, {4, 4});
    v = other;
  }
};

struct Test4
{
  static std::string name()
  {
    return "004";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.assign(8, ItemType {9, 9});
  }
};

struct Test5
{
  static std::string name()
  {
    return "005";
  }

  template <class T>
  static void operate_on(T && v)
  {
    std::vector<ItemType> other({
      {1, 1},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    v.assign(other.begin() + 1, other.end() - 1);
  }
};

struct Test6
{
  static std::string name()
  {
    return "006";
  }

  template <class T>
  static void operate_on(T && v)
  {
    VecType other({
      {1, 1},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    v.assign(other.begin() + 1, other.end() - 1);
  }
};

struct Test7
{
  static std::string name()
  {
    return "007";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.assign(7, v[3]);
  }
};

struct Test8
{
  static std::string name()
  {
    return "008";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.assign({
      {1, 1},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
  }
};

struct Test9
{
  static std::string name()
  {
    return "009";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.clear();
  }
};

struct Test10
{
  static std::string name()
  {
    return "010";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.insert(v.begin(), ItemType {4, 4});
    v.insert(v.end(), ItemType {8, 8});
    v.insert(v.begin() + 4, ItemType {3, 3});
    v.insert(v.end() - 2, ItemType {7, 7});
  }
};

struct Test11
{
  static std::string name()
  {
    return "011";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.insert(v.begin() + 1, 3, ItemType {4, 4});
    v.insert(v.begin(), 3, ItemType {0, 0});
    v.insert(v.end(), 3, ItemType {9, 9});
  }
};

struct Test12
{
  static std::string name()
  {
    return "012";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.insert(v.begin() + 3, 3, v[2]);
  }
};

struct Test13
{
  static std::string name()
  {
    return "013";
  }

  template <class T>
  static void operate_on(T && v)
  {
    VecType other({
      {1, 0},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    v.insert(v.begin() + 2, other.begin() + 1, other.end() - 1);
  }
};

struct Test15
{
  static std::string name()
  {
    return "015";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.emplace(v.begin() + 2, 5, 6);
  }
};

struct Test16
{
  static std::string name()
  {
    return "016";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.emplace(v.end(), 5, 6);
  }
};

struct Test17
{
  static std::string name()
  {
    return "017";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.erase(v.begin());
  }
};

struct Test18
{
  static std::string name()
  {
    return "018";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.erase(v.begin() + 1);
  }
};

struct Test19
{
  static std::string name()
  {
    return "019";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.erase(v.end() - 1);
  }
};

struct Test20
{
  static std::string name()
  {
    return "020";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.erase(v.begin(), v.end());
  }
};

struct Test21
{
  static std::string name()
  {
    return "021";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.erase(v.begin() + 1, v.end() - 1);
  }
};

struct Test22
{
  static std::string name()
  {
    return "022";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.push_back({5, 5});
  }
};

struct Test23
{
  static std::string name()
  {
    return "023";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.push_back(v[2]);
    v.push_back(v[2]);
  }
};

struct Test24
{
  static std::string name()
  {
    return "024";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.emplace_back(5, 5);
  }
};

struct Test25
{
  static std::string name()
  {
    return "025";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.pop_back();
  }
};

struct Test26
{
  static std::string name()
  {
    return "026";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.resize(3);
  }
};

struct Test27
{
  static std::string name()
  {
    return "027";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.resize(7);
  }
};

struct Test28
{
  static std::string name()
  {
    return "028";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.resize(9, {3, 3});
  }
};

struct Test29
{
  static std::string name()
  {
    return "029";
  }

  template <class T>
  static void operate_on(T && v)
  {
    v.resize(2, {3, 3});
  }
};

struct Test32
{
  static std::string name()
  {
    return "032";
  }

  template <class T>
  static void operate_on(T && v)
  {
    std::vector<ItemType> other({
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v = other;
  }
};

struct Test33
{
  static std::string name()
  {
    return "033";
  }

  template <class T>
  static void operate_on(T && v)
  {
    VecType other({
      {4, 4},
      {5, 5},
      {6, 6}
    });
    v = other;
  }
};

struct Test34
{
  static std::string name()
  {
    return "034";
  }

  template <class T>
  static void operate_on(T && v)
  {
    VecType other({
      {1, 0},
      {1, 2},
      {2, 2},
      {1, 3},
      {2, 3},
      {3, 3}
    });
    v.insert(v.begin() + 2,
             {
               {99, 99},
               {88, 88},
               {77, 77}
    });
  }
};

template <class... Tests>
void do_tests(Tests... ts)
{
  (do_test(ts), ...);
}

int main()
{
  do_tests(Test2 {},
           Test3 {},
           Test4 {},
           Test5 {},
           Test6 {},
           Test7 {},
           Test8 {},
           Test9 {},
           Test10 {},
           Test11 {},
           Test12 {},
           Test13 {},
           Test15 {},
           Test16 {},
           Test17 {},
           Test18 {},
           Test19 {},
           Test20 {},
           Test21 {},
           Test22 {},
           Test23 {},
           Test24 {},
           Test25 {},
           Test26 {},
           Test27 {},
           Test28 {},
           Test29 {},
           Test32 {},
           Test33 {},
           Test34 {});

  //Test 27 may fail since base resize in the SoA container just changes the size,
  //does not default-construct every element.
  //(For what we do, and given that we want to work with POD-like stuff,
  // it's simpler/more efficient this way.)

  std::cout << "DONE!" << std::endl;
  
  return 0;
}
