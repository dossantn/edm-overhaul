#include <iostream>
#include <vector>
#include <string>
#include <cstdio>

#include "Marionette.h"

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(id, ID, unsigned);
MARIONETTE_DECLARE_PER_ITEM_PROPERTY(energy, Energy, double);

MARIONETTE_DECLARE_JAGGED_VECTOR_PROPERTY(entries, Entries, int, ID, Energy);

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(extra, Extra, unsigned);

using OurTypes = Marionette::InterfaceDescription::PropertyList<Extra, Entries>;

using OurCollectionHolder = Marionette::Collections::CollectionHolder<Marionette::LayoutTypes::StandardVectorPerItem, OurTypes>;

using OurCollection = Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, OurTypes>;

template <class Obj>
void print(const Obj & o)
{
  std::cout << o.extra() << " " << o.entries().size();
  for (auto && x: o.entries())
    {
      std::cout << " (" << x.id() << ", " << x.energy() << ")";
    }
  std::cout << std::endl;
}

int main()
{
  OurCollection coll;

  coll.resize(10);

  std::cout << coll.size() << std::endl;

  std::cout << "<><><><><><><><><><><><><><><><><><><><><><><><><>" << std::endl;

  unsigned count = 0;

  for (auto && c: coll)
    {
      c.entries().push_back({12u, 11.2});
      c.extra() = count;
      print(c);
      ++count;
    }

  for (auto && c: coll)
    {
      print(c);
    }

  std::cout << "DONE!" << std::endl;
  
  return 0;
}
