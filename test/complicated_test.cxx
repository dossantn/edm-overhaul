#include <iostream>
#include <vector>
#include <string>
#include <cstdio>

#include "Marionette.h"

static constexpr unsigned int num_groups = 4;

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(sum, Sum, double);

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(subtotal, Subtotal, double);

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(index, Index, unsigned int);

MARIONETTE_DECLARE_JAGGED_VECTOR_PROPERTY(elements, Elements, size_t, Index);

struct GroupPrinter : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    friend std::ostream & operator<<(std::ostream & s, const F & obj)
    {
      s << obj.subtotal() << " (" << obj.elements().size() << (obj.elements().size() ? ":" : "");
      for (auto && x: obj.elements())
        {
          s << " " << x;
        }
      s << ")";

      return s;
    }
  };
};

MARIONETTE_DECLARE_PROPERTY_ARRAY_PROPERTY(grouped, Grouped, num_groups, Subtotal, Elements, GroupPrinter);

struct PrettyPrinter : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    friend std::ostream & operator<<(std::ostream & s, const F & obj)
    {
      char buf[32];
      snprintf(buf, 31, "%-10lf", static_cast<double>(obj.sum()));
      s << buf << ": ";
      for (int i = 0; i < static_cast<int>(num_groups); ++i)
        {
          if (i > 0)
            {
              s << "            ";
            }
          s << obj.grouped()[i];
          s << "\n";
        }
      return s;
    }
  };
};

using ExampleTypes  = Marionette::InterfaceDescription::PropertyList<Sum, Grouped, PrettyPrinter>;
using OurCollection = Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, ExampleTypes>;
using OurObject     = Marionette::Collections::Object<Marionette::Collections::OwningObject<>, ExampleTypes>;

#include <cstdlib>

struct Test
{
  double   value;
  unsigned id;

  static Test random()
  {
    return Test {double(std::rand() - RAND_MAX / 2) / RAND_MAX, static_cast<unsigned>(std::rand())};
  }
};

std::vector<Test> get_some(unsigned int num)
{
  std::vector<Test> ret;

  while (ret.size() < num)
    {
      ret.push_back(Test::random());
    }

  return ret;
}

template <class WeCouldUseConceptsToEnsureItHasObjectInterface>
void process_some(WeCouldUseConceptsToEnsureItHasObjectInterface && obj, const std::vector<Test> & v)
{
  obj.grouped()[0].elements().push_back(1u);
  obj.grouped()[1].elements().push_back(2u);

  obj.sum() = 0;

  obj.grouped().setSubtotal(0);

  for (const auto & t: v)
    {
      obj.sum() += t.value;
      const int classif = static_cast<int>(t.id % num_groups);
      obj.grouped()[classif].subtotal() += t.value;
      obj.grouped()[classif].elements().push_back(t.id);
    }
}

int main()
{
  std::srand(7);

  OurCollection coll(5);

  std::cout << coll.size() << std::endl;

  for (auto && c: coll)
    {
      process_some(c, get_some(static_cast<unsigned>(1 + (std::rand() % 20))));
    }
  process_some(coll.back(), get_some(4));

  std::cout << "********************" << std::endl;

  OurObject obj;

  std::cout << "********************" << std::endl;

  obj.grouped()[0].elements().push_back(1u);
  obj.grouped()[1].elements().push_back(2u);

  std::cout << "********************" << std::endl;

  process_some(static_cast<OurObject &>(obj), get_some(32));

  std::cout << "Outside:\n" << obj << "---------------------------------------------------\n" << std::endl;

  coll.push_back(obj);

  unsigned int count = 0;

  for (auto && c: coll)
    {
      std::cout << "---------------------------------------------------\n" << count << ":\n" << c << std::endl;
      ++count;
    }

  std::cout << "DONE!" << std::endl;

  return 0;
}
