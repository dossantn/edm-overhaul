#include <iostream>
#include <vector>
#include <string>

#define NDEBUG

#include "Marionette.h"

MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(energy, Energy, 256, float);

MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(time, Time, 256, float);

struct Foo : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    constexpr int foo() const
    {
      return 42;
    }
  };
};

struct Bar : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    constexpr float bar() const
    {
      return static_cast<const F *>(this)->energy();
    }
  };
};

struct Alternative
{
  int   size = 0;
  float energy[256];
  float time[256];
};

using ExampleType = Marionette::InterfaceDescription::PropertyList<Energy, Time, Foo, Bar>;

using OurCollection = Marionette::Collections::Collection<Marionette::LayoutTypes::LocalStruct<int, int>, ExampleType>;

__global__ void test_handwritten(Alternative * alt)
{
  const int index = blockIdx.x * blockDim.x + threadIdx.x;

  if (index < alt->size)
    {
      alt->time[index] = alt->energy[index] * index + 42;
    }
}

__global__ void test_normal(OurCollection * coll)
{
  const int index = blockIdx.x * blockDim.x + threadIdx.x;

  if (index < coll->size())
    {
      coll->setTime(index, (*coll)[index].bar() * index + (*coll)[index].foo());
    }
}

void print(const OurCollection & c)
{
  std::cout << c.size() << ":\n";
  for (auto && v: c)
    {
      std::cout << v.energy() << " " << v.time() << "\n";
    }
  std::cout << std::flush;
}

void print(const Alternative & alt)
{
  std::cout << alt.size << ":\n";
  for (int i = 0; i < alt.size; ++i)
    {
      std::cout << alt.energy[i] << " " << alt.time[i] << "\n";
    }
  std::cout << std::flush;
}

int main()
{
  std::cout << sizeof(OurCollection) << " <- " << sizeof(Alternative) << "\n-----------------------------------\n" << std::endl;

  OurCollection coll(42);

  std::cout << coll.size() << std::endl;

  std::vector<float> desired_times(50, 100.f);

  coll.energy() = desired_times;

  Alternative alt;

  alt.size = 42;

  for (int i = 0; i < 42; ++i)
    {
      alt.energy[i] = 100.f;
    }

  print(alt);

  std::cout << "\n...\n";

  print(coll);

  OurCollection * gpu_coll = nullptr;
  Alternative *   gpu_alt  = nullptr;

  cudaMalloc(&gpu_alt, sizeof(Alternative));
  cudaMalloc(&gpu_coll, sizeof(OurCollection));

  cudaMemcpy(gpu_alt, &alt, sizeof(Alternative), cudaMemcpyHostToDevice);
  cudaMemcpy(gpu_coll, &coll, sizeof(OurCollection), cudaMemcpyHostToDevice);

  test_handwritten<<<4, 64>>>(gpu_alt);
  test_normal<<<4, 64>>>(gpu_coll);

  cudaMemcpy(&alt, gpu_alt, sizeof(Alternative), cudaMemcpyDeviceToHost);
  cudaMemcpy(&coll, gpu_coll, sizeof(OurCollection), cudaMemcpyDeviceToHost);

  std::cout << "\n-----------------------------------\n";

  print(alt);

  std::cout << "\n...\n";

  print(coll);

  std::cout << "DONE!" << std::endl;
  
  return 0;
}
