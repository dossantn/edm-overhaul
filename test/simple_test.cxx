#include <iostream>
#include <vector>
#include <string>

#include "Marionette.h"

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(energy, Energy, float);

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(time, Time, float);

struct Foo : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    int foo() const
    {
      return 42;
    }
  };
};

struct Bar : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    float bar() const
    {
      return static_cast<const F *>(this)->energy();
    }
  };
};

struct FooBar : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    float foobar() const
    {
      return static_cast<const F *>(this)->foo() + static_cast<const F *>(this)->bar();
    }
  };
};

using ExampleType = Marionette::InterfaceDescription::PropertyList<Energy, Time, Foo, Bar, FooBar>;

using OurCollection = Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, ExampleType>;

int main()
{
  OurCollection coll(42);

  std::cout << "-------------------------------------\n" << coll.size() << "\n-------------------------------------\n" << std::endl;

  std::vector<float> desired_times(50, 10.f);

  coll.time() = desired_times;

  std::cout << coll.size() << std::endl;

  std::cout << "..." << std::endl;

  float incrementor = 1.f;

  for (auto && e: coll.energy())
    {
      e = incrementor;
      incrementor += 1.f;
      std::cout << e << std::endl;
    }

  std::cout << "..." << std::endl;

  for (auto && obj: coll)
    {
      obj.setEnergy(obj.energy() * obj.time());
      std::cout << obj.energy() << std::endl;
    }

  std::cout << "..." << std::endl;

  float energaverage = 0.f;
  float foobarverage = 0.f;

  for (const auto && obj: coll)
    {
      energaverage += obj.energy();
      foobarverage += obj.foobar();
      std::cout << obj.energy() << " " << obj.time() << " " << obj.foo() << " " << obj.bar() << " " << obj.foobar() << std::endl;
    }

  std::cout << "..." << std::endl;

  std::cout << energaverage << " " << foobarverage << " " << foobarverage - energaverage << " " << (foobarverage - energaverage) / coll.size() << std::endl;

  std::cout << "DONE!" << std::endl;
  
  return 0;
}
