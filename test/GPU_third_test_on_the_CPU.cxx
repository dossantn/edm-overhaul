#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

#include "Marionette.h"

//The general idea of this test is to emulate a somewhat realistic problem for our use case.
//Let us imagine a detector that is split into @p num_cells cells,
//with each cell having (static) coordinates xi, csi and psi and
//a group ID (from 0 to 31) to identify it.
//This detector measures some abstract phenomenon that is characterized
//by energy, time and a three-dimensional vector, and, for every "event"
//being considered, the cells can be assigned to up to two instances of the phenomenon
//with a non-negative weight.
//We can assume that the list of all the cells assigned to any phenomenon
//(including duplicates) is never longer than than @p max_cells_in_phen cells,
//and there are no more than @p max_phen phenomena in total.
//
//The code will receive an array of packed objects encoding the time, energy and vector
//of a phenomenon and the total number of cells assigned to it,
//as well as arrays for the first and second assignment and respective weights
//of each cell.
//
//The goal is to output a list of phenomenon information:
//energy, time and vector,
//the number of cells in each group,
//weighted average of xi, cis and psi
//over all cells assigned to the phenomenon
//and a list of cells and respective weights.

//Slight cheat: I will do the prefix sum on the CPU
//and pass the array as well, just to implement this quicker.

//-----------------------------------------------------------
//                PARAMETERS OF THE PROBLEM

static constexpr unsigned int max_phen = 10000;

static constexpr unsigned int group_size = 32;

//static constexpr unsigned int log_2_group_size = 5;

static constexpr unsigned int vector_size = 3;

static constexpr unsigned int num_cells = 75000;

static constexpr unsigned int max_cells_in_phen = 2 * num_cells;

//-----------------------------------------------------------
//                GENERAL DEFINITIONS

struct ConstantCellData
{
  float        cellXi[num_cells];
  float        cellCsi[num_cells];
  float        cellPsi[num_cells];
  unsigned int groupId[num_cells];
};

struct PackedPhenomInfo
{
  unsigned long long e_t_v;

  float get_energy() const
  {
    return float(e_t_v & 0xFFFFULL) / 0xFFFFU;
  }

  float get_time() const
  {
    return float((e_t_v & 0xFFFF0000ULL) >> 16) / 0xFFFFU;
  }

  float get_vector(const unsigned int i) const
  {

    return float(((e_t_v >> i) & 0xFF00000000ULL) >> 32) / 0xFFFFU - float(0x7FFF);
  }

  unsigned int num_cells;
};

struct EventData
{
  PackedPhenomInfo phenom[max_phen];

  int prefix_sum[max_phen];

  int   assignment_1[num_cells];
  float weight_1[num_cells];
  int   assignment_2[num_cells];
  float weight_2[num_cells];
};

//-----------------------------------------------------------
//                       HANDWRITTEN
//               The handwritten implementation...

struct Handwritten
{
  int size = 0;

  struct inner
  {
    float energy[max_phen];
    float time[max_phen];
    float vector[vector_size][max_phen];
  } values;

  float xiAvg[max_phen];
  float csiAvg[max_phen];
  float psiAvg[max_phen];

  unsigned int count[group_size][max_phen];

  int list_index[max_phen + 1];

  float        weights[max_cells_in_phen];
  unsigned int cells[max_cells_in_phen];
};

void unpack_phenomena_hand(int * cell_offset, Handwritten * out, const EventData * in, const ConstantCellData * constant)
{
  (void) constant;

  for (int phen = 0; phen < out->size; ++phen)
    {
      const PackedPhenomInfo p = in->phenom[phen];

      out->values.energy[phen] = p.get_energy();
      out->values.time[phen]   = p.get_time();
      for (int i = 0; i < 3; ++i)
        {
          out->values.vector[i][phen] = p.get_vector(i);
        }

      out->xiAvg[phen]  = 0;
      out->csiAvg[phen] = 0;
      out->psiAvg[phen] = 0;

      for (int i = 0; i < group_size; ++i)
        {
          out->count[i][phen] = 0;
        }

      out->list_index[phen + 1] = in->prefix_sum[phen];

      cell_offset[phen] = 0;
    }
  out->list_index[0] = 0;
}

void add_cells_hand(int * cell_offset, Handwritten * out, const EventData * in, const ConstantCellData * constant)
{
  for (int cell = 0; cell < num_cells; ++cell)
    {
      const int   p1 = in->assignment_1[cell];
      const int   p2 = in->assignment_2[cell];
      const float w1 = in->weight_1[cell];
      const float w2 = in->weight_2[cell];

      auto do_cell = [&](const int p, const float w) {
        if (p >= 0 && p < out->size)
          {
            out->count[constant->groupId[cell]][p] += 1;
            out->xiAvg[p] += w * constant->cellXi[cell];
            out->csiAvg[p] += w * constant->cellCsi[cell];
            out->psiAvg[p] += w * constant->cellPsi[cell];

            const int listoffset = cell_offset[p];

            cell_offset[p] += 1;

            const int listindex = out->list_index[p] + listoffset;

            out->weights[listindex] = w;
            out->cells[listindex]   = cell;
          }
      };

      do_cell(p1, w1);
      do_cell(p2, w2);
    }
}

void run_handwritten(const EventData * event, const ConstantCellData * constant, const int num_phen)
{
  Handwritten * cpu_out = new Handwritten();
  Handwritten * gpu_out = new Handwritten();

  int * extra = new int[max_phen];

  gpu_out->size = num_phen;

  unpack_phenomena_hand(extra, gpu_out, event, constant);

  add_cells_hand(extra, gpu_out, event, constant);

  memcpy(cpu_out, gpu_out, sizeof(Handwritten));

  for (int i = 0; i < num_phen; ++i)
    {
      std::cout << cpu_out->values.time[i] << " " << cpu_out->count[i % group_size][i] << " " << cpu_out->list_index[i] << " " << cpu_out->list_index[i + 1]
                << ":\n                ";

      std::vector<unsigned int> list;
      for (int j = cpu_out->list_index[i]; j < cpu_out->list_index[i + 1]; ++j)
        {
          list.push_back(cpu_out->cells[j]);
        }
      std::sort(list.begin(), list.end());

      for (const auto & c: list)
        {
          std::cout << " " << c;
        }

      std::cout << std::endl;
    }

  delete[] extra;
  delete gpu_out;
  delete cpu_out;
}

//-----------------------------------------------------------
//                       MARIONETTE
//               The Marionette implementation...

MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(energy, Energy, max_phen, float);
MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(time, Time, max_phen, float);
MARIONETTE_DECLARE_SIMPLE_PROPERTY_ARRAY_PROPERTY_SIZED(vector, Vector, max_phen, vector_size, float);

MARIONETTE_DECLARE_SUBCOLLECTION_PROPERTY_SIZED(values, Values, max_phen, Energy, Time, Vector);

MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(xiAvg, XiAvg, max_phen, float);
MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(csiAvg, CsiAvg, max_phen, float);
MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(psiAvg, PsiAvg, max_phen, float);

MARIONETTE_DECLARE_SIMPLE_PROPERTY_ARRAY_PROPERTY_SIZED(count, Count, max_phen, group_size, unsigned int);

MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(weight, Weight, max_cells_in_phen, float);
MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(cell, Cell, max_cells_in_phen, unsigned int);

MARIONETTE_DECLARE_JAGGED_VECTOR_PROPERTY_SIZED(list, List, max_phen, int, Weight, Cell);

using CollectionProperties = Marionette::InterfaceDescription::PropertyList<Values, XiAvg, CsiAvg, PsiAvg, Count, List>;

template <class Layout>
using OurCollectionBase = Marionette::Collections::Collection<Layout, CollectionProperties>;

template <class Context>
using OurCollection = OurCollectionBase<Marionette::LayoutTypes::DynamicStructInContext<Context, int, int>>;

using CPUCollection = OurCollection<Marionette::MemoryContexts::StandardCPU>;

using GPUCollection = OurCollection<Marionette::MemoryContexts::StandardCPU>;

void unpack_phenomena_mario(int * cell_offset, Marionette::Collections::ByValue<GPUCollection> out, const EventData * in, const ConstantCellData * constant)
{
  (void) constant;

  for (int phenom = 0; phenom < out.size(); ++phenom)
    {
      const PackedPhenomInfo p = in->phenom[phenom];

      auto && this_item = out[phenom];

      this_item.values().energy() = p.get_energy();
      this_item.values().time()   = p.get_time();

      for (int i = 0; i < 3; ++i)
        {
          this_item.values().vector()[i] = p.get_vector(i);
        }

      this_item.xiAvg()  = 0;
      this_item.csiAvg() = 0;
      this_item.psiAvg() = 0;

      this_item.count() = Marionette::Utility::array_initialize<Marionette::Utility::ArrayInitialize::RepeatLast>(0);

      out.list().underlying_size_array()[phenom + 1] = in->prefix_sum[phenom];

      cell_offset[phenom] = 0;
    }
  out.list().underlying_size_array()[0] = 0;
  out.list().underlying_total_size()    = in->prefix_sum[out.size() - 1];
}

void add_cells_mario(int * cell_offset, Marionette::Collections::ByValue<GPUCollection> out, const EventData * in, const ConstantCellData * constant)
{
  for (int cell = 0; cell < num_cells; ++cell)
    {
      const int   p1 = in->assignment_1[cell];
      const int   p2 = in->assignment_2[cell];
      const float w1 = in->weight_1[cell];
      const float w2 = in->weight_2[cell];

      auto do_cell = [&](const int p, const float w) {
        if (p >= 0 && p < out.size())
          {
            auto && this_item = out[p];

            this_item.count()[constant->groupId[cell]] += 1;
            this_item.xiAvg() += w * constant->cellXi[cell];
            this_item.csiAvg() += w * constant->cellCsi[cell];
            this_item.psiAvg() += w * constant->cellPsi[cell];

            const int list_offset = cell_offset[p];

            cell_offset[p] += 1;

            auto && this_cell_entry = this_item.list()[list_offset];

            this_cell_entry.weight() = w;
            this_cell_entry.cell()   = cell;
          }
      };

      do_cell(p1, w1);
      do_cell(p2, w2);
    }
}

void run_mario(const EventData * event, const ConstantCellData * constant, const int num_phen)
{
  CPUCollection cpu_out;
  GPUCollection gpu_out;

  gpu_out.resize(num_phen);

  int * extra = new int[max_phen];

  unpack_phenomena_mario(extra, Marionette::Collections::pass_by_value(gpu_out), event, constant);

  add_cells_mario(extra, Marionette::Collections::pass_by_value(gpu_out), event, constant);

  cpu_out = gpu_out;

  for (int i = 0; i < cpu_out.size(); ++i)
    {
      const auto & this_item = cpu_out[i];
      std::cout << this_item.values().time() << " " << this_item.count()[i % group_size] << " " << cpu_out.list().underlying_size_array()[i] << " "
                << cpu_out.list().underlying_size_array()[i + 1] << ":\n                ";

      std::vector<unsigned int> list;

      for (const auto && l: this_item.list())
        {
          list.push_back(l.cell());
        }

      std::sort(list.begin(), list.end());

      for (const auto & c: list)
        {
          std::cout << " " << c;
        }

      std::cout << std::endl;
    }

  delete[] extra;
}

//-----------------------------------------------------------
//                       INFRASTRUCTURE

void build_constant_cell_data(ConstantCellData & ccd)
{
  for (int i = 0; i < num_cells; ++i)
    {
      ccd.cellXi[i]  = ((i % 2032) - 544) / 100.f;
      ccd.cellCsi[i] = (((i + 1234) % 3152) - 1505) / 150.f;
      ccd.cellCsi[i] = (((i + 488) % 5004) - 2784) / 234.f;
      ccd.groupId[i] = ((i % 16) + (i / 32)) % 32;
    }
}

#include <cstdlib>

void generate_event(EventData & ed, int & num_phen)
{
  num_phen =
    static_cast<int>((static_cast<unsigned int>(std::rand() + std::rand()) % (max_phen / 250)) + ((std::rand() % 100) > 95) * (std::rand() % (max_phen)));

  std::cout << "Total number of phenomena: " << num_phen << std::endl;

  std::vector<std::vector<unsigned int>> cell_per_phen_list(num_phen);

  if (num_phen >= max_phen)
    {
      std::cout << "\nGENERATED MORE PHENOMENA THAN SUPPORTED; BRACE FOR ERRORS!\n" << std::endl;
    }

  for (int i = 0; i < num_phen; ++i)
    {
      const unsigned long long result = std::rand();

      ed.phenom[i].e_t_v     = (result << 32) | static_cast<unsigned long long>(std::rand());
      ed.phenom[i].num_cells = 0;
    }

  int num_in_phen = 0;

  for (int i = 0; i < num_cells; ++i)
    {
      const unsigned int r1 = std::rand();
      const unsigned int r2 = std::rand();
      const unsigned int r3 = std::rand();

      const unsigned int first_index = (r1 + r2 + r1 * r2) % max_phen;
      const unsigned int second_index =
        (static_cast<unsigned int>(std::abs(static_cast<long long>(r1) - r2) + std::abs(static_cast<long long>(r2) - r3)) + r1 * r2 * r3) % max_phen;

      auto update_phen = [&](const unsigned int idx, int & assignment, float & weight) {
        if (idx < num_phen)
          {
            ++(ed.phenom[idx].num_cells);
            assignment = idx;
            weight     = float(std::rand()) / RAND_MAX;

            ++num_in_phen;

            cell_per_phen_list[idx].push_back(i);
          }
        else
          {
            assignment = -1;
            weight     = -1;
          }
      };

      update_phen(first_index, ed.assignment_1[i], ed.weight_1[i]);
      update_phen(second_index, ed.assignment_2[i], ed.weight_2[i]);
    }

  std::cout << "Total cells assigned to phenomena: " << num_in_phen << std::endl;

  if (num_in_phen >= max_cells_in_phen)
    {
      std::cout << "\nASSIGNED MORE CELLS TO PHENOMENA THAN SUPPORTED; BRACE FOR ERRORS!\n" << std::endl;
    }

  int old = 0;

  for (int i = 0; i < num_phen; ++i)
    {
      old += ed.phenom[i].num_cells;
      ed.prefix_sum[i] = old;
    }

  for (unsigned int i = 0; i < num_phen; ++i)
    {
      std::cout << "x x " << (i == 0 ? 0 : ed.prefix_sum[i - 1]) << " " << ed.prefix_sum[i] << "\n               ";
      for (const auto & c: cell_per_phen_list[i])
        {
          std::cout << " " << c;
        }
      std::cout << std::endl;
    }
}

void do_tests()
{
  ConstantCellData * ccd_ptr = new ConstantCellData();
  EventData *        ed_ptr  = new EventData();

  build_constant_cell_data(*ccd_ptr);

  int num_phen = 0;

  generate_event(*ed_ptr, num_phen);

  std::cout << "\n\n----------------------------------------------------------------------------\n\n";

  run_handwritten(ed_ptr, ccd_ptr, num_phen);

  std::cout << "\n\n----------------------------------------------------------------------------\n\n";

  run_mario(ed_ptr, ccd_ptr, num_phen);

  std::cout << "\n\n----------------------------------------------------------------------------\n\n";

  delete ccd_ptr;
  delete ed_ptr;
}

int main()
{
  std::srand(123456);

  do_tests();

  std::cout << "DONE!" << std::endl;
  
  return 0;
}
