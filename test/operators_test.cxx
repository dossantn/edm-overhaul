#include <iostream>
#include <vector>
#include <string>

#include "Marionette.h"

static int global_count = 0;

struct Tester
{
};

struct Overrider : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    void operator()(int) const
    {
      std::cout << "Overriden" << std::endl;
      ++global_count;
    }
  };
};

struct BasicOperator : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    F & operator=(const Tester &)
    {
      std::cout << "TESTED!" << std::endl;
      return static_cast<F &>(*this);
    }
  };
};

MARIONETTE_DECLARE_PER_ITEM_PROPERTY(value, Value, int);

using ExampleType = Marionette::InterfaceDescription::PropertyList<Overrider, BasicOperator, Value>;

using OurCollection = Marionette::Collections::Collection<Marionette::LayoutTypes::StandardVectorPerItem, ExampleType>;

struct Final : BasicOperator::ObjectFunctions<Final, int>
{
};

int main()
{

  int test;

  Tester tester;

  OurCollection coll(1);

  auto && x = coll[0];

  x(1);

  x = tester;

  constexpr int expected_final_count = 1;

  std::cout << "-------------------------------------------------------------\nFinished. " << global_count << " / " << expected_final_count
            << " tests successful." << std::endl;
  
  std::cout << "DONE!" << std::endl;
  
  return 0;
}
