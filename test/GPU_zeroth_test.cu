#include "Marionette.h"
#pragma hdrstop

#include <iostream>
#include <vector>
#include <string>

#define NDEBUG

MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(energy, Energy, 256, float);

MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(time, Time, 256, float);

struct Foo : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    constexpr int foo() const
    {
      return 42;
    }
  };
};

struct Bar : Marionette::InterfaceDescription::NoObject
{
  template <class F, class Layout>
  struct ObjectFunctions
  {
    constexpr float bar() const
    {
      return static_cast<const F *>(this)->energy();
    }
  };
};

struct Alternative
{
  int   size = 0;
  float energy[256];
  float time[256];
};

using CollectionProperties = Marionette::InterfaceDescription::PropertyList<Energy, Time, Foo, Bar>;

template <class Layout>
using OurCollectionBase = Marionette::Collections::Collection<Layout, CollectionProperties>;

template <class Context>
using OurCollection = OurCollectionBase<Marionette::LayoutTypes::DynamicStructInContext<Context, int, int>>;

using CPUCollection = OurCollection<Marionette::MemoryContexts::CUDAHostPinned>;

using GPUCollection = OurCollection<Marionette::MemoryContexts::CUDAStandardGPU>;

__global__ void test_handwritten(Alternative * alt)
{
  const int index = blockIdx.x * blockDim.x + threadIdx.x;

  if (index < alt->size)
    {
      alt->time[index] = alt->energy[index] * index + 42;
    }
}

template <class Dummy = void>
__global__ void test_normal(Marionette::Collections::ByValue<GPUCollection> coll)
{
  const int index = blockIdx.x * blockDim.x + threadIdx.x;

  if (index < coll.size())
    {
      coll.setTime(index, coll[index].bar() * index + coll[index].foo());
    }
  if (index == 0)
    {
      printf("GPU: %d\n", (int) Marionette::MemoryContexts::get_current_execution_context());
    }
}

void print(const CPUCollection & c)
{
  std::cout << c.size() << ":\n";
  for (auto && v: c)
    {
      std::cout << v.energy() << " " << v.time() << "\n";
    }
  std::cout << std::flush;
}

void print(const Alternative & alt)
{
  std::cout << alt.size << ":\n";
  for (int i = 0; i < alt.size; ++i)
    {
      std::cout << alt.energy[i] << " " << alt.time[i] << "\n";
    }
  std::cout << std::flush;
}

int main()
{
  std::cout << sizeof(CPUCollection) << " <- " << sizeof(Alternative) << "\n-----------------------------------\n" << std::endl;

  CPUCollection coll(42);

  std::cout << coll.size() << std::endl;

  std::vector<float> desired_times(50, 100.f);

  coll.energy() = desired_times;

  Alternative alt;

  alt.size = 42;

  for (int i = 0; i < 42; ++i)
    {
      alt.energy[i] = 100.f;
    }

  print(alt);

  std::cout << "\n...\n";

  print(coll);

  std::cout << "\n-----------------------------------\n";

  Alternative * gpu_alt = nullptr;

  cudaMalloc(&gpu_alt, sizeof(Alternative));

  cudaMemcpy(gpu_alt, &alt, sizeof(Alternative), cudaMemcpyHostToDevice);

  GPUCollection gpu_coll = coll;

  printf("CPU: %d\n", (int) Marionette::MemoryContexts::get_current_execution_context());

  test_handwritten<<<4, 64>>>(gpu_alt);
  test_normal<<<4, 64>>>(Marionette::Collections::pass_by_value(gpu_coll));

  cudaMemcpy(&alt, gpu_alt, sizeof(Alternative), cudaMemcpyDeviceToHost);

  coll = gpu_coll;

  std::cout << "\n-----------------------------------\n";

  print(alt);

  std::cout << "\n...\n";

  print(coll);

  cudaFree(gpu_alt);

  std::cout << "DONE!" << std::endl;
  
  return 0;
}
