//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_UTILITYHELPERS_H
#define MARIONETTE_UTILITYHELPERS_H

#include <utility>
#include <type_traits>
#include <iterator>

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

namespace Marionette
{
  namespace Utility
  {
    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto is_random_access_iterator_or_pointer_helper(Ts...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class category = typename T::iterator_category>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto is_random_access_iterator_or_pointer_helper(T)
      {
        return std::integral_constant<bool, std::is_same_v<category, std::random_access_iterator_tag>> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto is_random_access_iterator_or_pointer_helper(const T *)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto is_random_access_iterator_or_pointer_helper(T *)
      {
        return std::true_type {};
      }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class It>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_random_access_iterator_or_pointer()
    {
      return decltype(impl::is_random_access_iterator_or_pointer_helper(std::declval<It>()))::value;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class RetT>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES arrow_operator_wrapper
    {
      T wrapped;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr RetT * operator->()
      {
        return static_cast<RetT *>(&wrapped);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const RetT * operator->() const
      {
        return static_cast<const RetT *>(&wrapped);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr RetT & operator*()
      {
        return static_cast<RetT &>(wrapped);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const RetT & operator*() const
      {
        return static_cast<const RetT &>(wrapped);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherT, class OtherRetT>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES friend constexpr bool operator==(const arrow_operator_wrapper<T, RetT> &           one,
                                                                                             const arrow_operator_wrapper<OtherT, OtherRetT> & other)
      {
        return one.wrapped == other.wrapped;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherT,
                                                               class OtherRetT,
                                                               class disabler = std::enable_if_t < std::is_constructible_v<OtherT, T> &&
                                                                                std::is_constructible_v<OtherRetT *, T *> >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr
                                                                                  operator arrow_operator_wrapper<OtherT, OtherRetT>() const
      {
        return {wrapped};
      }

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
      __pragma("nv_diag_suppress 20012")
  #else
      _Pragma("nv_diag_suppress 20012")
  #endif
#endif
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr arrow_operator_wrapper() = default;
#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
      __pragma("nv_diag_default 20012")
  #else
      _Pragma("nv_diag_default 20012")
  #endif
#endif

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts, class disabler = std::enable_if_t<std::is_constructible_v<T, Ts...>>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr arrow_operator_wrapper(Ts &&... ts): wrapped(std::forward<Ts>(ts)...)
      {
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class RetT, class T>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto arrow_operator_wrap(T && t)
    {
      using RealRetT = std::conditional_t<std::is_same_v<RetT, InvalidType>, std::decay_t<T>, RetT>;
      return arrow_operator_wrapper<std::decay_t<T>, RealRetT> {std::forward<T>(t)};
    }

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <PropertiesIndexer idx, class T, bool b>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES IndexTypeAndCanMove
      {
        static constexpr PropertiesIndexer index = idx;

        static constexpr bool can_move = b;

        using Type = T;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ArrayInitializationHelperHelper;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <PropertiesIndexer index, class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ArrayInitializationHelperHelper<IndexTypeAndCanMove<index, T, false>>
      {
       protected:

        const T & m_t;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ArrayInitializationHelperHelper(const T & t): m_t(t)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const T & get() const
        {
          return m_t;
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <PropertiesIndexer index, class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ArrayInitializationHelperHelper<IndexTypeAndCanMove<index, T, true>>
      {
       protected:

        T m_t;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ArrayInitializationHelperHelper(const T & t): m_t(t)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ArrayInitializationHelperHelper(T && t):
          m_t(std::move(t))
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr T & get() &
        {
          return m_t;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const T & get() const &
        {
          return m_t;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const T && get() const &&
        {
          return m_t;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr T && get() &&
        {
          return m_t;
        }
      };
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <ArrayInitialize initialization, class... IdxTpMv>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ArrayInitializationHelper : impl::ArrayInitializationHelperHelper<IdxTpMv>...
    {
     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer extent()
      {
        return sizeof...(IdxTpMv);
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto get_index_and_type()
      {
        constexpr auto pre_ret =
          (TypeHolder<InvalidType> {} & ... & std::conditional_t<IdxTpMv::index == idx, TypeHolder<IdxTpMv>, TypeHolder<InvalidType>> {});

        return get_one_type_or_invalid(pre_ret);
      }

      template <PropertiesIndexer idx>
      using IndexedType = decltype(get_index_and_type<idx>());

      template <PropertiesIndexer idx>
      using HelperBase = impl::ArrayInitializationHelperHelper<IndexedType<idx>>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array() const &
      {
        static_assert(idx >= 0, "Non-negative indices, please!");

        if constexpr (idx >= extent())
          {
            if constexpr (initialization == ArrayInitialize::Default || extent() == 0)
              {
                return DefaultInitializer {};
              }
            else if constexpr (initialization == ArrayInitialize::Wrap)
              {
                return this->template get_multi_array<idx % extent()>();
              }
            else if constexpr (initialization == ArrayInitialize::RepeatLast)
              {
                return this->template get_multi_array<extent() - 1>();
              }
            else
              {
                static_assert(Utility::always_false<std::integral_constant<PropertiesIndexer, idx>>, "LOGIC ERROR.");

                return InvalidType {};
              }
          }
        else
          {
            return static_cast<const typename IndexedType<idx>::Type &>(HelperBase<idx>::get());
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array() &&
      {
        static_assert(idx >= 0, "Non-negative indices, please!");

        if constexpr (idx >= extent())
          {
            if constexpr (initialization == ArrayInitialize::Default || extent() == 0)
              {
                return DefaultInitializer {};
              }
            else if constexpr (initialization == ArrayInitialize::Wrap)
              {
                return this->template get_multi_array<idx % extent()>();
              }
            else if constexpr (initialization == ArrayInitialize::RepeatLast)
              {
                return this->template get_multi_array<extent() - 1>();
              }
            else
              {
                static_assert(Utility::always_false<std::integral_constant<PropertiesIndexer, idx>>, "LOGIC ERROR.");

                return InvalidType {};
              }
          }
        else
          {
            if constexpr (IndexedType<idx>::can_move &&
                          (initialization == ArrayInitialize::Default || (initialization == ArrayInitialize::RepeatLast && idx < extent() - 1)))
              {
                return static_cast<typename IndexedType<idx>::Type &&>(HelperBase<idx>::get());
              }
            else
              {
                return static_cast<const typename IndexedType<idx>::Type &>(HelperBase<idx>::get());
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer... idx, class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ArrayInitializationHelper(Ts &&... ts):
        impl::ArrayInitializationHelperHelper<IdxTpMv>(std::forward<Ts>(ts))...
      {
      }
    };

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <ArrayInitialize initializer = ArrayInitialize::Default, class... Ts, PropertiesIndexer... idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto array_initialize_helper(const std::integer_sequence<PropertiesIndexer, idx...> &,
                                                                                                   Ts &&... ts)
      {
        using RetT = ArrayInitializationHelper<initializer, IndexTypeAndCanMove<idx, std::decay_t<Ts>, std::is_rvalue_reference_v<decltype(ts)>>...>;

        return RetT(std::forward<Ts>(ts)...);
      }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <ArrayInitialize initializer, class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto array_initialize(Ts &&... ts)
    {
      return impl::array_initialize_helper<initializer>(std::make_integer_sequence<PropertiesIndexer, sizeof...(Ts)>(), std::forward<Ts>(ts)...);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class MemoryContext>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextAwareAllocator
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr T * allocate(typename MemoryContext::ContextInfo & info, const size_type size)
      {
        return static_cast<T *>(MemoryContext::allocate(info, static_cast<std::make_unsigned_t<size_type>>(size) * sizeof(T)));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void deallocate(typename MemoryContext::ContextInfo & info, T * ptr, const size_type size)
      {
        MemoryContext::deallocate(info, ptr, static_cast<std::make_unsigned_t<size_type>>(size) * sizeof(T));
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES LocalAllocator
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr T * allocate(typename MemoryContexts::Current::ContextInfo & info, const size_type size)
      {
        return static_cast<T *>(MemoryContexts::Current::allocate(info, static_cast<std::make_unsigned_t<size_type>>(size) * sizeof(T)));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
      deallocate(typename MemoryContexts::Current::ContextInfo & info, T * ptr, const size_type size)
      {
        MemoryContexts::Current::deallocate(info, ptr, static_cast<std::make_unsigned_t<size_type>>(size) * sizeof(T));
      }
    };

    namespace impl
    {
      //We will grow in steps of (capacity * grow_numerator) / grow_denominator.
      //Default to 2 like most standard libraries do.
      template <class T, class MemoryContext, class SizeType, class DifferenceType, SizeType grow_numerator = 2, SizeType grow_denominator = 1>
      struct BaseVectorImpl : public MemoryContext::ContextInfo
      {
        static_assert(std::is_trivially_destructible_v<T> && std::is_trivially_copyable_v<T>,
                      "We will only support trivially copyable and destructible types.");

        static_assert(grow_numerator > grow_denominator);
        static_assert(grow_denominator > 0);

        using size_type       = SizeType;
        using difference_type = DifferenceType;

        using WriteProxy     = T;
        using ConstReadProxy = const T &;
        using ReadProxy      = T &;

       protected:

        T *       m_ptr      = nullptr;
        size_type m_size     = 0;
        size_type m_capacity = 0;

       private:

        template <class A, class B, class C, class D, C, C>
        friend struct BaseVectorImpl;

        using MemContextInfo = typename MemoryContext::ContextInfo;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
        {
          return MemoryContext {};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto & memory_context_info() const
        {
          return static_cast<const MemContextInfo &>(*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & memory_context_info()
        {
          return static_cast<MemContextInfo &>(*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto interface_properties()
        {
          return InterfaceProperties {MemoryContext::access_properties(), ResizeProperties::Full, MutabilityProperties::Full};
        }

       private:

        struct AllocationHelper
        {
          BaseVectorImpl * destroyer;
          size_type        size;
          T *              ptr;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr AllocationHelper(BaseVectorImpl * f, const size_type sz):
            destroyer(f),
            size(sz),
            ptr(nullptr)
          {
            ptr = static_cast<T *>(MemoryContext::allocate(f->memory_context_info(), static_cast<std::make_unsigned_t<size_type>>(sz) * sizeof(T)));
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr AllocationHelper(BaseVectorImpl * f, const size_type sz, T * p):
            destroyer(f),
            size(sz),
            ptr(p)
          {
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES

#if __cplusplus >= 202002L
          constexpr
#endif
            ~AllocationHelper()
          {
            if (ptr != nullptr)
              {
                MemoryContext::deallocate(destroyer->memory_context_info(), ptr, size);
              }
          }
        };

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
#if __cplusplus >= 202002L
        constexpr
#endif
          void
          reallocate_to_size(const size_type exact_size)
        {
          if (exact_size != m_capacity)
            {
              if (exact_size == 0)
                {
                  MemoryContext::deallocate(this->memory_context_info(), m_ptr, m_capacity);
                  m_ptr      = nullptr;
                  m_capacity = 0;
                  return;
                }
              AllocationHelper dh(this, exact_size);

              if (m_ptr != nullptr && m_size > 0)
                {
                  MemoryContexts::memcopy_with_context(this->memory_context(),
                                                       this->memory_context(),
                                                       this->memory_context_info(),
                                                       this->memory_context_info(),
                                                       dh.ptr,
                                                       m_ptr,
                                                       exact_size > m_size ? m_size : exact_size);
                }

              T * old_ptr = m_ptr;

              m_ptr = dh.ptr;

              dh.ptr  = old_ptr;
              dh.size = m_capacity;

              m_capacity = exact_size;
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type
        calculate_reallocation_size(const size_type wanted)
        {
          size_type ret = m_capacity;

          if (ret < 1)
            {
              ret = 1;
            }
          else if (ret == 1)
            {
              ret = 2;
            }

          constexpr size_type s_diff = grow_numerator - grow_denominator;

#if MARIONETTE_USE_ASSERTIONS
          assert((ret * s_diff) / grow_denominator > 0);
#endif
          //We want to actually be increasing the capacity.
          //Otherwise we get an infinite loop...

          while (ret < wanted)
            {
              ret += (ret * s_diff) / grow_denominator;

              //For the GCC/Clang case of (2, 1),
              //pretty much anything works.
              //MSVC uses (3, 2) implemented like this.
              //We could maybe adjust,
              //but anyway not much is done
              //in regards to the vector growth factor...
            }

          return ret;
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const T * data() const
        {
          return m_ptr;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr T * data()
        {
          return m_ptr;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const size_type & size() const
        {
          return m_size;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const size_type & capacity() const
        {
          return m_capacity;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void clear()
        {
          this->resize(0);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void reserve(const size_type new_size)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(new_size >= 0);
#endif
          if (new_size > m_capacity)
            {
              this->reallocate_to_size(this->calculate_reallocation_size(new_size));
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void resize(const size_type new_size)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(new_size >= 0);
#endif
          if (new_size > m_capacity)
            {
              this->reallocate_to_size(this->calculate_reallocation_size(new_size));
            }
          m_size = new_size;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void shrink_to_fit()
        {
          this->reallocate_to_size(m_size);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool allows_simple_accessor()
        {
          return true;
        }

#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wunsafe-buffer-usage"
#endif
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (MemoryContext::access_properties() >= AccessProperties::GetContents),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const T & simple_accessor(const size_type i) const
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(i >= 0 && i < m_size);
#endif

          return m_ptr[i];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (MemoryContext::access_properties() >= AccessProperties::GetContents),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr T & simple_accessor(const size_type i)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(i >= 0 && i < m_size);
#endif

          return m_ptr[i];
        }

#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic pop
#endif

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
#if __cplusplus >= 202002L
        constexpr
#endif
          ~BaseVectorImpl()
        {
          MemoryContext::deallocate(this->memory_context_info(), m_ptr, m_capacity);
        }

       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherT>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void copier(const OtherT & other)
        {
          this->resize(other.size());
          MemoryContexts::memcopy_with_context(this->memory_context(),
                                               other.memory_context(),
                                               this->memory_context_info(),
                                               other.memory_context_info(),
                                               m_ptr,
                                               other.m_ptr,
                                               other.size());
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherT>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void mover(OtherT && other)
        {
          if (MemoryContexts::memory_contexts_compatible(this->memory_context_info(), other.memory_context_info()))
            {
              T * ptr = other.m_ptr;

              MemoryContexts::move_with_context(this->memory_context(), other.memory_context(), this->memory_context_info(), ptr, other.m_capacity);
              //If an exception is thrown here... we may leak or otherwise be unable to deallocate.

              m_ptr      = ptr;
              m_capacity = static_cast<SizeType>(other.m_capacity);

              other.m_ptr      = nullptr;
              other.m_capacity = 0;
            }
          else
            {
              this->copier(static_cast<const OtherT &>(other));
            }
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl():
          m_ptr(nullptr),
          m_size(0),
          m_capacity(0)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl(const BaseVectorImpl & other)
        {
          m_ptr      = nullptr;
          m_size     = 0;
          m_capacity = 0;
          copier(other);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl(BaseVectorImpl && other)
        {
          m_ptr      = nullptr;
          m_size     = 0;
          m_capacity = 0;
          mover(std::move(other));
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts, class disabler = std::enable_if_t<std::is_constructible_v<MemContextInfo, Ts...>>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl(Ts &&... ts): MemContextInfo(std::forward<Ts>(ts)...)
        {
          m_ptr      = nullptr;
          m_size     = 0;
          m_capacity = 0;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherContext,
                                                                 class OtherSzT,
                                                                 class OtherDfT,
                                                                 OtherSzT other_gn,
                                                                 OtherSzT other_gd,
                                                                 class... Ts,
                                                                 class disabler = std::enable_if_t<std::is_constructible_v<MemContextInfo, Ts...>>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl(
          const BaseVectorImpl<T, OtherContext, OtherSzT, OtherDfT, other_gn, other_gd> & other,
          Ts &&... ts):
          MemContextInfo(std::forward<Ts>(ts)...)
        {
          m_ptr      = nullptr;
          m_size     = 0;
          m_capacity = 0;
          copier(other);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
          class OtherContext,
          class OtherSzT,
          class OtherDfT,
          OtherSzT other_gn,
          OtherSzT other_gd,
          class... Ts,
          class disabler = std::enable_if_t < std::is_constructible_v<MemContextInfo, Ts...> &&
                           MemoryContexts::memory_contexts_potentially_compatible(MemoryContext {}, OtherContext {}) >>
                             MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl(
                               BaseVectorImpl<T, OtherContext, OtherSzT, OtherDfT, other_gn, other_gd> && other,
                               Ts &&... ts):
          MemContextInfo(std::forward<Ts>(ts)...)
        {
          m_ptr      = nullptr;
          m_size     = 0;
          m_capacity = 0;
          mover(std::move(other));
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl &
        operator=(const BaseVectorImpl & other)
        {
          copier(other);
          return (*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl &
        operator=(BaseVectorImpl && other)
        {
          mover(std::move(other));
          return (*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherContext, class OtherSzT, class OtherDfT, OtherSzT other_gn, OtherSzT other_gd>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl &
        operator=(const BaseVectorImpl<T, OtherContext, OtherSzT, OtherDfT, other_gn, other_gd> & other)
        {
          copier(other);
          return (*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
          class OtherContext,
          class OtherSzT,
          class OtherDfT,
          OtherSzT other_gn,
          OtherSzT other_gd,
          class disabler = std::enable_if_t<MemoryContexts::memory_contexts_potentially_compatible(MemoryContext {}, OtherContext {})>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr BaseVectorImpl &
        operator=(BaseVectorImpl<T, OtherContext, OtherSzT, OtherDfT, other_gn, other_gd> && other)
        {
          mover(other);
          return (*this);
        }
      };
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Type, class MemoryContext, class SizeType, class DifferenceType>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextAwareVectorBase :
      public impl::BaseVectorImpl<Type, MemoryContext, SizeType, DifferenceType>
    {
     private:

      template <class A, class B, class C, class D, C, C>
      friend struct impl::BaseVectorImpl;

      using Base = impl::BaseVectorImpl<Type, MemoryContext, SizeType, DifferenceType>;

     public:

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
      __pragma("nv_diag_suppress 20012")
  #else
      _Pragma("nv_diag_suppress 20012")
  #endif
#endif
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ContextAwareVectorBase() = default;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ContextAwareVectorBase &
      operator=(const ContextAwareVectorBase &) = default;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ContextAwareVectorBase &
      operator=(ContextAwareVectorBase &&) = default;

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
      __pragma("nv_diag_default 20012")
  #else
      _Pragma("nv_diag_default 20012")
  #endif
#endif

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts, class disabler = std::enable_if_t<std::is_constructible_v<Base, Ts...>>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ContextAwareVectorBase(Ts &&... ts): Base(std::forward<Ts>(ts)...)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class disabler = std::enable_if_t<std::is_assignable_v<Base &, T>>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ContextAwareVectorBase & operator=(T && t)
      {
        Base::operator=(std::forward<T>(t));
        return *this;
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Type, class SizeType, class DifferenceType>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES LocalVectorBase :
      public impl::BaseVectorImpl<Type, MemoryContexts::Current, SizeType, DifferenceType>
    {
     private:

      template <class A, class B, class C, class D, C, C>
      friend struct impl::BaseVectorImpl;

      using Base = impl::BaseVectorImpl<Type, MemoryContexts::Current, SizeType, DifferenceType>;

     public:

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
      __pragma("nv_diag_suppress 20012")
  #else
      _Pragma("nv_diag_suppress 20012")
  #endif
#endif
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr LocalVectorBase() = default;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr LocalVectorBase &
      operator=(const LocalVectorBase &) = default;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr LocalVectorBase &
      operator=(LocalVectorBase &&) = default;

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
      __pragma("nv_diag_default 20012")
  #else
      _Pragma("nv_diag_default 20012")
  #endif
#endif

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts, class disabler = std::enable_if_t<std::is_constructible_v<Base, Ts...>>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr LocalVectorBase(Ts &&... ts): Base(std::forward<Ts>(ts)...)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class disabler = std::enable_if_t<std::is_assignable_v<Base &, T>>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr LocalVectorBase & operator=(T && t)
      {
        Base::operator=(std::forward<T>(t));
        return *this;
      }
    };
  }
}

#endif
