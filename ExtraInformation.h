//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_EXTRAINFORMATION_H
#define MARIONETTE_EXTRAINFORMATION_H

#include "MarionetteBase.h"

#include "ExtraInformation/Generic.h"
#include "ExtraInformation/StandardLibrary.h"

#endif
