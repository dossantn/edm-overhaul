//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_VECTORLIKEWRAPPER_H
#define MARIONETTE_VECTORLIKEWRAPPER_H

#include <utility>
#include <type_traits>
#include <iterator>

#include "MarionetteBase.h"

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

#if MARIONETTE_INCLUDE_POTENTIALLY_THROWING_FUNCTIONS
  #include <stdexcept>
#endif

#if MARIONETTE_TURN_VECTOR_LIKE_WRAPPER_INTO_IDEMPOTENT

#else

namespace Marionette
{
  namespace Utility
  {

    namespace impl
    {

  #if MARIONETTE_WARN_ON_ELEMENTWISE_VECTOR_OPERATIONS

      template <bool b>
      struct elementwise_operations_warner
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_elementwise()
        {
        }
      };

      template <>
      struct elementwise_operations_warner<true>
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        [[deprecated("This is not really a deprecation warning. The code is performing elementwise vector operations. This may be less efficient. "
                     "Define 'MARIONETTE_WARN_ON_ELEMENTWISE_VECTOR_OPERATIONS' to 0 to suppress this warning.")]]
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_elementwise()
        {
        }
      };

  #endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_use_simple_accessor_helper(Ts &&...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, bool ret = T::allows_simple_accessor()>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_use_simple_accessor_helper(T &&)
      {
        return std::integral_constant<bool, ret> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool vlw_use_simple_accessor()
      {
        return decltype(vlw_use_simple_accessor_helper(std::declval<T>()))::value;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_use_base_class_access_operator_helper(Ts &&...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, bool ret = T::specifies_access_operator()>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_use_base_class_access_operator_helper(T &&)
      {
        return std::integral_constant<bool, ret> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool vlw_use_base_class_access_operator()
      {
        return decltype(vlw_use_base_class_access_operator_helper(std::declval<T>()))::value;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_base_class_has_access_operator_helper(Ts &&...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,
                                                               class... Ts,
                                                               class Result = decltype(std::declval<T>().operator[](std::declval<Ts>()...))>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_base_class_has_access_operator_helper(T &&, Ts &&...)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Base, class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool vlw_base_class_has_access_operator()
      {
        return decltype(vlw_base_class_has_access_operator_helper(std::declval<Base>(), std::declval<Ts>()...))::value;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Base, class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool access_operator_disable_helper()
      {
        if constexpr (vlw_use_base_class_access_operator<Base>())
          {
            return vlw_base_class_has_access_operator<Base, Ts...>();
          }
        else
          {
            return std::is_constructible_v<typename Base::size_type, Ts...>;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_base_class_has_at_function_helper(Ts &&...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Ts, class Result = decltype(std::declval<T>().at(std::declval<Ts>()...))>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_base_class_has_at_function_helper(T &&, Ts &&...)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Base, class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool vlw_base_class_has_at_function()
      {
        return decltype(vlw_base_class_has_at_function_helper(std::declval<Base>(), std::declval<Ts>()...))::value;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Base, class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool at_function_disable_helper()
      {
        if constexpr (vlw_use_base_class_access_operator<Base>())
          {
            return vlw_base_class_has_at_function<Base, Ts...>();
          }
        else
          {
            return std::is_constructible_v<typename Base::size_type, Ts...>;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_can_get_owning_pointer_helper(Ts &&...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Check = decltype(std::declval<T>().get_owning_pointer())>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_can_get_owning_pointer_helper(T &&)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool vlw_can_get_owning_pointer()
      {
        return decltype(vlw_can_get_owning_pointer_helper(std::declval<T>()))::value;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) vlw_get_hopefully_owning_pointer(const T * ptr)
      {
        if constexpr (vlw_can_get_owning_pointer<T>())
          {
            return ptr->get_owning_pointer();
          }
        else
          {
            return ptr;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) vlw_get_hopefully_owning_pointer(T * ptr)
      {
        if constexpr (vlw_can_get_owning_pointer<T>())
          {
            return ptr->get_owning_pointer();
          }
        else
          {
            return ptr;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PointerLikeT>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) vlw_get_hopefully_owning_pointer(PointerLikeT && t)
      {
        if constexpr (vlw_can_get_owning_pointer<std::decay_t<decltype(*t)>>())
          {
            return std::forward<PointerLikeT>(t)->get_owning_pointer();
          }
        else
          {
            return std::forward<PointerLikeT>(t);
          }
      }

      template <class T>
      using vlw_hopefully_owning_pointer_type = decltype(vlw_get_hopefully_owning_pointer(std::declval<T *>()));

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_can_get_proxied_pointer_helper(const Ts &...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Check = decltype(&T::get_proxied_pointer)>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_can_get_proxied_pointer_helper(const T &)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool vlw_can_get_proxied_pointer()
      {
        return decltype(vlw_can_get_proxied_pointer_helper(std::declval<const T &>()))::value;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Check = decltype(std::declval<T>().make_const_read_proxy(0))>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_make_const_read_proxy(const T &)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_make_const_read_proxy(const Ts &...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Check = typename T::ConstReadProxy>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_const_read_proxy(const T &)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_const_read_proxy(const Ts &...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Check = decltype(std::declval<T>().make_read_proxy(0))>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_make_read_proxy(const T &)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_make_read_proxy(const Ts &...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Check = typename T::ReadProxy>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_read_proxy(const T &)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_read_proxy(const Ts &...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Check = decltype(std::declval<T>().make_write_proxy())>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_make_write_proxy(const T &)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_make_write_proxy(const Ts &...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Check = typename T::WriteProxy>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_write_proxy(const T &)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_write_proxy(const Ts &...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T,
                                                            bool b = decltype(vlw_has_make_const_read_proxy(std::declval<T>()))::value,
                                                            bool c = decltype(vlw_has_const_read_proxy(std::declval<T>()))::value>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_const_read_proxy_helper
      {
        using Type = Utility::InvalidType;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, bool any>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_const_read_proxy_helper<T, true, any>
      {
        using Type = decltype(std::declval<T>().make_const_read_proxy(0));
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_const_read_proxy_helper<T, false, true>
      {
        using Type = typename T::ConstReadProxy;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T,
                                                            bool b = decltype(vlw_has_make_read_proxy(std::declval<T>()))::value,
                                                            bool c = decltype(vlw_has_read_proxy(std::declval<T>()))::value>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_read_proxy_helper
      {
        using Type = Utility::InvalidType;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, bool any>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_read_proxy_helper<T, true, any>
      {
        using Type = decltype(std::declval<T>().make_read_proxy(0));
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_read_proxy_helper<T, false, true>
      {
        using Type = typename T::ReadProxy;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T,
                                                            bool b = decltype(vlw_has_make_write_proxy(std::declval<T>()))::value,
                                                            bool c = decltype(vlw_has_read_proxy(std::declval<T>()))::value>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_write_proxy_helper
      {
        using Type = Utility::InvalidType;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, bool any>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_write_proxy_helper<T, true, any>
      {
        using Type = decltype(std::declval<T>().make_write_proxy());
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_write_proxy_helper<T, false, true>
      {
        using Type = typename T::WriteProxy;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_interface_properties_helper(Ts &&...)
      {
        return std::false_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Ret = decltype(T::interface_properties())>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto vlw_has_interface_properties_helper(T &&)
      {
        return std::true_type {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool vlw_has_interface_properties()
      {
        return decltype(vlw_has_interface_properties_helper(std::declval<T>()))::value;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Base>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr InterfaceProperties vlw_interface_properties()
      {
        if constexpr (impl::vlw_has_interface_properties<Base>())
          {
            return Base::interface_properties();
          }
        else
          {
            return InterfaceProperties {AccessProperties::Full, ResizeProperties::Full, MutabilityProperties::Full};
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Base, bool is_constant, class OwningPointerT, class RetT, int sign>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES vlw_iterator
      {
        template <class, bool, class, class, int>
        friend struct vlw_iterator;

       protected:

        using size_type = typename Base::size_type;

        OwningPointerT m_ptr   = {};
        size_type      m_index = 0;

       public:

        using value_type      = typename impl::vlw_write_proxy_helper<Base>::Type;
        using difference_type = typename Base::difference_type;
        using reference       = RetT;
        using pointer         = std::remove_reference_t<RetT> *;
        //To handle the cases where RetT is a reference to something...
        using iterator_category = std::random_access_iterator_tag;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator(const OwningPointerT p, const size_type idx): m_ptr(p), m_index(idx)
        {
        }

  #if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
    #ifdef _MSVC
        __pragma("nv_diag_suppress 20012")
    #else
        _Pragma("nv_diag_suppress 20012")
    #endif
  #endif
          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator() = default;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator(const vlw_iterator &) = default;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator(vlw_iterator &&) = default;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator &
        operator=(const vlw_iterator &) = default;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator &
        operator=(vlw_iterator &&) = default;

  #if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
    #ifdef _MSVC
        __pragma("nv_diag_default 20012")
    #else
        _Pragma("nv_diag_default 20012")
    #endif
  #endif

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void swap(vlw_iterator & other)
        {
          OwningPointerT tempptr = m_ptr;

          size_type tempindex = m_index;

          m_ptr   = other.m_ptr;
          m_index = other.m_index;

          other.m_ptr   = tempptr;
          other.m_index = tempindex;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties<Base>().can_refer_to_const_object(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr RetT operator*() const
        {
          const size_type true_index = m_index - static_cast<size_type>(sign < 0);

  #if MARIONETTE_USE_ASSERTIONS
          assert(true_index < m_ptr->size() && true_index >= 0);
  #endif

          if constexpr (impl::vlw_use_simple_accessor<Base>())
            {
              return m_ptr->simple_accessor(true_index);
            }
          else if constexpr (!is_constant && decltype(vlw_has_make_read_proxy(std::declval<Base>()))::value)
            {
              return m_ptr->make_read_proxy(true_index);
            }
          else if constexpr (decltype(vlw_has_make_const_read_proxy(std::declval<Base>()))::value)
            {
              return m_ptr->make_const_read_proxy(true_index);
            }
          else
            {
              return RetT {impl::vlw_get_hopefully_owning_pointer(m_ptr), true_index};
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties<Base>().can_refer_to_const_object(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator->() const
        {
          const size_type true_index = m_index - static_cast<size_type>(sign < 0);

  #if MARIONETTE_USE_ASSERTIONS
          assert(true_index < m_ptr->size() && true_index >= 0);
  #endif

          if constexpr (impl::vlw_use_simple_accessor<Base>())
            {
              return arrow_operator_wrap(m_ptr->simple_accessor(true_index));
            }
          else if constexpr (!is_constant && decltype(vlw_has_make_read_proxy(std::declval<Base>()))::value)
            {
              return arrow_operator_wrap(m_ptr->make_read_proxy(true_index));
            }
          else if constexpr (decltype(vlw_has_make_const_read_proxy(std::declval<Base>()))::value)
            {
              return arrow_operator_wrap(m_ptr->make_const_read_proxy(true_index));
            }
          else
            {
              return arrow_operator_wrap(RetT {impl::vlw_get_hopefully_owning_pointer(m_ptr), true_index});
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator & operator++()
        {
          m_index += static_cast<size_type>(sign);
          return *this;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator operator++(int)
        {
          vlw_iterator old = *this;
          m_index += static_cast<size_type>(sign);
          return old;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator & operator--()
        {
          m_index -= static_cast<size_type>(sign);
          return *this;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator operator--(int)
        {
          vlw_iterator old = *this;
          m_index -= static_cast<size_type>(sign);
          return old;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator &
        operator+=(const difference_type n)
        {
          m_index += static_cast<size_type>(n * sign);
          return *this;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator &
        operator-=(const difference_type n)
        {
          m_index -= static_cast<size_type>(n * sign);
          return *this;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend vlw_iterator
        operator+(const vlw_iterator & i, const difference_type n)
        {
          return vlw_iterator {i.m_ptr, i.m_index + static_cast<size_type>(n * sign)};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend vlw_iterator
        operator+(const difference_type n, const vlw_iterator & i)
        {
          return vlw_iterator {i.m_ptr, i.m_index + static_cast<size_type>(n * sign)};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend vlw_iterator
        operator-(const vlw_iterator & i, const difference_type n)
        {
          return vlw_iterator {i.m_ptr, i.m_index - static_cast<size_type>(n * sign)};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend vlw_iterator
        operator-(const difference_type n, const vlw_iterator & i)
        {
          return vlw_iterator {i.m_ptr, i.m_index - static_cast<size_type>(n * sign)};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend difference_type
        operator-(const vlw_iterator & a, const vlw_iterator & b)
        {
          return (sign > 0 ? static_cast<difference_type>(a.m_index - b.m_index) : static_cast<difference_type>(b.m_index - a.m_index));
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties<Base>().can_refer_to_const_object(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr RetT operator[](const difference_type n) const
        {
          const size_type true_index = m_index + static_cast<size_type>(n * sign) - static_cast<size_type>(sign < 0);

  #if MARIONETTE_USE_ASSERTIONS
          assert(true_index < m_ptr->size() && true_index >= 0);
  #endif

          if constexpr (impl::vlw_use_simple_accessor<Base>())
            {
              return m_ptr->simple_accessor(true_index);
            }
          else if constexpr (!is_constant && decltype(vlw_has_make_read_proxy(std::declval<Base>()))::value)
            {
              return arrow_operator_wrap(m_ptr->make_read_proxy(true_index));
            }
          else if constexpr (decltype(vlw_has_make_const_read_proxy(std::declval<Base>()))::value)
            {
              return arrow_operator_wrap(m_ptr->make_const_read_proxy(true_index));
            }
          else
            {
              return RetT {impl::vlw_get_hopefully_owning_pointer(m_ptr), true_index};
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend bool
        operator==(const vlw_iterator & a, const vlw_iterator & b)
        {
          return (a.m_index == b.m_index && a.m_ptr == b.m_ptr);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend bool
        operator<(const vlw_iterator & a, const vlw_iterator & b)
        {
          return (sign > 0 ? (a.m_index < b.m_index) : (b.m_index < a.m_index));
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend bool
        operator>(const vlw_iterator & a, const vlw_iterator & b)
        {
          return (sign > 0 ? (a.m_index > b.m_index) : (b.m_index > a.m_index));
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend bool
        operator!=(const vlw_iterator & a, const vlw_iterator & b)
        {
          return !(a == b);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend bool
        operator>=(const vlw_iterator & a, const vlw_iterator & b)
        {
          return !(a < b);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr friend bool
        operator<=(const vlw_iterator & a, const vlw_iterator & b)
        {
          return !(a > b);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherB,
                                                                 bool other_c,
                                                                 class OtherPtrT,
                                                                 class T,
                                                                 class disabler = std::enable_if_t<(other_c >= is_constant)>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr explicit operator vlw_iterator<OtherB, other_c, OtherPtrT, T, sign>() const
        {
          return {m_ptr, m_index};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherB,
                                                                 bool other_c,
                                                                 class OtherPtrT,
                                                                 class T,
                                                                 class disabler = std::enable_if_t<(is_constant >= other_c)>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr vlw_iterator(const vlw_iterator<OtherB, other_c, OtherPtrT, T, sign> & o):
          m_ptr(o.m_ptr),
          m_index(o.m_index)
        {
        }

        //Potential TO-DO: constrain this in a sufficiently smart way...
      };
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class VectorLikeBase>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES VectorLikeWrapper : public VectorLikeBase
    {
     protected:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
      vlw_interface_properties()
      {
        return impl::vlw_interface_properties<VectorLikeBase>();
      }

     public:

      using ConstReadProxy  = typename impl::vlw_const_read_proxy_helper<VectorLikeBase>::Type;
      using ReadProxy       = typename impl::vlw_read_proxy_helper<VectorLikeBase>::Type;
      using WriteProxy      = typename impl::vlw_write_proxy_helper<VectorLikeBase>::Type;
      using value_type      = WriteProxy;
      using reference       = ReadProxy;
      using const_reference = ConstReadProxy;
      using pointer         = WriteProxy *;       // ?
      using const_pointer   = const WriteProxy *; // ?
      using size_type       = typename VectorLikeBase::size_type;
      using difference_type = typename VectorLikeBase::difference_type;

      using const_iterator = impl::vlw_iterator<VectorLikeBase, true, impl::vlw_hopefully_owning_pointer_type<const VectorLikeBase>, ConstReadProxy, +1>;

      using const_reverse_iterator =
        impl::vlw_iterator<VectorLikeBase, true, impl::vlw_hopefully_owning_pointer_type<const VectorLikeBase>, ConstReadProxy, -1>;

      using iterator = impl::vlw_iterator<VectorLikeBase,
                                          !vlw_interface_properties().can_refer_to_modifiable_object(),
                                          impl::vlw_hopefully_owning_pointer_type<VectorLikeBase>,
                                          std::conditional_t<vlw_interface_properties().can_refer_to_modifiable_object(), ReadProxy, ConstReadProxy>,
                                          +1>;

      using reverse_iterator = impl::vlw_iterator<VectorLikeBase,
                                                  !vlw_interface_properties().can_refer_to_modifiable_object(),
                                                  impl::vlw_hopefully_owning_pointer_type<VectorLikeBase>,
                                                  std::conditional_t<vlw_interface_properties().can_refer_to_modifiable_object(), ReadProxy, ConstReadProxy>,
                                                  -1>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const_iterator cbegin() const
      {
        return const_iterator {impl::vlw_get_hopefully_owning_pointer(this), 0};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const_iterator cend() const
      {
        return const_iterator {impl::vlw_get_hopefully_owning_pointer(this), this->size()};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const_iterator begin() const
      {
        return cbegin();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const_iterator end() const
      {
        return cend();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr iterator begin()
      {
        return iterator {impl::vlw_get_hopefully_owning_pointer(this), 0};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr iterator end()
      {
        return iterator {impl::vlw_get_hopefully_owning_pointer(this), this->size()};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const_reverse_iterator crbegin() const
      {
        return const_reverse_iterator {impl::vlw_get_hopefully_owning_pointer(this), this->size()};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const_reverse_iterator crend() const
      {
        return const_reverse_iterator {impl::vlw_get_hopefully_owning_pointer(this), 0};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const_reverse_iterator rbegin() const
      {
        return crbegin();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const_reverse_iterator rend() const
      {
        return crend();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr reverse_iterator rbegin()
      {
        return reverse_iterator {impl::vlw_get_hopefully_owning_pointer(this), this->size()};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr reverse_iterator rend()
      {
        return reverse_iterator {impl::vlw_get_hopefully_owning_pointer(this), 0};
      }

      ///@remark If one cannot resize the underlying object,
      ///        assignment is done up to the minimum between the current size and the requested length.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_copy_into_collection(),
                                                               class T        = WriteProxy,
                                                               class disabler = std::enable_if_t < std::is_assignable_v<WriteProxy &, T> &&
                                                                                valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
                                                                                         assign(const size_type count, const T & ref)
      {
        if constexpr (std::is_same_v<T, ReadProxy> || std::is_same_v<T, ConstReadProxy>)
          {
            if constexpr (impl::vlw_can_get_proxied_pointer<T>())
              {
                if (ref.get_proxied_pointer() == impl::vlw_get_hopefully_owning_pointer(this))
                  {
                    WriteProxy wp = ref;
                    this->assign(count, wp);
                    return;
                  }
              }
            else
              {
                WriteProxy wp = ref;
                this->assign(count, wp);
                return;
              }
          }

        size_type desired_number = count;

        if constexpr (vlw_interface_properties().can_resize_fully())
          {
            this->resize(count, ref);
          }
        else
          {
            const size_type current_size = this->size();

            desired_number = current_size > count ? count : current_size;
          }

        for (size_type i = 0; i < desired_number; ++i)
          {
            (*this)[i] = ref;
          }
      }

      ///@remark If one cannot resize the underlying object,
      ///        assignment is done up to the minimum between the current size and the requested length.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        bool valid = vlw_interface_properties().can_copy_into_collection(),
        class InputIt,
        class disabler = std::enable_if_t < std::is_assignable_v<ReadProxy, decltype(*std::declval<InputIt>())> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void assign(InputIt first, const InputIt last)
      {
        if constexpr (vlw_interface_properties().can_resize_fully())
          {
            size_type count = 0;

            if constexpr (Utility::is_random_access_iterator_or_pointer<InputIt>())
              {
                count = static_cast<size_type>(last - first);
              }
            else
              {
                for (InputIt other = first; other != last; ++other)
                  {
                    ++count;
                  }
              }

            this->resize(count);
          }
        const size_type current_size = this->size();

        auto assign_it = this->begin();

        //If you are following a unsafe-buffer-usage from Clang,
        //it's probably an initializer list iterator
        //(which is a straight-up pointer).
        //I'd recommend not excluding this explicitly with pragmas
        //(pragma clang diagnostic push
        // pragma clang diagnostic ignored "-Wunsafe-buffer-usage")
        //for three reasons:
        //
        //1) '-Wunsafe-buffer-usage' is at the time of writing still "experimental",
        //   so users who do enable it (or use '-Weverything') should be aware
        //   that they are opting in to warnings that might not fit all possible use cases.
        //
        //2) Since it complains even for 'main(int argc, char ** argv)',
        //   any sort of blanket "fix all warnings everywhere" policy
        //   is somewhat doomed from the start with this option.
        //
        //3) Initializer list iterators triggering this warning
        //   (or returning raw pointers, for that matter...)
        //   strikes me as something that will be improved on in the future.
        //
        for (size_type count = 0; count < current_size && first != last; ++count, ++first, ++assign_it)
          {
            (*assign_it) = (*first);
          }
      }

      ///@remark If one cannot resize the underlying object,
      ///        assignment is done up to the minimum between the current size and the requested length.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_copy_into_collection(),
                                                               class T        = WriteProxy,
                                                               class disabler = std::enable_if_t < std::is_assignable_v<ReadProxy, T> &&
                                                                                valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
                                                                                         assign(std::initializer_list<T> list)
      {
        this->assign(list.begin(), list.end());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        bool valid = vlw_interface_properties().can_refer_to_const_object(),
        class... Ts,
        class disabler = std::enable_if_t < impl::access_operator_disable_helper<VectorLikeBase, Ts...>() &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator[](Ts &&... ts) const
      {
        if constexpr (impl::vlw_use_base_class_access_operator<VectorLikeBase>())
          {
            return VectorLikeBase::operator[](std::forward<Ts>(ts)...);
          }
        else
          {
            static_assert(sizeof...(Ts) == 1, "Vector-Like Wrapper access operator is one-dimensional!");

  #if MARIONETTE_USE_ASSERTIONS
            assert(static_cast<size_type>((std::forward<Ts>(ts) + ...)) < this->size() && (std::forward<Ts>(ts) + ...) >= 0);
  #endif

            return *(this->begin() + static_cast<difference_type>((std::forward<Ts>(ts) + ...)));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        bool valid = vlw_interface_properties().can_refer_to_modifiable_object(),
        class... Ts,
        class disabler = std::enable_if_t < impl::access_operator_disable_helper<VectorLikeBase, Ts...>() &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator[](Ts &&... ts)
      {
        if constexpr (impl::vlw_use_base_class_access_operator<VectorLikeBase>())
          {
            return VectorLikeBase::operator[](std::forward<Ts>(ts)...);
          }
        else
          {
            static_assert(sizeof...(Ts) == 1, "Vector-Like Wrapper access operator is one-dimensional!");

  #if MARIONETTE_USE_ASSERTIONS
            assert(static_cast<size_type>((std::forward<Ts>(ts) + ...)) < this->size() && (std::forward<Ts>(ts) + ...) >= 0);
  #endif

            return *(this->begin() + static_cast<difference_type>((std::forward<Ts>(ts) + ...)));
          }
      }

  #if MARIONETTE_INCLUDE_POTENTIALLY_THROWING_FUNCTIONS

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = vlw_interface_properties().can_refer_to_const_object(),
                                                               class... Ts,
                                                               class disabler = std::enable_if_t < impl::at_function_disable_helper<VectorLikeBase, Ts...>() &&
                                                                                valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES decltype(auto)
                                                                                         at(Ts &&... ts) const
      {
        if constexpr (impl::vlw_use_base_class_access_operator<VectorLikeBase>())
          {
            return VectorLikeBase::at(std::forward<Ts>(ts)...);
          }
        else
          {
            const size_type pos = (std::forward<Ts>(ts) + ...);
            if (pos < this->size() && pos >= 0)
              {
                return (*this)[pos];
              }
            else
              {
                throw std::out_of_range {""};
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = vlw_interface_properties().can_refer_to_modifiable_object(),
                                                               class... Ts,
                                                               class disabler = std::enable_if_t < impl::at_function_disable_helper<VectorLikeBase, Ts...>() &&
                                                                                valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES decltype(auto)
                                                                                         at(Ts &&... ts)
      {
        if constexpr (impl::vlw_use_base_class_access_operator<VectorLikeBase>())
          {
            return VectorLikeBase::at(std::forward<Ts>(ts)...);
          }
        else
          {
            const size_type pos = (std::forward<Ts>(ts) + ...);
            if (pos < this->size() && pos >= 0)
              {
                return (*this)[pos];
              }
            else
              {
                throw std::out_of_range {""};
              }
          }
      }

  #endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_refer_to_modifiable_object(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) front()
      {
  #if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
  #endif

        return *(this->begin());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_refer_to_const_object(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) front() const
      {
  #if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
  #endif

        return *(this->cbegin());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_refer_to_modifiable_object(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) back()
      {
  #if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
  #endif

        return *(this->rbegin());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_refer_to_const_object(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) back() const
      {
  #if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
  #endif

        return *(this->rcbegin());
      }

      //Impossible to implement data()...

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool empty() const
      {
        return this->size() == 0;
      }

     protected:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = vlw_interface_properties().can_refer_to_modifiable_object(),
                                                               class It1,
                                                               class It2,
                                                               class It3,
                                                               class disabler = std::enable_if_t < Utility::always_true<It1> &&
                                                                                valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
                                                                                         move_objects(It1 destination, It2 source, const It3 source_end)
      {
        for (; source != source_end; ++source, ++destination)
          {
            (*destination) = (*source);
          }
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_resize_fully(),
                                                               class T        = WriteProxy,
                                                               class disabler = std::enable_if_t < std::is_assignable_v<WriteProxy &, T> &&
                                                                                valid >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr iterator
                                                                                  insert(const const_iterator pos, size_type count, const T & value)
      {
        if constexpr (std::is_same_v<T, ReadProxy> || std::is_same_v<T, ConstReadProxy>)
          {
            if constexpr (impl::vlw_can_get_proxied_pointer<T>())
              {
                if (value.get_proxied_pointer() == impl::vlw_get_hopefully_owning_pointer(this))
                  {
                    WriteProxy wp = value;
                    return this->insert(pos, count, wp);
                  }
              }
            else
              {
                WriteProxy wp = value;
                return this->insert(pos, count, wp);
              }
          }

        const size_type start_index = static_cast<size_type>(pos - this->cbegin());

        const size_type remaining_size = static_cast<size_type>(this->cend() - pos);

  #if MARIONETTE_USE_ASSERTIONS
        assert(start_index >= 0 && start_index < this->size() + count);
        assert(remaining_size >= 0 && remaining_size <= this->size());
  #endif

        this->resize(this->size() + count);

        this->move_objects(this->rbegin(),
                           this->rbegin() + static_cast<difference_type>(count),
                           this->rbegin() + static_cast<difference_type>(count + remaining_size));

        const size_type max_i = this->size() - start_index;

        for (size_type i = 0; i < max_i && i < count; ++i)
          {
            (*this)[start_index + i] = value;
          }

        return this->begin() + static_cast<difference_type>(start_index);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_resize_fully(),
                                                               class T        = WriteProxy,
                                                               class disabler = std::enable_if_t < std::is_assignable_v<WriteProxy &, T> &&
                                                                                valid >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr iterator
                                                                                  insert(const_iterator pos, const T & value)
      {
        return insert(pos, 1, value);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = vlw_interface_properties().can_resize_fully(),
                                                               class InputIt,
                                                               class disabler = std::enable_if_t <
                                                                                  std::is_assignable_v<ReadProxy, decltype(*std::declval<InputIt>())> &&
                                                                                valid >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr iterator
                                                                                  insert(const_iterator pos, InputIt first, const InputIt last)
      {
        size_type count = 0;

        if constexpr (Utility::is_random_access_iterator_or_pointer<InputIt>())
          {
            count = static_cast<size_type>(last - first);
          }
        else
          {
            for (InputIt other = first; other != last; ++other)
              {
                ++count;
              }
          }

        const size_type start_index = static_cast<size_type>(pos - this->cbegin());

        const size_type remaining_size = static_cast<size_type>(this->cend() - pos);

  #if MARIONETTE_USE_ASSERTIONS
        assert(start_index >= 0 && start_index < this->size() + count);
        assert(remaining_size >= 0 && remaining_size <= this->size());
  #endif

        this->resize(this->size() + count);

        this->move_objects(this->rbegin(),
                           this->rbegin() + static_cast<difference_type>(count),
                           this->rbegin() + static_cast<difference_type>(count + remaining_size));

        //If you are following a unsafe-buffer-usage from Clang,
        //it's probably an initializer list iterator
        //(which is a straight-up pointer).
        //I'd recommend not excluding this explicitly with pragmas
        //(pragma clang diagnostic push
        // pragma clang diagnostic ignored "-Wunsafe-buffer-usage")
        //for three reasons:
        //
        //1) '-Wunsafe-buffer-usage' is at the time of writing still "experimental",
        //   so users who do enable it (or use '-Weverything') should be aware
        //   that they are opting in to warnings that might not fit all possible use cases.
        //
        //2) Since it complains even for 'main(int argc, char ** argv)',
        //   any sort of blanket "fix all warnings everywhere" policy
        //   is somewhat doomed from the start with this option.
        //
        //3) Initializer list iterators triggering this warning
        //   (or returning raw pointers, for that matter...)
        //   strikes me as something that will be improved on in the future.
        //
        for (iterator it = this->begin() + static_cast<difference_type>(start_index); first != last; ++first, ++it)
          {
            *it = *first;
          }

        return this->begin() + static_cast<difference_type>(start_index);
      }

      template <bool valid     = vlw_interface_properties().can_resize_fully(),
                class T        = WriteProxy,
                class disabler = std::enable_if_t<std::is_assignable_v<ReadProxy, T> && valid>>
      iterator insert(const_iterator pos, std::initializer_list<T> list)
      {
        return insert(pos, list.begin(), list.end());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = vlw_interface_properties().can_resize_fully(),
                                                               class... Args,
                                                               class disabler = std::enable_if_t < Utility::always_true<Args...> &&
                                                                                valid >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr iterator
                                                                                  emplace(const_iterator pos, Args &&... args)
      {
        return insert(pos, WriteProxy(std::forward<Args>(args)...));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION

      ///@p first and @p last must be a valid iterator into the vector, @p last may also be its end iterator,
      ///or @c first == last, in which case no operation is done and @c end() is returned.
      template <bool valid = vlw_interface_properties().can_resize_fully(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr iterator erase(const const_iterator first, const const_iterator last)
      {
        const const_iterator real_last = last;

        if (real_last <= first)
          {
            return this->end() - 1;
          }

        const size_type count = static_cast<size_type>(real_last - first);

        const size_type start_index = static_cast<size_type>(first - this->cbegin());

  #if MARIONETTE_USE_ASSERTIONS
        assert(count >= 0 && count <= this->size());
        assert(start_index >= 0 && start_index <= this->size());
  #endif

        if (count > 0)
          {
            move_objects(this->begin() + static_cast<difference_type>(start_index), first + static_cast<difference_type>(count), this->cend());
            this->resize(this->size() - count);
          }

        return this->begin() + static_cast<difference_type>(start_index + count);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION

      ///@p pos must be a valid iterator into the vector.
      template <bool valid = vlw_interface_properties().can_resize_fully(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr iterator erase(const const_iterator pos)
      {
        return this->erase(pos, pos + 1);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_resize_fully(),
                                                               class T        = WriteProxy,
                                                               class disabler = std::enable_if_t < std::is_assignable_v<WriteProxy &, T> &&
                                                                                valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
                                                                                         push_back(T && value)
      {
        if constexpr (std::is_same_v<T, ReadProxy> || std::is_same_v<T, ConstReadProxy>)
          {
            if constexpr (impl::vlw_can_get_proxied_pointer<T>())
              {
                if (value.get_proxied_pointer() == impl::vlw_get_hopefully_owning_pointer(this))
                  {
                    WriteProxy wp = std::forward<T>(value);
                    this->push_back(wp);
                    return;
                  }
              }
            else
              {
                WriteProxy wp = std::forward<T>(value);
                this->push_back(wp);
                return;
              }
          }

        const size_type old_size = this->size();
        this->resize(old_size + 1);
        (*this)[old_size] = value;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = vlw_interface_properties().can_resize_fully(),
                                                               class... Args,
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ReadProxy emplace_back(Args &&... args)
      {
        push_back(WriteProxy(std::forward<Args>(args)...));
        return back();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION

      ///@warning Undefined behaviour if @c pop_back is used when @c size is 0.
      template <bool valid = vlw_interface_properties().can_resize_fully(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void pop_back()
      {
  #if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
  #endif

        const size_type old_size = this->size();
        this->resize(old_size - 1);
        //UB if pop_back on size == 0
      }

      using VectorLikeBase::resize;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = vlw_interface_properties().can_resize_fully(),
                                                               class T        = WriteProxy,
                                                               class disabler = std::enable_if_t < std::is_assignable_v<WriteProxy &, T> &&
                                                                                valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
                                                                                         resize(const size_type new_size, const T & value)
      {
        if constexpr (std::is_same_v<T, ReadProxy> || std::is_same_v<T, ConstReadProxy>)
          {
            if constexpr (impl::vlw_can_get_proxied_pointer<T>())
              {
                if (value.get_proxied_pointer() == impl::vlw_get_hopefully_owning_pointer(this))
                  {
                    WriteProxy wp = value;
                    this->resize(new_size, wp);
                    return;
                  }
              }
            else
              {
                WriteProxy wp = value;
                this->resize(new_size, wp);
                return;
              }
          }

        const size_type old_size = this->size();
        this->resize(new_size);
        for (size_type i = old_size; i < new_size; ++i)
          {
            (*this)[i] = value;
          }
      }

      //No good way to implement swap,
      //may write it if needed,
      //but it's probably best to use the base object
      //if it can benefit from that.

      //Also no implementation of comparison operators,
      //too many varieties to consider,
      //it's best to write something specific
      //if and when necessary.

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts, class disabler = std::enable_if_t<std::is_constructible_v<VectorLikeBase, Ts...>>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr VectorLikeWrapper(Ts &&... ts): VectorLikeBase(std::forward<Ts>(ts)...)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class disabler = std::enable_if_t<std::is_assignable_v<VectorLikeBase &, T>>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr VectorLikeWrapper & operator=(T && t)
      {
        VectorLikeBase::operator=(std::forward<T>(t));
        return *this;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class disabler = std::enable_if_t<std::is_convertible_v<VectorLikeBase, T>>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr explicit operator T() const
      {
        return static_cast<T>(static_cast<const VectorLikeBase &>(*this));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class VecLikeT,
        class SFINAE_helper = std::is_convertible<VectorLikeBase, VecLikeT>,
        class disabler      = std::enable_if_t < vlw_interface_properties().can_copy_out_of_collection() &&
                         std::is_constructible_v<typename VecLikeT::value_type, ConstReadProxy> && !SFINAE_helper::value &&
                         !std::is_same_v<std::initializer_list<typename VecLikeT::value_type>, VecLikeT> >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr explicit operator VecLikeT() const
      {
  #if MARIONETTE_WARN_ON_ELEMENTWISE_VECTOR_OPERATIONS
        impl::elementwise_operations_warner<Utility::always_true<VecLikeT>>::warn_elementwise();
  #endif

        return VecLikeT {this->cbegin(), this->cend()};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T        = WriteProxy,
                                                               class disabler = std::enable_if_t<std::is_constructible_v<T, ConstReadProxy>>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr explicit operator std::initializer_list<T>() const
      {
  #if MARIONETTE_WARN_ON_ELEMENTWISE_VECTOR_OPERATIONS
        impl::elementwise_operations_warner<Utility::always_true<T>>::warn_elementwise();
  #endif
        return std::initializer_list<T>(this->cbegin(), this->cend());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        bool valid     = vlw_interface_properties().can_resize_fully(),
        class disabler = std::enable_if_t < valid && !std::is_constructible_v<VectorLikeBase, size_type> >>
                                                       MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr VectorLikeWrapper(const size_type count)
      {
        this->resize(count);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class T        = WriteProxy,
        class disabler = std::enable_if_t < std::is_assignable_v<ReadProxy, T> && vlw_interface_properties().can_resize_fully() &&
                         !std::is_constructible_v<VectorLikeBase, const size_type, const T &> >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr VectorLikeWrapper(const size_type count, const T & value)
      {
  #if MARIONETTE_WARN_ON_ELEMENTWISE_VECTOR_OPERATIONS
        impl::elementwise_operations_warner<Utility::always_true<T>>::warn_elementwise();
  #endif
        this->resize(count);
        for (size_type i = 0; i < count; ++i)
          {
            (*this)[i] = value;
          }
      }

  #if MARIONETTE_PROVIDE_STANDARD_VECTOR_CONSTRUCTORS_AND_ASSIGNMENT
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class It,
        class disabler = std::enable_if_t < !std::is_constructible_v<VectorLikeBase, const It, const It> &&
                         std::is_assignable_v<ReadProxy, decltype(*std::declval<It>())> &&
                         vlw_interface_properties().can_resize_fully() >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr VectorLikeWrapper(const It first, const It last)
      {
    #if MARIONETTE_WARN_ON_ELEMENTWISE_VECTOR_OPERATIONS
        impl::elementwise_operations_warner<Utility::always_true<It>>::warn_elementwise();
    #endif

        this->assign(first, last);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class T        = WriteProxy,
        class disabler = std::enable_if_t < !std::is_constructible_v<VectorLikeBase, std::initializer_list<T>> && std::is_assignable_v<ReadProxy, T> &&
                         vlw_interface_properties().can_resize_fully() >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES VectorLikeWrapper(std::initializer_list<T> list):
        VectorLikeWrapper(list.begin(), list.end())
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class VecLikeT,
        class SFINAE_helper = std::is_constructible<VectorLikeBase, VecLikeT>,
        class disabler      = std::enable_if_t < std::is_assignable_v<ReadProxy, decltype(*(std::declval<VecLikeT>().begin()))> && !SFINAE_helper::value &&
                         vlw_interface_properties().can_resize_fully() >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr VectorLikeWrapper(const VecLikeT & vl):
        VectorLikeWrapper(vl.begin(), vl.end())
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class VecLikeT,
        class disabler = std::enable_if_t < std::is_assignable_v<ReadProxy, decltype(*(std::declval<VecLikeT>().begin()))> &&
                         !std::is_assignable_v<VectorLikeBase &, VecLikeT> &&
                         vlw_interface_properties().can_refer_to_modifiable_object() >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr VectorLikeWrapper & operator=(const VecLikeT & vl)
      {
    #if MARIONETTE_WARN_ON_ELEMENTWISE_VECTOR_OPERATIONS
        impl::elementwise_operations_warner<Utility::always_true<VecLikeT>>::warn_elementwise();
    #endif
        if constexpr (std::is_same_v<VectorLikeWrapper, VecLikeT>)
          {
            if (&vl == this)
              {
                return *this;
              }
          }
        this->assign(vl.begin(), vl.end());
        return *this;
      }
  #endif

  #if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const VectorLikeBase & cast_to_base() const
      {
        return (*this);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr VectorLikeBase & cast_to_base()
      {
        return (*this);
      }

  #endif
    };
  }
}
#endif

#endif
