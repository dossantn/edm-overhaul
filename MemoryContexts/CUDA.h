//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MEMORYCONTEXTS_CUDA_H
#define MARIONETTE_MEMORYCONTEXTS_CUDA_H

#include <utility>
#include <type_traits>
#include <cstdlib>

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

#if MARIONETTE_ENABLE_PLATFORM_SUPPORT_FOR_CUDA

  #ifndef MARIONETTE_CUDA_CLANG_WARNING
    #define MARIONETTE_CUDA_CLANG_WARNING 1
  #endif

  #include <cuda_runtime_api.h>

  #if defined(__clang__) && defined(__CUDA__) && defined(_MSC_VER)

    //Clang compiling for the host on Windows with assertions on
    //gives some problems given the single-pass compilation:
    //it will ultimately try to use _wassert, which is only
    //defined for the host (obviously).
    //So we provide a device version here.
    //UB to explicitly use a reserved identifier,
    //but not a problem since this will not get executed...

    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wreserved-identifier"

MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
template <class CharT1, class CharT2, class numT>
__device__ inline void _wassert(const CharT1 * message, const CharT2 * file, const numT line)
{
  constexpr unsigned int buffer_size = 1024;
  unsigned char          real_message[buffer_size], real_file[buffer_size];

  for (unsigned int i = 0; i < buffer_size && *message; ++i, ++message)
    {
      real_message[i] = static_cast<unsigned char>(*message);
    }

  for (unsigned int i = 0; i < buffer_size && *file; ++i, ++file)
    {
      real_file[i] = static_cast<unsigned char>(*file);
    }

  //Yeah, this is the crudest form of conversion to single char.
  //Blame Microsoft for not being UTF-8.
  //We could do something more elaborate,
  //but since this is a bugfix of a bug that shouldn't even occur... yeah.

  printf("Assertion failed: %s, file %s, line %d\n", real_message, real_file, line);
}

    #pragma clang diagnostic pop

  #endif

namespace Marionette
{
  namespace MemoryContexts
  {

    namespace impl_CUDA
    {
      ///@brief Provides a way to report errors in CUDA kernels.
      ///
      ///@remark Standard CUDA definition that can be found almost everywhere...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION __host__ __device__ inline void
      CUDA_error_check_assert(cudaError_t code, const char * file, int line, bool abort = true)
      {
        if (code != cudaSuccess)
          {
            printf("GPU Error: %s (%s %d)\n", cudaGetErrorString(code), file, line);
            if (abort)
              {
                if constexpr (static_cast<PropertiesIndexer>(get_current_execution_context()) == static_cast<PropertiesIndexer>(ExecutionContext::CUDA_GPU))
                  {
  #if defined(__CUDA_ARCH__) && (__CUDA_ARCH__ > 0)
                    asm("trap;");
  #else
                    assert(code == cudaSuccess);
  #endif
                  }
                else
                  {
                    std::exit(code);
                    //Throwing might make some sense
                    //since it guarantees stack unwinding,
                    //but the problem is that some CUDA errors
                    //are sticky and will keep on happening
                    //e. g. on free, leading pretty quickly
                    //to a std::terminate that will be even
                    //less graceful about tearing everything down...
                  }
              }
          }
      }

  ///@brief Wraps `CUDA_error_check_assert` using `__FILE__` and `__LINE__`
  ///       to improve error reporting (and debugging) capabilities.
  ///       One can provide an additional argument to determine
  ///       whether or not to exit on error, with @c true being
  ///       the default.
  ///
  ///@remark Yes, it's indeed a macro with a default argument. I can explain.
  #define MARIONETTE_CUDA_ERROR_CHECK(...) MARIONETTE_CUDA_ERROR_CHECK_HELPER(__VA_ARGS__, true)

  #define MARIONETTE_CUDA_ERROR_CHECK_GET_FIRST(x, ...) x

  #define MARIONETTE_CUDA_ERROR_CHECK_HELPER(ans, ...)                                                                                                         \
    do                                                                                                                                                         \
      {                                                                                                                                                        \
        ::Marionette::MemoryContexts::impl_CUDA::CUDA_error_check_assert((ans), __FILE__, __LINE__, MARIONETTE_CUDA_ERROR_CHECK_GET_FIRST(__VA_ARGS__, true)); \
      }                                                                                                                                                        \
    while (0)

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool with_stream, bool with_device>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextInfo;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextInfo<false, false>
      {
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextInfo<true, false>
      {
        cudaStream_t stream = nullptr;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextInfo<false, true>
      {
        int device;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
  #if !defined(_MSC_VER) || defined(__clang__)
        constexpr
  #endif
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
          ContextInfo():
          device(0)
        {
          if constexpr (get_current_execution_context() == ExecutionContext::CPU)
            {
              MARIONETTE_CUDA_ERROR_CHECK(cudaGetDevice(&device));
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ContextInfo(int dev): device(dev)
        {
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextInfo<true, true>
      {
        cudaStream_t stream;
        int          device;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
  #if !defined(_MSC_VER) || defined(__clang__)
        constexpr
  #endif
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
          ContextInfo():
          stream(nullptr),
          device(0)
        {
          if constexpr (get_current_execution_context() == ExecutionContext::CPU)
            {
              MARIONETTE_CUDA_ERROR_CHECK(cudaGetDevice(&device));
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
  #if !defined(_MSC_VER) || defined(__clang__)
        constexpr
  #endif
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
          ContextInfo(cudaStream_t st):
          stream(st),
          device(0)
        {
          if constexpr (get_current_execution_context() == ExecutionContext::CPU)
            {
              MARIONETTE_CUDA_ERROR_CHECK(cudaGetDevice(&device));
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ContextInfo(cudaStream_t st, int dev):
          stream(st),
          device(dev)
        {
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool actually_restore>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DeviceRestorer
      {
        int prev_device;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
  #if !defined(_MSC_VER) || defined(__clang__)
        constexpr
  #endif
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
          DeviceRestorer()
        {
          if constexpr (actually_restore && get_current_execution_context() == ExecutionContext::CPU)
            {
              MARIONETTE_CUDA_ERROR_CHECK(cudaGetDevice(&prev_device));
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
  #if __cplusplus >= 202002L
        constexpr
            //C++20 added constexpr destructors,
            //invalid before...
  #endif
          ~DeviceRestorer()
        {
          if constexpr (actually_restore && get_current_execution_context() == ExecutionContext::CPU)
            {
              MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(prev_device));
            }
        }
      };

  #if defined(__clang__)
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wunsafe-buffer-usage"
  #endif
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void device_memset(T * ptr, const int memset_value, const size_type size)
      {
        T setter;

        memset(&setter, memset_value, sizeof(T));

        for (size_type i = 0; i < size; ++i)
          {
            new (ptr + i) T(setter);
            //Like in generic_same_context_memcopy,
            //ensure the lifetime is properly started.
          }
      }

  #if defined(__clang__)
    #pragma clang diagnostic pop
  #endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types allocation_type, class size_type, class T, class... Extra>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
      memset_helper(T * ptr, const int memset_value, const size_type pre_size, Extra &&... extra)
      {
        static_assert(sizeof...(Extra) <= 1, "memset_helper supports at most one extra argument for the stream.");

        const std::make_unsigned_t<size_type> size = static_cast<std::make_unsigned_t<size_type>>(pre_size);

        if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::CPU_pinned ||
                      allocation_type == impl_CUDA::CUDA_allocation_types::CPU_pinned_write_combined)
          {
            static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>,
                          "CPU memory only memsettable from the CPU...");

            std::memset(ptr, memset_value, size * sizeof(T));
          }
        else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::GPU_device_dynamic)
          {
            static_assert(get_current_execution_context() == ExecutionContext::CUDA_GPU || Utility::always_false<size_type>,
                          "GPU memory allocated on the device is not memsettable except locally...");

            device_memset(ptr, memset_value, size);
          }
        else
          {
            constexpr ExecutionContext exe_context = get_current_execution_context();

            if constexpr (exe_context == ExecutionContext::CPU)
              {
                if constexpr (sizeof...(Extra) > 0)
                  {
                    MARIONETTE_CUDA_ERROR_CHECK(cudaMemsetAsync(ptr, memset_value, size * sizeof(T), std::forward<Extra>(extra)...));
                  }
                else
                  {
                    MARIONETTE_CUDA_ERROR_CHECK(cudaMemset(ptr, memset_value, size * sizeof(T)));
                  }
              }
            else if constexpr (exe_context == ExecutionContext::CUDA_GPU)
              {
                device_memset(ptr, memset_value, size);
              }
            else
              {
                static_assert(Utility::always_false<T>, "Invalid context to be operating on CUDA GPU memory...");
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool
      is_CPU_allocation(const CUDA_allocation_types at)
      {
        return (at == CUDA_allocation_types::CPU_pinned || at == CUDA_allocation_types::CPU_pinned_write_combined);
      }

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <impl_CUDA::CUDA_allocation_types allocation_type, bool stores_stream, bool stores_device>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CUDAMemoryContext
    {
      using ContextInfo = impl_CUDA::ContextInfo<stores_stream, stores_device>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool
      context_infos_compatible(const ContextInfo & c1, const ContextInfo & c2)
      {
        if constexpr (stores_device && allocation_type != impl_CUDA::CUDA_allocation_types::managed)
          {
            if (c1.device != c2.device)
              {
                return false;
              }
          }
        if constexpr (get_current_execution_context() != ExecutionContext::CPU && stores_stream && allocation_type == impl_CUDA::CUDA_allocation_types::managed)
          {
            if (c1.stream != c2.stream)
              {
                return false;
              }
          }

        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static
  #if __cplusplus >= 202002L
        constexpr
  #endif
        void
        memset(const ContextInfo & ci, T * ptr, const int memset_value, const size_type size)
      {
        [[maybe_unused]] impl_CUDA::DeviceRestorer<stores_device> dr;

        if constexpr (stores_device && get_current_execution_context() == ExecutionContext::CPU)
          {
            MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(ci.device));
          }

        if constexpr (stores_stream)
          {
            impl_CUDA::memset_helper<allocation_type>(ptr, memset_value, size, ci.stream);
          }
        else
          {
            impl_CUDA::memset_helper<allocation_type>(ptr, memset_value, size);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class T, bool valid = !stores_stream, class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static
  #if __cplusplus >= 202002L
        constexpr
  #endif
        void
        memset(const ContextInfo & ci, T * ptr, const int memset_value, const size_type size, cudaStream_t stream)
      {
        [[maybe_unused]] impl_CUDA::DeviceRestorer<stores_device> dr;

        if constexpr (stores_device && get_current_execution_context() == ExecutionContext::CPU)
          {
            MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(ci.device));
          }

        impl_CUDA::memset_helper<allocation_type>(ptr, memset_value, size, stream);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class T, bool valid = !stores_device, class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static
  #if __cplusplus >= 202002L
        constexpr
  #endif
        void
        memset(const ContextInfo & ci, T * ptr, const int memset_value, const size_type size, int device)
      {
        [[maybe_unused]] impl_CUDA::DeviceRestorer<true> dr;

        if constexpr (get_current_execution_context() == ExecutionContext::CPU)
          {
            MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(device));
          }

        memset(ci, ptr, memset_value, size);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type,
                                                               class T,
                                                               bool valid     = !stores_stream && !stores_device,
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static
  #if __cplusplus >= 202002L
        constexpr
  #endif
        void
        memset(const ContextInfo & ci, T * ptr, const int memset_value, const size_type size, cudaStream_t stream, int device)
      {
        [[maybe_unused]] impl_CUDA::DeviceRestorer<true> dr;

        if constexpr (get_current_execution_context() == ExecutionContext::CPU)
          {
            MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(device));
          }

        impl_CUDA::memset_helper<allocation_type>(ptr, memset_value, size, stream);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static
  #if __cplusplus >= 202002L
        constexpr
  #endif
        void *
        allocate(const ContextInfo & ci, const size_type pre_size)
      {
        const std::make_unsigned_t<size_type> size = static_cast<std::make_unsigned_t<size_type>>(pre_size);

        [[maybe_unused]] impl_CUDA::DeviceRestorer<stores_device> dr;

        if constexpr (stores_device && get_current_execution_context() == ExecutionContext::CPU)
          {
            MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(ci.device));
          }

        void * ret = nullptr;

        if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::GPU_device_dynamic)
          {
            static_assert(get_current_execution_context() == ExecutionContext::CUDA_GPU || Utility::always_false<size_type>,
                          "Local GPU device memory is, as the name implies, allocated on the device.");

            ret = malloc(size);
          }
        else
          {
            static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>,
                          "CUDA memory allocations should happen on the host, never the device.");

            if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::GPU_normal)
              {
                if constexpr (stores_stream && CUDART_VERSION >= 11020)
                  //CUDA 11.2 or later is needed for asynchronous allocation.
                  {
                    MARIONETTE_CUDA_ERROR_CHECK(cudaMallocAsync(&ret, size, ci.stream));
                  }
                else
                  {
                    MARIONETTE_CUDA_ERROR_CHECK(cudaMalloc(&ret, size));
                  }
              }
            else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::CPU_pinned)
              {
                MARIONETTE_CUDA_ERROR_CHECK(cudaMallocHost(&ret, size));
              }
            else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::CPU_pinned_write_combined)
              {
                MARIONETTE_CUDA_ERROR_CHECK(cudaHostAlloc(&ret, size, cudaHostAllocWriteCombined));
              }
            else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::managed)
              {
                MARIONETTE_CUDA_ERROR_CHECK(cudaMallocManaged(&ret, size));
                if constexpr (stores_stream)
                  {
                    if (ci.stream != nullptr)
                      {
                        MARIONETTE_CUDA_ERROR_CHECK(cudaStreamAttachMemAsync(ci.stream, ret, size, cudaMemAttachSingle));
                      }
                  }
              }
            else
              {
                static_assert(Utility::always_false<size_type>, "allocation_type not (yet) supported.");
              }
          }
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static
  #if __cplusplus >= 202002L
        constexpr
  #endif
        void
        deallocate(const ContextInfo & ci, void * ptr, const size_type pre_size)
      {
        [[maybe_unused]] const std::make_unsigned_t<size_type> size = static_cast<std::make_unsigned_t<size_type>>(pre_size);

        [[maybe_unused]] impl_CUDA::DeviceRestorer<stores_device> dr;

        if constexpr (stores_device && get_current_execution_context() == ExecutionContext::CPU)
          {
            MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(ci.device));
          }

        if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::GPU_device_dynamic)
          {
            static_assert(get_current_execution_context() == ExecutionContext::CUDA_GPU || Utility::always_false<size_type>,
                          "Local GPU device memory is, as the name implies, deallocated on the device.");

            free(ptr);
          }
        else
          {
            if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::GPU_normal)
              {
                if constexpr (stores_stream && CUDART_VERSION >= 11020)
                  //CUDA 11.2 or later is needed for asynchronous allocation.
                  {
                    MARIONETTE_CUDA_ERROR_CHECK(cudaFreeAsync(ptr, ci.stream));
                  }
                else
                  {
                    MARIONETTE_CUDA_ERROR_CHECK(cudaFree(ptr));
                  }
              }
            else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::CPU_pinned)
              {
                MARIONETTE_CUDA_ERROR_CHECK(cudaFreeHost(ptr));
              }
            else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::CPU_pinned_write_combined)
              {
                MARIONETTE_CUDA_ERROR_CHECK(cudaFreeHost(ptr));
              }
            else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::managed)
              {
                MARIONETTE_CUDA_ERROR_CHECK(cudaFree(ptr));
              }
            else
              {
                static_assert(Utility::always_false<size_type>, "allocation_type not (yet) supported.");
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr AccessProperties access_properties()
      {
        if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::CPU_pinned ||
                      allocation_type == impl_CUDA::CUDA_allocation_types::CPU_pinned_write_combined)
          {
            if constexpr (get_current_execution_context() == ExecutionContext::CPU)
              {
                return AccessProperties::Full;
              }
            else
              {
                return AccessProperties::GetAddress;
              }
          }
        else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::GPU_normal)
          {
            if constexpr (get_current_execution_context() == ExecutionContext::CUDA_GPU)
              {
                return AccessProperties::Full;
              }
            else
              {
                return AccessProperties::GetAddress;
              }
          }
        else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::managed)
          {
            return AccessProperties::Full;
          }
        else if constexpr (allocation_type == impl_CUDA::CUDA_allocation_types::GPU_device_dynamic)
          {
            if constexpr (get_current_execution_context() == ExecutionContext::CUDA_GPU)
              {
                return AccessProperties::Full;
              }
            else
              {
                return AccessProperties::Invalid;
                //We should not even be able to instantiate this outside of GPU code...
              }
          }
        else
          {
            return AccessProperties::Invalid;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ExtraFunctions
      {
        ///@brief Atomically increase the tagged size of the final collection by @p increment.
        ///
        ///If the collection has an extent greater than 1 (if spans a property array),
        ///the previous tagged size of the last possible index is return.
        ///Otherwise, simply returns the previous
        ///
        ///@warning Use only in circumstances where the collection can indeed be grown like this.
        ///         If it is a collection that holds only global properties that do not track their
        ///         own sizes, you will end up resizing the original size...
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type,
                                                                 bool valid = Layout::interface_properties().can_resize_fully() &&
                                                                              get_current_execution_context() == ExecutionContext::CUDA_GPU &&
                                                                              allocation_type != impl_CUDA::CUDA_allocation_types::CPU_pinned &&
                                                                              allocation_type != impl_CUDA::CUDA_allocation_types::CPU_pinned_write_combined,
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr typename Layout::size_type atomic_grow_size(const typename Layout::size_type increment)
        {
          auto * dhis = static_cast<F *>(this);

          using SizeTag = std::decay_t<decltype(dhis->size_tag())>;

          constexpr PropertiesIndexer extent = dhis->extent();

          const PropertiesIndexer base_index = Utility::FriendshipProvider::get_multi_array_index(*dhis);

          if constexpr (extent == 1)
            {
              return atomicAdd(Utility::FriendshipProvider::template get_tagged_size_pointer<SizeTag>(*dhis, base_index), increment);
            }
          else
            {
              typename Layout::size_type ret;

              for (PropertiesIndexer i = 0; i < extent; ++i)
                {
                  ret = atomicAdd(Utility::FriendshipProvider::template get_tagged_size_pointer<SizeTag>(*dhis, base_index * extent + i), increment);
                }

              return ret;
            }
        }
      };
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at1,
                                                             impl_CUDA::CUDA_allocation_types at2,
                                                             bool                             s1,
                                                             bool                             s2,
                                                             bool                             d1,
                                                             bool                             d2,
                                                             std::enable_if_t<at1 != at2 || s1 != s2 || d1 != d2, bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool memory_contexts_potentially_compatible(const CUDAMemoryContext<at1, s1, d1> &,
                                                                                                                const CUDAMemoryContext<at2, s2, d2> &)
    {
      if constexpr (get_current_execution_context() != ExecutionContext::CPU)
        {
          if constexpr (at2 == impl_CUDA::CUDA_allocation_types::managed && s2 && (!s1 || at1 != impl_CUDA::CUDA_allocation_types::managed))
            {
              //Changing the managed memory attachment can only be done on the host side...
              return false;
            }
        }

      if constexpr (at1 == at2)
        {
          return true;
        }
      else if constexpr (at1 == impl_CUDA::CUDA_allocation_types::GPU_normal && at2 == impl_CUDA::CUDA_allocation_types::managed)
        //Treating managed memory as normal memory is possible,
        //but not the other way around.
        {
          return true;
        }
      else
        {
          //No compatibility between CPU_pinned and CPU_pinned_write_combined
          //given their different behaviours and performance characteristics
          //(though it would technically be possible to treat CPU_pinned_write_combined
          // as CPU_pinned, read performance could be different enough
          // that it might matter).
          return false;
        }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at1,
                                                             impl_CUDA::CUDA_allocation_types at2,
                                                             bool                             s1,
                                                             bool                             s2,
                                                             bool                             d1,
                                                             bool                             d2,
                                                             std::enable_if_t<at1 != at2 || s1 != s2 || d1 != d2, bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool
    memory_contexts_compatible(const CUDAMemoryContext<at1, s1, d1> &                       dealloc_context,
                               const CUDAMemoryContext<at2, s2, d2> &                       alloc_context,
                               const typename CUDAMemoryContext<at1, s1, d1>::ContextInfo & dealloc_info,
                               const typename CUDAMemoryContext<at2, s2, d2>::ContextInfo & alloc_info)
    {
      if constexpr (!memory_contexts_potentially_compatible(dealloc_context, alloc_context))
        {
          return false;
        }

      if constexpr (get_current_execution_context() != ExecutionContext::CPU)
        {
          if constexpr (at2 == impl_CUDA::CUDA_allocation_types::managed && s2)
            {
              if constexpr (at1 == impl_CUDA::CUDA_allocation_types::managed && s1)
                {
                  return dealloc_context.stream == alloc_context.stream;
                }
              else
                {
                  return false;
                  //This should be covered by memory_contexts_potentially_compatible,
                  //but stil...
                }
            }
        }

      if constexpr (d1 || d2)
        {
          int dev_from = -1, dev_to = -1;
          if constexpr (d1)
            {
              dev_from = alloc_info.device;
            }
          if constexpr (d2)
            {
              dev_to = dealloc_info.device;
            }
          if (dev_from != dev_to && at2 != impl_CUDA::CUDA_allocation_types::managed)
            {
              return false;
            }
        }
      return true;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at1,
                                                             impl_CUDA::CUDA_allocation_types at2,
                                                             bool                             s1,
                                                             bool                             s2,
                                                             bool                             d1,
                                                             bool                             d2,
                                                             class size_type,
                                                             class T,
                                                             std::enable_if_t<at1 != at2 || s1 != s2 || d1 != d2, bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
    move_with_context([[maybe_unused]] const CUDAMemoryContext<at1, s1, d1> &      dest,
                      [[maybe_unused]] const CUDAMemoryContext<at2, s2, d2> &      source,
                      const typename CUDAMemoryContext<at1, s1, d1>::ContextInfo & dest_info,
                      const typename CUDAMemoryContext<at2, s2, d2>::ContextInfo & source_info,
                      T *&                                                         ptr,
                      const size_type                                              size)
    {
  #if MARIONETTE_USE_ASSERTIONS
      assert(memory_contexts_compatible(dest, source, dest_info, source_info));
  #endif

      if constexpr (at2 == impl_CUDA::CUDA_allocation_types::managed)
        {
          if constexpr (at1 != impl_CUDA::CUDA_allocation_types::managed || !s1)
            {
              static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>,
                            "Changing the stream attachment of managed memory can only be done from the CPU!");

              MARIONETTE_CUDA_ERROR_CHECK(cudaStreamAttachMemAsync(nullptr, ptr, size, cudaMemAttachGlobal));
              //Detach the memory from any particular stream.
            }
          else if constexpr (s1)
            {
              if constexpr (get_current_execution_context() == ExecutionContext::CPU)
                {
                  cudaStream_t dest_str   = dest_info.stream;
                  cudaStream_t source_str = nullptr;

                  if constexpr (s2)
                    {
                      source_str = source_info.stream;
                    }

                  if (dest_str != source_str)
                    {
                      if (dest_str == nullptr)
                        {
                          MARIONETTE_CUDA_ERROR_CHECK(cudaStreamAttachMemAsync(nullptr, ptr, size, cudaMemAttachGlobal));
                        }
                      else
                        {
                          MARIONETTE_CUDA_ERROR_CHECK(cudaStreamAttachMemAsync(dest_str, ptr, size, cudaMemAttachSingle));
                        }
                    }
                }
              else
                {
                  if constexpr (s2)
                    {
  #if MARIONETTE_USE_ASSERTIONS
                      assert(dest_info.stream == source_info.stream);
  #endif
                    }
                  else
                    {
  #if MARIONETTE_USE_ASSERTIONS
                      assert(dest_info.stream == nullptr);
  #endif
                    }
                }
            }
        }
    }

    namespace impl_CUDA
    {

  #if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
    #ifdef _MSVC
      __pragma("nv_diag_suppress 20011")
    #else
      _Pragma("nv_diag_suppress 20011")
    #endif
  #endif
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at1,
                                                                 impl_CUDA::CUDA_allocation_types at2,
                                                                 bool                             s1,
                                                                 bool                             s2,
                                                                 bool                             d1,
                                                                 bool                             d2,
                                                                 class size_type,
                                                                 class T1,
                                                                 class T2,
                                                                 class... Extra>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void memcopy_helper_helper(
          const CUDAMemoryContext<at1, s1, d1> &,
          const CUDAMemoryContext<at2, s2, d2> &,
          const typename CUDAMemoryContext<at1, s1, d1>::ContextInfo & dest_info,
          const typename CUDAMemoryContext<at2, s2, d2>::ContextInfo & source_info,
          T1 *                                                         ptr_to,
          const T2 *                                                   ptr_from,
          const size_type                                              pre_size,
          Extra &&... extra)
      {
        const std::make_unsigned_t<size_type> size = static_cast<std::make_unsigned_t<size_type>>(pre_size);

        static_assert(sizeof...(Extra) <= 1, "memcopy_helper_helper supports at most one extra argument for the stream.");

        auto memcopy_function_caller = [&](const auto memcopy_flag) {
          if constexpr (sizeof...(Extra) > 0)
            {
              cudaMemcpyAsync(ptr_to, ptr_from, sizeof(T2) * size, memcopy_flag, std::forward<Extra>(extra)...);
            }
          else
            {
              cudaMemcpy(ptr_to, ptr_from, sizeof(T2) * size, memcopy_flag);
            }
        };

        if constexpr (at1 == CUDA_allocation_types::managed || at2 == CUDA_allocation_types::managed)
          {
            memcopy_function_caller(cudaMemcpyDefault);
          }
        else if constexpr (is_CPU_allocation(at1) && is_CPU_allocation(at2))
          {
            std::memcpy(ptr_to, ptr_from, sizeof(T2) * size);
          }
        else if constexpr (is_CPU_allocation(at1) && !is_CPU_allocation(at2))
          {
            memcopy_function_caller(cudaMemcpyDeviceToHost);
          }
        else if constexpr (!is_CPU_allocation(at1) && is_CPU_allocation(at2))
          {
            memcopy_function_caller(cudaMemcpyHostToDevice);
          }
        else if constexpr (!is_CPU_allocation(at1) && !is_CPU_allocation(at2))
          {
            int dest_dev = -1, source_dev = -1;

            if constexpr (d1)
              {
                dest_dev = dest_info.device;
              }
            if constexpr (d2)
              {
                source_dev = source_info.device;
              }

            if (dest_dev != source_dev)
              {
                if constexpr (sizeof...(Extra) > 0)
                  {
                    cudaMemcpyPeerAsync(ptr_to, dest_dev, ptr_from, source_dev, sizeof(T2) * size, std::forward<Extra>(extra)...);
                  }
                else
                  {
                    cudaMemcpyPeer(ptr_to, dest_dev, ptr_from, source_dev, sizeof(T2) * size);
                  }
              }
            else
              {
                memcopy_function_caller(cudaMemcpyDeviceToDevice);
              }
          }
        else
          {
            static_assert(Utility::always_false<T2>, "LOGIC ERROR.");
          }
      }

  #if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
    #ifdef _MSVC
      __pragma("nv_diag_default 20011")
    #else
      _Pragma("nv_diag_default 20011")
    #endif
  #endif

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at1,
                                                                 impl_CUDA::CUDA_allocation_types at2,
                                                                 bool                             s1,
                                                                 bool                             s2,
                                                                 bool                             d1,
                                                                 bool                             d2,
                                                                 class size_type,
                                                                 class T1,
                                                                 class T2>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
  #if __cplusplus >= 202002L
        constexpr
  #endif
        void memcopy_helper(const CUDAMemoryContext<at1, s1, d1> &                       dest,
                            const CUDAMemoryContext<at2, s2, d2> &                       source,
                            const typename CUDAMemoryContext<at1, s1, d1>::ContextInfo & dest_info,
                            const typename CUDAMemoryContext<at2, s2, d2>::ContextInfo & source_info,
                            T1 *                                                         ptr_to,
                            const T2 *                                                   ptr_from,
                            const size_type                                              size)
      {
        static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>);

        [[maybe_unused]] impl_CUDA::DeviceRestorer<d1> dr;

        if constexpr (d1)
          {
            MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(dest_info.device));
          }

        if constexpr (s1)
          {
            memcopy_helper_helper(dest, source, dest_info, source_info, ptr_to, ptr_from, size, dest_info.stream);
          }
        else
          {
            memcopy_helper_helper(dest, source, dest_info, source_info, ptr_to, ptr_from, size);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at1,
                                                               impl_CUDA::CUDA_allocation_types at2,
                                                               bool                             d1,
                                                               bool                             s2,
                                                               bool                             d2,
                                                               class size_type,
                                                               class T1,
                                                               class T2,
                                                               class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
  #if __cplusplus >= 202002L
      constexpr
  #endif
        void
        memcopy_helper(const CUDAMemoryContext<at1, false, d1> &                       dest,
                       const CUDAMemoryContext<at2, s2, d2> &                          source,
                       const typename CUDAMemoryContext<at1, false, d1>::ContextInfo & dest_info,
                       const typename CUDAMemoryContext<at2, s2, d2>::ContextInfo &    source_info,
                       T1 *                                                            ptr_to,
                       const T2 *                                                      ptr_from,
                       const size_type                                                 size,
                       cudaStream_t                                                    stream)
      {
        static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>);

        [[maybe_unused]] impl_CUDA::DeviceRestorer<d1> dr;

        if constexpr (d1)
          {
            MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(dest_info.device));
          }

        memcopy_helper_helper(dest, source, dest_info, source_info, ptr_to, ptr_from, size, stream);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at1,
                                                               impl_CUDA::CUDA_allocation_types at2,
                                                               bool                             s1,
                                                               bool                             s2,
                                                               bool                             d2,
                                                               class size_type,
                                                               class T1,
                                                               class T2,
                                                               class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
  #if __cplusplus >= 202002L
      constexpr
  #endif
        void
        memcopy_helper(const CUDAMemoryContext<at1, s1, false> &                       dest,
                       const CUDAMemoryContext<at2, s2, d2> &                          source,
                       const typename CUDAMemoryContext<at1, s1, false>::ContextInfo & dest_info,
                       const typename CUDAMemoryContext<at2, s2, d2>::ContextInfo &    source_info,
                       T1 *                                                            ptr_to,
                       const T2 *                                                      ptr_from,
                       const size_type                                                 size,
                       int                                                             device)
      {
        static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>);

        [[maybe_unused]] impl_CUDA::DeviceRestorer<true> dr;

        MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(device));

        memcopy_helper(dest, source, dest_info, source_info, ptr_to, ptr_from, size);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at1,
                                                               impl_CUDA::CUDA_allocation_types at2,
                                                               bool                             s2,
                                                               bool                             d2,
                                                               class size_type,
                                                               class T1,
                                                               class T2,
                                                               class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
  #if __cplusplus >= 202002L
      constexpr
  #endif
        void
        memcopy_helper(const CUDAMemoryContext<at1, false, false> &                       dest,
                       const CUDAMemoryContext<at2, s2, d2> &                             source,
                       const typename CUDAMemoryContext<at1, false, false>::ContextInfo & dest_info,
                       const typename CUDAMemoryContext<at2, s2, d2>::ContextInfo &       source_info,
                       T1 *                                                               ptr_to,
                       const T2 *                                                         ptr_from,
                       const size_type                                                    size,
                       cudaStream_t                                                       stream,
                       int                                                                device)
      {
        static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>);

        [[maybe_unused]] impl_CUDA::DeviceRestorer<true> dr;

        MARIONETTE_CUDA_ERROR_CHECK(cudaSetDevice(device));

        memcopy_helper_helper(dest, source, dest_info, source_info, ptr_to, ptr_from, size, stream);
      }

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at1,
                                                             impl_CUDA::CUDA_allocation_types at2,
                                                             bool                             s1,
                                                             bool                             s2,
                                                             bool                             d1,
                                                             bool                             d2,
                                                             class size_type,
                                                             class T1,
                                                             class T2,
                                                             class... Args,
                                                             std::enable_if_t<at1 != at2 || s1 != s2 || d1 != d2, bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
    memcopy_with_context(const CUDAMemoryContext<at1, s1, d1> &                       dest,
                         const CUDAMemoryContext<at2, s2, d2> &                       source,
                         const typename CUDAMemoryContext<at1, s1, d1>::ContextInfo & dest_info,
                         const typename CUDAMemoryContext<at2, s2, d2>::ContextInfo & source_info,
                         T1 *                                                         ptr_to,
                         const T2 *                                                   ptr_from,
                         const size_type                                              size,
                         Args &&... args)
    {
      static_assert(std::is_trivially_destructible_v<T1> && std::is_trivially_copyable_v<T2>);

      if constexpr (get_current_execution_context() == ExecutionContext::CUDA_GPU)
        {
          static_assert((!impl_CUDA::is_CPU_allocation(at1) && !impl_CUDA::is_CPU_allocation(at1)) ||
                          (get_current_execution_context() == ExecutionContext::CPU),
                        "CPU-allocated memory cannot be operated with in the GPU.");
          Helpers::generic_same_context_memcopy(ptr_to, ptr_from, size);
        }
      else if constexpr (get_current_execution_context() == ExecutionContext::CPU)
        {
          static_assert((at1 != impl_CUDA::CUDA_allocation_types::GPU_device_dynamic && at2 != impl_CUDA::CUDA_allocation_types::GPU_device_dynamic) ||
                          (get_current_execution_context() == ExecutionContext::CUDA_GPU),
                        "Device local memory can only be memcopied from within the device...");

          impl_CUDA::memcopy_helper(dest, source, dest_info, source_info, ptr_to, ptr_from, size, std::forward<Args>(args)...);
        }
      else
        {
          static_assert(Utility::always_false<T1>, "Invalid execution context for copying CUDA-related memory...");
        }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at,
                                                             bool                             s,
                                                             bool                             d,
                                                             class size_type,
                                                             class T1,
                                                             class T2,
                                                             class... Args>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
    memcopy_with_context(const CUDAMemoryContext<at, s, d> & dest,
                         const StandardCPU &,
                         const typename CUDAMemoryContext<at, s, d>::ContextInfo & dest_info,
                         const StandardCPU::ContextInfo &,
                         T1 *            ptr_to,
                         const T2 *      ptr_from,
                         const size_type size,
                         Args &&... args)
    {
      static_assert(std::is_trivially_destructible_v<T1> && std::is_trivially_copyable_v<T2>);

      static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>,
                    "Invalid execution context for copying CUDA-related memory...");

      using FakeMemoryContext = CUDAMemoryContext<impl_CUDA::CUDA_allocation_types::CPU_pinned, false, false>;

      impl_CUDA::memcopy_helper(dest, FakeMemoryContext {}, dest_info, FakeMemoryContext::ContextInfo {}, ptr_to, ptr_from, size, std::forward<Args>(args)...);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at,
                                                             bool                             s,
                                                             bool                             d,
                                                             class size_type,
                                                             class T1,
                                                             class T2,
                                                             class... Args>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
    memcopy_with_context(const StandardCPU &,
                         const CUDAMemoryContext<at, s, d> & source,
                         const StandardCPU::ContextInfo &,
                         const typename CUDAMemoryContext<at, s, d>::ContextInfo & source_info,
                         T1 *                                                      ptr_to,
                         const T2 *                                                ptr_from,
                         const size_type                                           size,
                         Args &&... args)
    {
      static_assert(std::is_trivially_destructible_v<T1> && std::is_trivially_copyable_v<T2>);

      static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>,
                    "Invalid execution context for copying CUDA-related memory...");

      using FakeMemoryContext = CUDAMemoryContext<impl_CUDA::CUDA_allocation_types::CPU_pinned, false, false>;

      impl_CUDA::memcopy_helper(FakeMemoryContext {},
                                source,
                                FakeMemoryContext::ContextInfo {},
                                source_info,
                                ptr_to,
                                ptr_from,
                                size,
                                std::forward<Args>(args)...);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at,
                                                             bool                             s,
                                                             bool                             d,
                                                             class OtherContext,
                                                             class... Args,
                                                             std::enable_if_t<impl_CUDA::is_CPU_allocation(at), bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void memcopy_with_context(const CUDAMemoryContext<at, s, d> &,
                                                                                              const OtherContext & source,
                                                                                              const typename CUDAMemoryContext<at, s, d>::ContextInfo &,
                                                                                              const typename OtherContext::ContextInfo & source_info,
                                                                                              Args &&... args)
    {
      memcopy_with_context(StandardCPU {}, source, StandardCPU::ContextInfo {}, source_info, std::forward<Args>(args)...);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <impl_CUDA::CUDA_allocation_types at,
                                                             bool                             s,
                                                             bool                             d,
                                                             class OtherContext,
                                                             class... Args,
                                                             std::enable_if_t<impl_CUDA::is_CPU_allocation(at), bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void memcopy_with_context(const OtherContext & dest,
                                                                                              const CUDAMemoryContext<at, s, d> &,
                                                                                              const typename OtherContext::ContextInfo & dest_info,
                                                                                              const typename CUDAMemoryContext<at, s, d>::ContextInfo &,
                                                                                              Args &&... args)
    {
      memcopy_with_context(dest, StandardCPU {}, dest_info, StandardCPU::ContextInfo {}, std::forward<Args>(args)...);
    }
  }
}

#endif

#endif
