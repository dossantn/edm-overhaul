//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MEMORYCONTEXTS_CPU_H
#define MARIONETTE_MEMORYCONTEXTS_CPU_H

#include <utility>
#include <type_traits>
#include <cstring>
//For memset
#include <cstdlib>
//For malloc/free
#include <memory>
#include <vector>

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

namespace Marionette
{
  namespace MemoryContexts
  {
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES StandardCPU
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextInfo
      {
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool
      context_infos_compatible(const ContextInfo &, const ContextInfo &)
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      memset(const ContextInfo &, T * ptr, const int memset_value, const size_type size)
      {
        static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>,
                      "Impossible to memset CPU memory from another execution context.");
        using std::memset;
        memset(ptr, memset_value, static_cast<std::make_unsigned_t<size_type>>(size) * sizeof(T));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void * allocate(const ContextInfo &, const size_type size)
      {
        static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>,
                      "Impossible to allocate CPU memory from another execution context.");
        using std::malloc;
        return malloc(static_cast<std::make_unsigned_t<size_type>>(size));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void deallocate(const ContextInfo &, void * ptr, const size_type)
      {
        static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>,
                      "Impossible to free CPU memory from another execution context.");
        using std::free;
        free(ptr);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr AccessProperties access_properties()
      {
        if constexpr (get_current_execution_context() == ExecutionContext::CPU)
          {
            return AccessProperties::Full;
          }
        else
          {
            return AccessProperties::GetAddress;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ExtraFunctions
      {
      };
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class T1, class T2>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void memcopy_with_context(const StandardCPU &,
                                                                                              const StandardCPU &,
                                                                                              const StandardCPU::ContextInfo &,
                                                                                              const StandardCPU::ContextInfo &,
                                                                                              T1 *            ptr_to,
                                                                                              const T2 *      ptr_from,
                                                                                              const size_type size)
    {
      static_assert(std::is_trivially_destructible_v<T1> && std::is_trivially_copyable_v<T2>);

      static_assert(get_current_execution_context() == ExecutionContext::CPU || Utility::always_false<size_type>,
                    "CPU to CPU memcopies can only be done from the CPU...");

      std::memcpy(ptr_to, ptr_from, static_cast<std::make_unsigned_t<size_type>>(size) * sizeof(T2));
      //In principle, T1 and T2 are the same.
      //We use the second type's size to be safe,
      //because we are assuming the mempcy size
      //to have been calculated to stay within bounds.
    }
  }
}

#endif
