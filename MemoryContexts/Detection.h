//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MEMORYCONTEXTS_DETECTION_H
#define MARIONETTE_MEMORYCONTEXTS_DETECTION_H

#include <utility>
#include <type_traits>

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

namespace Marionette
{
  namespace MemoryContexts
  {
#if MARIONETTE_ENABLE_PLATFORM_SUPPORT_FOR_CUDA

  #if (defined(__clang__) && defined(__CUDA__)) || defined(__CUDACC__)

    //Make the parts of NVCC and Clang that have to digest both host and device code
    //properly recognize both.
    //
    //Potential TO-DO: try and leverage recursive macros
    //                 to see if we can get them to survive
    //                 multiple rounds of preprocessing
    //                 to actually be able to fully distinguish
    //                 "preprocessing for CPU, GPU must be
    //                  syntactically valid but do not use GPU options"
    //                 from "preprocessing for GPU, CPU must be
    //                  syntactically valid but do not use CPU options"...

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool operator==(const ExecutionContext c1,
                                                                                                                                   const ExecutionContext c2)
    {
      const PropertiesIndexer p1 = static_cast<PropertiesIndexer>(c1);
      const PropertiesIndexer p2 = static_cast<PropertiesIndexer>(c2);

      const PropertiesIndexer cpu = static_cast<PropertiesIndexer>(ExecutionContext::CPU);
      const PropertiesIndexer gpu = static_cast<PropertiesIndexer>(ExecutionContext::CUDA_GPU);

      if ((p1 == cpu && p2 == gpu) || (p1 == gpu && p2 == cpu))
        {
          return true;
        }
      else
        {
          return p1 == p2;
        }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool operator!=(const ExecutionContext c1,
                                                                                                                                   const ExecutionContext c2)
    {
      return !(c1 == c2);
    }

  #endif

    namespace impl_CUDA
    {
  #ifdef __clang__
      __host__ std::false_type in_device_check()
      {
        return {};
      }

      __device__ std::true_type in_device_check()
      {
        return {};
      }
  #else
    #if !defined(__CUDA_ARCH__) || (__CUDA_ARCH__ == 0)
      __host__ std::false_type in_device_check()
      {
        return {};
      }
    #else
      __device__ std::true_type in_device_check()
      {
        return {};
      }
    #endif
  #endif
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Dummy = void>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool compiling_for_CUDA_device()
    {
      return decltype(impl_CUDA::in_device_check())::value;
    }
#else
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Dummy = void>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool compiling_for_CUDA_device()
    {
      return false;
    }
#endif

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ExecutionContext
    get_current_execution_context()
    {
      if constexpr (compiling_for_CUDA_device())
        {
          return ExecutionContext::CUDA_GPU;
        }
      else
        {
          return ExecutionContext::CPU;
        }
    }

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <ExecutionContext context>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CurrentExecutionContextGetter
      {
        using Context = Invalid;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CurrentExecutionContextGetter<ExecutionContext::CPU>
      {
        using Context = StandardCPU;
      };

#if MARIONETTE_ENABLE_PLATFORM_SUPPORT_FOR_CUDA
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CurrentExecutionContextGetter<ExecutionContext::CUDA_GPU>
      {
        using Context = CUDAInDevice;
      };
#endif
    }

    using Current = typename impl::CurrentExecutionContextGetter<get_current_execution_context()>::Context;
  }
}

#endif
