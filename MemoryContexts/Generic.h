//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MEMORYCONTEXTS_GENERIC_H
#define MARIONETTE_MEMORYCONTEXTS_GENERIC_H

#include <utility>
#include <type_traits>
#include <new>
//Placement new

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

namespace Marionette
{
  namespace MemoryContexts
  {

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class MemContext>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool memory_contexts_potentially_compatible(const MemContext &, const MemContext &)
    {
      return true;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class MemContext>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool
    memory_contexts_compatible(const MemContext &,
                               const MemContext &,
                               const typename MemContext::ContextInfo & alloc_info,
                               const typename MemContext::ContextInfo & dealloc_info)
    {
      return MemContext::context_infos_compatible(alloc_info, dealloc_info);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
      class MemContextDest,
      class MemContextSource,
      class size_type,
      class T,
      std::enable_if_t<memory_contexts_potentially_compatible(MemContextDest {}, MemContextSource {}), bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void move_with_context([[maybe_unused]] const MemContextDest &                 dest,
                                                                                           [[maybe_unused]] const MemContextSource &               source,
                                                                                           [[maybe_unused]] typename MemContextDest::ContextInfo & dest_info,
                                                                                           [[maybe_unused]]
                                                                                           typename MemContextSource::ContextInfo & source_info,
                                                                                           T *&,
                                                                                           const size_type)
    {
#if MARIONETTE_USE_ASSERTIONS
      assert(memory_contexts_compatible(dest, source, dest_info, source_info));
#endif

      return;
    }

    namespace Helpers
    {
#if defined(__clang__)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wunsafe-buffer-usage"
#endif

      //This general function is meant to cover cases
      //where we are copying between the same context
      //where memcopy isn't available or desirable
      //(such as in CUDA, where it does byte-by-byte copies
      // instead of copying in larger blocks...).

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type,
                                                               class T1,
                                                               class T2,
                                                               class disabler = std::enable_if_t<(sizeof(T2) % sizeof(T1) == 0 && sizeof(T2) >= sizeof(T1))>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
      generic_same_context_memcopy(T1 * MARIONETTE_RESTRICT_POINTER_QUALIFICATION       ptr_to,
                                   const T2 * MARIONETTE_RESTRICT_POINTER_QUALIFICATION ptr_from,
                                   const size_type                                      size)
      {
        static_assert(std::is_trivially_destructible_v<T1> && std::is_trivially_copyable_v<T2>);

        constexpr size_type s_increment = sizeof(T2) / sizeof(T1);

        for (size_type i = 0; i < size; ++i)
          {
            new (ptr_to) T2(*ptr_from);
            //We use placement new to ensure the object lifetime starts here, validly.
            //Since everything is trivially destructible and trivially copyable,
            //any sane compiler would not do anything different from this,
            //but technically obj_a = obj_b requires obj_a to have had its lifetime initiated...
            //
            //There's std::uninitialized_copy, but we run into potential portability issues
            //when we compile for CUDA, and I think the underlying implementation for trivial types
            //does essentially this from a quick perusal of the GCC and Clang standard library implementations.
            //(MSVC is a bit more obfuscated, but if I understand correctly it is similar, give or take.)
            //In short: nothing magical about std::uninitialized_copy in any of the big three,
            //it's precisely what we want to do here...

            ptr_to += s_increment;
            ++ptr_from;
          }
        //Funnily enough, it seems GCC optimizes this to a memmove,
        //while Clang fully understands it can memcopy given __restrict__.
        //This also happens for the straight-up loop with operator=,
        //which hints at it not being from any potential UB from placement new
        //but information getting lost somewhere along the optimization stages...
      }

#if defined(__clang__)
  #pragma clang diagnostic pop
#endif

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Context,
                                                             class size_type,
                                                             class T1,
                                                             class T2,
                                                             std::enable_if_t<(sizeof(T2) % sizeof(T1) == 0 && sizeof(T2) >= sizeof(T1)), bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void memcopy_with_context(const Context &,
                                                                                              const Context &,
                                                                                              const typename Context::ContextInfo &,
                                                                                              const typename Context::ContextInfo &,
                                                                                              T1 * MARIONETTE_RESTRICT_POINTER_QUALIFICATION       ptr_to,
                                                                                              const T2 * MARIONETTE_RESTRICT_POINTER_QUALIFICATION ptr_from,
                                                                                              const size_type                                      size)
    {
      static_assert(Context::access_properties() >= AccessProperties::GetContents,
                    "The general form of memcopy_with_context can only be called if one can actually access the memory...");

      Helpers::generic_same_context_memcopy(ptr_to, ptr_from, size);
    }
  }
}

#endif
