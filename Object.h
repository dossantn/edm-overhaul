//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_OBJECT_H
#define MARIONETTE_OBJECT_H

#include <utility>
#include <type_traits>
#include <memory>

#include "ObjectHelpers.h"
#include "MarionetteBase.h"

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

#if MARIONETTE_INCLUDE_POTENTIALLY_THROWING_FUNCTIONS
  #include <stdexcept>
#endif

namespace Marionette
{
  namespace Collections
  {

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class ObjectType, class T, class MetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES Object
    {
      static_assert(Utility::always_false<T>, "Please initialize the Object properly with a TypeHolder of properties!");
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class ObjType, class... Properties, class MetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES Object<ObjType, Utility::TypeHolder<Properties...>, MetaInfo> :
      public std::decay_t<decltype(impl::get_object_base_classes<Object<ObjType, Utility::TypeHolder<Properties...>, MetaInfo>,
                                                    ObjType,
                                                    Utility::TypeHolder<Properties...>,
                                                    MetaInfo>(
        impl::get_obtainable_object_properties<impl::object_is_write_proxy(ObjType {})>(Utility::TypeHolder<Properties...> {})))>
    {
      static_assert(sizeof...(Properties) > 0, "At least one property is required for the object to work...");
      static_assert(MetaInfo::extent > 0, "A positive extent is assumed throughout the code.");
      static_assert(ObjType::interface_properties().valid(), "The ObjectType must have valid memory to be instantiated!");

      friend struct Utility::FriendshipProvider;

      template <class A, class B, class C>
      friend struct OwningObject;

      template <class A, class B, bool c, bool d>
      friend struct ProxyObject;

      template <class A, class B, class C>
      friend struct Object;

      template <class A, class B, class C>
      friend struct CollectionHolder;

     public:

      using ObjectType = ObjType;

      using PropertyList = Utility::TypeHolder<Properties...>;

      static_assert(InterfaceDescription::check_validity(PropertyList {}), "Must be a valid set of properties!");

      using size_type       = typename ObjType::size_type;
      using difference_type = typename ObjType::difference_type;

     protected:

      using ObtainableProperties = std::decay_t<decltype(impl::get_obtainable_object_properties<impl::object_is_write_proxy(ObjType {})>(PropertyList {}))>;

      using Base = std::decay_t<decltype(impl::get_object_base_classes<Object, ObjType, PropertyList, MetaInfo>(ObtainableProperties {}))>;

      using TrueBase = typename ObjType::template object_holder<PropertyList>;

     public:

      //using Base::Base;

      ///@brief The number of arrays this object represents.
      ///       (For unspecified access into a PropertyArray.)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer extent()
      {
        return MetaInfo::extent;
      }

      ///@brief The tag from which the object will read its size.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto size_tag()
      {
        return typename MetaInfo::SizeTag {};
      }

      ///@brief The kind of interface one will provide for this object.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
      interface_properties()
      {
        return ObjType::interface_properties();
      }

      //--------------------------------------------------------------------------------------------------

     public:

      ///@brief Whether the object owns the data
      ///       or is simply a pointer (to an owning object
      ///       or to a collection that will own it).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool object_is_non_owning()
      {
        return impl::object_is_pointer_storage(ObjType {});
      }

      ///@brief Get the pointer through which this non-owning object proxies something else.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = object_is_non_owning(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_proxied_pointer() const
      {
        return TrueBase::get_proxied_pointer();
      }

      ///@brief Get the pointer through which this non-owning object proxies something else.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = object_is_non_owning(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_proxied_pointer()
      {
        return TrueBase::get_proxied_pointer();
      }

      ///@brief Get the index the object uses to proxy into a collection.
      ///
      ///Also returns 0 if it holds a pointer to an owning object.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = object_is_non_owning(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_proxied_index() const
      {
        return TrueBase::get_proxied_index();
      }

      ///@brief Get the index the object uses to proxy into a collection.
      ///
      ///Also returns 0 if it holds a pointer to an owning object.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = object_is_non_owning(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_proxied_index()
      {
        return TrueBase::get_proxied_index();
      }

      ///@brief Whether this object is an owning object
      ///       (or holds a pointer to one).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_write_proxy()
      {
        return impl::object_is_write_proxy(ObjType {});
      }

      ///@brief Returns the kind of pointer one can use to build proxies.
      ///       Equivalent in usage to `get_layout_holder_pointer` for collections.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      get_layout_or_object_pointer() const
      {
        if constexpr (object_is_non_owning())
          {
            return this->get_proxied_pointer();
          }
        else
          {
            return this;
          }
      }

      ///@brief Returns the kind of pointer one can use to build proxies.
      ///       Equivalent in usage to `get_layout_holder_pointer` for collections.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      get_layout_or_object_pointer()
      {
        if constexpr (object_is_non_owning())
          {
            return this->get_proxied_pointer();
          }
        else
          {
            return this;
          }
      }

      //--------------------------------------------------------------------------------------------------

     protected:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool
      object_stores_multi_array_index()
      {
        return impl::layout_has_multi_array_index(ObjType {});
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      get_multi_array_index() const
      {
        return TrueBase::get_multi_array_index();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array_index()
      {
        return TrueBase::get_multi_array_index();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
      update_multi_array_index(const PropertiesIndexer new_index)
      {
        return TrueBase::update_multi_array_index(new_index);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = ObjType::interface_properties().can_refer_to_const_object(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object(const PropertiesIndexer mult_arr_idx) const
      {
        return TrueBase::template get_object<Property>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = ObjType::interface_properties().can_refer_to_modifiable_object(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object(const PropertiesIndexer mult_arr_idx)
      {
        return TrueBase::template get_object<Property>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = ObjType::interface_properties().can_refer_to_const_object(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object_pointer(const PropertiesIndexer mult_arr_idx) const
      {
        return TrueBase::template get_object_pointer<Property>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = ObjType::interface_properties().can_refer_to_modifiable_object(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object_pointer(const PropertiesIndexer mult_arr_idx)
      {
        return TrueBase::template get_object_pointer<Property>(mult_arr_idx);
      }

      ///@brief For non-owning proxies into collections that hold a jagged vector,
      ///       create the appropriate view into a collection
      ///       of the jagged vector property.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               class OtherMetaInfo,
                                                               bool valid     = !is_write_proxy(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      build_proxy_jagged_vector_outer_collection(const PropertiesIndexer mult_arr_idx) const
      {
        return TrueBase::template build_proxy_jagged_vector_outer_collection<Property, OtherMetaInfo>(mult_arr_idx);
      }

      ///@brief For non-owning proxies into collections that hold a jagged vector,
      ///       create the appropriate view into a collection
      ///       of the jagged vector property.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               class OtherMetaInfo,
                                                               bool valid     = !is_write_proxy(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      build_proxy_jagged_vector_outer_collection(const PropertiesIndexer mult_arr_idx)
      {
        return TrueBase::template build_proxy_jagged_vector_outer_collection<Property, OtherMetaInfo>(mult_arr_idx);
      }

      ///@brief Get a reference to (or view into) the actual collection
      ///       that is used to implement the jagged vector.
      ///
      ///For proxies into collections, this should be equivalent to:
      ///`build_proxy_jagged_vector_outer_collection<Property, OtherMetaInfo>(mult_arr_idx).underlying_collection()`
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class OtherMetaInfo>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      get_jagged_vector_underlying_collection(const PropertiesIndexer mult_arr_idx) const
      {
        return TrueBase::template get_jagged_vector_underlying_collection<Property, OtherMetaInfo>(mult_arr_idx);
      }

      ///@brief Get a reference to (or view into) the actual collection
      ///       that is used to implement the jagged vector.
      ///
      ///For proxies into collections, this should be equivalent to:
      ///`build_proxy_jagged_vector_outer_collection<Property, OtherMetaInfo>(mult_arr_idx).underlying_collection()`
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class OtherMetaInfo>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      get_jagged_vector_underlying_collection(const PropertiesIndexer mult_arr_idx)
      {
        return TrueBase::template get_jagged_vector_underlying_collection<Property, OtherMetaInfo>(mult_arr_idx);
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Returns @c true if this object has a single (non-global) @c PerItemObject.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_single_property_object()
      {
        return InterfaceDescription::single_per_item_property<ignore_globals>(PropertyList {});
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool allows_simple_accessor()
      {
        return (is_single_property_object<true>() && (MetaInfo::extent == 1));
        //Just one basic type means we can access stuff "directly".
      }

     private:

      using simple_accessor_property = decltype(InterfaceDescription::get_per_item_property(PropertyList {}));

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (allows_simple_accessor() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) simple_accessor() const
      {
        return this->template get_object<simple_accessor_property>(this->get_multi_array_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (allows_simple_accessor() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) simple_accessor()
      {
        return this->template get_object<simple_accessor_property>(this->get_multi_array_index());
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SpecificallyInvalidType
      {
      };

      using TypeToAccessOrNot = std::conditional_t<allows_simple_accessor() && ObjType::interface_properties().can_refer_to_const_object(),
                                                   typename simple_accessor_property::Type,
                                                   SpecificallyInvalidType>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr operator TypeToAccessOrNot() const
      {
        if constexpr (allows_simple_accessor() && ObjType::interface_properties().can_refer_to_const_object())
          {
            return this->simple_accessor();
          }
        else
          {
            return SpecificallyInvalidType {};
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,
                                                               class disabler = std::enable_if_t < allows_simple_accessor() &&
                                                                                ObjType::interface_properties().can_refer_to_modifiable_object() &&
                                                                                std::is_convertible_v<typename simple_accessor_property::Type, T> >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr explicit
                                                                                  operator T()
      {
        return this->simple_accessor();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,
                                                               class disabler = std::enable_if_t < allows_simple_accessor() &&
                                                                                ObjType::interface_properties().can_refer_to_const_object() &&
                                                                                std::is_convertible_v<typename simple_accessor_property::Type, T> >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr explicit
                                                                                  operator T() const
      {
        return this->simple_accessor();
      }

     private:

      using OperatorsChecker = impl::object_operator_checker<MetaInfo::extent == 1, Base, typename simple_accessor_property::Type>;

      using const_operators_checker = typename OperatorsChecker::const_operators;

      using operators_checker = typename OperatorsChecker::operators;

      using only_all_complex_operators_checker = typename OperatorsChecker::operators_without_main_property;

     public:

#define MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(NAME, OP)                                                                                                      \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class T,                                                                                                                                                   \
    class Prop = typename const_operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) < T>                        \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP(T && t) const                                  \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return this->simple_accessor() OP std::forward<T>(t);                                                                                                  \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return static_cast<const Prop &>(*this) OP std::forward<T>(t);                                                                                         \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _const_binary_dummy_struct_for_macro_ending)

#define MARIONETTE_OBJECT_BINARY_OPERATOR(NAME, OP)                                                                                                            \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class T,                                                                                                                                                   \
    class Prop = typename operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) < T>                              \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP(T && t)                                        \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return this->simple_accessor() OP std::forward<T>(t);                                                                                                  \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return static_cast<Prop &>(*this) OP std::forward<T>(t);                                                                                               \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _binary_dummy_struct_for_macro_ending)

#define MARIONETTE_OBJECT_PRE_UNARY_OPERATOR_CONST(NAME, OP)                                                                                                   \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class Prop = typename const_operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) <>                          \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP() const                                        \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return OP this->simple_accessor();                                                                                                                     \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return OP static_cast<const Prop &>(*this);                                                                                                            \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _const_pre_unary_dummy_struct_for_macro_ending)

#define MARIONETTE_OBJECT_PRE_UNARY_OPERATOR(NAME, OP)                                                                                                         \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class Prop = typename operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) <>                                \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP()                                              \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return OP this->simple_accessor();                                                                                                                     \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return OP static_cast<Prop &>(*this);                                                                                                                  \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _pre_unary_dummy_struct_for_macro_ending)

#define MARIONETTE_OBJECT_POST_UNARY_OPERATOR_CONST(NAME, OP)                                                                                                  \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class Prop = typename const_operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) <>                          \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP(int) const                                     \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return this->simple_accessor() OP;                                                                                                                     \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return static_cast<const Prop &>(*this) OP;                                                                                                            \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _const_post_unary_dummy_struct_for_macro_ending)

#define MARIONETTE_OBJECT_POST_UNARY_OPERATOR(NAME, OP)                                                                                                        \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class Prop = typename operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) <>                                \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP(int)                                           \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return this->simple_accessor() OP;                                                                                                                     \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return static_cast<Prop &>(*this) OP;                                                                                                                  \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _post_unary_dummy_struct_for_macro_ending)

#define MARIONETTE_OBJECT_FUNCTIONAL_UNARY_OPERATOR_CONST(NAME, OP)                                                                                            \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class... Ts,                                                                                                                                               \
    class Prop = typename const_operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) <>                          \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP() const                                        \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return this->simple_accessor().operator OP();                                                                                                          \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return static_cast<const Prop *>(this)->operator OP();                                                                                                 \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _const_functional_unary_dummy_struct_for_macro_ending)

#define MARIONETTE_OBJECT_FUNCTIONAL_UNARY_OPERATOR(NAME, OP)                                                                                                  \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class... Ts,                                                                                                                                               \
    class Prop = typename operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) <>                                \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP()                                              \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return this->simple_accessor().operator OP();                                                                                                          \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return static_cast<Prop *>(this)->operator OP();                                                                                                       \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _functional_unary_dummy_struct_for_macro_ending)

#define MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR_CONST(NAME, OP)                                                                                                  \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class... Ts,                                                                                                                                               \
    class Prop = typename const_operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) < Ts...>                    \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP(Ts &&... ts) const                             \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return this->simple_accessor().operator OP(std::forward<Ts>(ts)...);                                                                                   \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return static_cast<const Prop *>(this)->operator OP(std::forward<Ts>(ts)...);                                                                          \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _const_functional_dummy_struct_for_macro_ending)

#define MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR(NAME, OP)                                                                                                        \
                                                                                                                                                               \
 public:                                                                                                                                                       \
                                                                                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <                                                                                                    \
    class... Ts,                                                                                                                                               \
    class Prop = typename operators_checker::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) < Ts...>                          \
  , class disabler = std::enable_if_t < !std::is_same_v<Prop, Utility::InvalidType> >>                                                                         \
                     MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator OP(Ts &&... ts)                                   \
  {                                                                                                                                                            \
    if constexpr (std::is_same_v<Prop, typename simple_accessor_property::Type>)                                                                               \
      {                                                                                                                                                        \
        return this->simple_accessor().operator OP(std::forward<Ts>(ts)...);                                                                                   \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return static_cast<Prop *>(this)->operator OP(std::forward<Ts>(ts)...);                                                                                \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _functional_dummy_struct_for_macro_ending)

      //MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR(assign, =);

      MARIONETTE_OBJECT_BINARY_OPERATOR(add_eq, +=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(sub_eq, -=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(mul_eq, *=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(div_eq, /=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(mod_eq, %=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(bitand_eq, &=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(bitor_eq, |=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(bitxor_eq, ^=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(lsh_eq, <<=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(rsh_eq, >>=);

      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR(pre_inc, ++);
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR(pre_dec, --);
      MARIONETTE_OBJECT_POST_UNARY_OPERATOR(post_inc, ++);
      MARIONETTE_OBJECT_POST_UNARY_OPERATOR(post_dec, --);

      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR_CONST(self_add, +);
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR(self_add, +);
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR_CONST(self_sub, -);
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR(self_sub, -);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(add, +);
      MARIONETTE_OBJECT_BINARY_OPERATOR(add, +);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(sub, -);
      MARIONETTE_OBJECT_BINARY_OPERATOR(sub, -);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(mul, *);
      MARIONETTE_OBJECT_BINARY_OPERATOR(mul, *);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(div, /);
      MARIONETTE_OBJECT_BINARY_OPERATOR(div, /);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(mod, %);
      MARIONETTE_OBJECT_BINARY_OPERATOR(mod, %);
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR_CONST(self_compl, ~);
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR(self_compl, ~);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(bit_and, &);
      MARIONETTE_OBJECT_BINARY_OPERATOR(bit_and, &);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(bit_or, |);
      MARIONETTE_OBJECT_BINARY_OPERATOR(bit_or, |);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(bit_xor, ^);
      MARIONETTE_OBJECT_BINARY_OPERATOR(bit_xor, ^);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(lsh, <<);
      MARIONETTE_OBJECT_BINARY_OPERATOR(lsh, <<);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(rsh, >>);
      MARIONETTE_OBJECT_BINARY_OPERATOR(rsh, >>);

      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR_CONST(self_not, !);
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR(self_not, !);

#if MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(logic_and, &&);
      MARIONETTE_OBJECT_BINARY_OPERATOR(logic_and, &&);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(logic_or, ||);
      MARIONETTE_OBJECT_BINARY_OPERATOR(logic_or, ||);
#endif

      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(eq, ==);
      MARIONETTE_OBJECT_BINARY_OPERATOR(eq, ==);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(neq, !=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(neq, !=);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(lss, <);
      MARIONETTE_OBJECT_BINARY_OPERATOR(lss, <);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(grt, >);
      MARIONETTE_OBJECT_BINARY_OPERATOR(grt, >);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(leq, <=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(leq, <=);
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(geq, >=);
      MARIONETTE_OBJECT_BINARY_OPERATOR(geq, >=);

#if MARIONETTE_CONSIDER_SPACESHIP_OPERATOR_OVERLOAD
      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(spaceship, <=>);
      MARIONETTE_OBJECT_BINARY_OPERATOR(spaceship, <=>);
#endif

      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR_CONST(deref, *);
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR(deref, *);

#if MARIONETTE_OVERLOAD_ADDRESS_OF_OBJECT

      public:
      
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (allows_simple_accessor() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator&() const
      {
        return &(this->simple_accessor());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (allows_simple_accessor() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator&()
      {
        return &(this->simple_accessor());
      }

#elif MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR_CONST(addressof, &);
      MARIONETTE_OBJECT_PRE_UNARY_OPERATOR(addressof, &);
#endif

      MARIONETTE_OBJECT_FUNCTIONAL_UNARY_OPERATOR_CONST(arrow, ->);
      MARIONETTE_OBJECT_FUNCTIONAL_UNARY_OPERATOR(arrow, ->);

      MARIONETTE_OBJECT_BINARY_OPERATOR_CONST(pointer_to_member, ->*);
      MARIONETTE_OBJECT_BINARY_OPERATOR(pointer_to_member, ->*);

#if MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS

  #define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_COMMA ,
      MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR_CONST(MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_COMMA);
      MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR(MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_COMMA);
  #undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_COMMA

#endif

      MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR_CONST(access, []);
      MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR(access, []);
      MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR_CONST(call, ());
      MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR(call, ());

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts,
                                                               class Prop     = typename only_all_complex_operators_checker::template assignment_checker<Ts...>,
                                                               class disabler = std::enable_if_t<!std::is_same_v<Prop, Utility::InvalidType>>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object & operator=(Ts &&... ts)
      {
        static_cast<Prop *>(this)->operator=(std::forward<Ts>(ts)...);
        return (*this);
      }

#undef MARIONETTE_OBJECT_BINARY_OPERATOR_CONST
#undef MARIONETTE_OBJECT_BINARY_OPERATOR
#undef MARIONETTE_OBJECT_PRE_UNARY_OPERATOR_CONST
#undef MARIONETTE_OBJECT_PRE_UNARY_OPERATOR
#undef MARIONETTE_OBJECT_POST_UNARY_OPERATOR_CONST
#undef MARIONETTE_OBJECT_POST_UNARY_OPERATOR
#undef MARIONETTE_OBJECT_FUNCTIONAL_UNARY_OPERATOR_CONST
#undef MARIONETTE_OBJECT_FUNCTIONAL_UNARY_OPERATOR
#undef MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR_CONST
#undef MARIONETTE_OBJECT_FUNCTIONAL_OPERATOR

      //--------------------------------------------------------------------------------------------------

     public:

      ///@brief Returns @c true if this object has a single (non-global) @c PropertyArray.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_array_property_object()
      {
        return InterfaceDescription::single_property_array_property<ignore_globals>(PropertyList {});
      }

     private:

      using PropertyArrayType = decltype(InterfaceDescription::get_property_array_property(PropertyList {}));

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool specifies_access_operator()
      {
        return (is_array_property_object<true>() && MetaInfo::extent == 1);
      }

      ///@brief Access each separate entry of the @c PropertyArray.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = specifies_access_operator(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      operator[](const std::conditional_t<is_array_property_object<true>(), PropertiesIndexer, Utility::TypeHolder<>> mult_arr_idx) const
      {
        if constexpr (is_array_property_object<true>())
          {
            constexpr PropertiesIndexer multi_array_size = PropertyArrayType::number;

#if MARIONETTE_USE_ASSERTIONS
            assert(mult_arr_idx >= 0 && mult_arr_idx < multi_array_size);
#endif

            const PropertiesIndexer final_index = this->get_multi_array_index() * multi_array_size + mult_arr_idx;

            if constexpr (is_write_proxy())
              {
                return impl::build_multi_array_indexed_object<true, ObjType, impl::properties_getter<PropertyArrayType>, MetaInfo>(
                  this->get_layout_or_object_pointer(),
                  final_index);
              }
            else
              {
                return impl::build_multi_array_indexed_object<true, ObjType, impl::properties_getter<PropertyArrayType>, MetaInfo>(
                  this->get_layout_or_object_pointer(),
                  final_index,
                  this->get_proxied_index());
              }
          }
      }

      ///@brief Access each separate entry of the @c PropertyArray.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = specifies_access_operator(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      operator[](const std::conditional_t<is_array_property_object<true>(), PropertiesIndexer, Utility::TypeHolder<>> mult_arr_idx)
      {
        if constexpr (is_array_property_object<true>())
          {
            constexpr PropertiesIndexer multi_array_size = PropertyArrayType::number;

#if MARIONETTE_USE_ASSERTIONS
            assert(mult_arr_idx >= 0 && mult_arr_idx < multi_array_size);
#endif

            const PropertiesIndexer final_index = this->get_multi_array_index() * multi_array_size + mult_arr_idx;

            if constexpr (is_write_proxy())
              {
                return impl::build_multi_array_indexed_object<false, ObjType, impl::properties_getter<PropertyArrayType>, MetaInfo>(
                  this->get_layout_or_object_pointer(),
                  final_index);
              }
            else
              {
                return impl::build_multi_array_indexed_object<false, ObjType, impl::properties_getter<PropertyArrayType>, MetaInfo>(
                  this->get_layout_or_object_pointer(),
                  final_index,
                  this->get_proxied_index());
              }
          }
      }

#if MARIONETTE_INCLUDE_POTENTIALLY_THROWING_FUNCTIONS

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = specifies_access_operator(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES decltype(auto)
      at(const std::conditional_t<is_array_property_object<true>(), PropertiesIndexer, Utility::TypeHolder<>> mult_arr_idx)
      {
        if constexpr (is_array_property_object<true>())
          {
            constexpr PropertiesIndexer multi_array_size = PropertyArrayType::number;

            if (mult_arr_idx >= 0 && mult_arr_idx < multi_array_size)
              {
                return (*this)[mult_arr_idx];
              }
            else
              {
                throw std::out_of_range {""};
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = specifies_access_operator(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES decltype(auto)
      at(const std::conditional_t<is_array_property_object<true>(), PropertiesIndexer, Utility::TypeHolder<>> mult_arr_idx) const
      {
        if constexpr (is_array_property_object<true>())
          {
            constexpr PropertiesIndexer multi_array_size = PropertyArrayType::number;

            if (mult_arr_idx >= 0 && mult_arr_idx < multi_array_size)
              {
                return (*this)[mult_arr_idx];
              }
            else
              {
                throw std::out_of_range {""};
              }
          }
      }

#endif

      //--------------------------------------------------------------------------------------------------

     protected:

      ///@brief Force an unspecified access into a property array into a specific index.
      ///
      ///@remark Useful for writing the transfers, probably not in normal usage...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer mult_arr_idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array() const
      {
        static_assert(mult_arr_idx < MetaInfo::extent);

        const PropertiesIndexer final_index = this->get_multi_array_index() * MetaInfo::extent + mult_arr_idx;

        if constexpr (is_write_proxy())
          {
            return impl::build_multi_array_indexed_object<true, ObjType, PropertyList, MetaInfo>(this->get_layout_or_object_pointer(), final_index);
          }
        else
          {
            return impl::build_multi_array_indexed_object<true, ObjType, PropertyList, MetaInfo>(this->get_layout_or_object_pointer(),
                                                                                                 final_index,
                                                                                                 this->get_proxied_index());
          }
      }

      ///@brief Force an unspecified access into a property array into a specific index.
      ///
      ///@remark Useful for writing the transfers, probably not in normal usage...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer mult_arr_idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array()
      {
        static_assert(mult_arr_idx < MetaInfo::extent);

        const PropertiesIndexer final_index = this->get_multi_array_index() * MetaInfo::extent + mult_arr_idx;

        if constexpr (is_write_proxy())
          {
            return impl::build_multi_array_indexed_object<false, ObjType, PropertyList, MetaInfo>(this->get_layout_or_object_pointer(), final_index);
          }
        else
          {
            return impl::build_multi_array_indexed_object<false, ObjType, PropertyList, MetaInfo>(this->get_layout_or_object_pointer(),
                                                                                                  final_index,
                                                                                                  this->get_proxied_index());
          }
      }

      //--------------------------------------------------------------------------------------------------

      //TO-DO: Come up with a potential generalization that allows
      //       a full collection interface with sensible behaviour
      //       instead of hacking it here.

      ///@brief Returns @c true if this object has a single (non-global) @c JaggedVector.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_jagged_vector_object()
      {
        return InterfaceDescription::single_jagged_vector_property<ignore_globals>(PropertyList {}) && MetaInfo::extent == 1;
      }

      static_assert(!is_jagged_vector_object() || MetaInfo::extent == 1, "Unspecified accesses into jagged vectors give erroneous results!");

     protected:

      using JaggedVectorProperty = decltype(InterfaceDescription::get_jagged_vector_property(PropertyList {}));

      ///@brief Return the underlying collection that stores every property of the
      ///       jagged vector (not the outermost collection corresponding to the jagged vector property).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = is_jagged_vector_object<true>(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) underlying_collection() const
      {
        return this->template get_jagged_vector_underlying_collection<JaggedVectorProperty, MetaInfo>(this->get_multi_array_index());
      }

      ///@brief Return the underlying collection that stores every property of the
      ///       jagged vector (not the outermost collection corresponding to the jagged vector property).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = is_jagged_vector_object<true>(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) underlying_collection()
      {
        return this->template get_jagged_vector_underlying_collection<JaggedVectorProperty, MetaInfo>(this->get_multi_array_index());
      }

      ///@brief For non-write-objects, return the actual jagged vector collection.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = is_jagged_vector_object<true>() && !is_write_proxy(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) jagged_vector_collection() const
      {
        return this->template build_proxy_jagged_vector_outer_collection<JaggedVectorProperty, MetaInfo>(this->get_multi_array_index());
      }

      ///@brief For non-write-objects, return the actual jagged vector collection.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = is_jagged_vector_object<true>() && !is_write_proxy(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) jagged_vector_collection()
      {
        return this->template build_proxy_jagged_vector_outer_collection<JaggedVectorProperty, MetaInfo>(this->get_multi_array_index());
      }

      using OriginalVectorType =
        typename impl::jagged_vector_types_getter_helper<is_jagged_vector_object<true>(), ObjType, JaggedVectorProperty, MetaInfo>::Type;

     public:

      //using size_type            = typename OriginalVectorType::size_type;
      //using difference_type      = typename OriginalVectorType::difference_type;

      using value_type             = typename OriginalVectorType::value_type;
      using reference              = typename OriginalVectorType::reference;
      using const_reference        = typename OriginalVectorType::const_reference;
      using pointer                = typename OriginalVectorType::pointer;
      using const_pointer          = typename OriginalVectorType::const_pointer;
      using const_iterator         = typename OriginalVectorType::const_iterator;
      using const_reverse_iterator = typename OriginalVectorType::const_reverse_iterator;
      using iterator =
        std::conditional_t<ObjType::interface_properties().can_refer_to_modifiable_object(), typename OriginalVectorType::iterator, const_iterator>;
      using reverse_iterator = std::
        conditional_t<ObjType::interface_properties().can_refer_to_modifiable_object(), typename OriginalVectorType::reverse_iterator, const_reverse_iterator>;
      using ConstReadProxy = typename OriginalVectorType::ConstReadProxy;
      using ReadProxy      = typename OriginalVectorType::ReadProxy;
      using WriteProxy     = typename OriginalVectorType::WriteProxy;

     protected:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type get_starting_index() const
      {
        if constexpr (is_write_proxy())
          {
            return static_cast<size_type>(0);
          }
        else
          {
            return static_cast<size_type>(this->jagged_vector_collection().get_item_starting_index(this->get_proxied_index()));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type get_past_last_index() const
      {
        if constexpr (is_write_proxy())
          {
            return static_cast<size_type>(this->underlying_collection().size());
          }
        else
          {
            return static_cast<size_type>(this->jagged_vector_collection().get_item_past_last_index(this->get_proxied_index()));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type get_reverse_starting_index() const
      {
        return static_cast<size_type>(this->underlying_collection().size()) - this->get_past_last_index();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type get_reverse_past_last_index() const
      {
        return static_cast<size_type>(this->underlying_collection().size()) - this->get_starting_index();
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type size() const
      {
        return static_cast<size_type>(this->get_past_last_index() - this->get_starting_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto cbegin() const
      {
        return this->underlying_collection().cbegin() + static_cast<difference_type>(this->get_starting_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto cend() const
      {
        return this->underlying_collection().cbegin() + static_cast<difference_type>(this->get_past_last_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto begin() const
      {
        return cbegin();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto end() const
      {
        return cend();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto begin()
      {
        return this->underlying_collection().begin() + static_cast<difference_type>(this->get_starting_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto end()
      {
        return this->underlying_collection().begin() + static_cast<difference_type>(this->get_past_last_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto crbegin() const
      {
        return this->underlying_collection().crbegin() + static_cast<difference_type>(this->get_reverse_starting_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto crend() const
      {
        return this->underlying_collection().crbegin() + static_cast<difference_type>(this->get_reverse_past_last_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto rbegin() const
      {
        return crbegin();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto rend() const
      {
        return crend();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto rbegin()
      {
        return this->underlying_collection().crbegin() + static_cast<difference_type>(this->get_reverse_starting_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto rend()
      {
        return this->underlying_collection().rbegin() + static_cast<difference_type>(this->get_reverse_past_last_index());
      }

      ///@remark If one cannot resize the underlying object,
      ///        assignment is done up to the minimum between the current size and the requested length.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class T        = typename OriginalVectorType::WriteProxy,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_refer_to_modifiable_object()),
        class disabler = std::enable_if_t < std::is_assignable_v<typename OriginalVectorType::WriteProxy &, T> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void assign(const size_type count, const T & ref)
      {
        if constexpr (std::is_same_v<T, typename OriginalVectorType::ReadProxy> || std::is_same_v<T, typename OriginalVectorType::ConstReadProxy>)
          {
            if constexpr (std::is_convertible_v<decltype(ref.get_proxied_pointer()), decltype(this->underlying_collection().get_owning_pointer())>)
              {
                if (ref.get_proxied_pointer() == this->underlying_collection().get_owning_pointer())
                  {
                    typename OriginalVectorType::WriteProxy wp = ref;
                    this->assign(count, wp);
                    return;
                  }
              }
          }

        size_type desired_number = count;

        if constexpr (ObjType::interface_properties().can_resize_jagged_vector_object())
          {
            const size_type previous_size = this->size();
            if (previous_size < count)
              {
                desired_number = previous_size;
                this->resize(count, ref);
              }
          }
        else
          {
            const size_type current_size = this->size();

            desired_number = current_size > count ? count : current_size;
          }
        for (size_type i = 0; i < desired_number; ++i)
          {
            (*this)[i] = ref;
          }
      }

      ///@remark If one cannot resize the underlying object,
      ///        assignment is done up to the minimum between the current size and the requested length.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class InputIt,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_refer_to_modifiable_object()),
        class disabler = std::enable_if_t < std::is_assignable_v<typename OriginalVectorType::ReadProxy, decltype(*std::declval<InputIt>())> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void assign(InputIt first, const InputIt last)
      {
        if constexpr (ObjType::interface_properties().can_resize_jagged_vector_object())
          {
            size_type count = 0;
            if constexpr (Utility::is_random_access_iterator_or_pointer<InputIt>())
              {
                count = static_cast<size_type>(last - first);
              }
            else
              {
                for (InputIt other = first; other != last; ++other)
                  {
                    ++count;
                  }
              }
            this->resize(count);
          }
        const size_type current_size = this->size();

        auto assign_it = this->begin();

        //If you are following a unsafe-buffer-usage from Clang,
        //it's probably an initializer list iterator
        //(which is a straight-up pointer).
        //I'd recommend not excluding this explicitly with pragmas
        //(pragma clang diagnostic push
        // pragma clang diagnostic ignored "-Wunsafe-buffer-usage")
        //for three reasons:
        //
        //1) '-Wunsafe-buffer-usage' is at the time of writing still "experimental",
        //   so users who do enable it (or use '-Weverything') should be aware
        //   that they are opting in to warnings that might not fit all possible use cases.
        //
        //2) Since it complains even for 'main(int argc, char ** argv)',
        //   any sort of blanket "fix all warnings everywhere" policy
        //   is somewhat doomed from the start with this option.
        //
        //3) Initializer list iterators triggering this warning
        //   (or returning raw pointers, for that matter...)
        //   strikes me as something that will be improved on in the future.
        //
        for (size_type count = 0; count < current_size && first != last; ++count, ++first, ++assign_it)
          {
            (*assign_it) = (*first);
          }
      }

      ///@remark If one cannot resize the underlying object,
      ///        assignment is done up to the minimum between the current size and the requested length.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class T        = typename OriginalVectorType::WriteProxy,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_refer_to_modifiable_object()),
        class disabler = std::enable_if_t < std::is_assignable_v<typename OriginalVectorType::ReadProxy, T> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void assign(std::initializer_list<T> list)
      {
        this->assign(list.begin(), list.end());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      operator[](const std::conditional_t<is_jagged_vector_object(), size_type, Utility::InvalidType> pos)
      {
        if constexpr (is_jagged_vector_object())
          {
#if MARIONETTE_USE_ASSERTIONS
            assert(pos >= 0 && pos < this->size());
#endif

            return *(this->begin() + static_cast<difference_type>(pos));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
      operator[](const std::conditional_t<is_jagged_vector_object(), size_type, Utility::InvalidType> pos) const
      {
        if constexpr (is_jagged_vector_object())
          {
#if MARIONETTE_USE_ASSERTIONS
            assert(pos >= 0 && pos < this->size());
#endif

            return *(this->cbegin() + static_cast<difference_type>(pos));
          }
      }

#if MARIONETTE_INCLUDE_POTENTIALLY_THROWING_FUNCTIONS

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES decltype(auto)
      at(const std::conditional_t<is_jagged_vector_object(), size_type, Utility::InvalidType> pos)
      {
        if constexpr (is_jagged_vector_object())
          {
            if (pos < this->size() && pos >= 0)
              {
                return (*this)[pos];
              }
            else
              {
                throw std::out_of_range {""};
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES decltype(auto)
      at(const std::conditional_t<is_jagged_vector_object(), size_type, Utility::InvalidType> pos) const
      {
        if constexpr (is_jagged_vector_object())
          {
            if (pos < this->size() && pos >= 0)
              {
                return (*this)[pos];
              }
            else
              {
                throw std::out_of_range {""};
              }
          }
      }

#endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) front()
      {
#if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
#endif

        return *(this->begin());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) front() const
      {
#if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
#endif

        return *(this->cbegin());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) back()
      {
#if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
#endif

        return *(this->rbegin());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) back() const
      {
#if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
#endif

        return *(this->rcbegin());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool empty() const
      {
        return this->size() == 0;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_resize_jagged_vector_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void reserve(const size_type new_size)
      {
        this->underlying_collection().reserve(this->underlying_collection().size() - this->size() + new_size);
        //We could, at the cost of making it slightly more memory-hungry,
        //skip the subtraction of this->size()...
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type capacity() const
      {
        return this->underlying_collection().capacity() - this->underlying_collection().size() + this->size();
      }

      ///@remark This is a no-op.
      ///        It makes no sense for a single entry to be able to shrink everything.
      ///        If memory savings are needed, call the shrink_to_fit of the whole collection.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_resize_jagged_vector_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void shrink_to_fit()
      {
        //It is fun to see that
        //this is fully standards,
        //or at least cpp-ref, compliant:
        //"It depends on the implementation
        // whether the request is fulfilled."
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_resize_jagged_vector_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void clear()
      {
        this->underlying_collection().erase(this->begin(), this->end());
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().decrease_item_size(this->get_proxied_index(), this->size());
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class It       = const_iterator,
        class T        = typename OriginalVectorType::WriteProxy,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_resize_jagged_vector_object()),
        class disabler = std::enable_if_t < std::is_assignable_v<typename OriginalVectorType::WriteProxy &, T> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto insert(const It pos, size_type count, const T & t)
      {
        const auto ret = this->underlying_collection().insert(pos, count, t);
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().increase_item_size(this->get_proxied_index(), count);
          }
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class It       = const_iterator,
        class T        = typename OriginalVectorType::WriteProxy,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_resize_jagged_vector_object()),
        class disabler = std::enable_if_t < std::is_assignable_v<typename OriginalVectorType::WriteProxy &, T> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto insert(It pos, const T & value)
      {
        const auto ret = this->underlying_collection().insert(pos, value);
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().increase_item_size(this->get_proxied_index(), 1);
          }
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class InputIt,
        class PosIt    = const_iterator,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_resize_jagged_vector_object()),
        class disabler = std::enable_if_t < std::is_assignable_v<typename OriginalVectorType::ReadProxy, decltype(*std::declval<InputIt>())> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto insert(PosIt pos, InputIt first, const InputIt last)
      {
        size_type count = 0;

        if constexpr (Utility::is_random_access_iterator_or_pointer<InputIt>())
          {
            count = static_cast<size_type>(last - first);
          }
        else
          {
            for (InputIt other = first; other != last; ++other)
              {
                ++count;
              }
          }
        const auto ret = this->underlying_collection().insert(pos, first, last);
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().increase_item_size(this->get_proxied_index(), count);
          }
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class It       = const_iterator,
        class T        = typename OriginalVectorType::WriteProxy,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_resize_jagged_vector_object()),
        class disabler = std::enable_if_t < std::is_assignable_v<typename OriginalVectorType::ReadProxy, T> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto insert(It pos, std::initializer_list<T> list)
      {
        const auto ret = this->underlying_collection().insert(pos, list);
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().increase_item_size(this->get_proxied_index(), list.size());
          }
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Args,
                                                               class It       = const_iterator,
                                                               bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_resize_jagged_vector_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto emplace(It pos, Args &&... args)
      {
        const auto ret = this->underlying_collection().emplace(pos, std::forward<Args>(args)...);
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().increase_item_size(this->get_proxied_index(), 1);
          }
        return ret;
      }

      ///@p first and @p last must be a valid iterator into the "vector", @p last may also be its end iterator,
      ///or @c first == last, in which case no operation is done and @c end() is returned.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class It       = const_iterator,
                                                               bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_resize_jagged_vector_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto erase(const It first, const It last)
      {
        [[maybe_unused]] const size_type count = static_cast<size_type>(last - first);

        const auto ret = this->underlying_collection().erase(first, last);
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().decrease_item_size(this->get_proxied_index(), count);
          }
        return ret;
      }

      ///@p pos must be a valid iterator into the "vector".
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class It       = const_iterator,
                                                               bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_resize_jagged_vector_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto erase(const It pos)
      {
        const auto ret = this->underlying_collection().erase(pos);
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().decrease_item_size(this->get_proxied_index(), 1);
          }
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class T        = typename OriginalVectorType::WriteProxy,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_resize_jagged_vector_object()),
        class disabler = std::enable_if_t < std::is_assignable_v<typename OriginalVectorType::WriteProxy &, T> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void push_back(T && t)
      {
        this->underlying_collection().insert(this->end(), std::forward<T>(t));
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().increase_item_size(this->get_proxied_index(), 1);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class... Args,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_resize_jagged_vector_object()),
        class disabler = std::enable_if_t < Utility::always_true<Args...> && valid >>
                                                                               MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
                                                                               emplace_back(Args &&... args)
      {
        this->underlying_collection().emplace(this->end(), std::forward<Args>(args)...);
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().increase_item_size(this->get_proxied_index(), 1);
          }
        return back();
      }

      ///@warning Undefined behaviour if @c pop_back is used when @c size is 0.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_resize_jagged_vector_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void pop_back()
      {
#if MARIONETTE_USE_ASSERTIONS
        assert(this->size() > 0);
#endif

        this->underlying_collection().erase(this->end() - 1);
        if constexpr (!is_write_proxy())
          {
            this->jagged_vector_collection().decrease_item_size(this->get_proxied_index(), 1);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = (is_jagged_vector_object() &&
                                                                             ObjType::interface_properties().can_resize_jagged_vector_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void resize(const size_type new_size)
      {
        const size_type old_size = this->size();
        if (old_size < new_size)
          {
            static_assert(std::is_default_constructible_v<typename OriginalVectorType::WriteProxy>, "We need to default construct elements!");
            const size_type delta = static_cast<size_type>(new_size - old_size);

            typename OriginalVectorType::WriteProxy def_const {};
            //We are assuming WriteProxy is default constructible.

            this->underlying_collection().insert(this->end(), delta, def_const);

            if constexpr (!is_write_proxy())
              {
                this->jagged_vector_collection().increase_item_size(this->get_proxied_index(), delta);
              }
          }
        else if (old_size > new_size)
          {
            const size_type delta = static_cast<size_type>(old_size - new_size);
            this->underlying_collection().erase(this->end() - static_cast<difference_type>(delta), this->end());
            if constexpr (!is_write_proxy())
              {
                this->jagged_vector_collection().decrease_item_size(this->get_proxied_index(), delta);
              }
          }
        else /*if (old_size == new_size)*/
          {
            //Nothing to do.
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class T        = typename OriginalVectorType::WriteProxy,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_resize_jagged_vector_object()),
        class disabler = std::enable_if_t < std::is_assignable_v<typename OriginalVectorType::WriteProxy &, T> &&
                         valid >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void resize(const size_type new_size, const T & t)
      {
        const size_type old_size = this->size();
        if (new_size > old_size)
          {
            const size_type delta = static_cast<size_type>(new_size - old_size);
            this->underlying_collection().insert(this->end(), delta, t);
            if constexpr (!is_write_proxy())
              {
                this->jagged_vector_collection().increase_item_size(this->get_proxied_index(), delta);
              }
          }
        else
          {
            this->resize(new_size);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class VecLikeT,
        bool valid     = (is_jagged_vector_object() && ObjType::interface_properties().can_refer_to_const_object() &&
                      std::is_constructible_v<typename VecLikeT::value_type, typename OriginalVectorType::ConstReadProxy> &&
                      !std::is_same_v<std::initializer_list<typename VecLikeT::value_type>, VecLikeT>),
        class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr explicit operator VecLikeT() const
      {
        return VecLikeT {this->cbegin(), this->cend()};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T        = typename OriginalVectorType::WriteProxy,
                                                               class disabler = std::enable_if_t < is_jagged_vector_object() &&
                                                                                std::is_constructible_v<T, typename OriginalVectorType::ConstReadProxy> >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr explicit
                                                                                  operator std::initializer_list<T>() const
      {
        return std::initializer_list<T>(this->cbegin(), this->cend());
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Returns @c true if this object only has (non-global) @c NoObject properties.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_no_object_object()
      {
        return InterfaceDescription::only_no_object_properties<ignore_globals>(PropertyList {});
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Obtain the sub-object corresponding to property @p Property.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool allow_any_property = false, class OverrideMetaInfo = void>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get() const
      {        
        static_assert(allow_any_property || ObtainableProperties::template contains<Property>() || PropertyList::template contains<Property>(), "Must get valid property!");

        using RealMetaInfo = std::conditional_t<std::is_void_v<OverrideMetaInfo>, MetaInfo, OverrideMetaInfo>;

        if constexpr (is_write_proxy())
          {
            if constexpr (object_stores_multi_array_index())
              {
                return impl::get_new_object<true, Property, PropertyList, RealMetaInfo, ObjType>(this->get_layout_or_object_pointer(),
                                                                                                 this->get_multi_array_index());
              }
            else
              {
                return impl::get_new_object<true, Property, PropertyList, RealMetaInfo, ObjType>(this->get_layout_or_object_pointer());
              }
          }
        else
          {
            if constexpr (object_stores_multi_array_index())
              {
                return impl::get_new_object<true, Property, PropertyList, RealMetaInfo, ObjType>(this->get_layout_or_object_pointer(),
                                                                                                 this->get_multi_array_index(),
                                                                                                 this->get_proxied_index());
              }
            else
              {
                return impl::get_new_object<true, Property, PropertyList, RealMetaInfo, ObjType>(this->get_layout_or_object_pointer(),
                                                                                                 this->get_proxied_index());
              }
          }
      }

      ///@brief Obtain the sub-object corresponding to property @p Property.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool allow_any_property = false, class OverrideMetaInfo = void>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get()
      {
        static_assert(allow_any_property || ObtainableProperties::template contains<Property>() || PropertyList::template contains<Property>(), "Must get valid property!");

        using RealMetaInfo = std::conditional_t<std::is_void_v<OverrideMetaInfo>, MetaInfo, OverrideMetaInfo>;

        if constexpr (is_write_proxy())
          {
            if constexpr (object_stores_multi_array_index())
              {
                return impl::get_new_object<false, Property, PropertyList, RealMetaInfo, ObjType>(this->get_layout_or_object_pointer(),
                                                                                                  this->get_multi_array_index());
              }
            else
              {
                return impl::get_new_object<false, Property, PropertyList, RealMetaInfo, ObjType>(this->get_layout_or_object_pointer());
              }
          }
        else
          {
            if constexpr (object_stores_multi_array_index())
              {
                return impl::get_new_object<false, Property, PropertyList, RealMetaInfo, ObjType>(this->get_layout_or_object_pointer(),
                                                                                                  this->get_multi_array_index(),
                                                                                                  this->get_proxied_index());
              }
            else
              {
                return impl::get_new_object<false, Property, PropertyList, RealMetaInfo, ObjType>(this->get_layout_or_object_pointer(),
                                                                                                  this->get_proxied_index());
              }
          }
      }

      //--------------------------------------------------------------------------------------------------

#if MARIONETTE_GENERIC_OPERATE_API_SHOULD_BE_PUBLIC

     public:

#else

     protected:

#endif

      ///@brief Apply @p op, with any additional arguments, to every element.
      ///
      ///@p op will receive as first argument the property definition (which should be an empty structure),
      ///then a reference to the corresponding object (or an appropriate proxy for jagged vectors and sub-collections),
      ///then any additional arguments.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Op, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void operate_on_all(Op && op, Args &&... args) const
      {
        (std::forward<Op>(op)(Properties {}, this->get<Properties>(), std::forward<Args>(args)...), ...);
      }

      ///@brief Apply @p op, with any additional arguments, to every element.
      ///
      ///@p op will receive as first argument the property definition (which should be an empty structure),
      ///then a reference to the corresponding object (or an appropriate proxy for jagged vectors and sub-collections),
      ///then any additional arguments.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Op, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void operate_on_all(Op && op, Args &&... args)
      {
        (std::forward<Op>(op)(Properties {}, this->get<Properties>(), std::forward<Args>(args)...), ...);
      }

      //--------------------------------------------------------------------------------------------------

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return Transfers::Transfer<Object, Object>::can_copy();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return Transfers::Transfer<Object, Object>::can_copy();
      }

     public:

      //"Eric's trick" so copy/move constructors and assignment works as expected.
      //See: https://stackoverflow.com/a/52636474

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object &
      operator=(const std::conditional_t<!can_copy(), Object, Utility::InvalidType> &) = delete;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object &
      operator=(const std::conditional_t<can_copy(), Object, Utility::InvalidType> & o)
      {
        if constexpr (can_copy())
          {
            if (this != std::addressof(o))
              {
                Transfers::Transfer<Object, Object>::copy(*this, o);
              }
          }
        return *this;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object &
      operator=(std::conditional_t<!can_move(), Object, Utility::InvalidType> &&) = delete;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object &
      operator=(std::conditional_t<can_move(), Object, Utility::InvalidType> && o)
      {
        if constexpr (can_move())
          {
            if (this != std::addressof(o))
              {
                Transfers::Transfer<Object, Object>::move(*this, std::move(o));
              }
          }
        return *this;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object(
        const std::conditional_t<!can_copy() || object_is_non_owning(), Object, Utility::InvalidType> &) = delete;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object(
        const std::conditional_t<can_copy() && !object_is_non_owning(), Object, Utility::InvalidType> & o):
        Base()
      {
        Transfers::Transfer<Object, Object>::copy(*this, o);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object(
        std::conditional_t<!can_move() || object_is_non_owning(), Object, Utility::InvalidType> &&) = delete;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object(
        std::conditional_t<can_move() && !object_is_non_owning(), Object, Utility::InvalidType> && o):
        Base()
      {
        Transfers::Transfer<Object, Object>::move(*this, std::move(o));
      }

      //--------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherT,
                                                               class disabler = std::enable_if_t <
                                                                                  Transfers::Transfer<Object, std::decay_t<OtherT>>::can_copy() ||
                                                                                std::is_assignable_v<Base &, const OtherT &> >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object &
                                                                                  operator=(const OtherT & t)
      {
        if constexpr (Transfers::Transfer<Object, std::decay_t<OtherT>>::can_copy())
          {
            Transfers::Transfer<Object, std::decay_t<OtherT>>::copy(*this, t);
          }
        else /*if constexpr (std::is_assignable_v<Base &, const OtherT &>)*/
          {
            Base::operator=(t);
          }
        return *this;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class OtherT,
        class disabler = std::enable_if_t < Transfers::Transfer<Object, std::decay_t<OtherT>>::can_move() ||
                         std::is_assignable_v<Base &, OtherT &&> >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto
                           operator=(OtherT && t) -> std::enable_if_t<std::is_rvalue_reference_v<decltype(t)>, Object &>
      {
        if constexpr (Transfers::Transfer<Object, std::decay_t<OtherT>>::can_move())
          {
            Transfers::Transfer<Object, std::decay_t<OtherT>>::move(*this, std::move(t));
          }
        else /*if constexpr (std::is_assignable_v<Base &, OtherT &&>)*/
          {
            Base::operator=(std::move(t));
          }
        return *this;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class OtherT,
        class disabler = std::enable_if_t < Transfers::Transfer<Object, std::decay_t<OtherT>>::can_copy() && !object_is_non_owning() &&
                         !std::is_constructible_v<Base, const OtherT &> >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object(const OtherT & t): Base()
      {
        Transfers::Transfer<Object, std::decay_t<OtherT>>::copy(*this, t);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class OtherT,
        class disabler = std::enable_if_t < Transfers::Transfer<Object, std::decay_t<OtherT>>::can_move() && !object_is_non_owning() &&
                         !std::is_constructible_v<Base, const OtherT &> >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object(OtherT && t):
        Base()
      {
        Transfers::Transfer<Object, std::decay_t<OtherT>>::move(*this, std::move(t));
      }

      //--------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... OtherTs,
                                                               class disabler = std::enable_if_t<std::is_constructible_v<TrueBase, OtherTs...>>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object(const OtherTs &... ts): Base(ts...)
      {
      }

     protected:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class P, class... Ps, class T, class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void initialize_helper(Utility::TypeHolder<P, Ps...>, T && t, Ts &&... ts)
      {
        if constexpr (sizeof...(Ps) == sizeof...(Ts))
          {
            this->template get<P>() = std::forward<T>(t);
            (void) ((void) (this->template get<Ps>() = std::forward<Ts>(ts)), ...);
          }
        else if constexpr (sizeof...(Ps) > sizeof...(Ts))
          {
            initialize_helper(Utility::TypeHolder<P, Ps...> {}, std::forward<T>(t), std::forward<Ts>(ts)..., Utility::DefaultInitializer {});
          }
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... OtherTs,
                                                               class disabler = std::enable_if_t < !std::is_constructible_v<TrueBase, OtherTs...> &&
                                                                                (sizeof...(OtherTs) > 1) >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Object(
                                                                                    OtherTs &&... ts)
      {
        initialize_helper(PropertyList {}, std::forward<OtherTs>(ts)...);
      }

      //--------------------------------------------------------------------------------------------------

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_transfer_specification(T &&)
      {
        return Transfers::Transfer<Object, std::decay_t<T>> {};
      }
#endif
    };
  }

}

#endif
