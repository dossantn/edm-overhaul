//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_FRIENDSHIPPROVIDER_H
#define MARIONETTE_FRIENDSHIPPROVIDER_H

#include <utility>

namespace Marionette
{
  namespace Utility
  {

    ///@class FriendshipProvider
    ///The least inelegant hack to go around access specifier restrictions
    ///in heavily templated code where friend declarations could get messier...
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES FriendshipProvider
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_multi_array_index(T && t, Args &&... args)
      {
        return std::forward<T>(t).get_multi_array_index(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) update_multi_array_index(T && t, Args &&... args)
      {
        return std::forward<T>(t).update_multi_array_index(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_array(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_array<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_tagged_size_pointer(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_tagged_size_pointer<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_tagged_size(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_tagged_size<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) set_tagged_size(T && t, Args &&... args)
      {
        return std::forward<T>(t).template set_tagged_size<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_tagged_capacity(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_tagged_capacity<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_object(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_object<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_object_pointer(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_object_pointer<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_jagged_vector_underlying_collection(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_jagged_vector_underlying_collection<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) underlying_collection(T && t, Args &&... args)
      {
        return std::forward<T>(t).underlying_collection(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) actual_underlying_collection(T && t, Args &&... args)
      {
        return std::forward<T>(t).actual_underlying_collection(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_multi_array(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_multi_array<idx>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_actual_multi_array(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_multi_array<idx>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_starting_index(T && t, Args &&... args)
      {
        return std::forward<T>(t).get_starting_index(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_past_last_index(T && t, Args &&... args)
      {
        return std::forward<T>(t).get_past_last_index(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_actual_collection(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_actual_collection<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_copy_from(T && t, Args &&... args)
      {
        std::forward<T>(t).template array_copy_from<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_copy_to(T && t, Args &&... args)
      {
        return std::forward<T>(t).template array_copy_to<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) base_resize(T && t, Args &&... args)
      {
        return std::forward<T>(t).template base_resize<SizeTag, extent>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) base_reserve(T && t, Args &&... args)
      {
        return std::forward<T>(t).template base_reserve<SizeTag, extent>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) base_clear(T && t, Args &&... args)
      {
        return std::forward<T>(t).template base_clear<SizeTag, extent>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) base_shrink_to_fit(T && t, Args &&... args)
      {
        return std::forward<T>(t).template base_shrink_to_fit<SizeTag, extent>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_resize(T && t, Args &&... args)
      {
        return std::forward<T>(t).template array_resize<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_reserve(T && t, Args &&... args)
      {
        return std::forward<T>(t).template array_reserve<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_clear(T && t, Args &&... args)
      {
        return std::forward<T>(t).template array_clear<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_shrink_to_fit(T && t, Args &&... args)
      {
        return std::forward<T>(t).template array_shrink_to_fit<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) resize_jagged_vector(T && t, Args &&... args)
      {
        return std::forward<T>(t).template resize_jagged_vector<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) reserve_jagged_vector(T && t, Args &&... args)
      {
        return std::forward<T>(t).template reserve_jagged_vector<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) clear_jagged_vector(T && t, Args &&... args)
      {
        return std::forward<T>(t).template clear_jagged_vector<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) shrink_jagged_vector(T && t, Args &&... args)
      {
        return std::forward<T>(t).template shrink_jagged_vector<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TArgs, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_array_pointer(T && t, Args &&... args)
      {
        return std::forward<T>(t).template get_array_pointer<TArgs...>(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) memory_context_info(T && t, Args &&... args)
      {
        return std::forward<T>(t).memory_context_info(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_size_tag_array(T && t, Args &&... args)
      {
        return std::forward<T>(t).get_size_tag_array(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_size_tag_array_pointer(T && t, Args &&... args)
      {
        return std::forward<T>(t).get_size_tag_array_pointer(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) holder_ptr(T && t, Args &&... args)
      {
        return std::forward<T>(t).holder_ptr(std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_layout_holder_pointer(T && t, Args &&... args)
      {
        return std::forward<T>(t).get_layout_holder_pointer(std::forward<Args>(args)...);
      }
    };
  }
}

#endif
