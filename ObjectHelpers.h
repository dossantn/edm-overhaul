//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_OBJECTHELPERS_H
#define MARIONETTE_OBJECTHELPERS_H

#include <utility>
#include <type_traits>

#include "MarionetteBase.h"
#include "CollectionHelpers.h"
#include "ObjectOperatorHelpers.h"

namespace Marionette
{
  namespace Collections
  {
    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool replace_globals, class Property>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto obtainable_object_property_helper(Property p)
      {
        if constexpr (InterfaceDescription::is_global(p))
          {
            if constexpr (replace_globals && InterfaceDescription::has_extra_write_proxy_types(p))
              {
                return Utility::TypeHolder<typename Property::ExtraWriteProxyProperty> {};
              }
            else
              {
                return Utility::TypeHolder<Utility::InvalidType> {};
              }
          }
        else
          {
            return Utility::TypeHolder<Property> {};
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool replace_globals, class... Props>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_obtainable_object_properties(Utility::TypeHolder<Props...>)
      {
        auto th = (Utility::TypeHolder<Utility::InvalidType> {} | ... | obtainable_object_property_helper<replace_globals>(Props {}));

        if constexpr (InterfaceDescription::single_property_array_property(th))
          {
            using Property = decltype(InterfaceDescription::get_property_array_property(th));
            return th | typename Property::Properties {};
          }
        else if constexpr (InterfaceDescription::single_subcollection_property(th))
          {
            using Property = decltype(InterfaceDescription::get_subcollection_property(th));
            return th | typename Property::Properties {};
          }
        //else if constexpr (InterfaceDescription::single_jagged_vector_property(th))
        //  {
        //    TO-DO: Come up with a potential generalization that allows
        //           a full collection interface with sensible behaviour.
        //  }
        else
          {
            return th;
          }
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class Final,
        class Layout,
        class Property,
        class T        = typename Property::Type::template Functions<Final, Layout>,
        class disabler = std::enable_if_t<InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_underlying_object_extensions(Property)
      {
        return Utility::TypeHolder<T> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Final, class Layout, class Property, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_underlying_object_extensions(Property, Args...)
      {
        return Utility::TypeHolder<Utility::InvalidType> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool add_self_functions,
                                                               class Final,
                                                               class Layout,
                                                               class Property,
                                                               class T = typename Property::template ObjectFunctions<Final, Layout>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_single_property_object_base(Property p)
      {
        if constexpr (add_self_functions)
          {
            return get_underlying_object_extensions<Final, Layout>(p) | Utility::TypeHolder<T> {};
          }
        else
          {
            return Utility::TypeHolder<T> {};
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool add_self_functions, class Final, class Layout, class Property, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_single_property_object_base(Property p, Args...)
      {
        if constexpr (add_self_functions)
          {
            return get_underlying_object_extensions<Final, Layout>(p);
          }
        else
          {
            return Utility::TypeHolder<Utility::InvalidType> {};
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Final, class Layout, class OriginalProperties, class MetaInfo, class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_object_base_classes_base(Properties... ps)
      {
        using PossibleSinglePerItem = decltype(InterfaceDescription::get_per_item_property(OriginalProperties {}));

        auto ret = (Utility::TypeHolder<Utility::InvalidType> {} | ... |
                    get_single_property_object_base<std::is_same_v<Properties, PossibleSinglePerItem>, Final, Layout>(ps));

        return Utility::clean_type_holder(ret);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Final, class Layout, class OriginalProperties, class MetaInfo, class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_object_base_classes(Utility::TypeHolder<Properties...>)
      {
        constexpr auto ret = get_object_base_classes_base<Final, Layout, OriginalProperties, MetaInfo>(Properties {}...);

        using ObjectHolder = typename Layout::template object_holder<OriginalProperties>;

        using FinalTypeHolder = typename decltype(ret)::template prepend<ObjectHolder>;

        return DeriveFromAll<true, FinalTypeHolder> {};
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class ObjectType, bool ret = ObjectType::object_is_reference()>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool object_is_pointer_storage(ObjectType)
      {
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool object_is_pointer_storage(Ts...)
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class ObjectType, bool ret = ObjectType::is_write_proxy()>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool object_is_write_proxy(ObjectType)
      {
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool object_is_write_proxy(Ts...)
      {
        return false;
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool force_constant,
                                                               class CurrentObjectType,
                                                               class WantedProperties,
                                                               class MetaInfo,
                                                               class PtrType,
                                                               class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto build_multi_array_indexed_object(PtrType && ptr, Args &&... args)
      {
        using OriginalLayout = typename CurrentObjectType::OriginalLayout;

        using NewType = ProxyObject<OriginalLayout, std::decay_t<PtrType>, force_constant, true>;

        return Object<NewType, WantedProperties, typename MetaInfo::template override_extent<1>> {std::forward<PtrType>(ptr), std::forward<Args>(args)...};
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES JaggedVectorInvalidDefaultTypedefs
      {
        using value_type             = Utility::InvalidType;
        using reference              = Utility::InvalidType;
        using const_reference        = Utility::InvalidType;
        using pointer                = Utility::InvalidType;
        using const_pointer          = Utility::InvalidType;
        using size_type              = Utility::InvalidType;
        using difference_type        = Utility::InvalidType;
        using const_iterator         = Utility::InvalidType;
        using const_reverse_iterator = Utility::InvalidType;
        using iterator               = Utility::InvalidType;
        using reverse_iterator       = Utility::InvalidType;
        using ConstReadProxy         = Utility::InvalidType;
        using ReadProxy              = Utility::InvalidType;
        using WriteProxy             = Utility::InvalidType;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool valid, class ObjType, class JVProp, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES jagged_vector_types_getter_helper
      {
        using Type = JaggedVectorInvalidDefaultTypedefs;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class ObjType, class JVProp, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES jagged_vector_types_getter_helper<true, ObjType, JVProp, MetaInfo>
      {
        using Type = typename ObjType::template JaggedVectorCollectionType<JVProp, MetaInfo>;
      };

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class HasProp, class TestProp>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool global_property_checker_helper(HasProp, TestProp)
      {
        if constexpr (InterfaceDescription::is_global(HasProp {}))
          {
            if constexpr (InterfaceDescription::has_extra_write_proxy_types(HasProp {}))
              {
                return std::is_same_v<typename HasProp::ExtraWriteProxyProperty, TestProp>;
              }
          }
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Properties, class Property>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool global_property_checker(Utility::TypeHolder<Properties...>, Property)
      {
        return (global_property_checker_helper(Properties {}, Property {}) || ...);
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool force_constant,
                                                               class Property,
                                                               class OriginalProperties,
                                                               class MetaInfo,
                                                               class ObjectType,
                                                               class PtrType,
                                                               class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_new_object(PtrType && ptr, Args &&... args)
      {
        using WantedLayoutType =
          Collections::ProxyObject<typename ObjectType::OriginalLayout, std::decay_t<PtrType>, force_constant, ObjectType::stores_multi_array_index()>;

        using PotentialNewSizeTag =
          std::conditional_t<InterfaceDescription::track_individual_size(Property {}), Utility::TypeHolder<Property>, typename MetaInfo::SizeTag>;

        using MaybePropertyArray = decltype(InterfaceDescription::get_property_array_property(OriginalProperties {}));

        using MaybeJaggedVector = decltype(InterfaceDescription::get_jagged_vector_property(OriginalProperties {}));

        constexpr PropertiesIndexer extent_multiplier = (MaybePropertyArray::Properties::template contains<Property>() ? MaybePropertyArray::number : 1);

        using UpdatedMetaInfo = typename MetaInfo::template override_size_tag<PotentialNewSizeTag>::template multiply_extent<extent_multiplier>;

        constexpr InterfaceDescription::SupportedTypes type = InterfaceDescription::classify_property(Property {});

        static_assert(type != InterfaceDescription::SupportedTypes::Invalid, "Must use valid properties only!");

        if constexpr (std::is_same_v<Property, MaybePropertyArray>)
          {
            return Object<WantedLayoutType, typename Property::Properties, typename UpdatedMetaInfo::template multiply_extent<MaybePropertyArray::number>> {std::forward<PtrType>(ptr), std::forward<Args>(args)...};
          }
        else if constexpr (std::is_same_v<Property, MaybeJaggedVector>)
          {
            return Utility::InvalidType {};
            //TO-DO: Come up with a potential generalization that allows
            //       a full collection interface with sensible behaviour.
          }
        else if constexpr (type == InterfaceDescription::SupportedTypes::SubCollection)
          {
            return Object<WantedLayoutType, typename Property::Properties, UpdatedMetaInfo> {std::forward<PtrType>(ptr), std::forward<Args>(args)...};
          }
        else
          {
            return Object<WantedLayoutType, Utility::TypeHolder<Property>, UpdatedMetaInfo> {std::forward<PtrType>(ptr), std::forward<Args>(args)...};
          }
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, bool b>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TypeUnpacker
      {
        using Type = EmptyMultipleOperatorChecks;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool b>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TypeUnpacker<Utility::TypeHolder<Utility::InvalidType>, b>
      {
        using Type = EmptyMultipleOperatorChecks;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Ts, bool add_constant>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TypeUnpacker<Utility::TypeHolder<Ts...>, add_constant>
      {
        using Type = MultipleOperatorChecks<object_operators_helper<Ts, true, add_constant>...>;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool can_have_operators, class ObjBase, class ActualType>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES object_operator_checker
      {
       private:

        using BaseClasses = decltype(Utility::skip_first_type(get_derived_from_types(ObjBase {})));

        using BaseClassesWithType = decltype(BaseClasses {} | Utility::TypeHolder<ActualType> {});

       public:

        using const_operators = typename TypeUnpacker<BaseClassesWithType, true>::Type;

        using operators = typename TypeUnpacker<BaseClassesWithType, false>::Type;

        using operators_without_main_property = typename TypeUnpacker<BaseClasses, false>::Type;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class ObjBase, class ActualType>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES object_operator_checker<false, ObjBase, ActualType>
      {
        using const_operators = EmptyMultipleOperatorChecks;

        using operators = EmptyMultipleOperatorChecks;

        using operators_without_main_property = EmptyMultipleOperatorChecks;
      };

    }

  }

}

#endif
