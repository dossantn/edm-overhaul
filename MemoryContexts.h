//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MEMORYCONTEXTS_H
#define MARIONETTE_MEMORYCONTEXTS_H

#include <utility>
#include <type_traits>

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

namespace Marionette
{
  namespace MemoryContexts
  {
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES Invalid;

    ///@class StandardCPU
    ///Standard memory held on the CPU, no context information to keep.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES StandardCPU;

#if MARIONETTE_ENABLE_PLATFORM_SUPPORT_FOR_CUDA

    namespace impl_CUDA
    {
      enum class CUDA_allocation_types : PropertiesIndexer
      {
        GPU_normal,
        CPU_pinned,
        CPU_pinned_write_combined,
        managed,
        GPU_device_dynamic
      };
      //Of course there is a wider variety of possible allocations.
      //We may or may not wish to support them in the future,
      //depending on users' needs.
    }

    ///@class CUDAMemoryContext
    ///Stores CUDA-related memory allocations.
    ///
    ///If @p stores_stream is `true`, the context info will have a
    ///`stream` member variable that holds a @c cudaStream_t
    ///that should be used for all operations on the memory.
    ///The default value is the default stream.
    ///
    ///If @p stores_device is `true`, the context info will have a
    ///`device` member variable that holds an @c int
    ///identifying the device with which the memory is associated.
    ///The default value is to use the current device when the @c ContextInfo is constructed.
    ///
    ///@p allocation_type controls the actual type of memory that is allocated.
    ///
    ///@p stores_stream controls whether a specific stream is associated with all operations
    ///performed on this memory.
    ///If `false`, an additional (first) argument to `memset`, `memcpy_with_context`
    ///and all other copy-related operations (such as `special_copy`) allows specifying a specific stream,
    ///otherwise they will be performed synchronously.
    ///If `true`, operations will always be performed on the specified stream.
    ///Copies between memory associated with different streams will use the
    ///destination's stream (if any).
    ///For the particular case of managed memory, if @p stores_stream is `true`
    ///and the stream was set to something other than the default (0) stream,
    ///the memory will be associated with the stream through `cudaStreamAttachMemAsync`
    ///with `cudaMemAttachSingle`, which means that it will be undefined behaviour to
    ///access it on a device through any other stream, but simultaneous CPU and GPU
    ///access is possible (and there are other performance benefits).
    ///
    ///@p stores_device controls whether a specific device is associated with the memory.
    ///If `false`, an additional (second) argument to `memset`, `memcpy_with_context`
    ///and all other copy-related operations (such as `special_copy`) allows specifying a specific device
    ///(the current device is restored after the operations have been completed),
    ///otherwise the current device will be used.
    ///If `true`, the current device will be set to the context's device
    ///and restored upon completion.
    ///For the particular case of managed memory, the device is only set if @p stores_stream
    ///is also `true` (since streams are associated with specific devices),
    ///otherwise the managed memory transfer would work as usual.
    ///
    ///Except for `GPU_device_dynamic` memory, which is allocated on the device side,
    ///all other allocations and deallocations must happen from the host side.
    ///
    ///For normal GPU memory, if the CUDA runtime supports it,
    ///allocations will be asynchronous if @p stores_stream is `true`.
    ///
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <impl_CUDA::CUDA_allocation_types allocation_type,
                                                          bool                             stores_stream = false,
                                                          bool                             stores_device = false>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CUDAMemoryContext;

    using CUDAStandardGPU = CUDAMemoryContext<impl_CUDA::CUDA_allocation_types::GPU_normal>;

    using CUDAHostPinned = CUDAMemoryContext<impl_CUDA::CUDA_allocation_types::CPU_pinned>;

    using CUDAHostPinnedWriteCombined = CUDAMemoryContext<impl_CUDA::CUDA_allocation_types::CPU_pinned_write_combined>;

    using CUDAInDevice = CUDAMemoryContext<impl_CUDA::CUDA_allocation_types::GPU_device_dynamic>;

#endif
  }

}

#include "MemoryContexts/Detection.h"
//This will implement the logic for detecting the appropriate contexts...

#include "MemoryContexts/Generic.h"
#include "MemoryContexts/CPU.h"
#include "MemoryContexts/CUDA.h"

#endif
