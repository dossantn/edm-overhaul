//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_H
#define MARIONETTE_H

#include "MarionetteBase.h"

#include "VectorLikeWrapper.h"

#include "CollectionHelpers.h"

#include "ObjectHelpers.h"
#include "ObjectOperatorHelpers.h"
#include "ObjectTypes.h"
#include "Object.h"

#include "Collection.h"

#include "LayoutTypes.h"

#include "MarionetteTransfers.h"

#include "ExtraInformation.h"

#endif
