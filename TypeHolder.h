//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_TYPEHOLDER_H
#define MARIONETTE_TYPEHOLDER_H

#include <utility>

namespace Marionette
{
  namespace Utility
  {

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Types>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TypeHolder
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool contains()
      {
        return (std::is_same_v<T, Types> || ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... OtherTs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto append_helper(TypeHolder<OtherTs...>)
      {
        return TypeHolder<Types..., OtherTs...> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... OtherTs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto append_helper(OtherTs &&...)
      {
        return TypeHolder<Types..., std::decay_t<OtherTs>...> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... OtherTs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto prepend_helper(TypeHolder<OtherTs...>)
      {
        return TypeHolder<OtherTs..., Types...> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... OtherTs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto prepend_helper(OtherTs &&...)
      {
        return TypeHolder<std::decay_t<OtherTs>..., Types...> {};
      }

      template <class... OtherTs>
      using append = decltype(append_helper(std::declval<const OtherTs &>()...));

      template <class... OtherTs>
      using prepend = decltype(prepend_helper(std::declval<const OtherTs &>()...));

      template <template <class...> class Op, class... Args>
      using apply_type = TypeHolder<Op<Types, Args...>...>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <template <class...> class Op, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto apply()
      {
        return apply_type<Op, Args...> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto number()
      {
        return sizeof...(Types);
      }
    };

    ///@brief Append only new, non-invalid types.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts, class T>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(TypeHolder<Ts...> t, TypeHolder<T>)
    {
      if constexpr (t.template contains<T>())
        {
          return t;
        }
      else
        {
          return TypeHolder<Ts..., T> {};
        }
    }

    ///@brief Append only new, non-invalid types.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... T1s, class... T2s>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(TypeHolder<T1s...> t, TypeHolder<T2s...>)
    {
      return (t | ... | TypeHolder<T2s> {});
    }

    ///@brief Append only new, non-invalid types.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(TypeHolder<InvalidType>, TypeHolder<T> t)
    {
      return t;
    }

    ///@brief Append only new, non-invalid types.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(TypeHolder<Ts...> t, TypeHolder<InvalidType>)
    {
      return t;
    }

    ///@brief Append only new, non-invalid types.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(TypeHolder<InvalidType> t,
                                                                                                                                  TypeHolder<InvalidType>)
    {
      return t;
    }

    ///@brief Get only the first non-invalid type.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts, class T>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator&(TypeHolder<Ts...> t, TypeHolder<T>)
    {
      return t;
    }

    ///@brief Get only the first non-invalid type.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... T1s, class... T2s>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator&(TypeHolder<T1s...> t, TypeHolder<T2s...>)
    {
      return (t & ... & TypeHolder<T2s> {});
    }

    ///@brief Get only the first non-invalid type.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator&(TypeHolder<InvalidType>, TypeHolder<T> t)
    {
      return t;
    }

    ///@brief Get only the first non-invalid type.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator&(TypeHolder<Ts...> t, TypeHolder<InvalidType>)
    {
      return t;
    }

    ///@brief Get only the first non-invalid type.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator&(TypeHolder<InvalidType> t,
                                                                                                                                  TypeHolder<InvalidType>)
    {
      return t;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto clean_type_holder(TypeHolder<InvalidType, Ts...>)
    {
      return TypeHolder<Ts...> {};
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto clean_type_holder(TypeHolder<Ts...> t)
    {
      return t;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_first_type(TypeHolder<T, Ts...>)
    {
      return T {};
    }

    namespace impl
    {
      ///@brief Aids in suppressing spurious unused value warnings...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto unused_value_suppressor(T && t)
      {
        return std::forward<T>(t);
      }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_last_type(TypeHolder<Ts...>)
    {
      return (impl::unused_value_suppressor(Ts {}), ...);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_one_type_or_invalid(TypeHolder<Ts...>)
    {
      return InvalidType {};
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_one_type_or_invalid(TypeHolder<T>)
    {
      return T {};
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto skip_first_type(TypeHolder<T, Ts...>)
    {
      return TypeHolder<Ts...> {};
    }
  }
}

#endif
