//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MARIONETTETRANSFERS_H
#define MARIONETTE_MARIONETTETRANSFERS_H

#include <utility>
#include <type_traits>

#include "MarionetteBase.h"
#include "MarionetteTransfersHelpers.h"

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
  #include <cstdio>
#endif

namespace Marionette
{
  namespace Transfers
  {
    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Destination, class Source, TransferPriority priority>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_transfer_specification(const bool trace)
      {
        if constexpr (static_cast<PropertiesIndexer>(priority) <= static_cast<PropertiesIndexer>(TransferPriority::Invalid) ||
                      static_cast<PropertiesIndexer>(priority) > static_cast<PropertiesIndexer>(TransferPriority::Maximum))
          {
#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
            if (trace)
              {
                printf("{> INVALID <}");
              }
#endif
            return TransferSpecification<Destination, Source, TransferPriority::Invalid> {};
          }
        else
          {
            using CollectionCheckDest   = is_collection<Destination>;
            using CollectionCheckSource = is_collection<Source>;

            //By construction, we specialize on the CollectionHolders,
            //not the Collections themselves, so redirect there.
            if constexpr (CollectionCheckDest::value || CollectionCheckSource::value)
              {
#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
                if (trace)
                  {
                    printf("{> Unwrap (%d) <}", static_cast<int>(priority));
                  }
#endif
                return get_transfer_specification<typename CollectionCheckDest::MaybeUnwrapped, typename CollectionCheckSource::MaybeUnwrapped, priority>(
                  trace);
              }
            else
              {
                constexpr bool handle_defaults = static_cast<PropertiesIndexer>(priority) <= static_cast<PropertiesIndexer>(TransferPriority::Default);

                using IndirectCheckDest   = is_collection_holder_with_deferred_layout<Destination>;
                using IndirectCheckSource = is_collection_holder_with_deferred_layout<Source>;

                //For our default provided transfer specifications,
                //we always want to treat deferred layouts as the
                //original layout, to benefit from any improvements.
                //And we go back to the original priority since the user
                //may have provided a specialization for the undeferred layout.
                if constexpr (handle_defaults && (IndirectCheckDest::value || IndirectCheckSource::value))
                  {
#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
                    if (trace)
                      {
                        printf("{> Redirect (%d): %d %d <}",
                               static_cast<int>(priority),
                               static_cast<int>(IndirectCheckDest::value),
                               static_cast<int>(IndirectCheckSource::value));
                      }
#endif
                    return get_transfer_specification<typename IndirectCheckDest::MaybeUndeferred, typename IndirectCheckSource::MaybeUndeferred>(trace);
                  }
                else
                  {
                    using SimplificationCheckDest   = is_simplifiable<handle_defaults, Destination>;
                    using SimplificationCheckSource = is_simplifiable<handle_defaults, Source>;

                    //For our default provided transfer specifications,
                    //we also want to get rid of NoObject no-ops,
                    //as (even when forbidding slicing copies)
                    //we can by definition skip those...
                    //And we go back to the original priority since the user
                    //may have provided a specialization for the simplified layout.
                    if constexpr (handle_defaults && (SimplificationCheckDest::value || SimplificationCheckSource::value))
                      {
#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
                        if (trace)
                          {
                            printf("{> Simplify (%d): %d %d <}",
                                   static_cast<int>(priority),
                                   static_cast<int>(SimplificationCheckDest::value),
                                   static_cast<int>(SimplificationCheckSource::value));
                          }
#endif
                        return get_transfer_specification<typename SimplificationCheckDest::MaybeSimplified,
                                                          typename SimplificationCheckSource::MaybeSimplified>(trace);
                      }
                    else
                      {
                        using TransSpec = TransferSpecification<Destination, Source, priority>;

                        if constexpr (is_transfer_specification_valid_v<TransSpec>)
                          {
#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
                            if (trace)
                              {
                                printf("{> %ld (%d) <}", TransSpec::transfer_ID(false), static_cast<int>(priority));
                              }
#endif
                            return TransSpec {};
                          }
                        else if constexpr (static_cast<PropertiesIndexer>(priority) > static_cast<PropertiesIndexer>(TransferPriority::Minimum))
                          {
                            constexpr TransferPriority new_priority = static_cast<TransferPriority>(static_cast<PropertiesIndexer>(priority) - 1);

                            static_assert(static_cast<PropertiesIndexer>(new_priority) < static_cast<PropertiesIndexer>(priority));

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
                            if (trace)
                              {
                                printf("{> (%d -> %d) <}", static_cast<int>(priority), static_cast<int>(new_priority));
                              }
#endif
                            return get_transfer_specification<Destination, Source, new_priority>(trace);
                          }
                        else
                          {
#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
                            if (trace)
                              {
                                printf("{> INVALID <}");
                              }
#endif
                            return TransferSpecification<Destination, Source, TransferPriority::Invalid> {};
                          }
                      }
                  }
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Destination, class Source, TransferPriority priority>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecificationGetter
      {
        using Type = decltype(get_transfer_specification<Destination, Source, priority>());
      };

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Destination, class Extra>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification<Destination, Utility::DefaultInitializer, TransferPriority::Maximum, Extra>
    {
     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike &, const SourceLike &)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike &, const SourceLike &, Args &&...)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &,
                                                                                            const SourceLike &,
                                                                                            const typename DestLike::size_type,
                                                                                            [[maybe_unused]] const typename DestLike::size_type   d_offset = 0,
                                                                                            [[maybe_unused]] const typename SourceLike::size_type s_offset = 0)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike &, SourceLike &&)
      {
      }

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr long
      transfer_ID(const bool trace = false)
      {
        if (trace)
          {
            printf("|> %d : %s : %05d <|", 0, __FILE__, __LINE__);
          }
        return 0;
      }
#endif
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class DestLayout, class SourceLayout, class Property>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    ArrayTransferSpecification<DestLayout,
                               SourceLayout,
                               Property,
                               std::enable_if_t<impl::layout_can_get_array_pointers_v<SourceLayout> && impl::layout_can_get_array_pointers_v<DestLayout>>>
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_transfer(const PropertiesIndexer              dst_mult_arr_idx,
                                                                                                   DestLike &                           dest,
                                                                                                   const PropertiesIndexer              src_mult_arr_idx,
                                                                                                   const SourceLike &                   src,
                                                                                                   const typename DestLike::size_type   num,
                                                                                                   const typename DestLike::size_type   d_offset,
                                                                                                   const typename SourceLike::size_type s_offset,
                                                                                                   Args &&... args)
      {
        const auto * src_ptr = Utility::FriendshipProvider::template get_array_pointer<Property>(src, src_mult_arr_idx, s_offset);

        Utility::FriendshipProvider::template array_copy_to<Property>(dest,
                                                                      dst_mult_arr_idx,
                                                                      src.memory_context(),
                                                                      src.memory_context_info(),
                                                                      src_ptr,
                                                                      num,
                                                                      d_offset,
                                                                      std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void transfer(const PropertiesIndexer              dst_mult_arr_idx,
                                                                                           DestLike &                           dest,
                                                                                           const PropertiesIndexer              src_mult_arr_idx,
                                                                                           const SourceLike &                   src,
                                                                                           const typename DestLike::size_type   num,
                                                                                           const typename DestLike::size_type   d_offset,
                                                                                           const typename SourceLike::size_type s_offset)
      {
        const auto * src_ptr = Utility::FriendshipProvider::template get_array_pointer<Property>(src, src_mult_arr_idx, s_offset);

        Utility::FriendshipProvider::template array_copy_to<Property>(dest,
                                                                      dst_mult_arr_idx,
                                                                      src.memory_context(),
                                                                      src.memory_context_info(),
                                                                      src_ptr,
                                                                      num,
                                                                      d_offset);
      }
    };

    //Now follow some ugly template class template function definitions
    //to handle the generic (invalid) TransferSpecification.

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Destination, class Source, TransferPriority priority, class Extra>
    template <class DestLike, class SourceLike>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void TransferSpecification<Destination, Source, priority, Extra>::copy(DestLike &         d,
                                                                                                                                           const SourceLike & s)
    {
      (void) d;
      (void) s;
#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
      printf("Trying to copy generic!\n");
      assert(d.size() + s.size() < 0);
#else
      static_assert(Utility::always_false<DestLike>, "Invalid copy! Re-check your transfer specifications or turn on debug.");
#endif
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Destination, class Source, TransferPriority priority, class Extra>
    template <class DestLike, class SourceLike, class... Args>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
    TransferSpecification<Destination, Source, priority, Extra>::special_copy(DestLike & d, const SourceLike & s, Args &&...)
    {
      (void) d;
      (void) s;
#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
      printf("Trying to special copy generic!\n");
      assert(d.size() + s.size() < 0);
#else
      static_assert(Utility::always_false<DestLike>, "Invalid special_copy! Re-check your transfer specifications or turn on debug.");
#endif
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Destination, class Source, TransferPriority priority, class Extra>
    template <class DestLike, class SourceLike>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
    TransferSpecification<Destination, Source, priority, Extra>::copy_some(DestLike &                           d,
                                                                           const SourceLike &                   s,
                                                                           const typename DestLike::size_type   num,
                                                                           const typename DestLike::size_type   d_offset,
                                                                           const typename SourceLike::size_type s_offset)
    {
      (void) d;
      (void) s;
      (void) num;
      (void) d_offset;
      (void) s_offset;

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
      printf("Trying to copy_some generic!");
      assert(d.size() + s.size() < 0);
#else
      static_assert(Utility::always_false<DestLike>, "Invalid copy_some! Re-check your transfer specifications or turn on debug.");
#endif
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Destination, class Source, TransferPriority priority, class Extra>
    template <class DestLike, class SourceLike>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void TransferSpecification<Destination, Source, priority, Extra>::move(DestLike &    d,
                                                                                                                                           SourceLike && s)
    {
      (void) d;
      (void) s;
#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
      printf("Trying to move generic!\n");
      assert(d.size() + s.size() < 0);
#else
      static_assert(Utility::always_false<DestLike>, "Invalid move! Re-check your transfer specifications or turn on debug.");
#endif
    }

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Destination, class Source, TransferPriority priority, class Extra>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr long
    TransferSpecification<Destination, Source, priority, Extra>::transfer_ID(const bool trace)
    {
      if (trace)
        {
          printf("|> INVALID <|");
        }
      return -0x7FFFFFFF;
    }
#endif
  }
}

#include "MarionetteTransfersObjects.h"
#include "MarionetteTransfersCollections.h"

#endif
