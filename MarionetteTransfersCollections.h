//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MARIONETTETRANSFERSCOLLECTIONS_H
#define MARIONETTE_MARIONETTETRANSFERSCOLLECTIONS_H

#include <utility>
#include <type_traits>

#include "MarionetteBase.h"
#include "CollectionHelpers.h"
#include "MarionetteTransfersHelpers.h"
#include "ExtraInformation.h"

namespace Marionette
{
  namespace Transfers
  {

#if MARIONETTE_WARN_ON_JAGGED_VECTOR_NON_RESIZING_COPY
    namespace impl
    {
      template <bool b>
      struct warn_on_jagged_vector_non_resizing_copy
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_jv_non_resizing_copy()
        {
        }
      };

      template <>
      struct warn_on_jagged_vector_non_resizing_copy<true>
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        [[deprecated("This is not really a deprecation warning. "
                     "The code is performing a copy of a collection that includes jagged vectors "
                     "and that cannot be resized. This may lead to data truncation and/or undefined behaviour. "
                     "Define 'MARIONETTE_WARN_ON_JAGGED_VECTOR_NON_RESIZING_COPY' to 0 to suppress this warning.")]]
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_jv_non_resizing_copy()
        {
        }
      };
    }
#endif

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... SourceProps,
                                                          class... DestProps,
                                                          class SourceLayout,
                                                          class DestLayout,
                                                          class SourceMetaInfo,
                                                          class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::CollectionHolder<DestLayout, Utility::TypeHolder<DestProps...>, DestMetaInfo>,
                          Collections::CollectionHolder<SourceLayout, Utility::TypeHolder<SourceProps...>, SourceMetaInfo>,
                          TransferPriority::DefaultTertiary,
                          std::enable_if_t<DestMetaInfo::extent == SourceMetaInfo::extent>>

    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(31);

     private:

      using SourceTypeHolder = Utility::TypeHolder<SourceProps...>;
      using DestTypeHolder   = Utility::TypeHolder<DestProps...>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return impl::properties_equivalent(DestTypeHolder {}, SourceTypeHolder {}) && DestLayout::interface_properties().can_copy_into_collection() &&
               SourceLayout::interface_properties().can_copy_out_of_collection();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return impl::properties_equivalent(DestTypeHolder {}, SourceTypeHolder {}) && DestLayout::interface_properties().can_move_into_collection() &&
               SourceLayout::interface_properties().can_move_out_of_collection();
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop, class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_helper(DestLike & d, const SourceLike & s, Args &&... args)
      {
        if constexpr (sizeof...(args) > 0)
          {
            Utility::FriendshipProvider::template get_actual_collection<Prop>(d).special_copy(
              Utility::FriendshipProvider::template get_actual_collection<Prop>(s),
              std::forward<Args>(args)...);
          }
        else
          {
            Utility::FriendshipProvider::template get_actual_collection<Prop>(d) = Utility::FriendshipProvider::template get_actual_collection<Prop>(s);
          }
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        if constexpr (DestLike::interface_properties().can_resize_fully())
          {
            d.resize(s.size(), false);
          }
#if MARIONETTE_WARN_ON_JAGGED_VECTOR_NON_RESIZING_COPY
        else if constexpr (!DestLike::interface_properties().can_resize_partially() && d.template is_jagged_vector_collection<false>())
          {
            impl::warn_on_jagged_vector_non_resizing_copy<d.template is_jagged_vector_collection<false>()>::warn_jv_non_resizing_copy();
          }
#endif

        (copy_helper<DestProps>(d, s), ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        if constexpr (DestLike::interface_properties().can_resize_fully())
          {
            d.resize(s.size(), false);
          }

        (copy_helper<DestProps>(d, s, std::forward<Args>(args)...), ...);
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some_helper(Prop                                 p,
                                                                                                   DestLike &                           d,
                                                                                                   const SourceLike &                   s,
                                                                                                   const typename DestLike::size_type   num,
                                                                                                   const typename DestLike::size_type   d_offset,
                                                                                                   const typename SourceLike::size_type s_offset)
      {
        if constexpr (!InterfaceDescription::is_global(p))
          {
            auto && dest_c   = d.template get_collection<Prop>();
            auto && source_c = s.template get_collection<Prop>();

            Transfer<std::decay_t<decltype(dest_c)>, std::decay_t<decltype(source_c)>>::copy_some(dest_c, source_c, num, d_offset, s_offset);
          }
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                           d,
                                                                                            const SourceLike &                   s,
                                                                                            const typename DestLike::size_type   num,
                                                                                            const typename DestLike::size_type   d_offset = 0,
                                                                                            const typename SourceLike::size_type s_offset = 0)
      {
        (copy_some_helper(DestProps {}, d, s, num, d_offset, s_offset), ...);
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move_helper(DestLike & d, SourceLike && s)
      {
        Utility::FriendshipProvider::template get_actual_collection<Prop>(d) =
          std::move(Utility::FriendshipProvider::template get_actual_collection<Prop>(std::move(s)));
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        (move_helper<DestProps>(d, std::move(s)), ...);
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Properties,
                                                          class Layout,
                                                          class MetaInfo,
                                                          Utility::ArrayInitialize initialization,
                                                          class... IdxTpMv,
                                                          class Extra>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::CollectionHolder<Layout, Utility::TypeHolder<Properties...>, MetaInfo>,
                          Utility::ArrayInitializationHelper<initialization, IdxTpMv...>,
                          TransferPriority::DefaultTertiary,
                          Extra>

    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(32);

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool get_actual, PropertiesIndexer idx, class Coll>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) getter(Coll && o)
      {
        if constexpr (get_actual)
          {
            return Utility::FriendshipProvider::template get_actual_multi_array<idx>(std::forward<Coll>(o));
          }
        else
          {
            return Utility::FriendshipProvider::template get_multi_array<idx>(std::forward<Coll>(o));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void single_transfer(DestLike & d, SourceLike && s, Args &&... args)
      {
        if constexpr (sizeof...(args) > 0)
          {
            getter<true, idx>(d).special_copy(getter<false, idx>(std::forward<SourceLike>(s)), std::forward<Args>(args)...);
          }
        else
          {
            getter<true, idx>(d) = getter<false, idx>(std::forward<SourceLike>(s));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, PropertiesIndexer... idx, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      multi_transfer(const std::integer_sequence<PropertiesIndexer, idx...> &, DestLike & d, SourceLike && s, Args &&... args)
      {
        (single_transfer<idx>(d, std::forward<SourceLike>(s), std::forward<Args>(args)...), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return Layout::interface_properties().can_copy_into_collection();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return Layout::interface_properties().can_move_out_of_collection() && MetaInfo::extent <= sizeof...(IdxTpMv);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        multi_transfer(std::make_integer_sequence<PropertiesIndexer, MetaInfo::extent>(), d, s);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        multi_transfer(std::make_integer_sequence<PropertiesIndexer, MetaInfo::extent>(), d, s, std::forward<Args>(args)...);
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestC, class SrcC>
      //Pass by value since they are going to be views.
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_some_helper(DestC d, SrcC s, const typename DestC::size_type num, const typename DestC::size_type d_offset, const typename SrcC::size_type s_offset)
      {
        Transfer<DestC, SrcC>::copy_some(d, s, num, d_offset, s_offset);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, PropertiesIndexer... idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void multi_some_copier(const std::integer_sequence<PropertiesIndexer, idx...> &,
                                                                                                    DestLike &                           d,
                                                                                                    const SourceLike &                   s,
                                                                                                    const typename DestLike::size_type   num,
                                                                                                    const typename DestLike::size_type   d_offset,
                                                                                                    const typename SourceLike::size_type s_offset)
      {
        (copy_some_helper(getter<true, idx>(d), getter<false, idx>(s), num, d_offset, s_offset), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                           d,
                                                                                            const SourceLike &                   s,
                                                                                            const typename DestLike::size_type   num,
                                                                                            const typename DestLike::size_type   d_offset = 0,
                                                                                            const typename SourceLike::size_type s_offset = 0)
      {
        multi_some_copier(std::make_integer_sequence<PropertiesIndexer, MetaInfo::extent>(), d, s, num, d_offset, s_offset);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        multi_transfer(std::make_integer_sequence<PropertiesIndexer, MetaInfo::extent>(), d, std::move(s));
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class SourceLayout, class DestLayout, class SourceMetaInfo, class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::CollectionHolder<DestLayout, Utility::TypeHolder<Property>, DestMetaInfo>,
                          Collections::CollectionHolder<SourceLayout, Utility::TypeHolder<Property>, SourceMetaInfo>,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<(DestMetaInfo::extent == SourceMetaInfo::extent) &&
                                           (impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::PropertyArray)>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(33);

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class Coll>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto actual_getter(Coll && c)
      {
        auto general = Utility::FriendshipProvider::template get_actual_collection<Property>(std::forward<Coll>(c));

        return Utility::FriendshipProvider::template get_actual_multi_array<idx>(general);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer... idx, class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_helper(std::integer_sequence<PropertiesIndexer, idx...>, DestLike & d, const SourceLike & s, Args &&... args)
      {
        if constexpr (sizeof...(args) > 0)
          {
            (actual_getter<idx>(d).special_copy(s[idx], std::forward<Args>(args)...), ...);
          }
        else
          {
            ((actual_getter<idx>(d) = s[idx]), ...);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer... idx, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      move_helper(std::integer_sequence<PropertiesIndexer, idx...>, DestLike & d, SourceLike && s)
      {
        ((actual_getter<idx>(d) = std::move(actual_getter<idx>(std::move(s)))), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_collection() && SourceLayout::interface_properties().can_copy_out_of_collection();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return DestLayout::interface_properties().can_move_into_collection() && SourceLayout::interface_properties().can_move_out_of_collection();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        copy_helper(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, s);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        copy_helper(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, s, std::forward<Args>(args)...);
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestC, class SrcC>
      //Pass by value since they are going to be views.
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_some_helper(DestC d, SrcC s, const typename DestC::size_type num, const typename DestC::size_type d_offset, const typename SrcC::size_type s_offset)
      {
        Transfer<DestC, SrcC>::copy_some(d, s, num, d_offset, s_offset);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, PropertiesIndexer... idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void multi_some_copier(const std::integer_sequence<PropertiesIndexer, idx...> &,
                                                                                                    DestLike &                           d,
                                                                                                    const SourceLike &                   s,
                                                                                                    const typename DestLike::size_type   num,
                                                                                                    const typename DestLike::size_type   d_offset,
                                                                                                    const typename SourceLike::size_type s_offset)
      {
        (copy_some_helper(d[idx], s[idx], num, d_offset, s_offset), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                           d,
                                                                                            const SourceLike &                   s,
                                                                                            const typename DestLike::size_type   num,
                                                                                            const typename DestLike::size_type   d_offset = 0,
                                                                                            const typename SourceLike::size_type s_offset = 0)
      {
        multi_some_copier(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, s, num, d_offset, s_offset);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        move_helper(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, std::move(s));
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property,
                                                          class Layout,
                                                          class MetaInfo,
                                                          Utility::ArrayInitialize initialization,
                                                          class... IdxTpMv>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::CollectionHolder<Layout, Utility::TypeHolder<Property>, MetaInfo>,
                          Utility::ArrayInitializationHelper<initialization, IdxTpMv...>,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::PropertyArray>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(34);

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class Coll>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto actual_getter(Coll & c)
      {
        auto general = Utility::FriendshipProvider::template get_actual_collection<Property>(c);

        return Utility::FriendshipProvider::template get_actual_multi_array<idx>(general);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void single_copier(DestLike & d, const SourceLike & s, Args &&... args)
      {
        if constexpr (sizeof...(args) > 0)
          {
            actual_getter<idx>(d).special_copy(s.template get_multi_array<idx>(), std::forward<Args>(args)...);
          }
        else
          {
            actual_getter<idx>(d) = s.template get_multi_array<idx>();
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer... idx, class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_helper(std::integer_sequence<PropertiesIndexer, idx...>, DestLike & d, const SourceLike & s, Args &&... args)
      {
        (single_copier<idx>(d, s, std::forward<Args>(args)...), ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void single_mover(DestLike & d, SourceLike && s)
      {
        actual_getter<idx>(d) = std::move(std::move(s).template get_multi_array<idx>());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer... idx, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      move_helper(std::integer_sequence<PropertiesIndexer, idx...>, DestLike & d, SourceLike && s)
      {
        (single_mover<idx>(d, std::move(s)), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return Layout::interface_properties().can_copy_into_collection() &&
               (std::is_assignable_v<Collections::Collection<Layout, typename Property::Properties, MetaInfo> &,
                                     const std::decay_t<typename IdxTpMv::Type> &> &&
                ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return Layout::interface_properties().can_copy_into_collection() && Property::number <= sizeof...(IdxTpMv) &&
               (std::is_assignable_v<Collections::Collection<Layout, typename Property::Properties, MetaInfo> &, std::decay_t<typename IdxTpMv::Type> &&> &&
                ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        copy_helper(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, s);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        copy_helper(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, s, std::forward<Args>(args)...);
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestC, class SrcC>
      //Pass by value since they are going to be views.
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_some_helper(DestC d, SrcC s, const typename DestC::size_type num, const typename DestC::size_type d_offset, const typename SrcC::size_type s_offset)
      {
        Transfer<DestC, SrcC>::copy_some(d, s, num, d_offset, s_offset);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, PropertiesIndexer... idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void multi_some_copier(const std::integer_sequence<PropertiesIndexer, idx...> &,
                                                                                                    DestLike &                           d,
                                                                                                    const SourceLike &                   s,
                                                                                                    const typename DestLike::size_type   num,
                                                                                                    const typename DestLike::size_type   d_offset,
                                                                                                    const typename SourceLike::size_type s_offset)
      {
        (copy_some_helper(d[idx], s[idx], num, d_offset, s_offset), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                           d,
                                                                                            const SourceLike &                   s,
                                                                                            const typename DestLike::size_type   num,
                                                                                            const typename DestLike::size_type   d_offset = 0,
                                                                                            const typename SourceLike::size_type s_offset = 0)
      {
        multi_some_copier(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, s, num, d_offset, s_offset);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        move_helper(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, std::move(s));
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class SourceLayout, class DestLayout, class SourceMetaInfo, class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::CollectionHolder<DestLayout, Utility::TypeHolder<Property>, DestMetaInfo>,
                          Collections::CollectionHolder<SourceLayout, Utility::TypeHolder<Property>, SourceMetaInfo>,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::NoObject>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(35);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike &, const SourceLike &)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike &, const SourceLike &, Args &&...)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &,
                                                                                            const SourceLike &,
                                                                                            const typename DestLike::size_type,
                                                                                            [[maybe_unused]] const typename DestLike::size_type   d_offset = 0,
                                                                                            [[maybe_unused]] const typename SourceLike::size_type s_offset = 0)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike &, SourceLike &&)
      {
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class SourceLayout, class DestLayout, class SourceMetaInfo, class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::CollectionHolder<DestLayout, Utility::TypeHolder<Property>, DestMetaInfo>,
                          Collections::CollectionHolder<SourceLayout, Utility::TypeHolder<Property>, SourceMetaInfo>,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<(impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::PerItemObject) &&
                                           (DestMetaInfo::extent == SourceMetaInfo::extent)>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(36);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_collection() && SourceLayout::interface_properties().can_copy_out_of_collection();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return false;
        //Since we will copy between the arrays, we're not moving anything.
        //Specializations may (and should) move if and when appropriate.

        //DestLayout::interface_properties().can_move_into_collection() && SourceLayout::interface_properties().can_move_out_of_collection();
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_helper(DestLike & d, const SourceLike & s, Args &&... args)
      {
        if constexpr (DestLike::interface_properties().can_resize_fully())
          {
            d.resize(s.size(), false);
          }

        const typename DestLike::size_type d_size   = d.size();
        const typename DestLike::size_type s_size   = static_cast<typename DestLike::size_type>(s.size());
        const typename DestLike::size_type min_size = (d_size > s_size ? s_size : d_size);

        for (PropertiesIndexer i = 0; i < DestMetaInfo::extent; ++i)
          {
            const PropertiesIndexer d_idx = Utility::FriendshipProvider::get_multi_array_index(d) * DestMetaInfo::extent + i;
            const PropertiesIndexer s_idx = Utility::FriendshipProvider::get_multi_array_index(s) * DestMetaInfo::extent + i;

            if constexpr (sizeof...(args) > 0)
              {
                ArrayTransferSpecification<DestLayout, SourceLayout, Property>::special_transfer(d_idx,
                                                                                                 d,
                                                                                                 s_idx,
                                                                                                 s,
                                                                                                 min_size,
                                                                                                 0,
                                                                                                 0,
                                                                                                 std::forward<Args>(args)...);
              }
            else
              {
                ArrayTransferSpecification<DestLayout, SourceLayout, Property>::transfer(d_idx, d, s_idx, s, min_size, 0, 0);
              }
          }
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        copy_helper(d, s);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        copy_helper(d, s, std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                                            d,
                                                                                            const SourceLike &                                    s,
                                                                                            const typename DestLike::size_type                    num,
                                                                                            [[maybe_unused]] const typename DestLike::size_type   d_offset = 0,
                                                                                            [[maybe_unused]] const typename SourceLike::size_type s_offset = 0)
      {
        for (PropertiesIndexer i = 0; i < DestMetaInfo::extent; ++i)
          {
            const PropertiesIndexer d_idx = Utility::FriendshipProvider::get_multi_array_index(d) * DestMetaInfo::extent + i;
            const PropertiesIndexer s_idx = Utility::FriendshipProvider::get_multi_array_index(s) * DestMetaInfo::extent + i;

            ArrayTransferSpecification<DestLayout, SourceLayout, Property>::transfer(d_idx, d, s_idx, s, num, d_offset, s_offset);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike &, SourceLike &&) = delete;
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class Layout, class MetaInfo, class T>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::CollectionHolder<Layout, Utility::TypeHolder<Property>, MetaInfo>,
                          T,
                          TransferPriority::DefaultTertiary,
                          std::enable_if_t<(impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::PerItemObject) &&
                                           !impl::is_collection_v<T> && !impl::is_collection_holder_v<T> && ExtraInformation::VectorLike<T>::is_valid()>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(37);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return Layout::interface_properties().can_copy_into_collection() && std::is_same_v<typename T::value_type, typename Property::Type> &&
               std::is_convertible_v<typename T::size_type, typename Layout::size_type>;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return false;
        //Since we will copy between the arrays, we're not moving anything.
        //Specializations may (and should) move if and when appropriate.
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_helper(DestLike & d, const SourceLike & s, Args &&... args)
      {
        if constexpr (DestLike::interface_properties().can_resize_fully())
          {
            d.resize(static_cast<typename DestLike::size_type>(s.size()), false);
          }

        const typename DestLike::size_type d_size   = d.size();
        const typename DestLike::size_type s_size   = static_cast<typename DestLike::size_type>(s.size());
        const typename DestLike::size_type min_size = (d_size > s_size ? s_size : d_size);

        for (PropertiesIndexer i = 0; i < MetaInfo::extent; ++i)
          {
            const PropertiesIndexer idx = Utility::FriendshipProvider::get_multi_array_index(d) * MetaInfo::extent + i;

            using ExtraInfo = ExtraInformation::VectorLike<T>;

            Utility::FriendshipProvider::template array_copy_to<Property>(d,
                                                                          idx,
                                                                          ExtraInfo::memory_context(),
                                                                          typename std::decay_t<decltype(ExtraInfo::memory_context())>::ContextInfo {},
                                                                          //Default constructed context info should be acceptable.
                                                                          ExtraInfo::get_start_of_data(s),
                                                                          min_size,
                                                                          static_cast<typename DestLike::size_type>(0),
                                                                          std::forward<Args>(args)...);
          }
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        copy_helper(d, s);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        copy_helper(d, s, std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                                            d,
                                                                                            const SourceLike &                                    s,
                                                                                            const typename DestLike::size_type                    num,
                                                                                            [[maybe_unused]] const typename DestLike::size_type   d_offset = 0,
                                                                                            [[maybe_unused]] const typename SourceLike::size_type s_offset = 0)
      {
        for (PropertiesIndexer i = 0; i < MetaInfo::extent; ++i)
          {
            const PropertiesIndexer idx = Utility::FriendshipProvider::get_multi_array_index(d) * MetaInfo::extent + i;

            using ExtraInfo = ExtraInformation::VectorLike<T>;

            Utility::FriendshipProvider::template array_copy_to<Property>(d,
                                                                          idx,
                                                                          ExtraInfo::memory_context(),
                                                                          typename std::decay_t<decltype(ExtraInfo::memory_context())>::ContextInfo {},
                                                                          //Default constructed context info should be acceptable.
                                                                          ExtraInfo::get_start_of_data(s) + s_offset,
                                                                          num,
                                                                          d_offset);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike &, SourceLike &&) = delete;
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx, class... TgPropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto prop_ex_to_size_tag(PropEx, Utility::TypeHolder<TgPropExs...>)
      {
        auto ret = (Utility::TypeHolder<Utility::InvalidType> {} & ... &
                    std::conditional_t<TgPropExs::PropertiesAndExtents::template contains<PropEx>(),
                                       Utility::TypeHolder<typename TgPropExs::SizeTag>,
                                       Utility::TypeHolder<Utility::InvalidType>> {});
        return Utility::get_one_type_or_invalid(ret);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TgPropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto list_only_size_tags(Utility::TypeHolder<TgPropExs...>)
      {
        return Utility::TypeHolder<typename TgPropExs::SizeTag...> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class DestProps, class SourceProps, class DestMetaInfo, class SourceMetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES collection_transfer_helper
      {
        using DestPropInfo   = InterfaceDescription::InterfaceInformation<DestProps>;
        using SourcePropInfo = InterfaceDescription::InterfaceInformation<SourceProps>;

        using DestSizeTagInfo   = typename DestPropInfo::template necessary_size_tags<typename DestMetaInfo::SizeTag>;
        using SourceSizeTagInfo = typename SourcePropInfo::template necessary_size_tags<typename SourceMetaInfo::SizeTag>;

        using DestSizeTags   = decltype(list_only_size_tags(DestSizeTagInfo {}));
        using SourceSizeTags = decltype(list_only_size_tags(SourceSizeTagInfo {}));

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class TgPropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto get_equivalent_source_size_tag(TgPropEx)
        {
          return prop_ex_to_size_tag(Utility::get_first_type(typename TgPropEx::PropertiesAndExtents {}), SourceSizeTagInfo {});
        }

        template <class TgPropEx>
        using EquivalentSizeTag = decltype(get_equivalent_source_size_tag(TgPropEx {}));

       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TgPropExs>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_transfer_helper(Utility::TypeHolder<TgPropExs...>)
        {
          return (!std::is_same_v<decltype(get_equivalent_source_size_tag(TgPropExs {})), Utility::InvalidType> && ...);
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_transfer()
        {
          return properties_equivalent(typename DestPropInfo::template flattened_per_item<true, true, true> {},
                                       typename SourcePropInfo::template flattened_per_item<true, true, true> {}) &&
                 can_transfer_helper(DestSizeTagInfo {});
        }
      };
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class SourceProps,
                                                          class DestProps,
                                                          class SourceLayout,
                                                          class DestLayout,
                                                          class SourceMetaInfo,
                                                          class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification<Collections::CollectionHolder<DestLayout, DestProps, DestMetaInfo>,
                                                                                    Collections::CollectionHolder<SourceLayout, SourceProps, SourceMetaInfo>,
                                                                                    TransferPriority::DefaultPrimary,
                                                                                    std::enable_if_t<DestMetaInfo::extent == SourceMetaInfo::extent>>

    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(39);

     private:

      using Helper = impl::collection_transfer_helper<DestProps, SourceProps, DestMetaInfo, SourceMetaInfo>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_collection() && SourceLayout::interface_properties().can_copy_out_of_collection() &&
               Helper::can_transfer();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return false;
        //Since all we'll be doing is copying stuff around,
        //we'll only provide copy assignment, not moving.
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool do_resize, class PropEx, class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_one_base(PropEx,
                                                                                                const PropertiesIndexer            d_index,
                                                                                                const PropertiesIndexer            s_index,
                                                                                                DestLike &                         d,
                                                                                                const SourceLike &                 s,
                                                                                                const typename DestLike::size_type num,
                                                                                                Args &&... args)
      {
        if constexpr (InterfaceDescription::classify_property(typename PropEx::Property{}) == InterfaceDescription::SupportedTypes::PerItemObject)
          {
            if constexpr (do_resize)
              {
                Utility::FriendshipProvider::template array_resize<typename PropEx::Property>(d, d_index, num);
              }

            if constexpr (sizeof...(Args) > 0)
              {
                ArrayTransferSpecification<DestLayout, SourceLayout, typename PropEx::Property>::special_transfer(d_index,
                                                                                                                  d,
                                                                                                                  s_index,
                                                                                                                  s,
                                                                                                                  num,
                                                                                                                  0,
                                                                                                                  0,
                                                                                                                  std::forward<Args>(args)...);
              }
            else
              {
                ArrayTransferSpecification<DestLayout, SourceLayout, typename PropEx::Property>::transfer(d_index, d, s_index, s, num, 0, 0);
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool do_resize, class PropEx, class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_one(PropEx p, DestLike & d, const SourceLike & s, const typename DestLike::size_type num, Args &&... args)
      {
        if constexpr (InterfaceDescription::classify_property(typename PropEx::Property{}) == InterfaceDescription::SupportedTypes::PerItemObject)
          {
            const PropertiesIndexer d_base_index = Utility::FriendshipProvider::get_multi_array_index(d) * PropEx::extent * DestMetaInfo::extent;
            const PropertiesIndexer s_base_index = Utility::FriendshipProvider::get_multi_array_index(s) * PropEx::extent * DestMetaInfo::extent;

            for (PropertiesIndexer i = 0; i < PropEx::extent * DestMetaInfo::extent; ++i)
              {
                copy_one_base<do_resize>(p, d_base_index + i, s_base_index + i, d, s, num, std::forward<Args>(args)...);
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool propagate_array_indices,
                                                               class TgPropEx,
                                                               class... PropExs,
                                                               class DestLike,
                                                               class SourceLike,
                                                               class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_size_tag_base(TgPropEx,
                                                                                                     Utility::TypeHolder<PropExs...>,
                                                                                                     const PropertiesIndexer d_index,
                                                                                                     const PropertiesIndexer s_index,
                                                                                                     DestLike &              d,
                                                                                                     const SourceLike &      s,
                                                                                                     Args &&... args)
      {
        constexpr bool do_resize =
          DestLike::interface_properties().can_resize_fully() ||
          (DestLike::interface_properties().can_resize_partially() && !std::is_same_v<typename TgPropEx::SizeTag, typename DestMetaInfo::SizeTag>);

        using SourceSizeTag = typename Helper::template EquivalentSizeTag<TgPropEx>;

        const typename SourceLike::size_type s_size = Utility::FriendshipProvider::template get_tagged_size<SourceSizeTag>(s, s_index);

        typename DestLike::size_type d_size = static_cast<typename DestLike::size_type>(s_size);

        if constexpr (do_resize)
          {
            Utility::FriendshipProvider::template base_resize<typename TgPropEx::SizeTag, 1>(d, d_index, d_size);
          }
        else
          {
            const typename DestLike::size_type current_d_size = Utility::FriendshipProvider::template get_tagged_size<typename TgPropEx::SizeTag>(d, d_index);

            if (current_d_size < d_size)
              {
                d_size = current_d_size;
              }
          }

        if constexpr (propagate_array_indices)
          {
            (copy_one_base<do_resize>(PropExs {}, d_index, s_index, d, s, d_size, std::forward<Args>(args)...), ...);
          }
        else
          {
            (copy_one<do_resize>(PropExs {}, d, s, d_size, std::forward<Args>(args)...), ...);
          }

        if constexpr (do_resize)
          {
            Utility::FriendshipProvider::template set_tagged_size<typename TgPropEx::SizeTag>(d, d_index, d_size);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class TgPropEx, class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_size_tag(TgPropEx tpe, DestLike & d, const SourceLike & s, Args &&... args)
      {
        if constexpr (!DestLike::interface_properties().can_resize_partially())
          {
#if MARIONETTE_WARN_ON_JAGGED_VECTOR_NON_RESIZING_COPY
            impl::warn_on_jagged_vector_non_resizing_copy<InterfaceDescription::is_jagged_vector_size_property(
              typename TgPropEx::SizeTag {})>::warn_jv_non_resizing_copy();
#endif
          }

        if constexpr (TgPropEx::extent == 1)
          {
            copy_size_tag_base<false>(tpe,
                                      typename TgPropEx::PropertiesAndExtents {},
                                      Utility::FriendshipProvider::get_multi_array_index(d),
                                      Utility::FriendshipProvider::get_multi_array_index(s),
                                      d,
                                      s,
                                      std::forward<Args>(args)...);
          }
        else
          {
            //By construction, this is the case where we have global properties
            //at the bottom of a property array and thus all the included properties
            //will have this same extent.

            const PropertiesIndexer d_base_index = Utility::FriendshipProvider::get_multi_array_index(d) * TgPropEx::extent * DestMetaInfo::extent;
            const PropertiesIndexer s_base_index = Utility::FriendshipProvider::get_multi_array_index(s) * TgPropEx::extent * DestMetaInfo::extent;

            for (PropertiesIndexer i = 0; i < TgPropEx::extent * DestMetaInfo::extent; ++i)
              {
                copy_size_tag_base<true>(tpe,
                                         typename TgPropEx::PropertiesAndExtents {},
                                         d_base_index + i,
                                         s_base_index + i,
                                         d,
                                         s,
                                         std::forward<Args>(args)...);
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... TgPropExs, class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_helper(Utility::TypeHolder<TgPropExs...>, DestLike & d, const SourceLike & s, Args &&... args)
      {
        (copy_size_tag(TgPropExs {}, d, s, std::forward<Args>(args)...), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        copy_helper(typename Helper::DestSizeTagInfo {}, d, s);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        copy_helper(typename Helper::DestSizeTagInfo {}, d, s, std::forward<Args>(args)...);
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some_helper_helper(PropEx,
                                                                                                          DestLike &                           d,
                                                                                                          const SourceLike &                   s,
                                                                                                          const typename DestLike::size_type   num,
                                                                                                          const typename DestLike::size_type   d_offset,
                                                                                                          const typename SourceLike::size_type s_offset)
      {
        const PropertiesIndexer d_base_index = Utility::FriendshipProvider::get_multi_array_index(d) * PropEx::extent * DestMetaInfo::extent;
        const PropertiesIndexer s_base_index = Utility::FriendshipProvider::get_multi_array_index(s) * PropEx::extent * DestMetaInfo::extent;

        for (PropertiesIndexer i = 0; i < PropEx::extent * DestMetaInfo::extent; ++i)
          {
            ArrayTransferSpecification<DestLayout, SourceLayout, typename PropEx::Property>::transfer(d_base_index + i,
                                                                                                      d,
                                                                                                      s_base_index + i,
                                                                                                      s,
                                                                                                      num,
                                                                                                      d_offset,
                                                                                                      s_offset);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... PropExs, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some_helper(Utility::TypeHolder<PropExs...>,
                                                                                                   DestLike &                           d,
                                                                                                   const SourceLike &                   s,
                                                                                                   const typename DestLike::size_type   num,
                                                                                                   const typename DestLike::size_type   d_offset,
                                                                                                   const typename SourceLike::size_type s_offset)
      {
        (copy_some_helper_helper(PropExs {}, d, s, num, d_offset, s_offset), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                           d,
                                                                                            const SourceLike &                   s,
                                                                                            const typename DestLike::size_type   num,
                                                                                            const typename DestLike::size_type   d_offset = 0,
                                                                                            const typename SourceLike::size_type s_offset = 0)
      {
        copy_some_helper(typename Helper::DestPropInfo::template flattened_per_item<true, false, false> {}, d, s, num, d_offset, s_offset);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike &, SourceLike &&) = delete;
    };

  }
}

#endif
