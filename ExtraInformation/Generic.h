//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_EXTRAINFORMATION_GENERIC_H
#define MARIONETTE_EXTRAINFORMATION_GENERIC_H

#include <utility>
#include <type_traits>

#include "../MarionetteBase.h"

namespace Marionette
{
  namespace ExtraInformation
  {
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Type, class Context>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES AllocatorLike<Utility::ContextAwareAllocator<Type, Context>>
    {

      template <class F, class Layout>
      using ExtraFunctions = typename Context::template ExtraFunctions<F, Layout>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool context_info_aware()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return Context {};
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Type>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES AllocatorLike<Utility::LocalAllocator<Type>>
    {

      template <class F, class Layout>
      using ExtraFunctions = typename MemoryContexts::Current::template ExtraFunctions<F, Layout>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool context_info_aware()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return MemoryContexts::Current {};
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class Context, class SizeType, class DifferenceType>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES VectorLike<Utility::ContextAwareVector<T, Context, SizeType, DifferenceType>>
    {
     public:

      template <class F, class Layout>
      using ExtraFunctions = typename Context::template ExtraFunctions<F, Layout>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_capacity()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_resize()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_reserve()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_clear()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_shrink_to_fit()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return Context {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class VecLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_start_of_data(VecLike && v)
      {
        return std::forward<VecLike>(v).data();
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class SizeType, class DifferenceType>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES VectorLike<Utility::LocalVector<T, SizeType, DifferenceType>>
    {
     public:

      template <class F, class Layout>
      using ExtraFunctions = typename MemoryContexts::Current::template ExtraFunctions<F, Layout>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_capacity()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_resize()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_reserve()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_clear()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_shrink_to_fit()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return MemoryContexts::Current {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class VecLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_start_of_data(VecLike && v)
      {
        return std::forward<VecLike>(v).data();
      }
    };
  }
}

#endif
