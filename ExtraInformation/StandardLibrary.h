//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_EXTRAINFORMATION_STANDARDLIBRARY_H
#define MARIONETTE_EXTRAINFORMATION_STANDARDLIBRARY_H

#include <utility>
#include <type_traits>

#include "../MarionetteBase.h"

namespace Marionette
{
  namespace ExtraInformation
  {
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES AllocatorLike<std::allocator<T>>
    {

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ExtraFunctions
      {
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool context_info_aware()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return MemoryContexts::StandardCPU {};
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class VecT, class Alloc>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES VectorLike<std::vector<VecT, Alloc>>
    {
     private:

      using AllocatorInfo = AllocatorLike<Alloc>;

     public:

      template <class F, class Layout>
      using ExtraFunctions = typename AllocatorInfo::template ExtraFunctions<F, Layout>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
      {
        return AllocatorInfo::is_valid();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_capacity()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_resize()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_reserve()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_clear()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_shrink_to_fit()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return AllocatorInfo::memory_context();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class VecLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_start_of_data(VecLike && v)
      {
        return std::forward<VecLike>(v).data();
      }
    };
  }
}
#endif
