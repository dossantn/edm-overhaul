//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_INTERFACEDESCRIPTIONHELPERS_H
#define MARIONETTE_INTERFACEDESCRIPTIONHELPERS_H

#include <utility>
#include <type_traits>

namespace Marionette
{
  namespace InterfaceDescription
  {

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class OriginalProperty>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES JaggedVectorSizeProperty : PerItemObject, GlobalProperty
    {
      static_assert(classify_property(OriginalProperty {}) == SupportedTypes::JaggedVector, "'JaggedVectorSizeProperty' requires a jagged vector property!");

      using Type = typename OriginalProperty::size_type;

      static constexpr bool track_individual_size = false;

      static constexpr int initial_size = 1;

      static constexpr int initial_memset = 0;

      static constexpr auto max_entries = property_maximum_size(OriginalProperty {}) + (property_maximum_size(OriginalProperty {}) != 0);

      //If it's a nonzero size, we specify a max size that's one plus that.
      //Otherwise, we also specify a max size of 0...

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CollectionFunctions
      {
        ///@brief The underlying array of each vector's size.
        ///
        ///@warning Care must be taken to avoid changing this to an inconsistent state.
        ///         For expert use only!
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_const_subcollection(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) underlying_size_array() const
        {
          auto dhis = static_cast<const F *>(this);

          const PropertiesIndexer mult_arr_idx = Utility::FriendshipProvider::get_multi_array_index(*dhis);

          return Utility::FriendshipProvider::template get_array<JaggedVectorSizeProperty>(*dhis, mult_arr_idx);
        }

        ///@brief The underlying array of each vector's size.
        ///
        ///@warning Care must be taken to avoid changing this to an inconsistent state.
        ///         For expert use only!
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_modifiable_subcollection(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) underlying_size_array()
        {
          auto dhis = static_cast<const F *>(this);

          const PropertiesIndexer mult_arr_idx = Utility::FriendshipProvider::get_multi_array_index(*dhis);

          return Utility::FriendshipProvider::template get_array<JaggedVectorSizeProperty>(*dhis, mult_arr_idx);
        }

        ///@brief The underlying array of each vector's size.
        ///
        ///@warning Care must be taken to avoid changing this to an inconsistent state.
        ///         For expert use only!
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_const_object(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) underlying_size_array(const typename Layout::size_type i) const
        {
          auto dhis = static_cast<const F *>(this);

          const PropertiesIndexer mult_arr_idx = Utility::FriendshipProvider::get_multi_array_index(*dhis);

          return Utility::FriendshipProvider::template get_array<JaggedVectorSizeProperty>(*dhis, mult_arr_idx, i);
        }

        ///@brief The underlying array of each vector's size.
        ///
        ///@warning Care must be taken to avoid changing this to an inconsistent state.
        ///         For expert use only!
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_modifiable_object(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) underlying_size_array(const typename Layout::size_type i)
        {
          auto dhis = static_cast<const F *>(this);

          const PropertiesIndexer mult_arr_idx = Utility::FriendshipProvider::get_multi_array_index(*dhis);

          return Utility::FriendshipProvider::template get_array<JaggedVectorSizeProperty>(*dhis, mult_arr_idx, i);
        }

        ///@brief The total size of the underlying collection.
        ///
        ///@warning Care must be taken to avoid changing this to an inconsistent state.
        ///         For expert use only!
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_const_object(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto & underlying_total_size() const
        {
          auto dhis = static_cast<const F *>(this);

          const PropertiesIndexer mult_arr_idx = Utility::FriendshipProvider::get_multi_array_index(*dhis);

          return *(Utility::FriendshipProvider::template get_tagged_size_pointer<JaggedVectorSizeProperty>(*dhis, mult_arr_idx));
        }

        ///@brief The total size of the underlying collection.
        ///
        ///@warning Care must be taken to avoid changing this to an inconsistent state.
        ///         For expert use only!
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_modifiable_object(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & underlying_total_size()
        {
          auto dhis = static_cast<const F *>(this);

          const PropertiesIndexer mult_arr_idx = Utility::FriendshipProvider::get_multi_array_index(*dhis);

          return *(Utility::FriendshipProvider::template get_tagged_size_pointer<JaggedVectorSizeProperty>(*dhis, mult_arr_idx));
        }

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_const_subcollection(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) jagged_vector_underlying_size(const PropertiesIndexer mult_arr_idx) const
        {
          auto dhis = static_cast<const F *>(this);

          return Utility::FriendshipProvider::template get_tagged_size<JaggedVectorSizeProperty>(*dhis, mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_const_subcollection(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) jagged_vector_sizes(const PropertiesIndexer mult_arr_idx) const
        {
          auto dhis = static_cast<const F *>(this);

          return Utility::FriendshipProvider::template get_array<JaggedVectorSizeProperty>(*dhis, mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_modifiable_subcollection(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) jagged_vector_sizes(const PropertiesIndexer mult_arr_idx)
        {
          auto dhis = static_cast<F *>(this);

          return Utility::FriendshipProvider::template get_array<JaggedVectorSizeProperty>(*dhis, mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_const_object(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) jagged_vector_sizes(const PropertiesIndexer          mult_arr_idx,
                                                                                                           const typename Layout::size_type i) const
        {
          auto dhis = static_cast<const F *>(this);

          return Utility::FriendshipProvider::template get_array<JaggedVectorSizeProperty>(*dhis, mult_arr_idx, i);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_modifiable_object(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) jagged_vector_sizes(const PropertiesIndexer          mult_arr_idx,
                                                                                                           const typename Layout::size_type i)
        {
          auto dhis = static_cast<F *>(this);

          return Utility::FriendshipProvider::template get_array<JaggedVectorSizeProperty>(*dhis, mult_arr_idx, i);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag,
                                                                 bool valid     = Layout::interface_properties().can_refer_to_const_subcollection(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr typename Layout::size_type
        global_object_size(const PropertiesIndexer mult_arr_idx) const
        {
          auto dhis = static_cast<const F *>(this);

          return Utility::FriendshipProvider::template get_tagged_size<SizeTag>(*dhis, mult_arr_idx) + 1;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag,
                                                                 bool valid     = Layout::interface_properties().can_refer_to_const_subcollection(),
                                                                 class disabler = std::enable_if_t<valid>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr typename Layout::size_type
        global_object_capacity(const PropertiesIndexer mult_arr_idx) const
        {
          auto dhis = static_cast<const F *>(this);

          return Utility::FriendshipProvider::template get_tagged_capacity<SizeTag>(*dhis, mult_arr_idx) - 1;
        }
      };
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_jagged_vector_size_property(Property)
    {
      return false;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_jagged_vector_size_property(JaggedVectorSizeProperty<Property>)
    {
      return true;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr SupportedTypes classify_property(Property)
    {
      if constexpr (std::is_base_of_v<PropertyArray, Property>)
        {
          return SupportedTypes::PropertyArray;
        }
      else if constexpr (std::is_base_of_v<SubCollection, Property>)
        {
          return SupportedTypes::SubCollection;
        }
      else if constexpr (std::is_base_of_v<JaggedVector, Property>)
        {
          return SupportedTypes::JaggedVector;
        }
      else if constexpr (std::is_base_of_v<PerItemObject, Property>)
        {
          return SupportedTypes::PerItemObject;
        }
      else if constexpr (std::is_base_of_v<NoObject, Property>)
        {
          return SupportedTypes::NoObject;
        }
      else
        {
          return SupportedTypes::Invalid;
        }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_global(Property)
    {
      return std::is_base_of_v<GlobalProperty, Property>;
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto extra_write_proxy_getter(Ts...)
      {
        return Utility::InvalidType {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class T = typename Property::ExtraWriteProxyProperty>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto extra_write_proxy_getter(Property)
      {
        return T {};
      }

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool has_extra_write_proxy_types(Property p)
    {
      return !std::is_same_v<decltype(impl::extra_write_proxy_getter(p)), Utility::InvalidType>;
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool property_specifies_size_getter(Ts...)
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class T = decltype(Property::max_entries)>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool property_specifies_size_getter(Property)
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool track_global_property_individual_size(Ts...)
      {
        if constexpr (((classify_property(Ts {}) == SupportedTypes::NoObject) || ...))
          {
            return false;
            //Unless they specifically specify otherwise
            //(though the reason for that eludes even the author...),
            //properties that require no storage don't have their sizes
            //tracked individually by default...
          }
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool ret = Property::track_individual_size>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool track_global_property_individual_size(Property)
      {
        return ret;
      }
    }

    ///@brief Whether @p Property has a @c max_entries static member variable.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool property_specifies_maximum_size(Property p)
    {
      return impl::property_specifies_size_getter(p);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto property_maximum_size(Property p)
    {
      if constexpr (property_specifies_maximum_size(p))
        {
          return Property::max_entries;
        }
      else
        {
          return 0;
        }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool track_individual_size(Property p)
    {
      if constexpr (is_global(p))
        {
          return impl::track_global_property_individual_size(p);
        }
      else
        {
          return false;
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool property_specifies_initial_size(Ts...)
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class T = decltype(Property::initial_size)>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool property_specifies_initial_size(Property)
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool property_specifies_initial_memset(Ts...)
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class T = decltype(Property::initial_memset)>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool property_specifies_initial_memset(Property)
      {
        return true;
      }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto initial_global_property_size(Property p)
    {
      if constexpr (is_global(p) && impl::property_specifies_initial_size(p))
        {
          return Property::initial_size;
        }
      else
        {
          return 0;
        }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool use_initial_global_property_memset(Property p)
    {
      if constexpr (is_global(p) && impl::property_specifies_initial_memset(p))
        {
          static_assert(sizeof(long long int) > sizeof(unsigned char), "We assume we can specify values greater than what an unsigned char can hold.");
          //If you're working in an architecture exotic enough
          //for long long to be the same size of char, you probably
          //know what you are doing and can disable this error by yourself.
          //(Emphasis on the probably.)
          //This is still here to remind you that specifying no initial memset
          //via setting it to a value greater than what unsigned char can provide
          //(or more negative than its symmetric) will fail in this case,
          //and probably be the source of some undefined behaviour...
          //
          //Also, if you are working in such an exotic architecture,
          //I would be very interested in knowing what you are trying to achieve
          //with this library, and what drove you to adopt it. Thanks.

          constexpr long long int limit = static_cast<long long int>(static_cast<unsigned char>(-1));

          return Property::initial_memset >= -limit && Property::initial_memset <= limit;
        }
      else
        {
          return false;
        }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto initial_global_property_memset_value(Property p)
    {
      if constexpr (is_global(p) && impl::property_specifies_initial_memset(p))
        {
          return Property::initial_memset;
        }
      else
        {
          return (int(1) + static_cast<unsigned char>(-1));
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer extent = 1, class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto append_extents(Utility::TypeHolder<Properties...>)
      {
        return Utility::TypeHolder<PropertyAndExtent<Properties, extent>...> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropertyOperator, class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto property_apply_operator(Utility::TypeHolder<Ts...>)
      {
        return (Utility::TypeHolder<Utility::InvalidType> {} | ... | PropertyOperator::operate(Ts {}));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropertyOperator, PropertiesIndexer starting_extent = 1, class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto interface_information_getter(Utility::TypeHolder<Properties...> th)
      {
        return Utility::clean_type_holder(property_apply_operator<PropertyOperator>(append_extents<starting_extent>(th)));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool include_non_global              = true,
                                                            bool include_global                  = true,
                                                            bool include_part_of_jagged_vectors  = true,
                                                            bool use_write_proxy_properties      = false,
                                                            bool keep_globals_with_write_proxy   = false,
                                                            bool append_jagged_vector_properties = true>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES FlattenedPerItemOperator
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropExt>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto operate(PropExt)
        {
          using Property = typename PropExt::Property;

          [[maybe_unused]] constexpr PropertiesIndexer extent = PropExt::extent;

          if constexpr (use_write_proxy_properties && is_global(Property {}) && has_extra_write_proxy_types(Property {}))
            {
              return operate(PropertyAndExtent<std::decay_t<decltype(extra_write_proxy_getter(Property {}))>, extent> {});
            }
          else if constexpr (is_global(Property {}) && !include_global)
            {
              return Utility::TypeHolder<Utility::InvalidType> {};
            }
          else
            {
              constexpr SupportedTypes prop_type = classify_property(Property {});

              if constexpr (prop_type == SupportedTypes::Invalid)
                {
                  static_assert(prop_type != SupportedTypes::Invalid,
                                "Property list contains at least one invalid property! Please check your interface description!");
                  return Utility::TypeHolder<Utility::InvalidType> {};
                }
              else if constexpr (prop_type == SupportedTypes::PropertyArray)
                {
                  return interface_information_getter<FlattenedPerItemOperator, (extent * Property::number)>(typename Property::Properties {});
                }
              else if constexpr (prop_type == SupportedTypes::SubCollection)
                {
                  return interface_information_getter<FlattenedPerItemOperator, extent>(typename Property::Properties {});
                }
              else if constexpr (prop_type == SupportedTypes::JaggedVector)
                {
                  if constexpr (include_part_of_jagged_vectors)
                    {
                      if constexpr (append_jagged_vector_properties)
                        {
                          return interface_information_getter<FlattenedPerItemOperator, extent>(
                            typename Property::Properties::template append<JaggedVectorSizeProperty<Property>> {});
                        }
                      else
                        {
                          return interface_information_getter<FlattenedPerItemOperator, extent>(typename Property::Properties {});
                        }
                    }
                  else
                    {
                      return Utility::TypeHolder<Utility::InvalidType> {};
                    }
                }
              else if constexpr (prop_type == SupportedTypes::NoObject)
                {
                  return Utility::TypeHolder<Utility::InvalidType> {};
                }
              else if constexpr ((!is_global(Property {}) && !include_non_global) || (is_global(Property {}) && !include_global))
                {
                  return Utility::TypeHolder<Utility::InvalidType> {};
                }
              else
                {
                  return Utility::TypeHolder<PropExt> {};
                }
            }
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool include_non_global            = true,
                                                            bool include_global                = true,
                                                            bool use_write_proxy_properties    = false,
                                                            bool keep_globals_with_write_proxy = false>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES FlattenedJaggedVectorOperator
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropExt>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto operate(PropExt)
        {
          using Property = typename PropExt::Property;

          [[maybe_unused]] constexpr PropertiesIndexer extent = PropExt::extent;

          if constexpr (use_write_proxy_properties && is_global(Property {}) && has_extra_write_proxy_types(Property {}))
            {
              return operate(PropertyAndExtent<std::decay_t<decltype(extra_write_proxy_getter(Property {}))>, extent> {});
            }
          else if constexpr (is_global(Property {}) && !include_global)
            {
              if constexpr (keep_globals_with_write_proxy)
                {
                  return Utility::TypeHolder<Property> {};
                }
              else
                {
                  return Utility::TypeHolder<Utility::InvalidType> {};
                }
            }
          else
            {
              constexpr SupportedTypes prop_type = classify_property(Property {});

              if constexpr (prop_type == SupportedTypes::Invalid)
                {
                  static_assert(prop_type != SupportedTypes::Invalid,
                                "Property list contains at least one invalid property! Please check your interface description!");
                  return Utility::TypeHolder<Utility::InvalidType> {};
                }
              else if constexpr (prop_type == SupportedTypes::PropertyArray)
                {
                  return interface_information_getter<FlattenedJaggedVectorOperator, (extent * Property::number)>(typename Property::Properties {});
                }
              else if constexpr (prop_type == SupportedTypes::SubCollection)
                {
                  return interface_information_getter<FlattenedJaggedVectorOperator, extent>(typename Property::Properties {});
                }
              else if constexpr (prop_type == SupportedTypes::JaggedVector)
                {
                  return Utility::TypeHolder<PropExt> {};
                }
              else if constexpr ((!is_global(Property {}) && !include_non_global) || (is_global(Property {}) && !include_global))
                {
                  return Utility::TypeHolder<Utility::InvalidType> {};
                }
              else
                {
                  return Utility::TypeHolder<Utility::InvalidType> {};
                }
            }
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... TypeHolders>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES FlatTypeGetter
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool contains_type()
        {
          return (TypeHolders::template contains<typename PropEx::Property::Type>() || ...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class TypeHolder, class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto maybe_add(TypeHolder th, PropEx)
        {
          if constexpr (TypeHolder::template contains<typename PropEx::Property::Type>())
            {
              return (th | (Utility::TypeHolder<PropEx> {}));
            }
          else
            {
              return th;
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(PropEx) const
        {
          if constexpr (contains_type<PropEx>())
            {
              return FlatTypeGetter<std::decay_t<decltype(maybe_add(TypeHolders {}, PropEx {}))>...> {};
            }
          else
            {
              return FlatTypeGetter<TypeHolders..., Utility::TypeHolder<typename PropEx::Property::Type, PropEx>> {};
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto to_type_holder()
        {
          return Utility::TypeHolder<TypeHolders...> {};
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OperatorType, class... PropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto flatten_properties_helper(Utility::TypeHolder<PropExs...>)
      {
        return (OperatorType {} | ... | PropExs {});
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool include_non_global            = true,
                                                               bool include_global                = true,
                                                               bool use_write_proxy_properties    = false,
                                                               bool keep_globals_with_write_proxy = false,
                                                               class... PropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto jagged_vector_properties_helper(Utility::TypeHolder<PropExs...> th)
      {
        auto ret = property_apply_operator<
          FlattenedPerItemOperator<include_non_global, include_global, true, use_write_proxy_properties, keep_globals_with_write_proxy, false>>(th);
        //include_part_of_jagged_vectors must be true so that we can unpack the jagged vector properties...

        return Utility::clean_type_holder(ret);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... SizeTagExTps>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SizeListAggregator
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SzTgExTp>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool contains_tag()
        {
          return (std::is_same_v<typename SizeTagExTps::SizeTag, typename SzTgExTp::SizeTag> || ...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OneSizeTagExTp, class SzTgExTp>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto maybe_add(OneSizeTagExTp ostet, SzTgExTp)
        {
          if constexpr (std::is_same_v<typename OneSizeTagExTp::SizeTag, typename SzTgExTp::SizeTag>)
            {
              constexpr PropertiesIndexer one_extent   = OneSizeTagExTp::extent;
              constexpr PropertiesIndexer other_extent = SzTgExTp::extent;

              constexpr auto joined_properties = ((typename OneSizeTagExTp::PropertiesAndExtents {}) | (typename SzTgExTp::PropertiesAndExtents {}));

              return SizeTagExtentAndTypes < typename OneSizeTagExTp::SizeTag, (one_extent < other_extent) ? one_extent : other_extent,
                     std::decay_t<decltype(joined_properties)> > {};
            }
          else
            {
              return ostet;
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SzTgExTp>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(SzTgExTp) const
        {
          if constexpr (contains_tag<SzTgExTp>())
            {
              return SizeListAggregator<std::decay_t<decltype(maybe_add(SizeTagExTps {}, SzTgExTp {}))>...> {};
            }
          else
            {
              return SizeListAggregator<SizeTagExTps..., SzTgExTp> {};
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... SzTgExTps>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(SizeListAggregator<SzTgExTps...>) const
        {
          return ((*this) | ... | SzTgExTps {});
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto to_type_holder()
        {
          return Utility::TypeHolder<SizeTagExTps...> {};
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class CurrentSizeTag              = Collections::DefaultSizeTag,
                                                               PropertiesIndexer starting_extent = 1,
                                                               class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto necessary_size_tags_helper(Utility::TypeHolder<Properties...>);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SizeTagListCreator
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SzTgExtTp>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto operate(SzTgExtTp)
        {
          using CurrentSizeTag = typename SzTgExtTp::SizeTag;

          using PropExt = typename SzTgExtTp::PropertiesAndExtents;
          //This is supposed to be a TypeHolder,
          //but it's initialized (for this)
          //as just the PropertyExtent for a single property,
          //otherwise it'd be more painful to extract it from there.

          using Property = typename PropExt::Property;

          constexpr PropertiesIndexer extent = SzTgExtTp::extent;

          constexpr SupportedTypes prop_type = classify_property(Property {});

          using RealCurrentTag = std::conditional_t<track_individual_size(Property {}), Utility::TypeHolder<Property>, CurrentSizeTag>;

          [[maybe_unused]] constexpr auto current_ret = SizeListAggregator<SizeTagExtentAndTypes<RealCurrentTag, extent, Utility::TypeHolder<PropExt>>> {};

          if constexpr (prop_type == SupportedTypes::PropertyArray)
            {
              return (current_ret | necessary_size_tags_helper<RealCurrentTag, (extent * Property::number)>(typename Property::Properties {}));
            }
          else if constexpr (prop_type == SupportedTypes::SubCollection)
            {
              return (current_ret | necessary_size_tags_helper<RealCurrentTag, extent>(typename Property::Properties {}));
            }
          else if constexpr (prop_type == SupportedTypes::JaggedVector)
            {
              return (current_ret | necessary_size_tags_helper<InterfaceDescription::JaggedVectorSizeProperty<Property>, extent>(
                                      typename Property::Properties::template append<JaggedVectorSizeProperty<Property>> {}));
            }
          else if constexpr (prop_type == SupportedTypes::NoObject)
            {
              return SizeListAggregator<> {};
            }
          else
            {
              return current_ret;
            }
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class CurrentSizeTag, PropertiesIndexer starting_extent, class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto necessary_size_tags_helper(Utility::TypeHolder<Properties...>)
      {
        return (SizeListAggregator<SizeTagExtentAndTypes<CurrentSizeTag, starting_extent>> {} | ... |
                SizeTagListCreator::operate(SizeTagExtentAndTypes<CurrentSizeTag, starting_extent, PropertyAndExtent<Properties, starting_extent>> {}));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Prop, MaximumSizeType max_size = 0>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES PropAndMaxSize
      {
        using Property = Prop;

        static constexpr MaximumSizeType maximum_size = max_size;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OriginalSizeMap, MaximumSizeType inherited_max_size = 0, class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto maximum_size_builder(Utility::TypeHolder<Properties...>);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OriginalSizeMap,
                                                               MaximumSizeType inherited_max_size,
                                                               bool            handle_globals = true,
                                                               class Property>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto maximum_size_builder_helper(Property p)
      {
        constexpr MaximumSizeType mapped_size  = OriginalSizeMap::size(p);
        constexpr MaximumSizeType new_max_size = mapped_size > inherited_max_size ? mapped_size : inherited_max_size;

        if constexpr (handle_globals && is_global(Property {}) && has_extra_write_proxy_types(Property {}))
          {
            return (maximum_size_builder_helper<OriginalSizeMap, new_max_size, false>(p) |
                    maximum_size_builder_helper<OriginalSizeMap, new_max_size>(extra_write_proxy_getter(Property {})));
          }
        else
          {
            constexpr SupportedTypes prop_type = classify_property(p);

            constexpr auto base_ret = Utility::TypeHolder<PropAndMaxSize<Property, new_max_size>> {};

            if constexpr (prop_type == SupportedTypes::PropertyArray || prop_type == SupportedTypes::SubCollection)
              {
                return base_ret | maximum_size_builder<OriginalSizeMap, new_max_size>(typename Property::Properties {});
              }
            else if constexpr (prop_type == SupportedTypes::JaggedVector)
              {
                return base_ret | maximum_size_builder<OriginalSizeMap, new_max_size>(
                                    typename Property::Properties::template append<JaggedVectorSizeProperty<Property>> {});
              }
            else
              {
                return base_ret;
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OriginalSizeMap, MaximumSizeType inherited_max_size, class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto maximum_size_builder(Utility::TypeHolder<Properties...>)
      {
        return (maximum_size_builder_helper<OriginalSizeMap, inherited_max_size>(Properties {}) | ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class List>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES size_map_from_list : DefaultSizeMap
      {
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... PropsSizes>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES size_map_from_list<Utility::TypeHolder<PropsSizes...>>
      {
       private:

        using List = Utility::TypeHolder<PropsSizes...>;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr MaximumSizeType get_size(Property)
        {
          return ((std::is_same_v<Property, typename PropsSizes::Property> ? PropsSizes::maximum_size : 0) + ...);
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr MaximumSizeType size(Property p)
        {
          if constexpr (List::template contains<Property>())
            {
              return get_size(p);
            }
          else
            {
              return property_maximum_size(p);
            }
        }
      };

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Properties>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES InterfaceInformation<Utility::TypeHolder<Properties...>>
    {
     private:

      using Types = Utility::TypeHolder<Properties...>;

     public:

      template <bool include_non_global             = true,
                bool include_global                 = true,
                bool include_part_of_jagged_vectors = true,
                bool use_write_proxy_properties     = false,
                bool keep_globals_with_write_proxy  = false>
      using flattened_per_item = decltype(impl::interface_information_getter<impl::FlattenedPerItemOperator<include_non_global,
                                                                                                            include_global,
                                                                                                            include_part_of_jagged_vectors,
                                                                                                            use_write_proxy_properties,
                                                                                                            keep_globals_with_write_proxy,
                                                                                                            true>>(Types {}));

      template <bool include_non_global             = true,
                bool include_global                 = true,
                bool include_part_of_jagged_vectors = true,
                bool use_write_proxy_properties     = false,
                bool keep_globals_with_write_proxy  = false>
      using flattened_properties_by_type =
        decltype(impl::flatten_properties_helper<impl::FlatTypeGetter<>>(std::declval<flattened_per_item<include_non_global,
                                                                                                         include_global,
                                                                                                         include_part_of_jagged_vectors,
                                                                                                         use_write_proxy_properties,
                                                                                                         keep_globals_with_write_proxy>>())
                   .to_type_holder());

      template <bool include_non_global = true, bool include_global = true, bool use_write_proxy_properties = false, bool keep_globals_with_write_proxy = false>
      using flattened_jagged_vector =
        decltype(impl::interface_information_getter<
                 impl::FlattenedJaggedVectorOperator<include_non_global, include_global, use_write_proxy_properties, keep_globals_with_write_proxy>>(Types {}));

      template <bool include_non_global = true, bool include_global = true, bool use_write_proxy_properties = false, bool keep_globals_with_write_proxy = false>
      using flattened_jagged_vector_properties =
        decltype(impl::jagged_vector_properties_helper<include_non_global, include_global, use_write_proxy_properties, keep_globals_with_write_proxy>(
          std::declval<flattened_jagged_vector<include_non_global, include_global, use_write_proxy_properties, keep_globals_with_write_proxy>>()));

      template <class StartingTag = Collections::DefaultSizeTag>
      using necessary_size_tags = decltype(impl::necessary_size_tags_helper<StartingTag>(Types {}).to_type_holder());

      template <class SizeMap>
      using per_item_size_map = impl::size_map_from_list<std::decay_t<decltype(impl::maximum_size_builder<SizeMap, 0>(Types {}))>>;
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    namespace impl
    {

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool forbid_jagged_vectors               = false,
                                                               bool inside_size_tracked_global_property = false,
                                                               bool inside_property_array               = false,
                                                               class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool check_validity_helper(Utility::TypeHolder<Properties...>);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool forbid_jagged_vectors               = false,
                                                               bool inside_size_tracked_global_property = false,
                                                               bool inside_property_array               = false,
                                                               class Property>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool check_validity_helper(Property)
      {
        constexpr SupportedTypes prop_type = classify_property(Property {});

        static_assert(prop_type != SupportedTypes::Invalid,
                      "At least one property was not recognized as valid! See documentation for 'Marionette::InterfaceDescription'.");

        if constexpr (prop_type == SupportedTypes::Invalid)
          {
            return false;
          }

        [[maybe_unused]] constexpr bool new_size_tracked_global_property = inside_size_tracked_global_property || track_individual_size(Property {});

        if constexpr (prop_type == SupportedTypes::PropertyArray)
          {
            static_assert(!(inside_property_array && inside_size_tracked_global_property),
                          "Global properties must be either outside of a property array, "
                          "at its top level or at its bottom.");

            if constexpr (inside_property_array && inside_size_tracked_global_property)
              {
                return false;
              }

            return check_validity_helper<forbid_jagged_vectors, new_size_tracked_global_property, true>(typename Property::Properties {});
          }
        else if constexpr (prop_type == SupportedTypes::SubCollection)
          {
            return check_validity_helper<forbid_jagged_vectors, new_size_tracked_global_property, inside_property_array>(typename Property::Properties {});
          }
        else if constexpr (prop_type == SupportedTypes::JaggedVector)
          {
            static_assert(!forbid_jagged_vectors, "Nested jagged vectors are not supported!");

            if constexpr (forbid_jagged_vectors)
              {
                return false;
              }

            return check_validity_helper<true, new_size_tracked_global_property, inside_property_array>(typename Property::Properties {});
          }
        else if constexpr (prop_type == SupportedTypes::PerItemObject)
          {
            static_assert(std::is_trivially_copyable_v<typename Property::Type>, "All basic properties must be trivially copyable!");

            return std::is_trivially_copyable_v<typename Property::Type>;
          }
        else
          {
            return true;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool forbid_jagged_vectors,
                                                               bool inside_size_tracked_global_property,
                                                               bool inside_property_array,
                                                               class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool check_validity_helper(Utility::TypeHolder<Properties...>)
      {
        return (check_validity_helper<forbid_jagged_vectors, inside_size_tracked_global_property, inside_property_array>(Properties {}) && ...);
      }

#if MARIONETTE_REQUIRE_UNIQUE_PROPERTIES
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool unique, class TH>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES UniquenessChecker
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_unique()
        {
          return unique;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Properties>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(Utility::TypeHolder<Properties...>) const
        {
          return ((*this) | ... | Properties {});
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator|(Property) const
        {
          if constexpr (!unique)
            {
              return UniquenessChecker<false, TH> {};
            }
          else if constexpr (TH::template contains<Property>())
            {
              return UniquenessChecker<false, TH> {};
            }
          else
            {
              constexpr SupportedTypes prop_type = classify_property(Property {});

              using NewTH = decltype(TH {} | Utility::TypeHolder<Property> {});

              if constexpr (prop_type == SupportedTypes::PropertyArray)
                {
                  return (UniquenessChecker<true, NewTH> {} | typename Property::Properties {});
                }
              else if constexpr (prop_type == SupportedTypes::SubCollection)
                {
                  return (UniquenessChecker<true, NewTH> {} | typename Property::Properties {});
                }
              else if constexpr (prop_type == SupportedTypes::JaggedVector)
                {
                  return (UniquenessChecker<true, NewTH> {} | typename Property::Properties {});
                }
              else if constexpr (prop_type == SupportedTypes::NoObject)
                {
                  return (*this);
                  //No need to worry about NoObject properties being non-unique.
                }
              else
                {
                  return UniquenessChecker<true, NewTH> {};
                }
            }
        }
      };
#endif

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool check_validity(T)
    {
      static_assert(Utility::always_false<T>,
                    "This is not a correct interface specification. "
                    "Please use 'Marionette::InterfaceDescription::PropertyList' to specify the desired interface, "
                    "unless you know what you are doing (in which case, check again, there is an error somewhere!).");
      return false;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool check_validity(Utility::TypeHolder<Ts...> th)
    {
#if MARIONETTE_REQUIRE_UNIQUE_PROPERTIES
      using UniqueCheck = decltype(impl::UniquenessChecker<true, Utility::TypeHolder<>> {} | th);

      static_assert(UniqueCheck::is_unique(), "Every property must either be a no-object property or appear at most once in the property list!");

      return (impl::check_validity_helper(Ts {}) && ...) && UniqueCheck::is_unique();
#else
      return (impl::check_validity_helper(Ts {}) && ...);
#endif
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------

    namespace impl
    {

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, SupportedTypes to_check, class Prop>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto property_type_getter_check_one(Prop p)
      {
        if constexpr (!is_global(p) || !ignore_globals)
          {
            constexpr SupportedTypes prop_type = classify_property(p);
            if constexpr (prop_type == to_check)
              {
                return 1;
              }
            else if constexpr (prop_type == SupportedTypes::NoObject)
              {
                return 0;
              }
            else
              {
                return 2;
              }
          }
        else
          {
            return 0;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, bool ignore_globals, SupportedTypes to_check>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES PropertyTypeGetterChecker;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Properties, bool ignore_globals, SupportedTypes to_check>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES PropertyTypeGetterChecker<Utility::TypeHolder<Properties...>, ignore_globals, to_check>
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool do_check()
        {
          return (property_type_getter_check_one<ignore_globals, to_check>(Properties {}) + ...) == 1;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto get_type()
        {
          if constexpr (do_check())
            {
              auto ret = (Utility::TypeHolder<Utility::InvalidType> {} & ... &
                          std::conditional_t<property_type_getter_check_one<ignore_globals, to_check>(Properties {}) == 1,
                                             Utility::TypeHolder<Properties>,
                                             Utility::TypeHolder<Utility::InvalidType>> {});
              return Utility::get_one_type_or_invalid(ret);
            }
          else
            {
              return Utility::InvalidType {};
            }
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto check_for_global_property(Prop p)
      {
        if constexpr (classify_property(p) == SupportedTypes::NoObject)
          {
            return 0;
          }
        else if constexpr (is_global(p))
          {
            return 1;
          }
        else
          {
            return 2;
          }
      }

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_global_property(PropertyList<Properties...>)
    {
      return (impl::check_for_global_property(Properties {}) + ...) == 1;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_global_property(PropertyList<Properties...>)
    {
      if constexpr (single_global_property(Utility::TypeHolder<Properties...> {}))
        {
          auto ret = (Utility::TypeHolder<Utility::InvalidType> {} & ... &
                      std::conditional_t<impl::check_for_global_property(Properties {}) == 1, Utility::TypeHolder<Properties>, Utility::InvalidType> {});
          return Utility::get_one_type_or_invalid(ret);
        }
      else
        {
          return Utility::InvalidType {};
        }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool only_no_object_properties(PropertyList<Properties...>)
    {
      return ((classify_property(Properties {}) == SupportedTypes::NoObject || (ignore_globals && is_global(Properties {}))) && ...);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_per_item_property(PropertyList<Properties...>)
    {
      return impl::PropertyTypeGetterChecker<Utility::TypeHolder<Properties...>, ignore_globals, SupportedTypes::PerItemObject>::do_check();
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_per_item_property(PropertyList<Properties...>)
    {
      return impl::PropertyTypeGetterChecker<Utility::TypeHolder<Properties...>, ignore_globals, SupportedTypes::PerItemObject>::get_type();
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_jagged_vector_property(PropertyList<Properties...>)
    {
      return impl::PropertyTypeGetterChecker<Utility::TypeHolder<Properties...>, ignore_globals, SupportedTypes::JaggedVector>::do_check();
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_jagged_vector_property(PropertyList<Properties...>)
    {
      return impl::PropertyTypeGetterChecker<Utility::TypeHolder<Properties...>, ignore_globals, SupportedTypes::JaggedVector>::get_type();
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_subcollection_property(PropertyList<Properties...>)
    {
      return impl::PropertyTypeGetterChecker<Utility::TypeHolder<Properties...>, ignore_globals, SupportedTypes::SubCollection>::do_check();
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_subcollection_property(PropertyList<Properties...>)
    {
      return impl::PropertyTypeGetterChecker<Utility::TypeHolder<Properties...>, ignore_globals, SupportedTypes::SubCollection>::get_type();
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_property_array_property(PropertyList<Properties...>)
    {
      return impl::PropertyTypeGetterChecker<Utility::TypeHolder<Properties...>, ignore_globals, SupportedTypes::PropertyArray>::do_check();
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_property_array_property(PropertyList<Properties...>)
    {
      return impl::PropertyTypeGetterChecker<Utility::TypeHolder<Properties...>, ignore_globals, SupportedTypes::PropertyArray>::get_type();
    }
  }
}

#endif
