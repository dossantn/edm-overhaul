//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_COLLECTIONHELPERS_H
#define MARIONETTE_COLLECTIONHELPERS_H

#include <utility>
#include <type_traits>

#include "MarionetteBase.h"
#include "LayoutTypes/InternalLayoutTypes.h"

namespace Marionette
{

  namespace Collections
  {
    namespace impl
    {

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Props>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_obtainable_collection_properties(Utility::TypeHolder<Props...> th)
      {
        if constexpr (InterfaceDescription::single_property_array_property(th))
          {
            using Property = decltype(InterfaceDescription::get_property_array_property(th));
            return th | typename Property::Properties {};
          }
        else if constexpr (InterfaceDescription::single_subcollection_property(th))
          {
            using Property = decltype(InterfaceDescription::get_subcollection_property(th));
            return th | typename Property::Properties {};
          }
        else if constexpr (InterfaceDescription::single_jagged_vector_property(th))
          {
            using Property = decltype(InterfaceDescription::get_jagged_vector_property(th));
            return th | typename Property::Properties {} | Utility::TypeHolder<InterfaceDescription::JaggedVectorSizeProperty<Property>> {};
          }
        else
          {
            return th;
          }
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Final,
                                                               class Layout,
                                                               class Property,
                                                               class T = typename Property::template CollectionFunctions<Final, Layout>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_single_property_collection_base(Property)
      {
        return Utility::TypeHolder<T> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Final, class Layout, class Property, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_single_property_collection_base(Property, Args...)
      {
        return Utility::TypeHolder<Utility::InvalidType> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Final, class Layout, class OriginalProperties, class MetaInfo, class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_collection_base_classes_base(Properties... ps)
      {
        auto ret = (Utility::TypeHolder<Utility::InvalidType> {} | ... | get_single_property_collection_base<Final, Layout>(ps));

        return Utility::clean_type_holder(ret | Utility::TypeHolder<typename Layout::template ExtraFunctions<Final, Layout>> {});
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool has_base, class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DeriveFromAll
      {
        static_assert(Utility::always_false<T>, "This should be initialized with a set of types.");
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool b, class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_derived_from_types(const DeriveFromAll<b, T> &)
      {
        return T {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Ts>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DeriveFromAll<false, Utility::TypeHolder<Ts...>> : public Ts...
      {
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class... Ts>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DeriveFromAll<true, Utility::TypeHolder<T, Ts...>> : public T, public Ts...
      {
#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
        __pragma("nv_diag_suppress 20012")
  #else
        _Pragma("nv_diag_suppress 20012")
  #endif
#endif
          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr DeriveFromAll() = default;
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr DeriveFromAll(const DeriveFromAll &) = default;
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr DeriveFromAll(DeriveFromAll &&) = default;
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr DeriveFromAll &
        operator=(const DeriveFromAll &) = default;
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr DeriveFromAll &
        operator=(DeriveFromAll &&) = default;
#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
        __pragma("nv_diag_default 20012")
  #else
        _Pragma("nv_diag_default 20012")
  #endif
#endif

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... OtherTs, class disabler = std::enable_if_t<std::is_constructible_v<T, OtherTs...>>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr DeriveFromAll(OtherTs &&... ts): T(std::forward<OtherTs>(ts)...)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherT, class disabler = std::enable_if_t<std::is_assignable_v<T &, OtherT>>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr DeriveFromAll & operator=(OtherT && t)
        {
          T::operator=(std::forward<OtherT>(t));
          return *this;
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Final, class Layout, class OriginalProperties, class MetaInfo, class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_collection_base_classes(Utility::TypeHolder<Properties...>)
      {

        constexpr auto ret = get_collection_base_classes_base<Final, Layout, OriginalProperties, MetaInfo>(Properties {}...);

        using LayoutHolder = typename Layout::template layout_holder<OriginalProperties>;

        using FinalTypeHolder = typename decltype(ret)::template prepend<LayoutHolder>;

        return DeriveFromAll<true, FinalTypeHolder> {};
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_can_get_array_pointers(Ts...)
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class LayoutType, bool ret = LayoutType::can_get_array_pointers()>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_can_get_array_pointers(LayoutType)
      {
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_has_size_tag_array(Ts...)
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class LayoutType, bool ret = LayoutType::has_size_tag_array()>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_has_size_tag_array(LayoutType)
      {
        return ret;
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class LayoutType, bool ret = LayoutType::stores_multi_array_index()>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_has_multi_array_index(LayoutType)
      {
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_has_multi_array_index(Ts...)
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class LayoutType, bool ret = LayoutType::collection_is_reference()>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_is_pointer_storage(LayoutType)
      {
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_is_pointer_storage(Ts...)
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class BaseT, class CastT>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES OwningPointerHolder : public BaseT
      {
        //using BaseT::BaseT;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr OwningPointerHolder(const OwningPointerHolder & other): BaseT()
        {
          this->get_layout_holder_pointer() = other.get_layout_holder_pointer();

          if constexpr (BaseT::layout_stores_multi_array_index())
            {
              this->get_multi_array_index() = other.get_multi_array_index();
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherCastT>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr OwningPointerHolder(const OwningPointerHolder<BaseT, OtherCastT> & other): BaseT()
        {
          this->get_layout_holder_pointer() = other.get_layout_holder_pointer();

          if constexpr (BaseT::layout_stores_multi_array_index())
            {
              this->get_multi_array_index() = other.get_multi_array_index();
            }
        }

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
        __pragma("nv_diag_suppress 20012")
  #else
        _Pragma("nv_diag_suppress 20012")
  #endif
#endif
          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr OwningPointerHolder() = default;

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
        __pragma("nv_diag_default 20012")
  #else
        _Pragma("nv_diag_default 20012")
  #endif
#endif
          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
            class PtrT     = decltype(nullptr),
            class disabler = std::enable_if_t < !BaseT::layout_stores_multi_array_index() &&
                             Utility::always_true<PtrT> >>
                               MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr explicit OwningPointerHolder(const PtrT ptr): BaseT(ptr)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
          class PtrT     = decltype(nullptr),
          class disabler = std::enable_if_t < BaseT::layout_stores_multi_array_index() &&
                           Utility::always_true<PtrT> >>
                             MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr OwningPointerHolder(const PtrT              ptr,
                                                                                                                 const PropertiesIndexer idx = 0):
          BaseT(ptr, idx)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr OwningPointerHolder &
        operator=(const OwningPointerHolder & other)
        {
          this->get_layout_holder_pointer() = other.get_layout_holder_pointer();

          if constexpr (BaseT::layout_stores_multi_array_index())
            {
              this->get_multi_array_index() = other.get_multi_array_index();
            }

          return *this;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES friend constexpr bool
        operator==(const OwningPointerHolder & one, const OwningPointerHolder & other)
        {
          if constexpr (BaseT::layout_stores_multi_array_index())
            {
              return (one.get_layout_holder_pointer() == other.get_layout_holder_pointer()) && (one.get_multi_array_index() == other.get_multi_array_index());
            }
          else
            {
              return (one.get_layout_holder_pointer() == other.get_layout_holder_pointer());
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator->()
        {
          return Utility::arrow_operator_wrap<CastT>(*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator->() const
        {
          return Utility::arrow_operator_wrap<CastT>(*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & operator*()
        {
          return static_cast<CastT &>(*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & operator*() const
        {
          return static_cast<CastT &>(*const_cast<OwningPointerHolder *>(this));
          //Yes, const cast is beyond evil here,
          //but how to otherwise emulate the fact
          //that a constant pointer to something
          //is distinct from a pointer to a constant something?
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class FinalT, class CastT = FinalT, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto owning_pointer_holder(Args &&... args)
      {
        return OwningPointerHolder<FinalT, CastT>(std::forward<Args>(args)...);
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool resizable_property_helper(Property p)
      {
        return (InterfaceDescription::is_global(p) && InterfaceDescription::track_individual_size(p)) ||
               (InterfaceDescription::classify_property(p) == InterfaceDescription::SupportedTypes::NoObject);
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool is_jagged_vector, class Layout, class Property, class MetaInfo>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto jagged_vector_collection_type_builder()
      {
        if constexpr (is_jagged_vector)
          {
            return CollectionHolder<Layout,
                                    typename Property::Properties::template append<InterfaceDescription::JaggedVectorSizeProperty<Property>>,
                                    typename MetaInfo::template override_size_tag<InterfaceDescription::JaggedVectorSizeProperty<Property>>> {};
          }
        else
          {
            return Utility::InvalidType {};
          }
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Layout>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto original_layout_getter(Layout lay)
      {
        if constexpr (layout_is_pointer_storage(lay))
          {
            return typename Layout::OriginalLayoutType {};
          }
        else
          {
            return lay;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <ResizeProperties     resize,
                                                               MutabilityProperties mutability,
                                                               class Layout,
                                                               class WantedProperties,
                                                               class MetaInfo,
                                                               class PtrType>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto build_multi_array_indexed(PtrType && ptr, const PropertiesIndexer idx)
      {
        using OriginalLayoutType = std::decay_t<decltype(original_layout_getter(Layout {}))>;
        using NewLayoutType      = LayoutTypes::Internal::ViewLayout<OriginalLayoutType, std::decay_t<PtrType>, resize, mutability, true>;

        return Collection<NewLayoutType, WantedProperties, typename MetaInfo::template override_extent<1>> {std::forward<PtrType>(ptr), idx};
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <ResizeProperties     max_resize,
                                                               MutabilityProperties max_mutability,
                                                               class Property,
                                                               class OriginalProperties,
                                                               class MetaInfo,
                                                               class Layout,
                                                               class PtrType,
                                                               class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_new_collection(PtrType && ptr, Args &&... args)
      {
        using OriginalLayout = std::decay_t<decltype(original_layout_getter(Layout {}))>;

        using WantedLayoutType =
          LayoutTypes::Internal::ViewLayout<OriginalLayout, std::decay_t<PtrType>, max_resize, max_mutability, impl::layout_has_multi_array_index(Layout {})>;

        using PotentialNewSizeTag =
          std::conditional_t<InterfaceDescription::track_individual_size(Property {}), Utility::TypeHolder<Property>, typename MetaInfo::SizeTag>;

        using MaybePropertyArray = decltype(InterfaceDescription::get_property_array_property(OriginalProperties {}));

        using MaybeJaggedVector = decltype(InterfaceDescription::get_jagged_vector_property(OriginalProperties {}));
        
        constexpr PropertiesIndexer extent_multiplier = (MaybePropertyArray::Properties::template contains<Property>() ? MaybePropertyArray::number : 1);

        using UpdatedMetaInfo = typename MetaInfo::template override_size_tag<PotentialNewSizeTag>::template multiply_extent<extent_multiplier>;

        constexpr InterfaceDescription::SupportedTypes type = InterfaceDescription::classify_property(Property {});

        static_assert(type != InterfaceDescription::SupportedTypes::Invalid, "Must use valid properties only!");

        if constexpr (std::is_same_v<Property, MaybePropertyArray>)
          {
            return Collection<WantedLayoutType, typename Property::Properties, typename UpdatedMetaInfo::template multiply_extent<MaybePropertyArray::number>> {std::forward<PtrType>(ptr), std::forward<Args>(args)...};
          }
        else if constexpr (std::is_same_v<Property, MaybeJaggedVector>)
          {
            return Collection<WantedLayoutType,
                              typename Property::Properties::template append<InterfaceDescription::JaggedVectorSizeProperty<Property>>,
                              typename UpdatedMetaInfo::template override_size_tag<InterfaceDescription::JaggedVectorSizeProperty<Property>>> {
              std::forward<PtrType>(ptr),
              std::forward<Args>(args)...};
          }
        else if constexpr (type == InterfaceDescription::SupportedTypes::SubCollection)
          {
            return Collection<WantedLayoutType, typename Property::Properties, UpdatedMetaInfo> {std::forward<PtrType>(ptr), std::forward<Args>(args)...};
          }
        else
          {
            return Collection<WantedLayoutType, Utility::TypeHolder<Property>, UpdatedMetaInfo> {std::forward<PtrType>(ptr), std::forward<Args>(args)...};
          }
      }

      //-----------------------------------------------------------------------------------------------------------------------------------------

#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool b>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES unintended_behaviour_warner
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_jv_update()
        {
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES  unintended_behaviour_warner<true>
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        [[deprecated("This is not really a deprecation warning. When using 'update_jagged_vector_size', please ensure "
                     "the array provided as argument is the prefix sum of the vector sizes, otherwise undefined behaviour "
                     "may occur.  Call 'update_jagged_vector_size<true>' to suppress this warning. "
                     "Define 'MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR' to 0 to suppress all warnings about this.")]]
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_jv_update()
        {
        }
      };
#endif

      //-----------------------------------------------------------------------------------------------------------------------------------------
      
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES PropertiesGetterHelper
      {
        using Type = typename T::Properties;
      };
      
      template <class T>
      using properties_getter = typename PropertiesGetterHelper<T>::Type;
      //This is to get around the use of the parameter pack name within Collection (and Object),
      //which is handled differently by each of the big three (Clang accepts everything, MSVC and GCC reject different constructs).
      
    }
  }

}

#endif
