//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MARIONETTETRANSFERSHELPERS_H
#define MARIONETTE_MARIONETTETRANSFERSHELPERS_H

#include <utility>
#include <type_traits>

#include "MarionetteBase.h"

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
  #include <cstdio>

  ///@brief Receives as first argument the base ID you want to assign to this transfer specification,
  ///       and as an optional second argument a statement that is added to the base ID to get the actual final ID.
  #define MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(...) MARIONETTE_TRANSFER_SPECIFICATION_DEBUG_HELPER(__FILE__, __LINE__, __VA_ARGS__, 0)

  #define MARIONETTE_TRANSFER_SPECIFICATION_DEBUG_HELPER_GET_FIRST(x, ...) x

  #define MARIONETTE_TRANSFER_SPECIFICATION_DEBUG_HELPER(file, line, base_id, ...)                                                                             \
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr long transfer_ID(const bool trace =  \
                                                                                                                                             false)            \
    {                                                                                                                                                          \
      if (trace)                                                                                                                                               \
        {                                                                                                                                                      \
          printf("|> %d : %s : %05d <|", base_id, file, line);                                                                                                 \
        }                                                                                                                                                      \
      return base_id + MARIONETTE_TRANSFER_SPECIFICATION_DEBUG_HELPER_GET_FIRST(__VA_ARGS__, 0);                                                               \
    }                                                                                                                                                          \
    struct struct_to_end_the_macro_with_semicolon

#else
  #define MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(...) struct struct_to_end_the_macro_with_semicolon
#endif

namespace Marionette
{
  namespace Transfers
  {
    namespace impl
    {
      template <class Prop>
      inline constexpr InterfaceDescription::SupportedTypes classify_prop_v = InterfaceDescription::classify_property(Prop {});

      //We use this as a (hopefully temporary) fix for some weird behaviours in MSVC
      //when it comes to functions returning enum classes in template disambiguation...
      //TO-DO: Check if it is still needed...

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_collection_holder
      {
        static constexpr bool value = false;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class Properties, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_collection_holder<Collections::CollectionHolder<Layout, Properties, MetaInfo>>
      {
        static constexpr bool value = true;
      };

      template <class T>
      inline constexpr bool is_collection_holder_v = is_collection_holder<T>::value;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_collection
      {
        static constexpr bool value = false;
        using MaybeUnwrapped        = T;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class Properties, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_collection<Collections::Collection<Layout, Properties, MetaInfo>>
      {
        static constexpr bool value = true;
        using MaybeUnwrapped        = Collections::CollectionHolder<Layout, Properties, MetaInfo>;
      };

      template <class T>
      inline constexpr bool is_collection_v = is_collection<T>::value;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_object
      {
        static constexpr bool value = false;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class Properties, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_object<Collections::Object<Layout, Properties, MetaInfo>>
      {
        static constexpr bool value = true;
      };

      template <class T>
      inline constexpr bool is_object_v = is_object<T>::value;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class SFINAE_helper = void>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_collection_holder_with_deferred_layout
      {
        static constexpr bool value = false;
        using MaybeUndeferred       = T;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class Properties, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
      is_collection_holder_with_deferred_layout<Collections::CollectionHolder<Layout, Properties, MetaInfo>,
                                                std::enable_if_t<Layout::collection_is_reference()>>
      {
        static constexpr bool value = true;
        using MaybeUndeferred       = Collections::CollectionHolder<typename Layout::OriginalLayoutType, Properties, MetaInfo>;
      };

      template <class T>
      inline constexpr bool is_collection_holder_with_deferred_layout_v = is_collection_holder_with_deferred_layout<T>::value;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_simplifiable_for_collection(Property p)
      {
        return InterfaceDescription::classify_property(p) == InterfaceDescription::SupportedTypes::NoObject;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_simplifiable_for_object(Property p)
      {
        return (InterfaceDescription::classify_property(p) == InterfaceDescription::SupportedTypes::NoObject) ||
               (InterfaceDescription::is_global(p) && !InterfaceDescription::has_extra_write_proxy_types(p));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto simplify_for_collection()
      {
        constexpr auto pre_ret =
          (Utility::TypeHolder<Utility::InvalidType> {} | ... |
           std::conditional_t<is_simplifiable_for_collection(Properties {}), Utility::TypeHolder<Utility::InvalidType>, Utility::TypeHolder<Properties>> {});

        return Utility::clean_type_holder(pre_ret);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Properties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto simplify_for_object()
      {
        constexpr auto pre_ret =
          (Utility::TypeHolder<Utility::InvalidType> {} | ... |
           std::conditional_t<is_simplifiable_for_object(Properties {}), Utility::TypeHolder<Utility::InvalidType>, Utility::TypeHolder<Properties>> {});

        return Utility::clean_type_holder(pre_ret);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool do_simplification, class T, class SFINAE_helper = void>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_simplifiable
      {
        static constexpr bool value = false;
        using MaybeSimplified       = T;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class... Properties, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
      is_simplifiable<true,
                      Collections::CollectionHolder<Layout, Utility::TypeHolder<Properties...>, MetaInfo>,
                      std::enable_if_t<((0 + ... + is_simplifiable_for_collection(Properties {})) > 0)>>
      {
        static constexpr bool value = true;
        using MaybeSimplified       = Collections::CollectionHolder<Layout, decltype(simplify_for_collection<Properties...>()), MetaInfo>;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class... Properties, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_simplifiable<true,
                                                                                Collections::Object<Layout, Utility::TypeHolder<Properties...>, MetaInfo>,
                                                                                std::enable_if_t<((0 + ... + is_simplifiable_for_object(Properties {})) > 0)>>
      {
        static constexpr bool value = true;
        using MaybeSimplified       = Collections::Object<Layout, decltype(simplify_for_object<Properties...>()), MetaInfo>;
      };

      template <bool really_do_simplification, class T>
      inline constexpr bool is_simplifiable_v = is_simplifiable<really_do_simplification, T>::value;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class SFINAE_helper = void>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES layout_can_get_array_pointers
      {
        static constexpr bool value = false;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES layout_can_get_array_pointers<T, std::enable_if_t<T::can_get_array_pointers()>>
      {
        static constexpr bool value = true;
      };

      template <class T>
      inline constexpr bool layout_can_get_array_pointers_v = layout_can_get_array_pointers<T>::value;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class Extra = void>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_transfer_specification_valid
      {
        static constexpr bool value = true;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
      is_transfer_specification_valid<T, std::enable_if_t<std::is_same_v<typename T::InvalidMarker, Utility::InvalidType>>>
      {
        static constexpr bool value = false;
      };

      template <class T>
      inline constexpr bool is_transfer_specification_valid_v = is_transfer_specification_valid<T>::value;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
      template <class... DestPropExs, class... SrcPropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool properties_equivalent(Utility::TypeHolder<DestPropExs...> dh,
                                                                                                 Utility::TypeHolder<SrcPropExs...>  sh)
      {
        constexpr bool dest_is_subset = (sh.template contains<DestPropExs>() && ...);

        if constexpr (bool(MARIONETTE_ALLOW_SLICING_COPIES))
          {
            return dest_is_subset;
          }
        else
          {
            constexpr bool source_is_subset = (dh.template contains<SrcPropExs>() && ...);

            return dest_is_subset && source_is_subset;

            //(A is subset of B && B is subset of A ) => (A = B)
          }
      }
    }
  }
}

#endif
