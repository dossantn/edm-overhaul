//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MARIONETTEBASE_H
#define MARIONETTE_MARIONETTEBASE_H

#ifndef MARIONETTE_ENABLE_PLATFORM_SUPPORT_FOR_CUDA
  ///@brief Whether or not to include CUDA-specific support in the library.
  ///
  ///Defaults to `1` only if we have reason to believe we will have CUDA support,
  ///but can of course be overridden by the end user.
  ///All CUDA device-specific operations are hidden behind `__CUDA_ARCH__` guards and the like,
  ///so it is also safe to allow support for CUDA as long as the runtime API can be linked,
  ///to compile the host side of things using a normal compiler.
  #if defined(__CUDACC__) || defined(__CUDA_ARCH__) || defined(__CUDA__)
    #define MARIONETTE_ENABLE_PLATFORM_SUPPORT_FOR_CUDA 1
  #else
    #define MARIONETTE_ENABLE_PLATFORM_SUPPORT_FOR_CUDA 0
  #endif

#endif

#ifndef MARIONETTE_PORTABILITY_ANNOTATION_ALLOW_FULL_OVERRIDE
  ///@brief If it evaluates to a truthy value,
  ///       allow the user to fully override the portability annotation
  ///       macros instead of only being able to add to them.
  ///
  ///The @c MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_CLASS,
  ///@c MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_FUNCTION,
  ///@c MARIONETTE_PORTABILITY_ANNOTATION_USER_CLASS_ATTRIBUTES and
  ///@c MARIONETTE_PORTABILITY_ANNOTATION_USER_FUNCTION_ATTRIBUTES macros
  ///enable adding user-provided annotations at the respective locations,
  ///but Marionette defines some more generic annotations that are included
  ///(e. g. `__host__ __device__` for CUDA or HIP) when appropriate.
  ///
  ///In some cases, the end user may wish to avoid adding such annotations,
  ///in which case @c MARIONETTE_PORTABILITY_ANNOTATION_ALLOW_FULL_OVERRIDE
  ///can be set to a truthy value and the relevant global macro defined to override it:
  ///@c MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS,
  ///@c MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION,
  ///@c MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES or
  ///@c MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES.
  ///
  ///The macros that are overridden will not take into account
  ///the `*_USER_*` macro, so it is redundant to specify both.
  ///
  ///Defaults to `0`.
  ///
  #define MARIONETTE_PORTABILITY_ANNOTATION_ALLOW_FULL_OVERRIDE 0
#endif

#ifndef MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_CLASS
  ///@brief Annotation(s) to be added before all class declarations and definitions
  ///       (and also before the template parameters if the class is templated)
  ///       besides the ones added by default by Marionette.
  ///
  ///Defaults to nothing.
  ///
  ///@remark If @c MARIONETTE_PORTABILITY_ANNOTATION_ALLOW_FULL_OVERRIDE
  ///        evaluates to a truthy value and the @c MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS
  ///        macro is defined, this will not be used.
  ///
  ///@remark Some platform-specific annotations, if necessary or relevant,
  ///        are already set internally by Marionette,
  ///        but the user should extend this according to their needs.
  ///
  ///@remark The `_Pragma` or `__pragma` compiler intrinsics can be useful
  ///        to include pragmas before classes.
  ///
  ///@warning Providing an incorrect or unacceptable value for this
  ///         will most likely result in many ugly compilation errors!
  #define MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_CLASS
#endif

#ifndef MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_FUNCTION
  ///@brief Annotation(s) to be added before all function declarations and definitions
  ///       (and also before the template parameters if the function is templated)
  ///       besides the ones added by default by Marionette.
  ///
  ///Defaults to nothing.
  ///
  ///@remark If @c MARIONETTE_PORTABILITY_ANNOTATION_ALLOW_FULL_OVERRIDE
  ///        evaluates to a truthy value and the @c MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
  ///        macro is defined, this will not be used.
  ///
  ///@remark Some platform-specific annotations, if necessary or relevant,
  ///        are already set internally by Marionette,
  ///        but the user should extend this according to their needs.
  ///
  ///@remark The `_Pragma` or `__pragma` compiler intrinsics can be useful
  ///        to include pragmas before functions.
  ///
  ///@warning Providing an incorrect or unacceptable value for this
  ///         will most likely result in many ugly compilation errors!
  #define MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_FUNCTION
#endif

#ifndef MARIONETTE_PORTABILITY_ANNOTATION_USER_CLASS_ATTRIBUTES
  ///@brief Annotation(s) to be added where class attributes usually go,
  ///       after the `class` or `struct` keyword and before the name.
  ///
  ///Defaults to nothing.
  ///
  ///@remark If @c MARIONETTE_PORTABILITY_ANNOTATION_ALLOW_FULL_OVERRIDE
  ///        evaluates to a truthy value and the @c MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
  ///        macro is defined, this will not be used.
  ///
  ///@remark Some platform-specific annotations, if necessary or relevant,
  ///        are already set internally by Marionette,
  ///        but the user should extend this according to their needs.
  ///
  ///@warning Providing an incorrect or unacceptable value for this
  ///         will most likely result in many ugly compilation errors!
  #define MARIONETTE_PORTABILITY_ANNOTATION_USER_CLASS_ATTRIBUTES
#endif

#ifndef MARIONETTE_PORTABILITY_ANNOTATION_USER_FUNCTION_ATTRIBUTES
  ///@brief Annotation(s) to be added where function attributes usually go,
  ///       before any declaration specifiers (e. g. `static`, `constexpr`, `friend`) or return types.
  ///
  ///Defaults to nothing.
  ///
  ///@remark If @c MARIONETTE_PORTABILITY_ANNOTATION_ALLOW_FULL_OVERRIDE
  ///        evaluates to a truthy value and the @c MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
  ///        macro is defined, this will not be used.
  ///
  ///@remark Some platform-specific annotations, if necessary or relevant,
  ///        are already set internally by Marionette,
  ///        but the user should extend this according to their needs.
  ///
  ///@warning Providing an incorrect or unacceptable value for this
  ///         will most likely result in many ugly compilation errors!
  #define MARIONETTE_PORTABILITY_ANNOTATION_USER_FUNCTION_ATTRIBUTES
#endif

#ifndef MARIONETTE_USE_ASSERTIONS
  ///@brief Whether to use assertions throughout the code to
  ///       prevent out-of-bounds accesses.
  ///
  ///Defaults to `1`.
  ///
  ///@remark Provided to allow more granularity in turning off assertions
  ///        than when using a top-level `#define NDEBUG`.
  #define MARIONETTE_USE_ASSERTIONS 1
#endif

#ifndef MARIONETTE_INCLUDE_POTENTIALLY_THROWING_FUNCTIONS
  ///@brief Whether to include functions that may throw
  ///       (namely `at()`).
  ///
  ///Defaults to `1`.
  ///
  #define MARIONETTE_INCLUDE_POTENTIALLY_THROWING_FUNCTIONS 1
#endif

#ifndef MARIONETTE_WARN_ON_ELEMENTWISE_VECTOR_OPERATIONS
  ///@brief If it evaluates to a truthy value,
  ///       mark the element-wise copies required by the
  ///       vector API provided within VectorLikeWrapper
  ///       with the `[[deprecated]]` attribute,
  ///       to warn when these are used instead of the
  ///       (hopefully more efficient) specializations of `TransferSpecification`.
  ///
  ///Defaults to `1`.
  ///
  #define MARIONETTE_WARN_ON_ELEMENTWISE_VECTOR_OPERATIONS 1
#endif

#ifndef MARIONETTE_WARN_ON_JAGGED_VECTOR_NON_RESIZING_COPY
  ///@brief If it evaluates to a truthy value,
  ///       warn when performing collection-wide copies
  ///       of jagged vectors without the collection being resizable,
  ///       which could otherwise (with mismatched sizes)
  ///       lead to silent truncations.
  ///
  ///Defaults to `1`.
  ///
  #define MARIONETTE_WARN_ON_JAGGED_VECTOR_NON_RESIZING_COPY 1
#endif

#ifndef MARIONETTE_WARN_ON_DEFAULT_PASS_BY_VALUE
  ///@brief If it evaluates to a truthy value,
  ///       use the `[[deprecated]]` attribute to
  ///       warn the user when a pass-by-value of
  ///       a collection falls back on the default
  ///       behaviour of copying a pointer to the
  ///       beginning of every array.
  ///
  ///Defaults to `1`.
  ///
  #define MARIONETTE_WARN_ON_DEFAULT_PASS_BY_VALUE 1
#endif

#ifndef MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
  ///@brief If it evaluates to a truthy value,
  ///       use the `[[deprecated]]` attribute to
  ///       warn the user of certain behaviours that,
  ///       while technically valid, may not be fully
  ///       intended, but for which no further granularity
  ///       in the warnings was warranted.
  ///
  ///Defaults to `1`.
  ///
  #define MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR 1
#endif

#ifndef MARIONETTE_REQUIRE_UNIQUE_PROPERTIES
  ///@brief If it evaluates to a truthy value,
  ///       check for, and produce a compile error on,
  ///       repeated properties within the same
  ///       collection or object specification.
  ///
  ///Defaults to `1`.
  ///
  ///@remark Checking incurs some compile-time cost
  ///        (with recursive template instantiations
  ///         based on the total number of properties),
  ///        and furthermore repeated properties may result
  ///        in other compile time errors by themselves,
  ///        though they are likely to be much less user-friendly.
  #define MARIONETTE_REQUIRE_UNIQUE_PROPERTIES 1
#endif

#ifndef MARIONETTE_GENERIC_OPERATE_API_SHOULD_BE_PUBLIC
  ///@brief Whether to make the @c operate_on_all_objects and @c operate_on_all_collections
  ///       accessible as `public` member functions of the struct-of-array
  ///       holder classes, or keep them just as protected.
  ///
  ///Defaults to `1`.
  ///
  ///@remark We provide the capability to disable this
  ///        since the generic operate API may not be
  ///        the most user-friendly, and, in general,
  ///        outside of very special cases,
  ///        should not be necessary.
  ///        (Plus, one always has `FriendshipProvider`...).
  #define MARIONETTE_GENERIC_OPERATE_API_SHOULD_BE_PUBLIC 1
#endif

#ifndef MARIONETTE_DEFAULT_GETTER_PREFIX
  ///@brief The default prefix to append to property getters. Can be overridden on a per interface basis.
  ///
  ///Defaults to nothing (no prefix).
  ///
  #define MARIONETTE_DEFAULT_GETTER_PREFIX
#endif

#ifndef MARIONETTE_DEFAULT_GETTER_USE_CAPITALS
  ///@brief Whether to capitalize the first letter of the property after the getter prefix.
  ///       Requires correctly specifying the capitalized forms of the properties in the interface description.
  ///
  ///Defaults to `0`.
  ///
  #define MARIONETTE_DEFAULT_GETTER_USE_CAPITALS 0
#endif

#ifndef MARIONETTE_DEFAULT_SETTER_PREFIX
  ///@brief The default prefix to append to property setters. Can be overridden on a per interface basis.
  ///
  ///Defaults to `set`.
  ///
  #define MARIONETTE_DEFAULT_SETTER_PREFIX set
#endif

#ifndef MARIONETTE_DEFAULT_SETTER_USE_CAPITALS
  ///@brief Whether to capitalize the first letter of the property after the getter prefix.
  ///       Requires correctly specifying the capitalized forms of the properties in the interface description.
  ///
  ///Defaults to `1`.
  ///
  #define MARIONETTE_DEFAULT_SETTER_USE_CAPITALS 1
#endif

#ifndef MARIONETTE_USE_SIMPLE_ACCESSORS
  ///@brief If it evaluates to a truthy value,
  ///       collection objects that hold a single basic type
  ///       allow direct access to it instead of building proxies
  ///       (which would add any other functions specified in the interface,
  ///        but might be slightly less efficient if not properly optimized).
  ///
  ///Defaults to `0`.
  ///
  #define MARIONETTE_USE_SIMPLE_ACCESSORS 0
#endif

#ifndef MARIONETTE_ALLOW_SLICING_COPIES
  ///@brief If it evaluates to a truthy value,
  ///       one can copy from a collection or an object to another
  ///       that holds just a subset of its properties.
  ///
  ///Defaults to `1`.
  ///
  #define MARIONETTE_ALLOW_SLICING_COPIES 1
#endif

#ifndef MARIONETTE_CONSIDER_SPACESHIP_OPERATOR_OVERLOAD
  ///@brief If it evaluates to a truthy value,
  ///       we check for the so-called spaceship operator
  ///       `operator<=>`, which is only valid in
  ///       C++20.
  ///
  ///Defaults to `1` if the feature test macro `__cpp_impl_three_way_comparison`
  ///returns a value that signals this feature is available, `0` otherwise.
  ///

  #if defined(__cpp_impl_three_way_comparison) && __cpp_impl_three_way_comparison >= 201907L
    #define MARIONETTE_CONSIDER_SPACESHIP_OPERATOR_OVERLOAD 1
  #else
    #define MARIONETTE_CONSIDER_SPACESHIP_OPERATOR_OVERLOAD 0
  #endif

  //The check is automated based on feature test flags,
  //which themselves are C++20, but the user can override this...

#endif

#ifndef MARIONETTE_OVERLOAD_ADDRESS_OF_OBJECT
  ///@brief If it evaluates to a truthy value,
  ///       taking the address of an object
  ///       will return the address of the
  ///       actual variable instead of the
  ///       potential wrapper or proxy.
  ///
  ///If such an overload is done,
  ///one can no longer take the address
  ///of our proxy objects and expect it
  ///to refer to that proxy object
  ///(barring the use of `std::addressof`).
  ///As, for most use cases,
  ///the address of the proxy object
  ///is of no consequence
  ///while getting the address
  ///of the underlying variable
  ///is often very useful,
  ///we do this by default.
  ///
  ///Defaults to `1`.
  ///
  #define MARIONETTE_OVERLOAD_ADDRESS_OF_OBJECT 1
#endif

#ifndef MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
  ///@brief If it evaluates to a truthy value,
  ///       objects also take into account the rarely overloaded
  ///       logic operators `operator&&` and `operator||`
  ///       and comma operator `operator,` (all binary).
  ///
  ///Defaults to `0`.
  ///
  ///While all overloadable, they are rare enough and
  ///may cause surprising enough behaviour
  ///that it is not very common to find them overloaded in user types,
  ///and furthermore that new surprising behaviour would be
  ///inadvertently passed on to our Object classes.
  ///
  #define MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS 0
#endif

#ifndef MARIONETTE_PROVIDE_STANDARD_VECTOR_CONSTRUCTORS_AND_ASSIGNMENT
  ///@brief If it evaluates to a truthy value,
  ///       our @c VectorLikeWrapper will ensure the
  ///       class implements the vector
  ///       constructors: size, size and default value,
  ///       initializer list, begin and end iterator.
  ///
  ///Defaults to `1`.
  ///
  ///@remark It can be useful for debugging transfer specializations
  ///        to disable this...
  #define MARIONETTE_PROVIDE_STANDARD_VECTOR_CONSTRUCTORS_AND_ASSIGNMENT 1
#endif

#ifndef MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
  ///@brief If it evaluates to a truthy value,
  ///       some checks and printouts are added
  ///       to the transfer specifications and
  ///       additional functions are available
  ///       in the interface of our Collections and Objects,
  ///       to aid in debugging the copy, move and/or conversion operations...
  ///
  ///Defaults to `0`.
  #define MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS 0

#endif

#ifndef MARIONETTE_TURN_VECTOR_LIKE_WRAPPER_INTO_IDEMPOTENT
  ///@brief If it evaluates to a truthy value,
  ///       VectorLikeWrapper becomes idempotent:
  ///       it is merely an alias for the type itself.
  ///       Only useful for debugging.
  ///
  ///Defaults to `0`.
  #define MARIONETTE_TURN_VECTOR_LIKE_WRAPPER_INTO_IDEMPOTENT 0

#endif

#ifndef MARIONETTE_RESTRICT_POINTER_QUALIFICATION
  #if defined(_MSC_VER)
    #define MARIONETTE_RESTRICT_POINTER_QUALIFICATION __restrict
  #else
    #define MARIONETTE_RESTRICT_POINTER_QUALIFICATION __restrict__
    //We may get some compiler errors if the compiler does not support restrict,
    //but this should cover the big three already, and attempting to cover all other cases
    //would be... not particularly relevant for now, especially since (at some point)
    //we may have a better, portable solution for this.
  #endif
#else
  #error "The macro MARIONETTE_RESTRICT_POINTER_QUALIFICATION is reserved by the Marionette library. Please reorder your includes if you define it elsewhere."
#endif

#include <cstddef>
#include <utility>
#include <type_traits>
#include <iterator>
#include <vector>
#include <memory>

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
  #include <cstdio>
#endif

#include "PortabilityAnnotations.h"

namespace Marionette
{

  ///@brief The type that we will use to index the properties of the objects.
  ///
  ///@remark Ultimately, this is limited by the maximum number of non-virtual
  ///        direct and indirect base classes. The suggested minimum limit
  ///        in the C++ standard is 16384, meaning that one can surely consider
  ///        objects with at least up to a few thousand different properties in total,
  ///        which should be more than enough for any sane use cases.
  ///        It appears that most mainstream compilers support at least 2^16 base classes.
  ///
  ///@remark On the other hand, certain kinds of operations might depend
  ///        on recursive template instantiations; while the limit is usually
  ///        controllable by some compiler-switch, the default would be 900 for GCC,
  ///        1024 for Clang (following the recommended value by the standard),
  ///        and, if testing on Godbolt serves, somewhere in the vicinity of 249 for MSVC,
  ///        though there does not seem to be a way to increase this last limit.
  ///        Care was taken in the implementation to avoid using recursive templates
  ///        (though that requires EBO to work properly, a second issue with MSVC
  ///         that can be fixed through the @c MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES definition)
  ///        for the majority of the functionality, but nesting sets of properties too deep
  ///        and other things like that may push the implementation to its limits.
  ///
  ///@remark Other possible problem is the maximum number of nested operations,
  ///        which can in some cases limit the extent of pack expansion
  ///        (this was seen in Clang, which provides a flag to override the default limit of 256).
  ///        During some preliminary testing, MSVC also fails with packs ~746 elements long,
  ///        with an error regarding `parser stack overflow, program too complex`.
  ///        Again, this should not be a concern in any sane use case,
  ///        but it should be kept in mind in case of silent/weird failures.
  ///
  ///@remark In short, and especially if GPU compatibility is in question,
  ///        using an @c int instead of a @c size_t makes perfect sense for this,
  ///        with the (slightly) higher possibility of an overflow being automatically prevented
  ///        by the stricter limits that most compilers have on dealing with templates,
  ///        parameter packs and associated metaprogramming tricks, which means
  ///        code that is liable to overflow will not even compile...
  ///
  using PropertiesIndexer = int;

  ///@brief The type that will be used to express the maximum size of properties.
  ///
  ///@remark Yes, we could have simply used std::size_t instead of typedef'ing it,
  ///        but in some cases maybe it will be useful to constrain this to a 32-bit integer...
  ///
  using MaximumSizeType = std::size_t;

  ///@brief Specifies the kind of access one has into a collection or object.
  ///
  ///We will provide binary `operator&` for access properties intersection
  ///(the most restrictive win, e. g., it's the minimum)
  ///and binary `operator|` for access properties union
  ///(the least restrictive win, e. g., it's the maximum).
  ///
  ///We will provide unary `operator-` for increasing the "restrictiveness"
  ///of the access properties.
  ///
  ///Comparisons for more or less "freedom" can be done
  ///through the usual comparison operators.
  ///
  ///\remark While the current set of properties has a total order,
  ///        it is not inconceivable that at some point it may only have
  ///        a partial order, depending on the fineness of the distinctions
  ///        we may want to make. The comparison operators will reflect this,
  ///        so that `a >= b` will still be true as long as `a` is not more restrictive
  ///        than b, as well as the intersection and union operators.
  enum class AccessProperties : PropertiesIndexer
  {
    Invalid = 0,       ///< Impossible to deal with the memory in any way, shape or form. Most restrictive access property.
    GetAddress,        ///< Possible to know where in memory the values are stored.
    GetContents,       ///< Possible to actually read the values.
    Full = GetContents ///< The least restrictive access property: everything is possible!
  };

  ///@brief Specifies the kind of resizing capabilities one has in regards to a collection or object.
  ///
  ///We will provide binary `operator&` for resize properties intersection
  ///(the most restrictive win, e. g., it's the minimum)
  ///and binary `operator|` for resize properties union
  ///(the least restrictive win, e. g., it's the maximum).
  ///
  ///We will provide unary `operator-` for increasing the "restrictiveness"
  ///of the resize properties.
  ///
  ///Comparisons for more or less "freedom" can be done
  ///through the usual comparison operators.
  ///
  ///\remark While the current set of properties has a total order,
  ///        it is not inconceivable that at some point it may only have
  ///        a partial order, depending on the fineness of the distinctions
  ///        we may want to make. The comparison operators will reflect this,
  ///        so that `a >= b` will still be true as long as `a` is not more restrictive
  ///        than b, as well as the intersection and union operators.
  enum class ResizeProperties : PropertiesIndexer
  {
    Invalid = 0,   ///< Impossible to deal with the memory in any way, shape or form. Most restrictive access property.
    NoResize,      ///< Impossible to resize the collection.
    ResizePartial, ///< Possible to modify values and resize particular arrays that have a different size tag (e. g. jagged vectors, global properties).
    Resize,        ///< Possible to resize the entire collection.
    Move,          ///< Possible to move from this collection (and potentially deallocate its memory).
    Full = Move    ///< The least restrictive access property: everything is possible!
  };

  ///@brief Specifies the kind of mutability of a collection or object.
  ///
  ///We will provide binary `operator&` for mutability properties intersection
  ///(the most restrictive win, e. g., it's the minimum)
  ///and binary `operator|` for mutability properties union
  ///(the least restrictive win, e. g., it's the maximum).
  ///
  ///We will provide unary `operator-` for increasing the "restrictiveness"
  ///of the mutability properties.
  ///
  ///Comparisons for more or less "freedom" can be done
  ///through the usual comparison operators.
  ///
  ///\remark While the current set of properties has a total order,
  ///        it is not inconceivable that at some point it may only have
  ///        a partial order, depending on the fineness of the distinctions
  ///        we may want to make. The comparison operators will reflect this,
  ///        so that `a >= b` will still be true as long as `a` is not more restrictive
  ///        than b, as well as the intersection and union operators.
  enum class MutabilityProperties : PropertiesIndexer
  {
    Invalid = 0,  ///< Impossible to deal with the memory in any way, shape or form. Most restrictive access property.
    NoModify,     ///< Impossible to modify the stored values.
    Modify,       ///< Possible to modify values.
    Full = Modify ///< The least restrictive mutability property: everything is possible!
  };

  ///@class InterfaceProperties
  ///
  ///A way to express the capabilities of the interface we want to provide
  ///for our collections and objects, namely in terms of how they
  ///can be accessed (see @c AccessProperties),
  ///resized (see @c ResizeProperties)
  ///or modified (see @c MutabilityProperties).
  ///
  ///@remark Template arguments used in type instantiations
  ///        should not depend the particular
  ///        value of the interface properties
  ///        returned by a layout, as it can change
  ///        e. g. between CPU and GPU.
  ///        One may limit the interface properties
  ///        (using the appropriate `operator &`),
  ///        such as what is done in our view layouts,
  ///        and use them freely for SFINAE'ing out
  ///        undesired functions, but never as an argument
  ///        to define a type based on their value...
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES InterfaceProperties;

  ///@namespace Utility
  ///@brief Contains useful helper functions and classes that are nevertheless ready for external consumption.
  namespace Utility
  {
    ///@class TypeHolder
    ///Holds a set of types, for easier manipulation of parameter packs in compile time.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Types>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TypeHolder;

    ///@class AlwaysFalse
    ///A class whose @c value static member is always @c false
    ///so that static_assert works.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Ts>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES AlwaysFalse
    {
      static constexpr bool value = false;
    };

    ///@brief False for every template argument,
    ///       useful for `static_assert` and other
    ///       compile-time checks that cannot be
    ///       trivially false (e. g. SFINAE).
    template <class... Ts>
    inline constexpr bool always_false = AlwaysFalse<Ts...>::value;

    ///@brief True for every template argument,
    ///       useful for `static_assert` and other
    ///       compile-time checks that cannot be
    ///       trivially true (e. g. SFINAE).
    template <class... Ts>
    inline constexpr bool always_true = !AlwaysFalse<Ts...>::value;

    ///@class InvalidType
    ///An altogether empty structure that serves to mark invalid types,
    ///potentially useful for compile-time checks and lookups.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES InvalidType
    {
      using Type                                = InvalidType;
      using Properties                          = TypeHolder<InvalidType>;
      static constexpr PropertiesIndexer number = 1;
      //For certain metaprogramming shenanigans.
    };

    ///@brief Remove an @c InvalidType from the beginning of the @c TypeHolder.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto clean_type_holder(TypeHolder<Ts...>);

    ///@brief Returns the first type of the @c TypeHolder.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_first_type(TypeHolder<T, Ts...>);

    ///@brief Returns the last type of the @c TypeHolder.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_last_type(TypeHolder<Ts...>);

    ///@brief If the @c TypeHolder has just one type, return it. Else, return @c InvalidType.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_one_type_or_invalid(TypeHolder<Ts...>);

    ///@brief Returns all the types but the first of the @c TypeHolder.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto skip_first_type(TypeHolder<T, Ts...>);

#if MARIONETTE_TURN_VECTOR_LIKE_WRAPPER_INTO_IDEMPOTENT
    template <class Base>
    using VectorLikeWrapper = Base;
#else
    ///@class VectorLikeWrapper
    ///
    ///Provides a vector-like API to a class,
    ///which must implement typedefs
    ///@c size_type and @c difference_type,
    ///as well as @c size, @c resize, @c clear,
    ///@c reserve, @c capacity and @c shrink_to_fit
    ///member functions if applicable.
    ///It must further either define `make_const_read_proxy`,
    ///`make_read_proxy` and `make_write_proxy` member functions
    ///(the first two take an index, the last takes no arguments,
    /// `make_const_read_proxy` and `make_write_proxy` are @c const)
    ///or have @c ReadProxy, @c ConstReadProxy, @c WriteProxy typedefs.
    ///A ConstReadProxy must be constructible from a ReadProxy
    ///and a WriteProxy must be constructible from both.
    ///A ReadProxy must be assignable from any of the others.
    ///ConstReadProxy and ReadProxy must be constructible
    ///from a pointer to the class + index of type size_type
    ///(with some caveats brought up by internal needs:
    /// if @p Base defines a `allows_simple_accessor()`
    /// constexpr function which returns @c true,
    /// read proxies are generated by `pointer->simple_accessor(index)`;
    /// if @p Base defines a `specifies_access_operator()`
    /// constexpr function which returns @c true,
    /// its `operator []` and `at()` functions are used
    /// instead of the natural definition for a vector;
    /// if @p Base defines a `get_owning_pointer` member function,
    /// the proxies are constructed from `ptr->get_owning_pointer()` and index).
    ///All must also be default constructible.
    ///
    ///If @p Base defines an `access_properties` member function,
    ///it will be used to constrain the API
    ///from allowing resizing, modifying or accessing the elements.
    ///Otherwise, it will be assumed all those operations are possible.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Base>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES VectorLikeWrapper;
#endif

    ///@class FriendshipProvider
    ///The least inelegant hack to go around access specifier restrictions
    ///in heavily templated code where friend declarations could get messier...
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES FriendshipProvider;

    ///@brief Checks if the iterator type @p It is a random-access iterator
    ///       or a pointer, for efficient initializer list interoperation.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class It>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_random_access_iterator_or_pointer();

    ///@class arrow_operator_wrapper
    ///Wraps a type such that `operator->` works as intended.
    ///Optionally static casts the pointer into a different pointer type.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class RetT = T>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES arrow_operator_wrapper;

    ///@brief Wraps a type in an @c arrow_operator_wrapper.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class RetT = InvalidType, class T>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto arrow_operator_wrap(T && t);

    ///@class DefaultInitializer
    ///
    ///A class to specify default initialization
    ///(e. g. in array property initialization).
    ///It will decay to a default-constructed value
    ///of any type one requests it to,
    ///though our Collections and Objects
    ///may have more optimal behaviour ("do nothing")
    ///as a special case of the relevant @c TransferSpecification.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DefaultInitializer
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr operator T() const
      {
        return T {};
      }
    };

    inline constexpr DefaultInitializer default_initialize {};

    ///@brief Describes the kind of array initialization one wants to do
    ///       in case a smaller number of initializers is provided
    ///       than necessary.
    enum class ArrayInitialize : PropertiesIndexer
    {
      Default = 0, ///< Default construct the remaining elements. (Also the C++ default for real array initialization...)
      RepeatLast,  ///< Repeat the last element for the rest.
      Wrap         ///< Wrap the elements using modular arithmetic (essentially, `arr[i] = init[i % init.extent()]`).
    };

    ///@class ArrayInitializationHelper
    ///
    ///Specifies how to initialize objects or collections that have an array-like behaviour,
    ///using the @p ArrayInitialize parameter to determine how to handle
    ///initializing objects or collections with more entries than the ones provided.
    ///Handles copying or moving, depending on the value category of the arguments.
    ///It's typically much more practical to initialize this through @c array_initialize.
    ///
    ///@remark When initializing or assigning to a collection that holds a property array,
    ///        the size of the last object to be assigned "wins".
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <ArrayInitialize initialization, class... Ts>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ArrayInitializationHelper;

    ///@brief Initializes multiple arrays using an appropriate instantiation of @c ArrayInitializationHelper
    ///Example usage:
    ///```
    ///array_like_coll = Marionette::Utility::array_initialize(c1, c2, Marionette::Utility::default_initialize, std::move(c3));
    ///```
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <ArrayInitialize initializer = ArrayInitialize::Default, class... Ts>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto array_initialize(Ts &&... ts);

    ///@brief A simple allocator that implements all behaviour
    ///       through the functions provided in the @c MemoryContext,
    ///       relying on @c MemoryContextInfo to maintain the state
    ///       (if any).
    ///
    ///@remark It can never satisfy the Allocator named requirement
    ///        from the standard library, as it explicitly depends
    ///        on a different interface for `allocate` and `deallocate`
    ///        (receiving the @c ContextInfo as an argument).
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class MemoryContext>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextAwareAllocator;

    ///@brief A version of @p ContextAwareAllocator that always uses the local context.
    ///
    ///@remark It can never satisfy the Allocator named requirement
    ///        from the standard library, as it explicitly depends
    ///        on a different interface for `allocate` and `deallocate`
    ///        (receiving the @c ContextInfo as an argument).
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES LocalAllocator;

    ///@brief A simple vector-like object that implements the
    ///       basic behaviour of allocating, reserving and freeing
    ///       based on a memory context.
    ///
    ///@remark By design, we will only care about supporting
    ///        trivially copyable and destructible types.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class MemoryContext, class SizeType, class DifferenceType>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextAwareVectorBase;

    ///@brief A @p VectorLikeWrapper wrapping a @p ContextAwareVectorBase
    ///       to provide the full vector interface (though with potential
    ///       differences in performance...).
    template <class T, class MemoryContext, class SizeType, class DifferenceType>
    using ContextAwareVector = VectorLikeWrapper<ContextAwareVectorBase<T, MemoryContext, SizeType, DifferenceType>>;

    ///@brief A simple vector-like object that implements the
    ///       basic behaviour of allocating, reserving and freeing
    ///       in the local memory context.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class SizeType, class DifferenceType>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES LocalVectorBase;

    ///@brief A @p VectorLikeWrapper wrapping a @p LocalVectorBase
    ///       to provide the full vector interface (though with potential
    ///       differences in performance...).
    template <class T, class SizeType, class DifferenceType>
    using LocalVector = VectorLikeWrapper<LocalVectorBase<T, SizeType, DifferenceType>>;

  }

  ///@namespace InterfaceDescription
  ///@brief Contains definitions used in defining interfaces for our struct-of-arrays-like holding classes.
  ///       Actual interface definitions must derive from these base classes.
  namespace InterfaceDescription
  {
    ///@class NoObject
    ///Base class for a property that does not need any stored data.
    ///(Useful for adding common functionality to several collections or objects.)
    ///
    ///Use a `template <class F, class Layout> CollectionFunctions` typedef to add functions to the class.
    ///Use a `template <class F, class Layout> ObjectFunctions` typedef to add functions to the objects.
    ///
    ///Functions should use `static_cast<F>(this)` to get access to other functions or properties of the final class.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES NoObject
    {
    };

    ///@class PerItemObject
    ///Base class for a property that is meant to be held
    ///for all entries in a given (SoA) collection.
    ///
    ///Use a @c Type typedef to specify the underlying type,
    ///which must be trivially copyable.
    ///
    ///Getters and setters, if desired, must be specified.
    ///
    ///Use a `template <class F, class Layout> CollectionFunctions` typedef to add functions to the class.
    ///Use a `template <class F, class Layout> ObjectFunctions` typedef to add functions to the objects.
    ///
    ///Functions should use `static_cast<F>(this)` to get access to other functions or properties of the final class.
    ///
    ///An overall maximum size for the entries of the object array
    ///can be specified through a `static constexpr MaximumSizeType max_entries` member variable.
    ///
    ///
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES PerItemObject
    {
    };

    ///@class JaggedVector
    ///Base class for a property that specifies a set of properties
    ///that are meant to be held as a jagged vector:
    ///one "vector" for each property per entry of the collection,
    ///all "vectors" held contiguously.
    ///
    ///Use a @c Properties typedef to specify the properties to be held like this
    ///in a @c Utility::TypeHolder. None of the properties (or their children)
    ///may correspond to a JaggedVector.
    ///(Nested jagged vectors would be possible, but much more cumbersome to implement.)
    ///
    ///Use a @c size_type typedef to specify the type that can hold the size of the vectors.
    ///Though the actual type to be used in the jagged vector interface is the same
    ///as the rest of the collection (and thus determined by the layout),
    ///this will determine the actual storage space for the vector sizes.
    ///(Though of course being consistent between them is encouraged.)
    ///
    ///Use a `template <class F, class Layout> CollectionFunctions` typedef to add functions to the class.
    ///Any `CollectionFunctions` of the properties inside the jagged vector are also added to the
    ///collection's interface directly (e. g., to allow accessing the contiguous arrays that hold the jagged vectors).
    ///Use a `template <class F, class Layout> ObjectFunctions` typedef to add functions to the objects.
    ///
    ///Functions should use `static_cast<F>(this)` to get access to other functions or properties of the final class.
    ///
    ///A maximum size for the number of jagged vectors
    ///(that is, the number of objects that the containing collection will hold data for)
    ///can be specified through a `static constexpr MaximumSizeType max_entries` member variable.
    ///
    ///@remark Currently, there is no support to refer to a sub-collection of a jagged vector property globally,
    ///        that is, `coll.jagged_vector()[i].property()[j]` does not work. The individual items can obviously
    ///        still be accessed: `coll.jagged_vector()[i][j].property()` works as intended.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES JaggedVector
    {
    };

    ///@class SubCollection
    ///Signals a set of properties that are meant to be held as a sub-collection:
    ///similar to a "proper collection", it can hold per-item properties, jagged vectors
    ///global objects or even other sub-collections (within nested template instantiation limits),
    ///providing an interface of the form: `coll.sub_coll().prop_1()`,
    ///with `coll[i].sub_coll().prop_1()`, `coll.sub_coll(i).prop_1()` and `coll.sub_coll().prop_1(i)`
    ///all being equivalent (barring intermediate proxy objects, which might/should/will be optimized out).
    ///
    ///Use a @c Properties typedef to specify the properties to be held like this
    ///in a @c Utility::TypeHolder.
    ///
    ///Use a `template <class F, class Layout> CollectionFunctions` typedef to add functions to the class.
    ///Use a `template <class F, class Layout> ObjectFunctions` typedef to add functions to the objects.
    ///
    ///Functions should use `static_cast<F>(this)` to get access to other functions or properties of the final class.
    ///
    ///An overall maximum size for the entries of the sub-collection
    ///can be specified through a `static constexpr MaximumSizeType max_entries` member variable.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SubCollection
    {
    };

    ///@class PropertyArray
    ///Base class for a set of properties that are meant to be held
    ///for all entries in a given (SoA) collection and that have a fixed number of entries
    ///that should be held separately.
    ///
    ///Use a @c Properties typedef to specify the properties to be replicated
    ///in a @c Utility::TypeHolder.
    ///Use a @c number static constexpr PropertiesIndexer member variable to specify the number of separate entries.
    ///
    ///Getters and setters, if desired, must be specified.
    ///
    ///Use a `template <class F, class Layout> CollectionFunctions` typedef to add functions to the class.
    ///Use a `template <class F, class Layout> ObjectFunctions` typedef to add functions to the objects.
    ///
    ///Functions should use `static_cast<F>(this)` to get access to other functions or properties of the final class.
    ///
    ///An overall maximum size for the entries of each object array
    ///can be specified through a `static constexpr MaximumSizeType max_entries` member variable.
    ///
    ///@warning If any of the properties or their children correspond to another PropertyArray,
    ///         the unspecified accesses must come at the end:
    ///         `coll.parr1()[a].parr2()[b].parr3()` works to refer to all the `parr3` sub-arrays,
    ///         `coll.parr1()[a].parr2().parr3()` also works to refer to all the `parr3` sub-arrays within the `parr2` sub-arrays.
    ///         `coll.parr1().parr2()[a]` will refer to the entire row of objects at index `a` of the `parr1().parr2()` array.
    ///
    ///@warning If any of the properties or their children correspond to JaggedVectors,
    ///         unspecified accesses into the properties will lead to unspecified behaviour:
    ///         `coll.parr1()[i].jagvec()[a][b]` works,
    ///         but `coll.parr1().jagvec()[a][b]` will give undefined behaviour
    ///         (in the current implementation, it will read from the first array,
    ///          but this behaviour is not to be relied upon as it makes no syntactic sense).
    ///
    ///@warning Unspecified accesses take the minimum size and capacity among the objects;
    ///         if the sizes are different (e. g. for global objects resized individually),
    ///         any operations that change the size may result in unwanted truncation of the larger objects.
    ///
    ///@warning User defined functions defined at the collection and/or object level
    ///         may result in undefined behaviour when using unspecified accesses
    ///         if they depend on reading from or otherwise operating with specific collection objects.
    ///         Simply getting a collection/object should be safe (e. g. the usual accessors).
    ///
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES PropertyArray
    {
    };

    ///@class GlobalProperty
    ///Signals that a property is not meant to be resized together with the rest of the collection,
    ///being managed through other means, and that the collection objects do not need to include it.
    ///
    ///If the property has a `static constexpr bool track_individual_size` variable that evaluates to @c false,
    ///the size of this GlobalProperty will not be individually tracked,
    ///using the "parent" size tag (which may itself be a global property, though such nested structures
    ///are overly complicated and are probably symptomatic of some design problems...).
    ///In this case, having a `template <class SizeTag> const global_object_size` and the equivalent
    ///`template <class SizeTag> const global_object_capacity` functions declared
    ///in the `CollectionFunctions` for this property that take a @c PropertiesIndexer as an argument
    ///(the array index, when dealing with property arrays), and the property's `SizeTag` as the template argument,
    ///is required for the data transfers to make sense.
    ///
    ///A GlobalProperty inside another GlobalProperty also has its size tracked individually
    ///(with the obvious exception in case `track_individual_size` is @c false).
    ///
    ///If a global property tracks its size individually, then it must either be
    ///outside of any property arrays or at the top or at the bottom level of the property arrays,
    ///otherwise undefined behaviour will occur.
    ///Valid examples: `<GlobalProperty, PropertyArray1<PropertyArray2<...>, ...>>`,
    ///`GlobalPropertyArray<PropertyArray1<PropertyArray2<...>, ...>>`,
    ///`PropertyArray1<PropertyArray2<GlobalProperty, ...>, ...>`.
    ///Invalid examples: `PropertyArray1<GlobalPropertyArray<PropertyArray2<...>, ...>, ...>`,
    ///`PropertyArray1<PropertyArray2<GlobalPropertyArray<...>, ...>, ...>`.
    ///
    ///Use a @c ExtraWriteProxyProperty typedef to specify an extra property to be held by a write proxy (but not by the collection),
    ///do not have that typedef or typedef it to @p Utility::InvalidType to disable this.
    ///If an extra property is indeed to be held by a write proxy,
    ///one must specify four static functions within the property itself:
    ///`write_to_read`, `read_to_write`, `read_to_read` and `write_to_write`.
    ///The functions should have the signature:
    ///`(const PropertiesIndexer dest_index, const PropertiesIndexer src_index, DestObj & d, const SrcObj & s)`,
    ///where the indices are used to access the correct variable when the variables
    ///are (directly or indirectly) part of property arrays.
    ///Do note that these functions may receive top-level objects that only include
    ///the property in question e. g. as part of a sub-collection or an inner part of a jagged vector,
    ///so they should be implemented as acting on the raw arrays/variables instead
    ///(use `Utility::FriendshipProvider::template get_object<...>(mult_arr_idx)`).
    ///By construction, any multi-array accesses are resolved before calling the functions,
    ///so they should not care if the object is part of a property array
    ///(other than using the indices appropriately).
    ///
    ///If this @c GlobalProperty is also a @c PerItemObject, one may provide two
    ///additional static member variables to customize behaviour:
    ///a `static constexpr <appropriately sized integer type> initial_size`
    ///specifies an initial size (leave it undefined or set it to 0 to ignore this),
    ///a `static constexpr int initial_memset` member variable to specifies the value
    ///that will be memset over the initial size (leave it undefined or set it to a value
    ///that is not representable in an `unsigned char` to ignore this).
    ///
    ///An overall maximum size for the entries
    ///can be specified through a `static constexpr MaximumSizeType max_entries` member variable.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES GlobalProperty
    {
    };

    ///@brief Lists the supported types of properties in an interface descriptor.
    enum class SupportedTypes
    {
      Invalid = -1,
      NoObject,
      PerItemObject,
      JaggedVector,
      SubCollection,
      PropertyArray
    };

    ///@class JaggedVectorSizeProperty
    ///
    ///The class that holds jagged vector sizes,
    ///for a jagged vector property @p OriginalProperty.
    ///Used internally, exposed here as user code
    ///can expect to find this property in collections
    ///that include jagged vectors...
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class OriginalProperty>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES JaggedVectorSizeProperty;

    ///@brief Whether or not the provided property corresponds to a @c JaggedVectorSizeProperty.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_jagged_vector_size_property(Property);

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr SupportedTypes classify_property(Property);

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_global(Property);

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool has_extra_write_proxy_types(Property);

    ///@brief The maximum number of entries that would be necessary
    ///       for a given property.
    ///
    ///By convention (and since sizes may or may not be signed),
    ///0 is used to signal that the size is unspecified
    ///(meaning that the property can only be used with LayoutTypes
    /// that support dynamic sizes, such as those based on @c std::vector).
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto property_maximum_size(Property p);

    ///@brief Whether or not the size of this property must be tracked individually.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool track_individual_size(Property p);

    ///@brief The initial size of a per-item global property.
    ///
    ///By convention (and since sizes may or may not be signed),
    ///0 is used to signal that the size is unspecified
    ///(meaning that the property can only be used with LayoutTypes
    /// that support dynamic sizes, such as those based on @c std::vector).
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto initial_global_property_size(Property p);

    ///@brief Whether or not to memset a per-item global property to a specific value.
    ///
    ///If the global property does not provide a `initial_memset` member variable
    ///or holds a value outside of the positive and and negative range of `unsigned char`.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool use_initial_global_property_memset(Property p);

    ///@brief The initial value to memset a per-item global property to.
    ///
    ///A value that does not fit in the positive and negative range of `unsigned char`
    ///signals that this was not specified.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto initial_global_property_memset_value(Property p);

    ///@class PropertyAndExtent
    ///Holds a property and its respective extent
    ///(in case any @c PropertyArray is used)
    ///for the @c InterfaceInformation.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Prop, PropertiesIndexer ext = 1>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES PropertyAndExtent
    {
      using Property = Prop;

      static constexpr PropertiesIndexer extent = ext;
    };

    ///@class SizeTagExtentAndTypes
    ///Holds a size tag and a list of @c PropertyExtent
    ///corresponding to every property under this size tag
    ///for the @c InterfaceInformation.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class SzTg, PropertiesIndexer ext = 1, class Types = Utility::TypeHolder<>>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SizeTagExtentAndTypes
    {
      using SizeTag = SzTg;

      using PropertiesAndExtents = Types;

      static constexpr PropertiesIndexer extent = ext;
    };

    ///@class DefaultSizeMap
    ///Returns the maximum size of a Property
    ///(through its @c size static member function).
    ///
    ///Users with special requirements are encouraged to declare an alternative class
    ///that leverages `if constexpr` to specify sizes for the relevant cases
    ///and fall back to `property_maximum_size` in all other cases.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DefaultSizeMap
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr MaximumSizeType size(Property p)
      {
        return property_maximum_size(p);
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class InterfaceSpecification>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES InterfaceInformation
    {
      ///@brief A flattened @c Utility::TypeHolder listing all per-item properties and their respective extents.
      ///
      ///To give a slightly convoluted and artificial example:
      ///```
      ///PropertyList<
      ///               P1,
      ///               P2,
      ///               NestedSubcollectionContaining<P3, P4>,
      ///               ArrayOf20Properties< P5, NestedSubcollectionContaining<P6, P7>, ArrayOf10Properties<P8> >
      ///            >
      ///```
      ///
      ///Would return:
      ///```
      ///Utility::TypeHolder<
      ///                      PropertyAndExtent<P1, 1>,
      ///                      PropertyAndExtent<P2, 1>,
      ///                      PropertyAndExtent<P3, 1>,
      ///                      PropertyAndExtent<P4, 1>,
      ///                      PropertyAndExtent<P5, 20>,
      ///                      PropertyAndExtent<P6, 20>,
      ///                      PropertyAndExtent<P7, 20>,
      ///                      PropertyAndExtent<P8, 200>
      ///                   >
      ///```
      ///
      ///The optional parameters control whether to list global or non-global properties
      ///(at least one of them must be true, else this will be empty...),
      ///whether to include properties that are part of jagged vectors,
      ///whether to consider the ExtraWriteProxyProperty instead of the GlobalProperty
      ///in case a global property specifies it (but treat the global property as normal otherwise)
      ///or to keep the properties that have a ExtraWriteProxyProperty defined
      ///(which is only relevant if @p include_global and @p use_write_proxy_properties are false).
      template <bool include_non_global             = true,
                bool include_global                 = true,
                bool include_part_of_jagged_vectors = true,
                bool use_write_proxy_properties     = false,
                bool keep_globals_with_write_proxy  = false>
      using flattened_per_item = void;

      ///@brief A flattened @c Utility::TypeHolder with a list of @c TypeHolders containing
      ///the types that are specified under each per-item property and the corresponding properties.
      ///
      ///To give a slightly convoluted and artificial example:
      ///```
      ///PropertyList<
      ///               P1<float>,
      ///               P2<int>,
      ///               NestedSubcollectionContaining<P3<float>, P4<double>>,
      ///               ArrayOf20Properties< P5<float>, NestedSubcollectionContaining< P6<float>, P7<int> >, ArrayOf10Properties< P8<float> > >
      ///            >
      ///```
      ///
      ///Would return:
      ///```
      ///Utility::TypeHolder<
      ///                      Utility::TypeHolder< float,  PropertyAndExtent< P1<float>, 1 >,
      ///                                                   PropertyAndExtent< P3<float>, 1 >,
      ///                                                   PropertyAndExtent< P5<float>, 20 >,
      ///                                                   PropertyAndExtent< P6<float>, 20 >,
      ///                                                   PropertyAndExtent< P8<float>, 200 >
      ///                                         >,
      ///                      Utility::TypeHolder< int,    PropertyAndExtent< P2<int>, 1 >,
      ///                                                   PropertyAndExtent< P7<int>, 20 >
      ///                                         >,
      ///                      Utility::TypeHolder< double, PropertyAndExtent< P4<double>, 1 > >
      ///                   >
      ///```
      ///
      ///The optional parameters control whether to list global or non-global properties
      ///(at least one of them must be true, else this will be empty...),
      ///whether to include properties that are part of jagged vectors
      ///and whether to consider the ExtraWriteProxyProperty instead of the GlobalProperty
      ///(which is independent of the @p include_global and @p include_non_global settings,
      /// but will completely replaces every global property...) or to keep the properties
      ///that have a ExtraWriteProxyProperty defined (which is only relevant if
      ///@p include_global and @p use_write_proxy_properties are false).
      template <bool include_non_global             = true,
                bool include_global                 = true,
                bool include_part_of_jagged_vectors = true,
                bool use_write_proxy_properties     = false,
                bool keep_globals_with_write_proxy  = false>
      using flattened_properties_by_type = void;

      ///@brief A flattened @c Utility::TypeHolder listing the properties corresponding to jagged vectors
      ///       (but not their constituents) and their respective extents.
      template <bool include_non_global = true, bool include_global = true, bool use_write_proxy_properties = false, bool keep_globals_with_write_proxy = false>
      using flattened_jagged_vector = void;

      ///@brief A flattened @c Utility::TypeHolder listing the properties that are part of jagged vectors and their respective extents,
      ///       excluding the jagged vector size property that is added internally to track vector sizes.
      template <bool include_non_global = true, bool include_global = true, bool use_write_proxy_properties = false, bool keep_globals_with_write_proxy = false>
      using flattened_jagged_vector_properties = void;

      ///@brief A flattened @c Utility::TypeHolder listing all the size tags that are necessary
      ///       to describe the collection, besides the @c StartingTag size tag
      ///       that is used for "standard" properties of the collection,
      ///       their respective extents and the properties under them.
      template <class StartingTag = Utility::TypeHolder<>>
      using necessary_size_tags = void;

      ///@brief Use the definitions in @p SizeMap to get the final maximum sizes
      ///       for every per-item property (taking into account the maximum sizes
      ///       requested by other properties they may be a part of).
      ///
      template <class SizeMap>
      using per_item_size_map = void;
    };

    template <class... Properties>
    using PropertyList = Utility::TypeHolder<Properties...>;

    ///@brief Check the validity of the @p InterfaceSpecification in question,
    ///       namely in terms of all the conditions for the different types of properties.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class InterfaceSpecification>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool check_validity(InterfaceSpecification);

    ///@brief Whether this set of properties only contains a single global property
    ///       and any number of `NoObject` properties.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_global_property(PropertyList<Properties...>);

    ///@brief If `single_global_property` is `true`, return the corresponding
    ///       global property. Otherwise, return `Utility::InvalidType`.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_global_property(PropertyList<Properties...>);

    ///@brief Whether this set of properties only contains `NoObject` properties. @p ignore_globals
    ///       controls whether or not to consider global properties in the checks.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool only_no_object_properties(PropertyList<Properties...>);

    ///@brief Whether this set of properties only contains one `PerItemObject`
    ///       and any number of `NoObject` properties. @p ignore_globals
    ///       controls whether or not to consider global properties in the checks.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_per_item_property(PropertyList<Properties...>);

    ///@brief If `single_per_item_property` is `true`, return the corresponding
    ///       `PerItemObject` property. Otherwise, return `Utility::InvalidType`.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_per_item_property(PropertyList<Properties...>);

    ///@brief Whether this set of properties only contains one `JaggedVector`
    ///       and any number of `NoObject` properties. @p ignore_globals
    ///       controls whether or not to consider global properties in the checks.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_jagged_vector_property(PropertyList<Properties...>);

    ///@brief If `single_jagged_vector_property` is `true`, return the corresponding
    ///       `JaggedVector` property. Otherwise, return `Utility::InvalidType`.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_jagged_vector_property(PropertyList<Properties...>);

    ///@brief Whether this set of properties only contains one `SubCollection`
    ///       and any number of `NoObject` properties. @p ignore_globals
    ///       controls whether or not to consider global properties in the checks.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_subcollection_property(PropertyList<Properties...>);

    ///@brief If `single_subcollection_property` is `true`, return the corresponding
    ///       `SubCollection` property. Otherwise, return `Utility::InvalidType`.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_subcollection_property(PropertyList<Properties...>);

    ///@brief Whether this set of properties only contains one `PropertyArray`
    ///       and any number of `NoObject` properties. @p ignore_globals
    ///       controls whether or not to consider global properties in the checks.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool single_property_array_property(PropertyList<Properties...>);

    ///@brief If `single_property_array_property` is `true`, return the corresponding
    ///       `PropertyArray` property. Otherwise, return `Utility::InvalidType`.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true, class... Properties>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_property_array_property(PropertyList<Properties...>);
  }

  ///@namespace LayoutTypes
  ///@brief Contains classes that define different strategies of storing data inside the struct-of-arrays-like holding classes.
  namespace LayoutTypes
  {
    ///@namespace Helpers
    ///@brief Contains some helper classes to encapsulate common functionality
    ///       to the implementation of data storage strategies.
    namespace Helpers
    {
      ///@class SizeTagAccessor
      ///@brief Contains the logic to access the size tags corresponding to the @p ListOfSizeTags.
      ///
      ///@p ListOfSizeTags is a @c Utility::TypeHolder of
      ///@c InterfaceDescription::SizeTagExtentAndTypes corresponding
      ///to the size tag, its extent and associated properties, such as what is defined
      ///by @c InterfaceDescription::InterfaceInformation::necessary_size_tags.
      ///Of course, implementers and end users are free to
      ///ignore this helper and manage this in their own way,
      ///as long as the final behaviour is consistent with this.
      ///
      ///This class is designed to be used
      ///through its static member functions,
      ///rather than derived from or instantiated.
      ///They receive as first argument
      ///an object whose @c operator[] with a @c PropertiesIndexer
      ///returns a (reference to the) size tag.
      ///(It can thus be e. g. a vector, a pointer to the beginning of an array...)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class ListOfSizeTags>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SizeTagHolder
      {
        static_assert(Utility::always_false<ListOfSizeTags>, "Please initialize the Collection properly with a TypeHolder of properties!");

        ///@brief Returns the total number of different size tags that is necessary,
        ///       taking into account the several extents.
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer
        total_number_of_tags();

        ///@brief Returns the 0-based index into a list of flattened size tags
        ///       (i. e. taking into account their extents)
        ///       that corresponds to the given @p Tag.
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer get_tag_index(const PropertiesIndexer mult_arr_idx);

        ///@brief Returns the item corresponding to the given multi-dimensional index ( @p mult_arr_idx )
        ///       of the given @p Tag, using the @c operator[] of @p Holder.
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag, class Holder>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_size_tag(Holder && h, const PropertiesIndexer mult_arr_idx);

        ///@brief Returns a Utility::TypeHolder with a list of @c InterfaceDescription::PropertyAndExtent
        ///       corresponding to the specified @c Tag.
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto get_properties_and_extents_matching_tag();

        ///@brief Returns the size tag corresponding to the per-item property.
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PerItemProperty>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto get_tag_matching_property();
      };
    }

    ///@namespace Internal
    ///@brief Contains data storage strategies that are meant exclusively for internal use.
    ///
    ///@remark The end user should not rely on any part of this interface.
    namespace Internal
    {
      ///@class ViewLayout
      ///
      ///A layout where a pointer-like object of type @p PtrType that refers to
      ///a different (pre-existing, with lifetime greater than the ViewLayout) collection
      ///laid out according to @p OriginalLayout. @p max_resize and @p max_mutability
      ///allow constraining the access properties, and @p needs_index (if `true`)
      ///means the @c ViewLayout will also store an index to refer to property arrays.
      ///
      ///All of this is meant to be used internally to express views into parts of collections.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class OriginalLayout,
                                                            class PtrType,
                                                            ResizeProperties     max_resize,
                                                            MutabilityProperties max_mutability,
                                                            bool                 needs_index = false>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ViewLayout;

      ///@class IndividualPointerHolder
      ///
      ///A layout that hold a pointer to every relevant array of the original collection,
      ///which is laid out according to @p OriginalLayout.
      ///@p constant may be used to constrain the access properties.
      ///
      ///Used mostly for a sane (but not necessarily performant) general implementation of @c pass_by_value
      ///for the cases where every per-item property is laid out contiguously in memory
      ///(which are the ones that we mostly care about, even if technically
      /// one could write a layout type that did not do this...).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class OriginalLayout, bool constant>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES IndividualPointerHolder;
    }

    ///@class VectorLikePerItem
    ///
    ///A layout type where each per-item property is laid out in a sequential memory region
    ///as provided by @p VectorLike<T>, though no other guarantees are made
    ///in regards to the relative memory locations for the different per-item properties.
    ///Size tags are also stored in one such VectorLike (which is slightly more wasteful
    ///than recycling the vector-like objects' `size()` member functions,
    ///but allows better inter-operability with other layout types).
    ///
    ///@p VectorLike must be templated on the type to be held (first argument),
    ///   plus any additional number of template arguments (passed through @p ExtraArgs),
    ///   and provide: `resize`, `clear` and `operator[]`.
    ///   Having `size`, `reserve`, `capacity` and `shrink_to_fit`
    ///   is recommended, but not strictly required.
    ///   (Not having `size` merely affects some assertions,
    ///    without `capacity` the code will assume the size is the maximum capacity
    ///    and the other operations, if missing, will be taken to be no-ops.)
    ///
    ///@p VectorLikeForSizes is a template like @p VectorLike (may be the same)
    ///   that is used to store the size tags, in case it might be relevant
    ///   to have the sizes stored separately.
    ///
    ///The user can additionally provide specific types in @p SizeType and @p DifferenceType,
    ///in case one wants specifically larger/smaller size tags.
    ///
    ///@warning Support for stateful allocators in a @c VectorLike is somewhat limited:
    ///         all the @c VectorLike objects are default-constructed and the state is not
    ///         propagated when copying or moving from or to this collection.
    ///         (The latter is not entirely unlike some of the caveats of `std::pmr::vector`,
    ///          though they cannot be easily papered over here as there is no good way
    ///          of actually setting an allocator through the `std::vector` interface,
    ///          short of constructing another object...)
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <template <class...> class VectorLike,
                                                          template <class...>
                                                          class VectorLikeForSizes,
                                                          class SizeType,
                                                          class DifferenceType,
                                                          class... ExtraArgs>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES VectorLikePerItem;

    ///@brief Specialization for a @c VectorLikePerItem that uses `Utility::ContextAwareVector` to hold the items.
    template <class Context, class SizeType, class DifferenceType>
    using ContextAwareVectorPerItem =
      VectorLikePerItem<Utility::ContextAwareVector, Utility::ContextAwareVector, SizeType, DifferenceType, Context, SizeType, DifferenceType>;

    ///@brief Specialization for a @c VectorLikePerItem that uses `Utility::LocalVector` to hold the items.
    template <class SizeType, class DifferenceType>
    using LocalVectorPerItem = VectorLikePerItem<Utility::LocalVector, Utility::LocalVector, SizeType, DifferenceType, SizeType, DifferenceType>;

    namespace impl
    {
      template <template <class...> class Allocator, class... ExtraAllocatorArgs>
      struct VectorWithAllocatorHelper
      {
        template <class T>
        using Type = std::vector<T, Allocator<T, ExtraAllocatorArgs...>>;
      };

    };

    ///@brief Specialization for a @c VectorLikePerItem that uses `std::vector` with an @p Allocator,
    ///       which takes as first argument the type of the vector and may take additional arguments.
    template <template <class...> class Allocator, class... ExtraAllocatorArgs>
    using StandardVectorWithAllocatorPerItem =
      VectorLikePerItem<impl::VectorWithAllocatorHelper<Allocator, ExtraAllocatorArgs...>::template Type,
                        impl::VectorWithAllocatorHelper<Allocator, ExtraAllocatorArgs...>::template Type,
                        typename impl::VectorWithAllocatorHelper<Allocator, ExtraAllocatorArgs...>::template Type<Utility::InvalidType>::size_type,
                        typename impl::VectorWithAllocatorHelper<Allocator, ExtraAllocatorArgs...>::template Type<Utility::InvalidType>::difference_type>;

    ///@brief Specialization for a @c VectorLikePerItem that uses `std::vector` with the default `std::allocator`.
    using StandardVectorPerItem = StandardVectorWithAllocatorPerItem<std::allocator>;

    ///@class LocalStruct
    ///
    ///A layout type where an array with a static maximum size is allocated for every property
    ///with automatic storage, i. e., on the stack. Essentially equivalent to declaring a @c struct
    ///with all the desired properties and their maximum sizes in the scope in question.
    ///
    ///@p SizeMap allows customizing the static maximum size depending on the user's needs
    ///(e. g. storing less data for use in a more constrained memory environment like an FPGA).
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class SizeType, class DifferenceType, class SizeMap = InterfaceDescription::DefaultSizeMap>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES LocalStruct;

    ///@class DynamicStruct
    ///
    ///Variant of @c StaticStruct where the @c struct is allocated dynamically
    ///according to a stateless @c Allocator, templated on the type (first argument)
    ///and using @p ExtraArgs for the rest of the arguments (if any).
    ///
    ///An instance of @c Allocator is constructed locally when allocating or deallocating
    ///the @c struct, and `allocate(sizeof(struct))` and `deallocate(ptr, sizeof(struct))`
    ///called. If `ExtraInformation::AllocatorLike<Allocator>::context_info_aware()` is `true`,
    ///the current `ContextInfo` will be passed as the first argument of these functions.
    ///
    ///If any state must be maintained for the allocations to be meaningful,
    ///please define a new @c MemoryContext whose @c ContextInfo expresses
    ///the relevant information, create a new stateless allocator that
    ///reads all relevant state from the @c ContextInfo
    ///and ensure `ExtraInformation::AllocatorLike::context_info_aware()` returns `true`.
    ///(See @c Utility::ContextAwareAllocator.)
    ///
    ///@p SizeMap allows customizing the static maximum size depending on the user's needs
    ///(e. g. storing less data for use in a more constrained memory environment like an FPGA).
    ///
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <template <class...> class Allocator,
                                                          class SizeType,
                                                          class DifferenceType,
                                                          class SizeMap,
                                                          class... ExtraArgs>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DynamicStruct;

    ///@brief Specialization of @c DynamicStruct that uses the @c ContextAwareAllocator for a given @p MemoryContext.
    template <class MemoryContext, class SizeType, class DifferenceType, class SizeMap = InterfaceDescription::DefaultSizeMap>
    using DynamicStructInContext = DynamicStruct<Utility::ContextAwareAllocator, SizeType, DifferenceType, SizeMap, MemoryContext>;

    ///@brief The default layout to use for jagged vectors in owning objects.
    template <class SizeType, class DifferenceType>
    using DefaultOwningObjectJaggedVectorLayout = LocalVectorPerItem<SizeType, DifferenceType>;
  }

  ///@namespace Collections
  ///@brief Contains the main classes that implement our struct-of-arrays-like holding classes.
  namespace Collections
  {
    using DefaultSizeTag = Utility::TypeHolder<>;

    ///@class DefaultMetaInfo
    ///Provides the default meta-information for collections and objects.
    ///The implementation will provide different meta-information classes
    ///as necessary to specify the desired behaviour, but the end user should
    ///never need to override anything here by hand.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class SizeTagType = DefaultSizeTag, PropertiesIndexer extent_of_arrays = 1>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DefaultMetaInfo
    {
      using SizeTag = SizeTagType;

      static constexpr PropertiesIndexer extent = extent_of_arrays;

      template <class T>
      using override_size_tag = DefaultMetaInfo<T, extent>;

      template <PropertiesIndexer n>
      using override_extent = DefaultMetaInfo<SizeTag, n>;

      template <PropertiesIndexer n>
      using multiply_extent = DefaultMetaInfo<SizeTag, extent * n>;
    };

    ///@class CollectionHolder
    ///The main class that handles all the main functionality of this library...
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class LayoutType, class Properties, class MetaInfo = DefaultMetaInfo<>>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CollectionHolder;

    ///@brief @c CollectionHolder augmented with the full vector-like interface.
    template <class LayoutType, class Properties, class MetaInfo = DefaultMetaInfo<>>
    using Collection = Utility::VectorLikeWrapper<CollectionHolder<LayoutType, Properties, MetaInfo>>;

    ///@class OwningObject
    ///Describes an object that owns all of its properties.
    ///(The only useful ObjectType when not interacting with a Collection,
    /// as all the others that are used internally essentially behave
    /// as pointer + index into the collection.)
    ///
    ///@p SizeType and @p DifferenceType define the types
    ///to be used when interacting with the object.
    ///
    ///@p CollectionLayoutType controls the way in which jagged vectors
    ///   will be laid out in memory, the default should be good enough
    ///   (but may be overridden based on the intended use case).
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class SizeType       = PropertiesIndexer,
                                                          class DifferenceType = PropertiesIndexer,
                                                          class JaggedVectorLayoutType =
                                                            typename LayoutTypes::DefaultOwningObjectJaggedVectorLayout<SizeType, DifferenceType>>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES OwningObject;

    ///@class ProxyObject
    ///Describes an object that acts as a proxy into a collection
    ///(or maybe an owning object, though this is all handled internally).
    ///If @p force_constant is @c true, the object will not allow
    ///modifications to any of its members.
    ///If @p with_multi_array_index is @c true, the object also stores
    ///an index to be able to access the correct arrays when dealing with a
    ///@c PropertyArray.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout,
                                                          class ProxyPointerType,
                                                          bool force_constant         = false,
                                                          bool with_multi_array_index = false>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ProxyObject;

    ///@class Object
    ///The actual object. @p ObjectType may be either @c OwningObject or @c ProxyObject.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class ObjectType, class Properties, class MetaInfo = DefaultMetaInfo<>>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES Object;

    ///@brief Express the information contained in the collection in a way that can be passed
    ///       (relatively) cheaply by value into e. g. a GPU kernel without triggering
    ///       unnecessary copies.
    ///
    ///The result of this is a collection that is constructible by-value with minimal copies
    ///(e. g. passing pointers around instead of copying the data), but not necessarily
    ///assignable with minimal copies (as assignment must end up changing the original).
    ///
    ///Passing views into other collections by value is only possible if we are not
    ///considering a sub-set of a property array.
    ///
    ///The return of `pass_by_value` will be some form of non-owning view of the original data,
    ///but not necessarily with a layout that matches `Internal::ViewLayout`.
    ///
    ///@remark For consistency, it is recommended that `auto x = pass_by_value(const T &)`
    ///        and `const auto y = pass_by_value(T &)` be essentially equivalent,
    ///        which the interface properties probably ensure automatically in our collections.
    ///        and any user-defined functions that behave in the same way.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class CollectionT>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto pass_by_value(CollectionT &);

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Layout, class Properties, class MetaInfo>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto pass_by_value(const CollectionHolder<Layout, Properties, MetaInfo> &)
    {
      static_assert(Utility::always_false<Layout>, "pass_by_value should be called with Collections, not CollectionHolders!");

      return Utility::InvalidType {};
    }

    template <class CollectionT>
    using ByValue = std::decay_t<decltype(pass_by_value(std::declval<CollectionT &>()))>;

    template <class CollectionT>
    using ByConstValue = std::decay_t<decltype(pass_by_value(std::declval<const CollectionT &>()))>;

  }

  ///@namespace Transfers
  ///@brief Provides functionality to (more) efficiently transfer data between holding classes of different types.
  ///
  ///@warning By design, all the copying and moving is of data.
  ///         Any other form of state (e. g. stateful allocators
  ///         or the @c ContextInfo) of the tranferred-into collection
  ///         is maintained.
  ///
  ///@warning Since, in the general case, transferring between two collections
  ///         may involve several memory copies, only weak exception guarantees
  ///         can be offered: if an exception is thrown, the copied-into object
  ///         or both the moved-into and moved-from objects may be in an undefined state.
  ///         If explicit strong exception guarantees are desired,
  ///         the end user should make a local copy of the original
  ///         object(s) and restore it in case an exception is thrown...
  namespace Transfers
  {
    ///@brief Classifies the overriding priority of a transfer,
    ///       because rules around (partial) template specializations
    ///       and ambiguity can be a source of pain for both implementers and users...
    ///
    ///By construction, the different possible values should be contiguous.
    ///
    ///@remark For some futureproofing, we are considering already three possible tiers
    ///        of transfer specifications for both default and user provided transfers.
    ///        This is naturally generalizable if further granularity is needed,
    ///        but there is a trade-off between the depth of the recursion
    ///        and the complexity of the SFINAE'ing that must go on to select the right priority indirectly.
    ///
    enum class TransferPriority : PropertiesIndexer
    {
      Invalid = 0,
      Minimum,
      DefaultTertiary = Minimum,
      DefaultSecondary,
      DefaultPrimary,
      Default = DefaultPrimary,
      UserProvidedTertiary,
      UserProvidedSecondary,
      UserProvidedPrimary,
      UserProvided = UserProvidedPrimary,
      Maximum      = UserProvided
    };

    namespace impl
    {
      ///@brief Helper function to assist in recursing through the several priorities
      ///       until a valid transfer specification is found.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Destination, class Source, TransferPriority priority = TransferPriority::Maximum>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_transfer_specification(const bool trace = false);

      ///@brief Helper type to get the result of @c get_transfer_specification.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Destination, class Source, TransferPriority priority = TransferPriority::Maximum>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecificationGetter;
    }

    ///@brief The actual transfer specification we will want to use
    ///       to transfer from @p Source to @p Destination.
    template <class Destination, class Source, TransferPriority priority = TransferPriority::Maximum>
    using Transfer = typename impl::TransferSpecificationGetter<Destination, Source, priority>::Type;

    ///@class TransferSpecification
    ///Holds all the relevant information for transferring elements from @p Destination to @p Source,
    ///both of which we assume provide the interface of our collections or objects.
    ///
    ///Actual transfers are templated to allow for source and destination objects
    ///that are of a potentially different class, to accommodate the cases
    ///where we want one class to behave like another.
    ///
    ///If users or downstream implementers want to specialize this, one should specialize on CollectionHolder
    ///rather than on Collection (since the latter is the VectorLikeWrapper of the former).
    ///
    ///User-provided specializations should set @p priority to `TransferPriority::UserProvided`
    ///(or, if more granularity is needed, `UserProvided<N>ary`, as appropriate...).
    ///
    ///@p Extra is used to aid in specializing the class via SFINAE.
    ///
    ///Since (by design) the sets of properties are considered "breadth-first",
    ///and furthermore certain specializations are based on the overall layout,
    ///overriding the transfer for a specific property (for e. g. logging purposes)
    ///is not guaranteed to work.
    ///
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Destination, class Source, TransferPriority priority, class Extra = void>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification
    {
      ///@brief Whether a copy from @p Source to @p Destination is possible.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return false;
      }

      ///@brief Whether a move from @p Source to @p Destination is possible.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return false;
      }

      ///@brief Copy all objects from a class like @p Source to a class like @p Destination.
      ///       (Or, if dealing with Objects instead of Collections,
      ///        copy the objects themselves...)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s);

      ///@brief Copy all objects from a class like @p Source to a class like @p Destination,
      ///       with additional (layout-dependent) arguments.
      ///
      ///The arguments and their meaning should be documented in each LayoutType.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&...);

      ///@brief Copy @p num objects from a class like @p Source to
      ///       a class like @p Destination, with optional offsets,
      ///       without resizing. Global properties are skipped.
      ///
      ///This function is only provided for collections, not objects.
      ///
      ///@remark Its usage is limited to copying jagged vectors at the moment.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                           d,
                                                                                            const SourceLike &                   s,
                                                                                            const typename DestLike::size_type   num,
                                                                                            const typename DestLike::size_type   d_offset = 0,
                                                                                            const typename SourceLike::size_type s_offset = 0);

      ///@brief If possible, move storage from a class like @p Source to a class like @p Destination.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s);

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
      ///@brief An ID for this transfer specification, to aid in debugging problems with specializations.
      ///       If @p trace is @c true, print some identifying information to trace the sequence of calls.
      ///
      ///@remark The ID has no particular significance and implementers
      ///        may freely choose IDs that overlap with the currently
      ///        used numbers, or change them, or whatever they desire.
      ///        This would only be used for debugging purposes
      ///        (e. g. when specializations seem not to be working as intended...),
      ///        it does not change the behaviour of the transfers whatsoever.
      ///
      ///@remark It's a long to ensure at least 32 bits, as per the standard...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr long
      transfer_ID(const bool trace = false);
#endif

      //This dummy class is only required for the generic invalid TransferSpecification,
      //not for any user-provided ones: this is used to signal that we are falling
      //precisely into the generic invalid case.
      using InvalidMarker = Utility::InvalidType;
    };

    ///@class ArrayTransferSpecification
    ///Specifies how to copy a single per-item property array from a collection
    ///that is laid out according to @p SourceLayout to one laid out according to @p DestLayout.
    ///@p Property is the per-item property in question (in case it is useful to specialize)
    ///and @p Extra allows additional SFINAE-based specializations.
    ///
    ///@remark We already include support for all layouts that define
    ///        a `can_get_array_pointers()` static constexpr function
    ///        that returns true. This is meant to cater for all other
    ///        (unusual) cases in which that is not true,
    ///        and thus it only needs to be specified when implementing
    ///        such an unusual layout.
    ///
    ///@remark We could use the priority system here too,
    ///        but it's much less relevant as this only gets called
    ///        when actually transferring the arrays,
    ///        which means we don't need to worry about
    ///        e. g. deferred layouts or stuff like that.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class DestLayout, class SourceLayout, class Property, class Extra = void>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ArrayTransferSpecification
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void transfer(const PropertiesIndexer              dst_mult_arr_idx,
                                                                                           DestLike &                           dest,
                                                                                           const PropertiesIndexer              src_mult_arr_idx,
                                                                                           const SourceLike &                   src,
                                                                                           const typename DestLike::size_type   num,
                                                                                           const typename DestLike::size_type   d_offset,
                                                                                           const typename SourceLike::size_type s_offset) = delete;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_transfer(const PropertiesIndexer              dst_mult_arr_idx,
                                                                                                   DestLike &                           dest,
                                                                                                   const PropertiesIndexer              src_mult_arr_idx,
                                                                                                   const SourceLike &                   src,
                                                                                                   const typename DestLike::size_type   num,
                                                                                                   const typename DestLike::size_type   d_offset,
                                                                                                   const typename SourceLike::size_type s_offset,
                                                                                                   Args &&...) = delete;
    };

  }

  ///@namespace MemoryContexts
  ///@brief Definitions to handle the fact that memory can live in different 'memory contexts'...
  namespace MemoryContexts
  {
    ///@brief The kinds of contexts (i. e. actually separate devices) we support.
    ///
    ///@warning Due to some Clang peculiarities, when compiling for CUDA,
    ///         for certain stages of the compilation CPU and CUDA_GPU will compare as equal
    ///         (there is an overloaded operator ==). If it is really necessary
    ///         to ensure these contexts do match, cast them to a @p PropertiesIndexer
    ///         and perform the comparison on the raw integers...
    enum class ExecutionContext : PropertiesIndexer
    {
      Invalid = -1,
      CPU,
      CUDA_GPU
      //Other platforms can and should be added as support for them becomes available...
    };

    ///@brief Return the current execution context.
    ///
    ///@warning Passing any class whose actual memory contents
    ///         depend on get_current_execution_context()
    ///         across execution context boundaries
    ///         will, in general, result in undefined behaviour.
    ///         This includes dependencies on `MemoryContexts::Current`.
    ///         If the differences are only in class behaviour
    ///         (e. g. `if constexpr` in functions or
    ///          template arguments that do not determine
    ///          any of the member variables),
    ///         it will most likely work as intended,
    ///         but caution is nonetheless advised.
    ///
    ///@warning Depending on the compiler, `static_assert` based on
    ///         `get_current_execution_context` without depending on
    ///         any template parameters can result in errors
    ///         (for the same reason `static_assert(false)` does:
    ///          the `static_assert` is checked regardless of template
    ///          instantiations). One can use `Utility::always_true` or
    ///          `Utility::always_false`, as appropriate, to
    ///          introduce an artificial dependence on the template
    ///          parameters.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ExecutionContext
    get_current_execution_context();

    ///@class Invalid
    ///@brief An example, invalid memory storage location, to show the necessary functions and typedefs.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES Invalid
    {
      ///@class ContextInfo
      ///@brief Additional (runtime) information associated with objects that live in this memory location.
      ///
      ///An example would be storing e. g. the device on which the memory is allocated,
      ///or a CUDA stream we want to associate with any operations done with the object.
      ///
      ///This should be a relatively simple structure, trivially copyable and destructible
      ///and default constructible to a sane (usable, even if not the most efficient) state.
      ///(E. g. for CUDA, associate the default CUDA stream to any relevant operations...)
      ///
      ///@warning If it is constructible from a single collection like the ones in this library
      ///         (which would make little sense, but it's theoretically possible that someone
      ///         might try to do it...), the constructor of the corresponding layout_holder will not
      ///         be able to receive just that argument (as it is not practical to disambiguate
      ///         from the case where a collection that would otherwise be copied through the
      ///         usual channels is downcast to the context info and ends up initializing it
      ///         while keeping the rest of the collection default initialized...).
      ///         The cleanest workaround would be to provide an additional (dummy) argument
      ///         to fully disambiguate the constructors.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ContextInfo
      {
      };

      ///@brief Whether or not memory associated with the two @c ContextInfo can be safely
      ///       allocated and deallocated from, respectively, @p allocating and @p deallocating.
      ///
      ///@remark This should be provided to make sure the overload of `memory_contexts_compatible`
      ///        for the same memory context works, in the general case. User code is
      ///        encouraged to rely on `memory_contexts_compatible` for all checks
      ///        even when the contexts are the same.
      ///
      ///@remark This is mainly to handle the cases where @c ContextInfo contains runtime state
      ///        that could point to different memory locations that one couldn't simple move between.
      ///        For instance, different devices, different memory pools...
      ///
      ///@remark This was deliberately written as a separate standalone function instead of relying on
      ///        a overloaded `operator=` on the @c ContextInfo to prevent accidental calls through
      ///        a collection (that may derive from @c ContextInfo).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool
      context_infos_compatible(const ContextInfo & deallocating, const ContextInfo & allocating) = delete;

      ///@brief Memsetting, with potential additional arguments
      ///       (that may not be a template parameter depending on the context).
      ///
      ///@remark The eligible additional arguments (and how they change the behaviour)
      ///        should be documented on a per-context basis. The case where there are no
      ///        additional arguments should always be supported (required information
      ///        is meant to be stored inside the appropriate @c ContextInfo).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class T, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      memset(const ContextInfo & info, T * ptr, const int memset_value, const size_type size, Args &&... args) = delete;

      ///@brief Allocate @p size bytes, with potential additional arguments
      ///       (that may not be a template parameter depending on the context),
      ///       and update the @c ContextInfo accordingly (if applicable).
      ///
      ///@remark Whenever appropriate, the @c ContextInfo may also be taken by const reference
      ///
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void * allocate(ContextInfo & info, const size_type size) = delete;

      ///@brief Free @p ptr, with potential additional arguments
      ///       (that may not be a template parameter depending on the context),
      ///       and update the @c ContextInfo accordingly (if applicable).
      ///
      ///@remark Whenever appropriate, the @c ContextInfo may also be taken by const reference.
      ///
      ///@warning We assume `deallocate(nullptr)` is valid (and a no-op).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class size_type, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void deallocate(ContextInfo & info, void * ptr, const size_type size) = delete;

      ///@brief The kind of access one has, given the current context, to objects in this memory context.
      ///
      ///@remark It is assumed this can be known at compile-time
      ///        (since that is the case for e. g. GPU memory),
      ///        through something like `get_current_execution_context`.
      ///        If not, the idea would be to return the most
      ///        permissive access properties possible and then
      ///        add runtime checks to the actual operations
      ///        (failing and/or throwing exceptions
      ///         if the object's contents cannot be accessed),
      ///        as this will have implications on the interface
      ///        that will be provided for the Collections.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr AccessProperties access_properties()
      {
        return AccessProperties::Invalid;
      }

      ///@class ExtraFunctions
      ///@brief Some additional functions that can be used on any collection that holds its memory
      ///       in this specific memory context.
      ///
      ///The functions should only depend on the general interface of a collection.
      ///@p F is the final collection (to allow CRTP to access other collection functions),
      ///@p Layout is the layout type (to allow constraining the interface based on the access properties
      ///without running afoul of incomplete object definitions like it would if using the final collection.)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ExtraFunctions
      {
      };
    };

    ///@brief Whether or not two memory contexts refer to memory that
    ///       (if confirmed by @c memory_contexts_compatible)
    ///       could be allocated and deallocated from @p AllocContext and @p DeallocContext.
    ///
    ///@remark For the vast majority of cases, this is not true.
    ///        This is just an additional degree of flexibility
    ///        that can pay off in some cases, like stateful
    ///        allocators through the context info
    ///        (which would thus require a different memory context
    ///         from non-stateful allocation).
    ///
    ///@remark An implementation to cover the trivially true case
    ///        where both contexts are the same is provided.
    ///
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DeallocContext, class AllocContext>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool memory_contexts_potentially_compatible(const DeallocContext & dealloc,
                                                                                                                const AllocContext &   alloc)
    {
      (void) dealloc;
      (void) alloc;
      return false;
    }

    ///@brief Whether or not the two memory context infos,
    ///       associated with the two memory contexts,
    ///       refer to memory that could be allocated and deallocated
    ///       from @p alloc_info and @p dealloc_info, respectively.
    ///
    ///@remark Obviously, this should only return @c true when
    ///        @c memory_contexts_potentially_compatible is also true...
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DeallocContext, class AllocContext>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool memory_contexts_compatible(const DeallocContext &                       dealloc,
                                                                                                    const AllocContext &                         alloc,
                                                                                                    const typename DeallocContext::ContextInfo & dealloc_info,
                                                                                                    const typename AllocContext::ContextInfo &   alloc_info)
    {
      (void) dealloc;
      (void) alloc;
      (void) dealloc_info;
      (void) alloc_info;
      return false;
    }

    ///@brief Move a pointer of size @p size between two memory contexts.
    ///
    ///@warning The value of the pointer itself may change.
    ///
    ///@warning Only valid if `memory_contexts_compatible` is `true`
    ///         for the contexts and context infos in question.
    ///         No copies are done if the pointer cannot
    ///         simply be updated to the new memory context.
    ///         It is up to the end user to check if the
    ///         contexts are compatible before moving.
    ///
    ///@remark A default implementation that asserts that the contexts
    ///        are compatible and is otherwise a no-op is provided.
    ///
    ///@remark Whenever appropriate, the two @c ContextInfo objects may also be taken by const reference.
    ///
    ///@remark The eligible additional arguments (and how they change the behaviour)
    ///        should be documented on a per-context basis. The case where there are no
    ///        additional arguments should always be supported (required information
    ///        is meant to be stored inside the appropriate @c ContextInfo).
    ///
    ///@remark We assume moving is less costly than allocating in the new context,
    ///        copying the contents over, and deallocating the original.
    ///        In the cases where this would be more efficient, simply
    ///        have `memory_contexts_compatible` return `false`...
    ///
    ///@warning This function should offer strong-ish exception guarantees,
    ///         namely that, if an exception is thrown, the pointer is deallocatable
    ///         by the initial memory context.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class MemContextDest, class MemContextSource, class size_type, class T, class... Args>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void move_with_context(const MemContextDest &                   dest,
                                                                                           const MemContextSource &                 source,
                                                                                           typename MemContextDest::ContextInfo &   dest_info,
                                                                                           typename MemContextSource::ContextInfo & source_info,
                                                                                           T *&                                     ptr,
                                                                                           const size_type                          size,
                                                                                           Args &&... args) = delete;

    ///@brief Memcopies between memory contexts, with potential additional arguments
    ///       (that may not be a template parameter depending on the context).
    ///       @p size is relative to the number of elements of type @p T2 in @p ptr_from.
    ///
    ///@warning We assume that @p ptr_to points to an array large enough to support
    ///         @p size elements of type @p T2.
    ///
    ///@remark The eligible additional arguments (and how they change the behaviour)
    ///        should be documented on a per-context basis. The case where there are no
    ///        additional arguments should always be supported (required information
    ///        is meant to be stored inside the appropriate @c ContextInfo).
    ///
    ///@remark It is deliberately templated on pointer type because certain platforms
    ///        (e. g. GPU) may benefit from trying to maximize transaction size,
    ///        which is harder to achieve solely through a typical `void *` C-like `memcpy`.
    ///
    ///@remark It is safe to assume pointers are not overlapping (i. e. `restrict`),
    ///        so that we can straight up apply a memcopy if desired.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class MemContextDest, class MemContextSource, class size_type, class T1, class T2, class... Args>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void memcopy_with_context(const MemContextDest &                         dest,
                                                                                              const MemContextSource &                       source,
                                                                                              const typename MemContextDest::ContextInfo &   dest_info,
                                                                                              const typename MemContextSource::ContextInfo & source_info,
                                                                                              T1 *                                           ptr_to,
                                                                                              const T2 *                                     ptr_from,
                                                                                              const size_type                                size,
                                                                                              Args &&... args) = delete;

  }

  ///@namespace ExtraInformation
  ///@brief Some extra specifications that can allow for greater optimization,
  ///       essentially providing additional information that would be otherwise
  ///       somewhere between nontrivial to impossible to get about certain classes.
  namespace ExtraInformation
  {
    ///@class AllocatorLike
    ///@brief Extra information on allocator-like types.
    ///
    ///As a matter of general principle, whenever dealing with templated classes
    ///in the provided layouts, T<Utility::InvalidType> is used as a generic check
    ///whenever it would not be meaningful to assign a specific type. Some
    ///particular specializations may have slightly different behaviour,
    ///but the general interface and capabilities should remain the same.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES AllocatorLike
    {
      ///@class ExtraFunctions
      ///@brief Some additional functions that can be used on a @c DynamicStructLike
      ///       collection that allocates its structure with this @c AllocatorLike
      ///       (or a @c VectorLikePerItem collection that relies on this @c AllocatorLike).
      ///
      ///This should also include the ExtraFunctions defined in the memory context...
      ///
      ///The functions should only depend on the general interface of a collection.
      ///@p F is the final collection (to allow CRTP to access other collection functions),
      ///@p Layout is the layout type (to allow constraining the interface based on the access properties
      ///without running afoul of incomplete object definitions like it would if using the final collection.)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ExtraFunctions
      {
      };

      ///@brief Whether or not this is indeed an allocator-like class we will be able to support.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
      {
        return false;
      }

      ///@brief Whether the `allocate` and `deallocate` functions must receive as first argument
      ///       the @c ContextInfo of the corresponding @c MemoryContext.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool context_info_aware() = delete;

      ///@brief Return the memory context for this allocator.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context() = delete;
    };

    ///@class VectorLike
    ///@brief Extra information on vector-like types.
    ///
    ///As a matter of general principle, whenever dealing with templated classes
    ///in the provided layouts, T<Utility::InvalidType> is used as a generic check
    ///whenever it would not be meaningful to assign a specific type. Some
    ///particular specializations may have slightly different behaviour,
    ///but the general interface and capabilities should remain the same.
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES VectorLike
    {

      ///@class ExtraFunctions
      ///@brief Some additional functions that can be used on a @c VectorLikePerItem
      ///       collection that uses vectors of this type.
      ///
      ///This should also include the ExtraFunctions defined in the memory context...
      ///
      ///The functions should only depend on the general interface of a collection.
      ///@p F is the final collection (to allow CRTP to access other collection functions),
      ///@p Layout is the layout type (to allow constraining the interface based on the access properties
      ///without running afoul of incomplete object definitions like it would if using the final collection.)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ExtraFunctions
      {
      };

      ///@brief Whether or not this is indeed a vector-like class we will be able to support.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
      {
        return false;
      }

      ///@brief Whether this vector-like class has an explicitly accessible @p size member function.
      ///
      ///@remark This should be true for most sane implementations, not the least for resizing.
      ///        This being said, one might not have a public @p size for some reason,
      ///        in which case this must return false...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size() = delete;

      ///@brief Whether this vector-like class has an explicitly accessible @p capacity member function.
      ///
      ///@remark This should be true for most implementations, but not necessarily all.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_capacity() = delete;

      ///@brief Whether this vector-like class has an explicitly accessible @p resize member function.
      ///
      ///@remark This should be true for most implementations, but not necessarily all.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_resize() = delete;

      ///@brief Whether this vector-like class has an explicitly accessible @p reserve member function.
      ///
      ///@remark This should be true for most implementations, but not necessarily all.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_reserve() = delete;

      ///@brief Whether this vector-like class has an explicitly accessible @p clear member function.
      ///
      ///@remark This should be true for most implementations, but not necessarily all.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_clear() = delete;

      ///@brief Whether this vector-like class has an explicitly accessible @p shrink_to_fit member function.
      ///
      ///@remark This should be true for most implementations, but not necessarily all.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_shrink_to_fit() = delete;

      ///@brief Return the memory context for this vector.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context() = delete;

      ///@brief Return a pointer to the beginning of the data held in the vector.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class VecLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_start_of_data(VecLike && v) = delete;
    };
  }

}

#include "FriendshipProvider.h"
#include "MemoryContexts.h"
#include "InterfaceProperties.h"
#include "TypeHolder.h"
#include "UtilityHelpers.h"
#include "InterfaceDescriptionHelpers.h"

///@brief Helper macro for the implementation of concatenation.
#define MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR_INNER(A, ...) A##__VA_ARGS__

///@brief "Argument-safe" way of concatenating two preprocessing tokens.
#define MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(A, B) MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR_INNER(A, B)

///@brief Helper macro for the implementation of stringification.
#define MARIONETTE_INTERNAL_HELPER_MACRO_STRINGIFICATOR_INNER(...) #__VA_ARGS__

///@brief "Argument-safe" way of stringifying a preprocesing token.
#define MARIONETTE_INTERNAL_HELPER_MACRO_STRINGIFICATOR(A) MARIONETTE_INTERNAL_HELPER_MACRO_STRINGIFICATOR_INNER(A)

#if MARIONETTE_DEFAULT_GETTER_USE_CAPITALS
  #define MARIONETTE_DEFAULT_GETTER_NAME(NAME, CAPITALNAME) MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(MARIONETTE_DEFAULT_GETTER_PREFIX, CAPITALNAME)
#else
  #define MARIONETTE_DEFAULT_GETTER_NAME(NAME, CAPITALNAME) MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(MARIONETTE_DEFAULT_GETTER_PREFIX, NAME)
#endif

#if MARIONETTE_DEFAULT_SETTER_USE_CAPITALS
  #define MARIONETTE_DEFAULT_SETTER_NAME(NAME, CAPITALNAME) MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(MARIONETTE_DEFAULT_SETTER_PREFIX, CAPITALNAME)
#else
  #define MARIONETTE_DEFAULT_SETTER_NAME(NAME, CAPITALNAME) MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(MARIONETTE_DEFAULT_SETTER_PREFIX, NAME)
#endif

///@brief Defines the collection functions one would expect for a given property (identified by @p CAPITALNAME).
///
///@p NAME and @p CAPITALNAME are tokens corresponding to the name of the property,
///with the latter being the capitalized form (e. g. @c energy and @c Energy).
///@p GETTER_MACRO and @p SETTER_MACRO are macros that define how to create the getter and setter names
///(for consistency, one should use @c MARIONETTE_DEFAULT_GETTER_NAME and @c MARIONETTE_DEFAULT_SETTER_NAME).
///@c FINAL and @c LAYOUT are the tokens corresponding to the template arguments of the @c CollectionFunctions.
///
///@warning Please re-specify the desired access specifiers after using this macro inside the @c CollectionFunctions class!
#define MARIONETTE_DECLARE_BASIC_COLLECTION_FUNCTIONS(NAME, CAPITALNAME, GETTER_MACRO, SETTER_MACRO, FINAL, LAYOUT)                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = LAYOUT::interface_properties().can_refer_to_const_subcollection(),                 \
                                                           class disabler = std::enable_if_t<valid>>                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) GETTER_MACRO(NAME, CAPITALNAME)() const                                       \
  {                                                                                                                                                            \
    return static_cast<const FINAL *>(this)->template get_collection<CAPITALNAME>();                                                                           \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = LAYOUT::interface_properties().can_refer_to_modifiable_subcollection(),            \
                                                           class disabler = std::enable_if_t<valid>>                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) GETTER_MACRO(NAME, CAPITALNAME)()                                             \
  {                                                                                                                                                            \
    return static_cast<FINAL *>(this)->template get_collection<CAPITALNAME>();                                                                                 \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,                                                                                            \
                                                           bool valid     = LAYOUT::interface_properties().can_refer_to_modifiable_subcollection(),            \
                                                           class disabler = std::enable_if_t<valid>>                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void SETTER_MACRO(NAME, CAPITALNAME)(T && t)                                                 \
  {                                                                                                                                                            \
    static_cast<FINAL *>(this)->template get_collection<CAPITALNAME>() = std::forward<T>(t);                                                                   \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = LAYOUT::interface_properties().can_refer_to_const_object(),                        \
                                                           class disabler = std::enable_if_t<valid>>                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) GETTER_MACRO(NAME, CAPITALNAME)(const typename LAYOUT::size_type i) const     \
  {                                                                                                                                                            \
    return static_cast<const FINAL *>(this)->template get_object<CAPITALNAME>(i);                                                                              \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = LAYOUT::interface_properties().can_refer_to_modifiable_object(),                   \
                                                           class disabler = std::enable_if_t<valid>>                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) GETTER_MACRO(NAME, CAPITALNAME)(const typename LAYOUT::size_type i)           \
  {                                                                                                                                                            \
    return static_cast<FINAL *>(this)->template get_object<CAPITALNAME>(i);                                                                                    \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,                                                                                            \
                                                           bool valid     = LAYOUT::interface_properties().can_refer_to_modifiable_object(),                   \
                                                           class disabler = std::enable_if_t<valid>>                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void SETTER_MACRO(NAME, CAPITALNAME)(const typename LAYOUT::size_type i, T && t)             \
  {                                                                                                                                                            \
    static_cast<FINAL *>(this)->template get_object<CAPITALNAME>(i) = std::forward<T>(t);                                                                      \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const char * name() const                     \
  {                                                                                                                                                            \
    return MARIONETTE_INTERNAL_HELPER_MACRO_STRINGIFICATOR(NAME);                                                                                              \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const char * capitalized_name() const         \
  {                                                                                                                                                            \
    return MARIONETTE_INTERNAL_HELPER_MACRO_STRINGIFICATOR(CAPITALNAME);                                                                                       \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct Marionette_basic_collection_functions_dummy

///@brief Defines the object functions one would expect for a given property (identified by @p CAPITALNAME).
///
///@p NAME and @p CAPITALNAME are tokens corresponding to the name of the property,
///with the latter being the capitalized form (e. g. @c energy and @c Energy).
///@p GETTER_MACRO and @p SETTER_MACRO are macros that define how to create the getter and setter names
///(for consistency, one should use @c MARIONETTE_DEFAULT_GETTER_NAME and @c MARIONETTE_DEFAULT_SETTER_NAME).
///@c FINAL and @c LAYOUT are the tokens corresponding to the template arguments of the @c ObjectFunctions.
///
///@warning Please re-specify the desired access specifiers after using this macro inside the @c ObjectFunctions class!
#define MARIONETTE_DECLARE_BASIC_OBJECT_FUNCTIONS(NAME, CAPITALNAME, GETTER_MACRO, SETTER_MACRO, FINAL, LAYOUT)                                                \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = LAYOUT::interface_properties().can_refer_to_const_object(),                        \
                                                           class disabler = std::enable_if_t<valid>>                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) GETTER_MACRO(NAME, CAPITALNAME)() const                                       \
  {                                                                                                                                                            \
    return static_cast<const FINAL *>(this)->template get<CAPITALNAME>();                                                                                      \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = LAYOUT::interface_properties().can_refer_to_modifiable_object(),                   \
                                                           class disabler = std::enable_if_t<valid>>                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) GETTER_MACRO(NAME, CAPITALNAME)()                                             \
  {                                                                                                                                                            \
    return static_cast<FINAL *>(this)->template get<CAPITALNAME>();                                                                                            \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,                                                                                            \
                                                           bool valid     = LAYOUT::interface_properties().can_refer_to_modifiable_object(),                   \
                                                           class disabler = std::enable_if_t<valid>>                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void SETTER_MACRO(NAME, CAPITALNAME)(T && t)                                                 \
  {                                                                                                                                                            \
    static_cast<FINAL *>(this)->template get<CAPITALNAME>() = std::forward<T>(t);                                                                              \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const char * name() const                     \
  {                                                                                                                                                            \
    return MARIONETTE_INTERNAL_HELPER_MACRO_STRINGIFICATOR(NAME);                                                                                              \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const char * capitalized_name() const         \
  {                                                                                                                                                            \
    return MARIONETTE_INTERNAL_HELPER_MACRO_STRINGIFICATOR(CAPITALNAME);                                                                                       \
  }                                                                                                                                                            \
                                                                                                                                                               \
 private:                                                                                                                                                      \
                                                                                                                                                               \
  struct Marionette_basic_object_functions_dummy

///@brief Defines the @c CollectionFunctions and @c ObjectFunctions one would expect for a given property (identified by @p CAPITALNAME).
///
///@p NAME and @p CAPITALNAME are tokens corresponding to the name of the property,
///with the latter being the capitalized form (e. g. @c energy and @c Energy).
///@p GETTER_MACRO and @p SETTER_MACRO are macros that define how to create the getter and setter names
///(for consistency, one should use @c MARIONETTE_DEFAULT_GETTER_NAME and @c MARIONETTE_DEFAULT_SETTER_NAME).
#define MARIONETTE_DECLARE_BASIC_PROPERTY_FUNCTIONS_CLASSES(NAME, CAPITALNAME, GETTER_MACRO, SETTER_MACRO)                                                     \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>                                                                                 \
  struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CollectionFunctions                                                                                \
  {                                                                                                                                                            \
    MARIONETTE_DECLARE_BASIC_COLLECTION_FUNCTIONS(NAME, CAPITALNAME, GETTER_MACRO, SETTER_MACRO, F, Layout);                                                   \
  };                                                                                                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>                                                                                 \
  struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ObjectFunctions                                                                                    \
  {                                                                                                                                                            \
    MARIONETTE_DECLARE_BASIC_OBJECT_FUNCTIONS(NAME, CAPITALNAME, GETTER_MACRO, SETTER_MACRO, F, Layout);                                                       \
  }

///@brief Defines the @c CollectionFunctions and @c ObjectFunctions one would expect for a given property (identified by @p CAPITALNAME),
///       with the default getter and setter names.
///
///@p NAME and @p CAPITALNAME are tokens corresponding to the name of the property,
///with the latter being the capitalized form (e. g. @c energy and @c Energy).
#define MARIONETTE_DECLARE_PROPERTY_FUNCTIONS_CLASSES(NAME, CAPITALNAME)                                                                                       \
  MARIONETTE_DECLARE_BASIC_PROPERTY_FUNCTIONS_CLASSES(NAME, CAPITALNAME, MARIONETTE_DEFAULT_GETTER_NAME, MARIONETTE_DEFAULT_SETTER_NAME)

///@brief Defines a per-item property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       of type @p TYPE and with at most @p MAXSIZE entries.
#define MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(NAME, CAPITALNAME, MAXSIZE, TYPE)                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CAPITALNAME :                                          \
    Marionette::InterfaceDescription::PerItemObject                                                                                                            \
  {                                                                                                                                                            \
    using Type                                               = TYPE;                                                                                           \
    static constexpr Marionette::MaximumSizeType max_entries = static_cast<Marionette::MaximumSizeType>(MAXSIZE);                                              \
    MARIONETTE_DECLARE_PROPERTY_FUNCTIONS_CLASSES(NAME, CAPITALNAME);                                                                                          \
  }

///@brief Defines a per-item property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       of type @p TYPE.
#define MARIONETTE_DECLARE_PER_ITEM_PROPERTY(NAME, CAPITALNAME, TYPE) MARIONETTE_DECLARE_PER_ITEM_PROPERTY_SIZED(NAME, CAPITALNAME, 0, TYPE)

///@brief Defines a jagged vector property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       with at most @p MAXSIZE entries, with the vector holding the properties specified in the variadic arguments,
///       with @p SIZETYPE being large enough to hold the size of the vector.
#define MARIONETTE_DECLARE_JAGGED_VECTOR_PROPERTY_SIZED(NAME, CAPITALNAME, MAXSIZE, SIZETYPE, ...)                                                             \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CAPITALNAME :                                          \
    Marionette::InterfaceDescription::JaggedVector                                                                                                             \
  {                                                                                                                                                            \
    using Properties                                         = Marionette::Utility::TypeHolder<__VA_ARGS__>;                                                   \
    using size_type                                          = SIZETYPE;                                                                                       \
    static constexpr Marionette::MaximumSizeType max_entries = static_cast<Marionette::MaximumSizeType>(MAXSIZE);                                              \
    MARIONETTE_DECLARE_PROPERTY_FUNCTIONS_CLASSES(NAME, CAPITALNAME);                                                                                          \
  }

///@brief Defines a jagged vector property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       with the vector holding the properties specified in the variadic arguments,
///       with @p SIZETYPE being large enough to hold the size of the vector.
#define MARIONETTE_DECLARE_JAGGED_VECTOR_PROPERTY(NAME, CAPITALNAME, SIZETYPE, ...)                                                                            \
  MARIONETTE_DECLARE_JAGGED_VECTOR_PROPERTY_SIZED(NAME, CAPITALNAME, 0, SIZETYPE, __VA_ARGS__)

///@brief Defines a sub-collection property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       with at most @p MAXSIZE entries, corresponding to the properties specified in the variadic arguments.
#define MARIONETTE_DECLARE_SUBCOLLECTION_PROPERTY_SIZED(NAME, CAPITALNAME, MAXSIZE, ...)                                                                       \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CAPITALNAME :                                          \
    Marionette::InterfaceDescription::SubCollection                                                                                                            \
  {                                                                                                                                                            \
    using Properties                                         = Marionette::Utility::TypeHolder<__VA_ARGS__>;                                                   \
    static constexpr Marionette::MaximumSizeType max_entries = static_cast<Marionette::MaximumSizeType>(MAXSIZE);                                              \
    MARIONETTE_DECLARE_PROPERTY_FUNCTIONS_CLASSES(NAME, CAPITALNAME);                                                                                          \
  }

///@brief Defines a sub-collection property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       corresponding to the properties specified in the variadic arguments.
#define MARIONETTE_DECLARE_SUBCOLLECTION_PROPERTY(NAME, CAPITALNAME, ...) MARIONETTE_DECLARE_SUBCOLLECTION_PROPERTY_SIZED(NAME, CAPITALNAME, 0, __VA_ARGS__)

///@brief Defines a property array property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       with at most @p MAXSIZE entries, corresponding to the properties specified in the variadic arguments, holding @p ARRAY_SIZE of each of them.
#define MARIONETTE_DECLARE_PROPERTY_ARRAY_PROPERTY_SIZED(NAME, CAPITALNAME, MAXSIZE, ARRAY_SIZE, ...)                                                          \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CAPITALNAME :                                          \
    Marionette::InterfaceDescription::PropertyArray                                                                                                            \
  {                                                                                                                                                            \
    using Properties                                           = Marionette::Utility::TypeHolder<__VA_ARGS__>;                                                 \
    static constexpr Marionette::PropertiesIndexer number      = static_cast<Marionette::PropertiesIndexer>(ARRAY_SIZE);                                       \
    static constexpr Marionette::MaximumSizeType   max_entries = static_cast<Marionette::MaximumSizeType>(MAXSIZE);                                            \
    MARIONETTE_DECLARE_PROPERTY_FUNCTIONS_CLASSES(NAME, CAPITALNAME);                                                                                          \
  }

///@brief Defines a property array property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       corresponding to the properties specified in the variadic arguments, holding @p ARRAY_SIZE of each of them.
#define MARIONETTE_DECLARE_PROPERTY_ARRAY_PROPERTY(NAME, CAPITALNAME, ARRAY_SIZE, ...)                                                                         \
  MARIONETTE_DECLARE_PROPERTY_ARRAY_PROPERTY_SIZED(NAME, CAPITALNAME, 0, ARRAY_SIZE, __VA_ARGS__)

///@brief Defines a property array property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       corresponding to an array of @p ARRAY_SIZE properties of type @p TYPE, with at most @p MAXSIZE entries.
#define MARIONETTE_DECLARE_SIMPLE_PROPERTY_ARRAY_PROPERTY_SIZED(NAME, CAPITALNAME, MAXSIZE, ARRAY_SIZE, TYPE)                                                  \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CAPITALNAME :                                          \
    Marionette::InterfaceDescription::PropertyArray                                                                                                            \
  {                                                                                                                                                            \
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SimpleArrayProperty :                                \
      Marionette::InterfaceDescription::PerItemObject                                                                                                          \
    {                                                                                                                                                          \
      using Type                                               = TYPE;                                                                                         \
      static constexpr Marionette::MaximumSizeType max_entries = (MAXSIZE);                                                                                    \
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>                                                                             \
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CollectionFunctions                                                                            \
      {                                                                                                                                                        \
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_const_subcollection(),           \
                                                                 class disabler = std::enable_if_t<valid>>                                                     \
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get() const                                                             \
        {                                                                                                                                                      \
          return static_cast<const F *>(this)->template get_collection<SimpleArrayProperty>();                                                                 \
        }                                                                                                                                                      \
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_modifiable_subcollection(),      \
                                                                 class disabler = std::enable_if_t<valid>>                                                     \
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get()                                                                   \
        {                                                                                                                                                      \
          return static_cast<F *>(this)->template get_collection<SimpleArrayProperty>();                                                                       \
        }                                                                                                                                                      \
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,                                                                                      \
                                                                 bool valid     = Layout::interface_properties().can_refer_to_modifiable_subcollection(),      \
                                                                 class disabler = std::enable_if_t<valid>>                                                     \
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void set(T && t)                                                                       \
        {                                                                                                                                                      \
          static_cast<F *>(this)->template get_collection<SimpleArrayProperty>() = std::forward<T>(t);                                                         \
        }                                                                                                                                                      \
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_const_object(),                  \
                                                                 class disabler = std::enable_if_t<valid>>                                                     \
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get(const typename Layout::size_type i) const                           \
        {                                                                                                                                                      \
          return static_cast<const F *>(this)->template get_object<SimpleArrayProperty>(i);                                                                    \
        }                                                                                                                                                      \
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_modifiable_object(),             \
                                                                 class disabler = std::enable_if_t<valid>>                                                     \
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get(const typename Layout::size_type i)                                 \
        {                                                                                                                                                      \
          return static_cast<F *>(this)->template get_object<SimpleArrayProperty>(i);                                                                          \
        }                                                                                                                                                      \
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,                                                                                      \
                                                                 bool valid     = Layout::interface_properties().can_refer_to_modifiable_object(),             \
                                                                 class disabler = std::enable_if_t<valid>>                                                     \
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void set(const typename Layout::size_type i, T && t)                                   \
        {                                                                                                                                                      \
          static_cast<F *>(this)->template get_object<SimpleArrayProperty>(i) = std::forward<T>(t);                                                            \
        }                                                                                                                                                      \
      };                                                                                                                                                       \
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>                                                                             \
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ObjectFunctions                                                                                \
      {                                                                                                                                                        \
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_const_object(),                  \
                                                                 class disabler = std::enable_if_t<valid>>                                                     \
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get() const                                                             \
        {                                                                                                                                                      \
          return static_cast<const F *>(this)->template get<SimpleArrayProperty>();                                                                            \
        }                                                                                                                                                      \
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_refer_to_modifiable_object(),             \
                                                                 class disabler = std::enable_if_t<valid>>                                                     \
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get()                                                                   \
        {                                                                                                                                                      \
          return static_cast<F *>(this)->template get<SimpleArrayProperty>();                                                                                  \
        }                                                                                                                                                      \
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,                                                                                      \
                                                                 bool valid     = Layout::interface_properties().can_refer_to_modifiable_object(),             \
                                                                 class disabler = std::enable_if_t<valid>>                                                     \
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void set(T && t)                                                                       \
        {                                                                                                                                                      \
          static_cast<F *>(this)->template get<SimpleArrayProperty>() = std::forward<T>(t);                                                                    \
        }                                                                                                                                                      \
      };                                                                                                                                                       \
    };                                                                                                                                                         \
    using Properties                                           = Marionette::Utility::TypeHolder<SimpleArrayProperty>;                                         \
    static constexpr Marionette::MaximumSizeType   max_entries = static_cast<Marionette::MaximumSizeType>(MAXSIZE);                                            \
    static constexpr Marionette::PropertiesIndexer number      = static_cast<Marionette::PropertiesIndexer>(ARRAY_SIZE);                                       \
    MARIONETTE_DECLARE_PROPERTY_FUNCTIONS_CLASSES(NAME, CAPITALNAME);                                                                                          \
  }

///@brief Defines a property array property with the name @p CAPITALNAME (and non-capitalized form @p NAME),
///       corresponding to an array of @p ARRAY_SIZE properties of type @p TYPE.
#define MARIONETTE_DECLARE_SIMPLE_PROPERTY_ARRAY_PROPERTY(NAME, CAPITALNAME, ARRAY_SIZE, TYPE)                                                                 \
  MARIONETTE_DECLARE_SIMPLE_PROPERTY_ARRAY_PROPERTY_SIZED(NAME, CAPITALNAME, 0, ARRAY_SIZE, TYPE)

#endif
