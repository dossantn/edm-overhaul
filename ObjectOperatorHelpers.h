//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_OBJECTOPERATORHELPERS_H
#define MARIONETTE_OBJECTOPERATORHELPERS_H

#include <utility>
#include <type_traits>

#include "MarionetteBase.h"

namespace Marionette
{
  namespace Collections
  {
    namespace impl
    {
#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(NAME)                                                                                              \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Us, class... Ts>                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(Ts && ...)    \
  {                                                                                                                                                            \
    return false;                                                                                                                                              \
  }                                                                                                                                                            \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _base_dummy)

#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE(NAME, PREOP)                                                                               \
  MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE_CHECKED(NAME, PREOP, true)
#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE_CHECKED(NAME, PREOP, CONDITION)                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Us, class... Ts>                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(Ts && ...)    \
  {                                                                                                                                                            \
    return false;                                                                                                                                              \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T = U, class Check = decltype(PREOP std::declval<T &>())>                                     \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)()             \
  {                                                                                                                                                            \
    return CONDITION;                                                                                                                                          \
  }                                                                                                                                                            \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _pre_dummy)

#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_POST(NAME, POSTOP)                                                                             \
  MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_POST_CHECKED(NAME, POSTOP, true)
#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_POST_CHECKED(NAME, POSTOP, CONDITION)                                                          \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Us, class... Ts>                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(Ts && ...)    \
  {                                                                                                                                                            \
    return false;                                                                                                                                              \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T = U, class Check = decltype(std::declval<T &>() POSTOP)>                                    \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)()             \
  {                                                                                                                                                            \
    return CONDITION;                                                                                                                                          \
  }                                                                                                                                                            \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _post_dummy)

#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(NAME, OP) MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY_CHECKED(NAME, OP, true)
#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY_CHECKED(NAME, OP, CONDITION)                                                                  \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Us, class... Ts>                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(Ts && ...)    \
  {                                                                                                                                                            \
    return false;                                                                                                                                              \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T, class Check = decltype(std::declval<U &>() OP std::declval<T &>())>                        \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)()             \
  {                                                                                                                                                            \
    return CONDITION;                                                                                                                                          \
  }                                                                                                                                                            \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _bin_dummy)

#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR(NAME) MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_CHECKED(NAME, true)
#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_CHECKED(NAME, CONDITION)                                                                    \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Us, class... Ts>                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(Ts && ...)    \
  {                                                                                                                                                            \
    return false;                                                                                                                                              \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts,                                                                                        \
                                                           class disabler =                                                                                    \
                                                             std::enable_if_t < (std::is_class_v<U> || std::is_union_v<U>) && (sizeof...(Ts) > 0) &&           \
                                                             (CONDITION) >> MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool        \
                                                                            MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)()               \
  {                                                                                                                                                            \
    return decltype(extra_operator_checks_helper<U>::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_,                                     \
                                                                                                            NAME)(std::declval<U>(),                           \
                                                                                                                  std::declval<Ts>()...))::value;              \
  }                                                                                                                                                            \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _operator_dummy)

#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_MEMBER(NAME, FUNC)                                                              \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Us, class... Ts>                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(Ts && ...)    \
  {                                                                                                                                                            \
    return std::false_type {};                                                                                                                                 \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T = U, class... Ts, class Ret = decltype(std::declval<T &>().FUNC(std::declval<Ts>()...))>    \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(T &&,         \
                                                                                                                                                 Ts && ...)    \
  {                                                                                                                                                            \
    return std::true_type {};                                                                                                                                  \
  }                                                                                                                                                            \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _member_dummy)

#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_STANDALONE(NAME, FUNC)                                                          \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Us, class... Ts>                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(Ts && ...)    \
  {                                                                                                                                                            \
    return std::false_type {};                                                                                                                                 \
  }                                                                                                                                                            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T = U, class... Ts, class Ret = decltype(FUNC(std::declval<T>(), std::declval<Ts>()...))>     \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(T &&,         \
                                                                                                                                                 Ts && ...)    \
  {                                                                                                                                                            \
    return std::true_type {};                                                                                                                                  \
  }                                                                                                                                                            \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _standalone_dummy)

#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_BOTH(NAME, FUNC)                                                                \
  MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_MEMBER(MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _1), FUNC);                \
  MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_STANDALONE(MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _2), FUNC);            \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Us, class... Ts>                                                                           \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME)(Ts && ... ts) \
  {                                                                                                                                                            \
    using ret_1 = decltype(MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME),                   \
                                                                         _1)(std::forward<Ts>(ts)...));                                                        \
    using ret_2 = decltype(MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME),                   \
                                                                         _2)(std::forward<Ts>(ts)...));                                                        \
    return std::integral_constant < bool, ret_1::value || ret_2::value > {};                                                                                   \
  }                                                                                                                                                            \
  struct MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(NAME, _both_dummy)

#define MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(NAME)                                                                                               \
  MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>                                                                                        \
  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_func_, NAME)()    \
  {                                                                                                                                                            \
    constexpr auto pre_ret  = (Utility::TypeHolder<Utility::InvalidType> {} & ... &                                                                            \
                              std::conditional_t<bool(ToCheck::template MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(has_operator_, NAME) < Ts... > ()),     \
                                                  Utility::TypeHolder<ToCheck>,                                                                                \
                                                  Utility::TypeHolder<Utility::InvalidType>> {});                                                              \
    constexpr auto ret_type = Utility::get_one_type_or_invalid(pre_ret);                                                                                       \
    if constexpr (std::is_same_v<std::decay_t<decltype(ret_type)>, Utility::InvalidType>)                                                                      \
      {                                                                                                                                                        \
        return Utility::InvalidType {};                                                                                                                        \
      }                                                                                                                                                        \
    else                                                                                                                                                       \
      {                                                                                                                                                        \
        return ret_type.checked_property();                                                                                                                    \
      }                                                                                                                                                        \
  }                                                                                                                                                            \
  template <class... Ts>                                                                                                                                       \
  using MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) =                                                                               \
    std::decay_t<decltype(MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_func_, NAME) < Ts... > ())>

#define MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(NAME)                                                                                       \
  template <class... Ts>                                                                                                                                       \
  using MARIONETTE_INTERNAL_HELPER_MACRO_CONCATENATOR(operator_checker_, NAME) = Utility::InvalidType

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class U>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES extra_operator_checks_helper
      {
       public:

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_BOTH(logic_and, operator&&);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_BOTH(logic_or, operator||);

#if MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_BOTH(addressof, operator&);
#endif
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_MEMBER(arrow, operator->);

#define MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_COMMA ,

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_MEMBER(comma_1, operator MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_COMMA);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_STANDALONE(comma_2, operator MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_COMMA);

#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_COMMA

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto has_operator_comma(Ts &&... ts)
        {
          using ret_1 = decltype(has_operator_comma_1(std::forward<Ts>(ts)...));
          using ret_2 = decltype(has_operator_comma_2(std::forward<Ts>(ts)...));
          return std::integral_constant < bool, ret_1::value || ret_2::value > {};
        }

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_MEMBER(access, operator[]);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_MEMBER(call, operator());
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, bool valid, bool add_const>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES object_operators_helper;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, bool ac>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES object_operators_helper<T, false, ac>
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto checked_property()
        {
          return T {};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool add_constant()
        {
          return ac;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
        {
          return false;
        }

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(add_eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(sub_eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(mul_eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(div_eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(mod_eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(bitand_eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(bitor_eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(bitxor_eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(lsh_eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(rsh_eq);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(pre_inc);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(pre_dec);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(post_inc);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(post_dec);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(self_add);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(self_sub);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(add);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(sub);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(mul);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(div);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(mod);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(self_compl);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(bit_and);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(bit_or);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(bit_xor);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(lsh);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(rsh);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(self_not);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(logic_and);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(logic_or);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(eq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(neq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(lss);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(grt);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(leq);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(geq);

#if MARIONETTE_CONSIDER_SPACESHIP_OPERATOR_OVERLOAD
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(spaceship);
#endif

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(deref);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(addressof);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(arrow);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(pointer_to_member);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(comma);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(access);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE(call);
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Prop, bool ac>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES object_operators_helper<Prop, true, ac>
      {
       private:

        using U = std::conditional_t<ac, const Prop, Prop>;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool add_constant()
        {
          return ac;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
        {
          return true;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto checked_property()
        {
          return U {};
        }

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(add_eq, +=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(sub_eq, -=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(mul_eq, *=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(div_eq, /=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(mod_eq, %=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(bitand_eq, &=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(bitor_eq, |=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(bitxor_eq, ^=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(lsh_eq, <<=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(rsh_eq, >>=);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE(pre_inc, ++);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE(pre_dec, --);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_POST(post_inc, ++);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_POST(post_dec, --);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE(self_add, +);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE(self_sub, -);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(add, +);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(sub, -);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(mul, *);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(div, /);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(mod, %);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE(self_compl, ~);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(bit_and, &);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(bit_or, |);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(bit_xor, ^);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(lsh, <<);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(rsh, >>);

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE(self_not, !);

#if MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR(logic_and);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR(logic_or);
#endif

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(eq, ==);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(neq, !=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(lss, <);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(grt, >);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(leq, <=);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(geq, >=);

#if MARIONETTE_CONSIDER_SPACESHIP_OPERATOR_OVERLOAD
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(spaceship, <=>);
#endif

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE(deref, *);

#if MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR(addressof);
#endif

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR(arrow);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY(pointer_to_member, ->*);

#if MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR(comma);
#endif

        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR(access);
        MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR(call);
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... ToCheck>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES MultipleOperatorChecks
      {
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(add_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(sub_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(mul_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(div_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(mod_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(bitand_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(bitor_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(bitxor_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(lsh_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(rsh_eq);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(pre_inc);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(pre_dec);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(post_inc);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(post_dec);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(self_add);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(self_sub);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(add);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(sub);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(mul);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(div);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(mod);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(self_compl);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(bit_and);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(bit_or);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(bit_xor);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(lsh);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(rsh);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(self_not);

#if MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(logic_and);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(logic_or);
#endif

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(neq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(lss);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(grt);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(leq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(geq);

#if MARIONETTE_CONSIDER_SPACESHIP_OPERATOR_OVERLOAD
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(spaceship);
#endif

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(deref);

#if MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(addressof);
#endif

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(arrow);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(pointer_to_member);

#if MARIONETTE_CONSIDER_UNUSUAL_OPERATOR_OVERLOADS
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(comma);
#endif

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(access);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS(call);

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class U, class T>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool assignment_checker_conditions_helper()
        {
          if constexpr (std::is_same_v<std::decay_t<U>, std::decay_t<T>> || std::is_base_of_v<std::decay_t<U>, std::decay_t<T>> ||
                        std::is_base_of_v<std::decay_t<T>, std::decay_t<U>>)
            {
              return false;
            }
          else
            {
              return std::is_assignable_v<U, T>;
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto assignment_checker_func()
        {
          constexpr auto pre_ret = (Utility::TypeHolder<Utility::InvalidType> {} & ... & std::conditional_t < !ToCheck::add_constant() && ToCheck::is_valid() &&
                                      assignment_checker_conditions_helper<std::decay_t<decltype(ToCheck::checked_property())>, Ts...>(),
                                    Utility::TypeHolder<ToCheck>,
                                    Utility::TypeHolder < Utility::InvalidType >> {});

          constexpr auto ret_type = Utility::get_one_type_or_invalid(pre_ret);
          if constexpr (std::is_same_v<std::decay_t<decltype(ret_type)>, Utility::InvalidType>)
            {
              return Utility::InvalidType {};
            }
          else
            {
              return ret_type.checked_property();
            }
        }

        template <class... Ts>
        using assignment_checker = std::decay_t<decltype(assignment_checker_func<Ts...>())>;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES EmptyMultipleOperatorChecks
      {
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(add_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(sub_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(mul_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(div_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(mod_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(bitand_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(bitor_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(bitxor_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(lsh_eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(rsh_eq);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(pre_inc);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(pre_dec);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(post_inc);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(post_dec);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(self_add);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(self_sub);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(add);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(sub);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(mul);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(div);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(mod);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(self_compl);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(bit_and);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(bit_or);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(bit_xor);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(lsh);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(rsh);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(self_not);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(logic_and);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(logic_or);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(eq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(neq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(lss);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(grt);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(leq);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(geq);

#if MARIONETTE_CONSIDER_SPACESHIP_OPERATOR_OVERLOAD
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(spaceship);
#endif

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(deref);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(addressof);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(arrow);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(pointer_to_member);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(comma);

        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(access);
        MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID(call);

        template <class... Ts>
        using assignment_checker = Utility::InvalidType;
      };

#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_BASE_CASE
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_PRE_CHECKED
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_POST
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_UNARY_POST_CHECKED
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_BINARY_CHECKED
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_CHECKED
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_MEMBER
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_STANDALONE
#undef MARIONETTE_OBJECT_HELPERS_OPERATOR_HELPER_CHECKER_OPERATOR_EXTRA_CHECK_BOTH
#undef MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS
#undef MARIONETTE_OBJECT_HELPERS_MULTIPLE_OPERATOR_CHECKS_INVALID

    }
  }
}

#endif
