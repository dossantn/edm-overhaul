//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_LAYOUTTYPES_H
#define MARIONETTE_LAYOUTTYPES_H

#include "MarionetteBase.h"

#include "LayoutTypes/LayoutHelpers.h"
#include "LayoutTypes/InternalLayoutTypes.h"
#include "LayoutTypes/VectorLike.h"
#include "LayoutTypes/StructLike.h"

#endif
