//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_PORTABILITY_ANNOTATIONS_H
#define MARIONETTE_PORTABILITY_ANNOTATIONS_H

//-----------------------------------------------------------------------------------------------------------------------------------------

#if MARIONETTE_PORTABILITY_ANNOTATION_ALLOW_FULL_OVERRIDE

  //Don't check for whether or not we can define the macros here:
  //the user specifically requested to be able to override everything.

#else

  //Error out from the start in case the important macros were already defined elsewhere.

  #ifdef MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS
    // clang-format off
  #error "The macro MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS is reserved by the Marionette library. Please reorder your includes if you define it elsewhere."
    // clang-format on
  #endif

  #ifdef MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
    // clang-format off
  #error "The macro MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION is reserved by the Marionette library. Please reorder your includes if you define it elsewhere."
    // clang-format on
  #endif

  #ifdef MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    // clang-format off
  #error "The macro MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is reserved by the Marionette library. Please reorder your includes if you define it elsewhere."
    // clang-format on
  #endif

  #ifdef MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
    // clang-format off
  #error "The macro MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES is reserved by the Marionette library. Please reorder your includes if you define it elsewhere."
    // clang-format on
  #endif

#endif

//-----------------------------------------------------------------------------------------------------------------------------------------

//Actual platform-specific defines and stuff

#ifndef MARIONETTE_PORTABILITY_MSVC_EBO_FIX
  #if defined(_MSC_VER) && _MSC_VER >= 1900 && _MSC_FULL_VER >= 190023918 && (!defined(__clang__) || !defined(__CUDA__))
    ///@brief MSVC behaves differently from (non-Windows) Clang/GCC when it comes to empty base object optimization
    ///       whenever multiple inheritance is in play, unless we ask it to optimize properly.
    ///
    ///       See: https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#windows-specific
    ///            and https://learn.microsoft.com/en-us/cpp/cpp/empty-bases
    #define MARIONETTE_PORTABILITY_MSVC_EBO_FIX __declspec(empty_bases)
  #else
    #if defined(_MSC_VER) && (!defined(__clang__) || !defined(__CUDA__))
      #pragma message("MSVC versions earlier than Visual Studio 2015 Update 3 "                                                                                \
                      "do not optimize multiple empty base class inheritance, "                                                                                \
                      "leading to additional, unexpected padding.")
    #endif
    #define MARIONETTE_PORTABILITY_MSVC_EBO_FIX
  #endif
#else
  // clang-format off
  #error "The macro MARIONETTE_PORTABILITY_MSVC_EBO_FIX is reserved by the Marionette library. Please reorder your includes if you define it elsewhere."
  // clang-format on
#endif

#ifndef MARIONETTE_PORTABILITY_NVCC_SKIP_CHECKS
  #if MARIONETTE_ENABLE_PLATFORM_SUPPORT_FOR_CUDA && !(defined(__clang__) && defined(__CUDA__))

    #if defined(_MSC_VER) && !defined(__clang__)
      #define MARIONETTE_PORTABILITY_NVCC_SKIP_CHECKS __pragma("hd_warning_disable") __pragma("nv_exec_check_disable")
    #else
      #define MARIONETTE_PORTABILITY_NVCC_SKIP_CHECKS _Pragma("hd_warning_disable") _Pragma("nv_exec_check_disable")
    #endif

    //Following from what Thrust does, it seems MSVC (as usual) is weird.

  #else

    #define MARIONETTE_PORTABILITY_NVCC_SKIP_CHECKS

  #endif
#else
  // clang-format off
  #error "The macro MARIONETTE_PORTABILITY_NVCC_SKIP_CHECKS is reserved by the Marionette library. Please reorder your includes if you define it elsewhere."
  // clang-format on
#endif

#ifndef MARIONETTE_PORTABILITY_CUDA_HOST_DEVICE

  #if MARIONETTE_ENABLE_PLATFORM_SUPPORT_FOR_CUDA
    #define MARIONETTE_PORTABILITY_CUDA_HOST_DEVICE __host__ __device__
  #else
    #define MARIONETTE_PORTABILITY_CUDA_HOST_DEVICE
  #endif

#else
  // clang-format off
  #error "The macro MARIONETTE_PORTABILITY_CUDA_HOST_DEVICE is reserved by the Marionette library. Please reorder your includes if you define it elsewhere."
  // clang-format on
#endif

//-----------------------------------------------------------------------------------------------------------------------------------------

//Declaration of the final things...

#ifndef MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS
  ///@brief Things to be placed before the class declarations and definitions (and also before the template arguments).
  ///       The user can add to this through @c MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_CLASS.
  ///
  ///@remark The `_Pragma` or `__pragma` compiler intrinsics can be useful...
  #define MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_CLASS
//Nothing to add as a pre-class annotation for now.
#endif

#ifndef MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
  ///@brief Things to be placed before the function declarations and definitions (and also before the template arguments).
  ///       The user can add to this through @c MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_FUNCTION.
  ///
  ///@remark The `_Pragma` or `__pragma` compiler intrinsics can be useful...
  #define MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_NVCC_SKIP_CHECKS MARIONETTE_PORTABILITY_ANNOTATION_USER_PRE_FUNCTION
#endif

#ifndef MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
  ///@brief Things to be placed after the (template and) keyword and before the name for all class declarations and definitions.
  ///       The user can add to this through @c MARIONETTE_PORTABILITY_ANNOTATION_USER_CLASS_ATTRIBUTES.
  #define MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES MARIONETTE_PORTABILITY_MSVC_EBO_FIX MARIONETTE_PORTABILITY_ANNOTATION_USER_CLASS_ATTRIBUTES
#endif

#ifndef MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
  ///@brief Things to be placed after the templates and before any declaration specifiers (`static`, `constexpr`, `friend`) or return types.
  ///       The user can add to this through @c MARIONETTE_PORTABILITY_ANNOTATION_USER_FUNCTION_ATTRIBUTES.
  #define MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES                                                                                                \
    MARIONETTE_PORTABILITY_CUDA_HOST_DEVICE MARIONETTE_PORTABILITY_ANNOTATION_USER_FUNCTION_ATTRIBUTES
#endif

#endif
