//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_MARIONETTETRANSFERSOBJECTS_H
#define MARIONETTE_MARIONETTETRANSFERSOBJECTS_H

#include <utility>
#include <type_traits>
#include <new>
//Placement new

#include "MarionetteBase.h"
#include "ObjectHelpers.h"
#include "MarionetteTransfersHelpers.h"

namespace Marionette
{
  namespace Transfers
  {
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... SourceProps,
                                                          class... DestProps,
                                                          class SourceLayout,
                                                          class DestLayout,
                                                          class SourceMetaInfo,
                                                          class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::Object<DestLayout, Utility::TypeHolder<DestProps...>, DestMetaInfo>,
                          Collections::Object<SourceLayout, Utility::TypeHolder<SourceProps...>, SourceMetaInfo>,
                          TransferPriority::DefaultTertiary,
                          std::enable_if_t<DestMetaInfo::extent == SourceMetaInfo::extent>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(1);

     private:

      using SourceTypeHolder = Utility::TypeHolder<SourceProps...>;
      using DestTypeHolder   = Utility::TypeHolder<DestProps...>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return impl::properties_equivalent(DestTypeHolder {}, SourceTypeHolder {}) && DestLayout::interface_properties().can_copy_into_object() &&
               SourceLayout::interface_properties().can_copy_out_of_object();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return impl::properties_equivalent(DestTypeHolder {}, SourceTypeHolder {}) && DestLayout::interface_properties().can_move_into_object() &&
               SourceLayout::interface_properties().can_move_out_of_object();
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_one(DestLike & d, const SourceLike & s)
      {
        d.template get<Property>() = s.template get<Property>();
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        (copy_one<DestProps>(d, s), ...);
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move_one(DestLike & d, SourceLike && s)
      {
        d.template get<Property>() = std::move(std::move(s).template get<Property>());
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        (copy_one<DestProps>(d, std::move(s)), ...);
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... Properties,
                                                          class Layout,
                                                          class MetaInfo,
                                                          Utility::ArrayInitialize initialization,
                                                          class... IdxTpMv,
                                                          class Extra>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification<Collections::Object<Layout, Utility::TypeHolder<Properties...>, MetaInfo>,
                                                                                    Utility::ArrayInitializationHelper<initialization, IdxTpMv...>,
                                                                                    TransferPriority::DefaultTertiary,
                                                                                    Extra>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(2);

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class Obj>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) getter(Obj && o)
      {
        return Utility::FriendshipProvider::template get_multi_array<idx>(std::forward<Obj>(o));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void single_transfer(DestLike & d, SourceLike && s)
      {
        getter<idx>(d) = getter<idx>(std::forward<SourceLike>(s));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, PropertiesIndexer... idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      multi_transfer(const std::integer_sequence<PropertiesIndexer, idx...> &, DestLike & d, SourceLike && s)
      {
        (single_transfer<idx>(d, std::forward<SourceLike>(s)), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return Layout::interface_properties().can_copy_into_object();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return Layout::interface_properties().can_move_into_object() && MetaInfo::extent <= sizeof...(IdxTpMv);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        multi_transfer(std::make_integer_sequence<PropertiesIndexer, MetaInfo::extent>(), d, s);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        multi_transfer(std::make_integer_sequence<PropertiesIndexer, MetaInfo::extent>(), d, std::move(s));
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class SourceLayout, class DestLayout, class SourceMetaInfo, class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::Object<DestLayout, Utility::TypeHolder<Property>, DestMetaInfo>,
                          Collections::Object<SourceLayout, Utility::TypeHolder<Property>, SourceMetaInfo>,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<(DestMetaInfo::extent == SourceMetaInfo::extent) && InterfaceDescription::is_global(Property {})>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(3);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return (!InterfaceDescription::has_extra_write_proxy_types(Property {}) ||
                (DestLayout::interface_properties().can_copy_into_object() && SourceLayout::interface_properties().can_copy_out_of_object()));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return !InterfaceDescription::has_extra_write_proxy_types(Property {});
        //Otherwise we copy...
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy([[maybe_unused]] DestLike & d, [[maybe_unused]] const SourceLike & s)
      {
        if constexpr (InterfaceDescription::has_extra_write_proxy_types(Property {}))
          {
            const PropertiesIndexer base_index_dest   = Utility::FriendshipProvider::get_multi_array_index(d);
            const PropertiesIndexer base_index_source = Utility::FriendshipProvider::get_multi_array_index(s);
            for (PropertiesIndexer i = 0; i < DestMetaInfo::extent; ++i)
              {
                if constexpr (Collections::impl::object_is_write_proxy(SourceLayout {}) && Collections::impl::object_is_write_proxy(DestLayout {}))
                  {
                    Property::write_to_write(base_index_dest + i, base_index_source + i, d, s);
                  }
                else if constexpr (Collections::impl::object_is_write_proxy(SourceLayout {}) && !Collections::impl::object_is_write_proxy(DestLayout {}))
                  {
                    Property::write_to_read(base_index_dest + i, base_index_source + i, d, s);
                  }
                else if constexpr (!Collections::impl::object_is_write_proxy(SourceLayout {}) && Collections::impl::object_is_write_proxy(DestLayout {}))
                  {
                    Property::read_to_write(base_index_dest + i, base_index_source + i, d, s);
                  }
                else if constexpr (!Collections::impl::object_is_write_proxy(SourceLayout {}) && !Collections::impl::object_is_write_proxy(DestLayout {}))
                  {
                    Property::read_to_read(base_index_dest + i, base_index_source + i, d, s);
                  }
                else
                  {
                    static_assert(Utility::always_false<DestLike>, "LOGIC ERROR.");
                  }
              }
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike &, SourceLike &&)
      {
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class SourceLayout, class DestLayout, class SourceMetaInfo, class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification<
      Collections::Object<DestLayout, Utility::TypeHolder<Property>, DestMetaInfo>,
      Collections::Object<SourceLayout, Utility::TypeHolder<Property>, SourceMetaInfo>,
      TransferPriority::DefaultSecondary,
      std::enable_if_t<impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::NoObject && !InterfaceDescription::is_global(Property {})>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(5);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike &, const SourceLike &)
      {
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike &, SourceLike &&)
      {
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class SourceLayout, class DestLayout, class SourceMetaInfo, class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::Object<DestLayout, Utility::TypeHolder<Property>, DestMetaInfo>,
                          Collections::Object<SourceLayout, Utility::TypeHolder<Property>, SourceMetaInfo>,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<(DestMetaInfo::extent == SourceMetaInfo::extent) &&
                                           (impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::PropertyArray) &&
                                           !InterfaceDescription::is_global(Property {})>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(6);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_object() && SourceLayout::interface_properties().can_copy_out_of_object();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return DestLayout::interface_properties().can_move_into_object() && SourceLayout::interface_properties().can_move_out_of_object();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        for (PropertiesIndexer i = 0; i < Property::number; ++i)
          {
            d[i] = s[i];
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        for (PropertiesIndexer i = 0; i < Property::number; ++i)
          {
            d[i] = std::move(std::move(s)[i]);
          }
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property,
                                                          class Layout,
                                                          class MetaInfo,
                                                          Utility::ArrayInitialize initialization,
                                                          class... IdxTpMv>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification<
      Collections::Object<Layout, Utility::TypeHolder<Property>, MetaInfo>,
      Utility::ArrayInitializationHelper<initialization, IdxTpMv...>,
      TransferPriority::DefaultSecondary,
      std::enable_if_t<impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::PropertyArray && !InterfaceDescription::is_global(Property {})>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(7);

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class Obj>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) getter(Obj && o)
      {
        return Utility::FriendshipProvider::template get_multi_array<idx>(std::forward<Obj>(o));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer idx, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void single_transfer(DestLike & d, SourceLike && s)
      {
        d[idx] = getter<idx>(std::forward<SourceLike>(s));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, PropertiesIndexer... idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      multi_transfer(const std::integer_sequence<PropertiesIndexer, idx...> &, DestLike & d, SourceLike && s)
      {
        (single_transfer<idx>(d, std::forward<SourceLike>(s)), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return Layout::interface_properties().can_copy_into_object() &&
               (std::is_assignable_v<Collections::Object<Layout, typename Property::Properties, MetaInfo> &, const typename IdxTpMv::Type &> && ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return Layout::interface_properties().can_move_into_object() && Property::number <= sizeof...(IdxTpMv) &&
               (std::is_assignable_v<Collections::Object<Layout, typename Property::Properties, MetaInfo> &, typename IdxTpMv::Type &&> && ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        multi_transfer(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, s);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        multi_transfer(std::make_integer_sequence<PropertiesIndexer, Property::number>(), d, std::move(s));
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class Layout, class MetaInfo, class AssignedType>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::Object<Layout, Utility::TypeHolder<Property>, MetaInfo>,
                          AssignedType,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::PerItemObject &&
                                           !impl::is_object_v<AssignedType> && !InterfaceDescription::is_global(Property {})>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(9);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return Layout::interface_properties().can_copy_into_object() && std::is_assignable_v<typename Property::Type &, const AssignedType &>;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return Layout::interface_properties().can_move_into_object() && std::is_assignable_v<typename Property::Type &, AssignedType &&>;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        for (PropertiesIndexer i = 0; i < MetaInfo::extent; ++i)
          {
            auto * ptr =
              Utility::FriendshipProvider::template get_object_pointer<Property>(d,
                                                                                 Utility::FriendshipProvider::get_multi_array_index(d) * MetaInfo::extent + i);

            new (ptr) typename Property::Type(s);
            //Start the lifetime "properly".
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        for (PropertiesIndexer i = 0; i < MetaInfo::extent - 1; ++i)
          {
            auto * ptr =
              Utility::FriendshipProvider::template get_object_pointer<Property>(d,
                                                                                 Utility::FriendshipProvider::get_multi_array_index(d) * MetaInfo::extent + i);

            new (ptr) typename Property::Type(static_cast<const SourceLike &>(s));
            //Start the lifetime "properly".
          }

        auto * final_ptr = Utility::FriendshipProvider::template get_object_pointer<Property>(
          d,
          (Utility::FriendshipProvider::get_multi_array_index(d) + 1) * MetaInfo::extent - 1);

        new (final_ptr) typename Property::Type(std::move(s));
        //Start the lifetime "properly".
      }
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class DestLayout, class SourceLayout, class DestMetaInfo, class SourceMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::Object<DestLayout, Utility::TypeHolder<Property>, DestMetaInfo>,
                          Collections::Object<SourceLayout, Utility::TypeHolder<Property>, SourceMetaInfo>,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::PerItemObject &&
                                           DestMetaInfo::extent == SourceMetaInfo::extent && !InterfaceDescription::is_global(Property {})>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(10);

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Obj>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) getter(Obj && o, const PropertiesIndexer idx)
      {
        return Utility::FriendshipProvider::template get_object<Property>(
          std::forward<Obj>(o),
          Utility::FriendshipProvider::get_multi_array_index(std::forward<Obj>(o)) * DestMetaInfo::extent + idx);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_object() && SourceLayout::interface_properties().can_copy_out_of_object();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return DestLayout::interface_properties().can_move_into_object() && SourceLayout::interface_properties().can_move_out_of_object();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        for (PropertiesIndexer i = 0; i < DestMetaInfo::extent; ++i)
          {
            auto * ptr = Utility::FriendshipProvider::template get_object_pointer<Property>(
              d,
              Utility::FriendshipProvider::get_multi_array_index(d) * DestMetaInfo::extent + i);

            new (ptr) typename Property::Type(getter(s, i));
            //Start the lifetime "properly".
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        for (PropertiesIndexer i = 0; i < DestMetaInfo::extent; ++i)
          {
            auto * ptr = Utility::FriendshipProvider::template get_object_pointer<Property>(
              d,
              Utility::FriendshipProvider::get_multi_array_index(d) * DestMetaInfo::extent + i);

            new (ptr) typename Property::Type(std::move(getter(std::move(s), i)));
            //Start the lifetime "properly".
          }
      }
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    //Jagged vector
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class SourceLayout, class DestLayout, class SourceMetaInfo, class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::Object<DestLayout, Utility::TypeHolder<Property>, DestMetaInfo>,
                          Collections::Object<SourceLayout, Utility::TypeHolder<Property>, SourceMetaInfo>,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::JaggedVector &&
                                           DestMetaInfo::extent == SourceMetaInfo::extent && !InterfaceDescription::is_global(Property {})>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(11);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_object() && SourceLayout::interface_properties().can_copy_out_of_object();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return false;
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop, class DestColl, class SourceColl>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_helper_helper(DestColl &                           d,
                                                                                                     const SourceColl &                   s,
                                                                                                     const typename DestColl::size_type   size,
                                                                                                     const typename DestColl::size_type   d_start,
                                                                                                     const typename SourceColl::size_type s_start)
      {
        if constexpr (!InterfaceDescription::is_global(Prop {}))
          {
            auto d_c = d.template get_collection<Prop>();
            auto s_c = s.template get_collection<Prop>();

            using TranSpec = Transfer<std::decay_t<decltype(d_c)>, std::decay_t<decltype(s_c)>>;

            TranSpec::copy_some(d_c, s_c, size, d_start, s_start);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Props, class DestColl, class SourceColl>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_helper(Utility::TypeHolder<Props...>,
                                                                                              DestColl &                           d,
                                                                                              const SourceColl &                   s,
                                                                                              const typename DestColl::size_type   size,
                                                                                              const typename DestColl::size_type   d_start,
                                                                                              const typename SourceColl::size_type s_start)
      {
        (copy_helper_helper<Props>(d, s, size, d_start, s_start), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        auto dest = Utility::FriendshipProvider::underlying_collection(d);
        auto src  = Utility::FriendshipProvider::underlying_collection(s);

        using DestColl   = std::decay_t<decltype(dest)>;
        using SourceColl = std::decay_t<decltype(src)>;

        if constexpr (DestLayout::interface_properties().can_resize_jagged_vector_object())
          {
            d.resize(static_cast<typename DestColl::size_type>(s.size()));

            copy_helper(typename Property::Properties {},
                        dest,
                        src,
                        static_cast<typename DestColl::size_type>(s.size()),
                        static_cast<typename DestColl::size_type>(Utility::FriendshipProvider::get_starting_index(d)),
                        static_cast<typename SourceColl::size_type>(Utility::FriendshipProvider::get_starting_index(s)));
          }
        else
          {
            const typename DestColl::size_type dest_size   = static_cast<typename DestColl::size_type>(d.size());
            const typename DestColl::size_type source_size = static_cast<typename DestColl::size_type>(s.size());

            copy_helper(typename Property::Properties {},
                        dest,
                        src,
                        (dest_size > source_size ? source_size : dest_size),
                        static_cast<typename DestColl::size_type>(Utility::FriendshipProvider::get_starting_index(d)),
                        static_cast<typename SourceColl::size_type>(Utility::FriendshipProvider::get_starting_index(s)));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s) = delete;
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property,
                                                          class SourceLayout,
                                                          class DestLayout,
                                                          class SourceMetaInfo,
                                                          class DestMetaInfo,
                                                          class... OtherProps>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::Object<DestLayout, Utility::TypeHolder<Property>, DestMetaInfo>,
                          Collections::Collection<SourceLayout, Utility::TypeHolder<OtherProps...>, SourceMetaInfo>,
                          TransferPriority::DefaultSecondary,
                          std::enable_if_t<impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::JaggedVector &&
                                           DestMetaInfo::extent == SourceMetaInfo::extent && !InterfaceDescription::is_global(Property {})>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(12);

     private:

      using SourceTypeHolder     = Utility::TypeHolder<OtherProps...>;
      using DestActualTypeHolder = typename Property::Properties;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_object() && SourceLayout::interface_properties().can_copy_out_of_collection() &&
               impl::properties_equivalent(DestActualTypeHolder {}, SourceTypeHolder {});
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return false;
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop, class DestColl, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_helper_helper(DestColl & d, const SourceLike & s, const typename DestColl::size_type size, const typename DestColl::size_type starting_index)
      {
        if constexpr (!InterfaceDescription::is_global(Prop {}))
          {
            auto d_c = d.template get_collection<Prop>();
            auto s_c = s.template get_collection<Prop>();

            using TranSpec = Transfer<std::decay_t<decltype(d_c)>, std::decay_t<decltype(s_c)>>;

            TranSpec::copy_some(d_c, s_c, size, starting_index, 0);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Props, class DestColl, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_helper(Utility::TypeHolder<Props...>,
                                                                                              DestColl &                         d,
                                                                                              const SourceLike &                 s,
                                                                                              const typename DestColl::size_type size,
                                                                                              const typename DestColl::size_type starting_index)
      {
        (copy_helper_helper<Props>(d, s, size, starting_index), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        auto dest = Utility::FriendshipProvider::underlying_collection(d);

        using DestColl = std::decay_t<decltype(dest)>;

        if constexpr (DestLayout::interface_properties().can_resize_jagged_vector_object())
          {
            d.resize(static_cast<typename DestColl::size_type>(s.size()));

            copy_helper(DestActualTypeHolder {},
                        dest,
                        s,
                        static_cast<typename DestColl::size_type>(s.size()),
                        static_cast<typename DestColl::size_type>(Utility::FriendshipProvider::get_starting_index(d)));
          }
        else
          {
            const typename DestColl::size_type dest_size   = d.size();
            const typename DestColl::size_type source_size = static_cast<typename DestColl::size_type>(s.size());

            copy_helper(DestActualTypeHolder {},
                        dest,
                        s,
                        (dest_size > source_size ? source_size : dest_size),
                        static_cast<typename DestColl::size_type>(Utility::FriendshipProvider::get_starting_index(d)));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s) = delete;
    };

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool has_begin_and_end(Args &&...)
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T,
                                                               class A = decltype(std::declval<const T &>().begin()),
                                                               class B = decltype(std::declval<const T &>().end())>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool has_begin_and_end()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class Layout, class MetaInfo, class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool jagged_vector_assignable_test()
      {
        if constexpr (impl::is_collection_holder_v<T> || impl::is_collection_v<T> || impl::is_object_v<T>)
          {
            return false;
          }
        else if constexpr (!has_begin_and_end<T>())
          {
            return false;
          }
        else if constexpr (InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::JaggedVector)
          {
            return std::is_assignable_v<typename Layout::template JaggedVectorCollectionType<Property, MetaInfo>::ReadProxy &,
                                        decltype(*(std::declval<const T &>().begin()))>;
          }
        else
          {
            return false;
          }
      }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Property, class Layout, class MetaInfo, class T>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification<
      Collections::Object<Layout, Utility::TypeHolder<Property>, MetaInfo>,
      T,
      TransferPriority::DefaultTertiary,
      std::enable_if_t<impl::classify_prop_v<Property> == InterfaceDescription::SupportedTypes::JaggedVector &&
                       impl::jagged_vector_assignable_test<Property, Layout, MetaInfo, T>() && !InterfaceDescription::is_global(Property {})>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(13);

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return Layout::interface_properties().can_copy_into_object();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        d.assign(s.begin(), s.end());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s) = delete;
    };

    //-----------------------------------------------------------------------------------------------------------------------------------------

    namespace impl
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestProps, class SourceProps>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool object_transfer_check()
      {
        using DestPropInfo   = InterfaceDescription::InterfaceInformation<DestProps>;
        using SourcePropInfo = InterfaceDescription::InterfaceInformation<SourceProps>;

        using DestSimpleProps = typename DestPropInfo::template flattened_per_item<true, false, false, false, false>;
        using DestGlobalProps = typename DestPropInfo::template flattened_per_item<false, false, false, false, true>;
        using DestJaggedProps = typename DestPropInfo::template flattened_jagged_vector<true, false, false, false>;

        using SourceSimpleProps = typename SourcePropInfo::template flattened_per_item<true, false, false, false, false>;
        using SourceGlobalProps = typename SourcePropInfo::template flattened_per_item<false, false, false, false, true>;
        using SourceJaggedProps = typename SourcePropInfo::template flattened_jagged_vector<true, false, false, false>;

        return properties_equivalent(DestSimpleProps {}, SourceSimpleProps {}) && properties_equivalent(DestGlobalProps {}, SourceGlobalProps {}) &&
               properties_equivalent(DestJaggedProps {}, SourceJaggedProps {});
      }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class SourceProps,
                                                          class DestProps,
                                                          class SourceLayout,
                                                          class DestLayout,
                                                          class SourceMetaInfo,
                                                          class DestMetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification<Collections::Object<DestLayout, DestProps, DestMetaInfo>,
                                                                                    Collections::Object<SourceLayout, SourceProps, SourceMetaInfo>,
                                                                                    TransferPriority::DefaultPrimary,
                                                                                    std::enable_if_t<DestMetaInfo::extent == SourceMetaInfo::extent>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(19);

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_object() && SourceLayout::interface_properties().can_copy_out_of_object() &&
               impl::object_transfer_check<DestProps, SourceProps>();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return DestLayout::interface_properties().can_move_into_object() && SourceLayout::interface_properties().can_move_out_of_object() &&
               impl::object_transfer_check<DestProps, SourceProps>();
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SimpleCopy
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop, class DestLike, class SourceLike>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        execute(Prop, const PropertiesIndexer d_index, const PropertiesIndexer s_index, DestLike & d, const SourceLike & s)
        {
          Utility::FriendshipProvider::template get_object<Prop>(d, d_index) = Utility::FriendshipProvider::template get_object<Prop>(s, s_index);
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SimpleMove
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop, class DestLike, class SourceLike>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        execute(Prop, const PropertiesIndexer d_index, const PropertiesIndexer s_index, DestLike & d, SourceLike && s)
        {
          Utility::FriendshipProvider::template get_object<Prop>(d, d_index) =
            std::move(Utility::FriendshipProvider::template get_object<Prop>(std::move(s), s_index));
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES GlobalCopy
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop, class DestLike, class SourceLike>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        execute(Prop, const PropertiesIndexer d_index, const PropertiesIndexer s_index, DestLike & d, const SourceLike & s)
        {
          if constexpr (Collections::impl::object_is_write_proxy(SourceLayout {}) && Collections::impl::object_is_write_proxy(DestLayout {}))
            {
              Prop::write_to_write(d_index, s_index, d, s);
            }
          else if constexpr (Collections::impl::object_is_write_proxy(SourceLayout {}) && !Collections::impl::object_is_write_proxy(DestLayout {}))
            {
              Prop::write_to_read(d_index, s_index, d, s);
            }
          else if constexpr (!Collections::impl::object_is_write_proxy(SourceLayout {}) && Collections::impl::object_is_write_proxy(DestLayout {}))
            {
              Prop::read_to_write(d_index, s_index, d, s);
            }
          else if constexpr (!Collections::impl::object_is_write_proxy(SourceLayout {}) && !Collections::impl::object_is_write_proxy(DestLayout {}))
            {
              Prop::read_to_read(d_index, s_index, d, s);
            }
          else
            {
              static_assert(Utility::always_false<DestLike>, "LOGIC ERROR.");
            }
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES JaggedCopy
      {
       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Prop, class DestLike, class SourceLike>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        execute(Prop, const PropertiesIndexer d_index, const PropertiesIndexer s_index, DestLike & d, const SourceLike & s)
        {
          auto && d_obj = d.template get<Prop, true, typename DestMetaInfo::template override_extent<1>>();
          auto && s_obj = s.template get<Prop, true, typename SourceMetaInfo::template override_extent<1>>();

          Utility::FriendshipProvider::update_multi_array_index(d_obj, d_index);
          Utility::FriendshipProvider::update_multi_array_index(s_obj, s_index);

          const typename SourceLike::size_type s_size     = s_obj.size();
          typename DestLike::size_type         d_new_size = static_cast<typename DestLike::size_type>(s_size);

          if constexpr (DestLayout::interface_properties().can_resize_jagged_vector_object())
            {
              d_obj.resize(d_new_size);
            }
          else
            {
              const typename DestLike::size_type d_current_size = d_obj.size();
              if (d_current_size < d_new_size)
                {
                  d_new_size = d_current_size;
                }
            }

          auto && d_coll = Utility::FriendshipProvider::underlying_collection(d_obj);
          auto && s_coll = Utility::FriendshipProvider::underlying_collection(s_obj);

          using DestColl   = std::decay_t<decltype(d_coll)>;
          using SourceColl = std::decay_t<decltype(s_coll)>;

          using TranSpec = Transfer<DestColl, SourceColl>;

          TranSpec::copy_some(d_coll,
                              s_coll,
                              d_new_size,
                              static_cast<typename DestColl::size_type>(Utility::FriendshipProvider::get_starting_index(d_obj)),
                              static_cast<typename SourceColl::size_type>(Utility::FriendshipProvider::get_starting_index(s_obj)));
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class F, class PropEx, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void execute_helper_helper(F &&, PropEx, DestLike & d, SourceLike && s)
      {
        const PropertiesIndexer d_base_index = Utility::FriendshipProvider::get_multi_array_index(d) * PropEx::extent * DestMetaInfo::extent;
        const PropertiesIndexer s_base_index = Utility::FriendshipProvider::get_multi_array_index(s) * PropEx::extent * DestMetaInfo::extent;

        for (PropertiesIndexer i = 0; i < PropEx::extent * DestMetaInfo::extent; ++i)
          {
            const PropertiesIndexer d_index = d_base_index + i;
            const PropertiesIndexer s_index = s_base_index + i;

            F::execute(typename PropEx::Property {}, d_index, s_index, d, std::forward<SourceLike>(s));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class F, class... PropExs, class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      execute_helper(F && f, Utility::TypeHolder<PropExs...>, DestLike & d, SourceLike && s)
      {
        (execute_helper_helper(std::forward<F>(f), PropExs {}, d, std::forward<SourceLike>(s)), ...);
      }

      using DestPropInfo = InterfaceDescription::InterfaceInformation<DestProps>;

      using SimpleProps = typename DestPropInfo::template flattened_per_item<true, false, false, false, false>;
      using GlobalProps = typename DestPropInfo::template flattened_per_item<false, false, false, false, true>;
      using JaggedProps = typename DestPropInfo::template flattened_jagged_vector<true, false, false, false>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        execute_helper(SimpleCopy {}, SimpleProps {}, d, s);
        execute_helper(GlobalCopy {}, GlobalProps {}, d, s);
        execute_helper(JaggedCopy {}, JaggedProps {}, d, s);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        execute_helper(SimpleMove {}, SimpleProps {}, d, std::move(s));
        execute_helper(GlobalCopy {}, GlobalProps {}, d, static_cast<const SourceLike &>(s));
        execute_helper(JaggedCopy {}, JaggedProps {}, d, static_cast<const SourceLike &>(s));
      }
    };
  }
}

#endif
