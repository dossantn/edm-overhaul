//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_LAYOUTTYPES_VECTORLIKE_H
#define MARIONETTE_LAYOUTTYPES_VECTORLIKE_H

#include <utility>
#include <type_traits>

#include "../MarionetteBase.h"
#include "../ExtraInformation.h"
#include "../MarionetteTransfersHelpers.h"

#include "LayoutHelpers.h"

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

namespace Marionette
{
  namespace LayoutTypes
  {
    namespace impl_VL
    {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR

      template <bool b>
      struct unintended_behaviour_warner
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_resize()
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_reserve()
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_clear()
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_shrink()
        {
        }
      };

      template <>
      struct unintended_behaviour_warner<true>
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        [[deprecated("This is not really a deprecation warning. Trying to call 'resize' on a VectorLike collection "
                     "when the underlying VectorLike does not support it. It will be a no-op. "
                     "Provide an additional 'false' template parameter when calling it to suppress this warning. "
                     "Define 'MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR' to 0 to suppress all warnings about this.")]]
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_resize()
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        [[deprecated("This is not really a deprecation warning. Trying to call 'reserve' on a VectorLike collection "
                     "when the underlying VectorLike does not support it. It will be a no-op. "
                     "Provide an additional 'false' template parameter when calling it to suppress this warning. "
                     "Define 'MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR' to 0 to suppress all warnings about this.")]]
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_reserve()
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        [[deprecated("This is not really a deprecation warning. Trying to call 'clear' on a VectorLike collection "
                     "when the underlying VectorLike does not support it. It will be a no-op. "
                     "Provide an additional 'false' template parameter when calling it to suppress this warning. "
                     "Define 'MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR' to 0 to suppress all warnings about this.")]]
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_clear()
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        [[deprecated("This is not really a deprecation warning. Trying to call 'resize' on a VectorLike collection "
                     "when the underlying VectorLike does not support it. It will be a no-op. "
                     "Provide an additional 'false' template parameter when calling it to suppress this warning. "
                     "Define 'MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR' to 0 to suppress all warnings about this.")]]
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_shrink()
        {
        }
      };

#endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <template <class> class VectorType, class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES dynamic_vector_holder_single
      {
        static_assert(Utility::always_false<T>, "Please initialize the Collection properly with a TypeHolder of properties!");
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <template <class> class VectorType,
                                                            class Final,
                                                            bool warn_on_functions,
                                                            class T,
                                                            class size_type = typename VectorType<PropertiesIndexer>::size_type>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES dynamic_vector_holder_aggregator
      {
        static_assert(Utility::always_false<T>, "Please initialize the Collection properly with a TypeHolder of properties!");
      };

#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wunsafe-buffer-usage"
#endif
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <template <class> class VectorType, class T, class... PropExs>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES dynamic_vector_holder_single<VectorType, Utility::TypeHolder<T, PropExs...>>
      {
       private:

        static constexpr PropertiesIndexer s_total_size = (PropExs::extent + ...);

       protected:

        VectorType<T> m_vecs[s_total_size];

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto &
        get_array(const PropertiesIndexer idx) const
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(idx >= 0 && idx < s_total_size);
#endif

          return m_vecs[idx];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto &
        get_array(const PropertiesIndexer idx)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(idx >= 0 && idx < s_total_size);
#endif

          return m_vecs[idx];
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <template <class> class VectorType,
                                                            class Final,
                                                            bool warn_on_functions,
                                                            class... Ts,
                                                            class size_type>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
      dynamic_vector_holder_aggregator<VectorType, Final, warn_on_functions, Utility::TypeHolder<Ts...>, size_type> :
        public dynamic_vector_holder_single<VectorType, Ts>...
      {
       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class T, class... PropExs>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer get_base_offset(Utility::TypeHolder<T, PropExs...>)
        {
          PropertiesIndexer ret = 0;

          bool found = false;

          (((found = found || std::is_same_v<Property, typename PropExs::Property>), ret += PropExs::extent * (!found)), ...);

          return ret * found;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class T, class... PropExs>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer get_property_extent(Utility::TypeHolder<T, PropExs...>)
        {
          return ((PropExs::extent * std::is_same_v<Property, typename PropExs::Property>) +...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto find_correct_type_holder()
        {
          return (Utility::TypeHolder<Utility::InvalidType> {} | ... |
                  std::conditional_t<Ts::template contains<typename Property::Type>(), Ts, Utility::TypeHolder<Utility::InvalidType>> {});
        }

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(const PropertiesIndexer mult_arr_idx) const
        {
          constexpr auto this_type_holder = find_correct_type_holder<Property>();

          static_assert(this_type_holder.number() > 1, "'get_array' must be called with a valid property!");
          //In case the collection does not contain the property,
          //find_correct_type_holder will return an empty type holder.
          //This should never happen due to the way this is handled
          //by the Collection class itself, which checks for a valid property.

          constexpr PropertiesIndexer base_index = get_base_offset<Property>(this_type_holder);

          [[maybe_unused]] constexpr PropertiesIndexer maximum_extent = get_property_extent<Property>(this_type_holder);

#if MARIONETTE_USE_ASSERTIONS
          assert(mult_arr_idx >= 0 && mult_arr_idx < maximum_extent);
#endif

          return dynamic_vector_holder_single<VectorType, std::decay_t<decltype(this_type_holder)>>::get_array(base_index + mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(const PropertiesIndexer mult_arr_idx)
        {
          constexpr auto this_type_holder = find_correct_type_holder<Property>();

          static_assert(this_type_holder.number() > 1, "'get_array' must be called with a valid property!");
          //In case the collection does not contain the property,
          //find_correct_type_holder will return an empty type holder.
          //This should never happen due to the way this is handled
          //by the Collection class itself, which checks for a valid property.

          constexpr PropertiesIndexer base_index = get_base_offset<Property>(this_type_holder);

          [[maybe_unused]] constexpr PropertiesIndexer maximum_extent = get_property_extent<Property>(this_type_holder);

#if MARIONETTE_USE_ASSERTIONS
          assert(mult_arr_idx >= 0 && mult_arr_idx < maximum_extent);
#endif

          return dynamic_vector_holder_single<VectorType, std::decay_t<decltype(this_type_holder)>>::get_array(base_index + mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(const PropertiesIndexer mult_arr_idx, const size_type i) const
        {
          const auto & arr = this->template get_array<Property>(mult_arr_idx);

          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_size())
            {
#if MARIONETTE_USE_ASSERTIONS
              assert(i >= 0 && static_cast<typename VectorType<typename Property::Type>::size_type>(i) < arr.size());
#endif
            }

          return arr[static_cast<typename VectorType<typename Property::Type>::size_type>(i)];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(const PropertiesIndexer mult_arr_idx, const size_type i)
        {
          auto & arr = this->template get_array<Property>(mult_arr_idx);

          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_size())
            {
#if MARIONETTE_USE_ASSERTIONS
              assert(i >= 0 && static_cast<typename VectorType<typename Property::Type>::size_type>(i) < arr.size());
#endif
            }

          return arr[static_cast<typename VectorType<typename Property::Type>::size_type>(i)];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const typename Property::Type * get_array_pointer(const PropertiesIndexer mult_arr_idx,
                                                                                                                          const size_type         i = 0) const
        {
          const auto & arr = this->template get_array<Property>(mult_arr_idx);

          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_size())
            {
#if MARIONETTE_USE_ASSERTIONS
              assert(i >= 0 && static_cast<typename VectorType<typename Property::Type>::size_type>(i) <= arr.size());
#endif
            }

          return ExtraInformation::VectorLike<VectorType<typename Property::Type>>::get_start_of_data(arr) + i;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr typename Property::Type * get_array_pointer(const PropertiesIndexer mult_arr_idx,
                                                                                                                    const size_type         i = 0)
        {
          auto & arr = this->template get_array<Property>(mult_arr_idx);

          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_size())
            {
#if MARIONETTE_USE_ASSERTIONS
              assert(i >= 0 && static_cast<typename VectorType<typename Property::Type>::size_type>(i) <= arr.size());
#endif
            }

          return ExtraInformation::VectorLike<VectorType<typename Property::Type>>::get_start_of_data(arr) + i;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class DestContext, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_copy_from(const PropertiesIndexer                   mult_arr_idx,
                                                                                             const DestContext &                       dest_context,
                                                                                             const typename DestContext::ContextInfo & context_info,
                                                                                             typename Property::Type *                 ptr,
                                                                                             const size_type                           num,
                                                                                             const size_type                           offset,
                                                                                             Args &&... args) const
        {
          const Final * dhis = static_cast<const Final *>(this);

          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_size())
            {
#if MARIONETTE_USE_ASSERTIONS
              const auto & arr = this->template get_array<Property>(mult_arr_idx);
              assert(num >= 0 && offset >= 0 && static_cast<typename VectorType<typename Property::Type>::size_type>(num + offset) <= arr.size());
#endif
            }

          MemoryContexts::memcopy_with_context(dest_context,
                                               dhis->memory_context(),
                                               context_info,
                                               dhis->memory_context_info(),
                                               ptr,
                                               this->template get_array_pointer<Property>(mult_arr_idx, offset),
                                               num,
                                               std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class SourceContext, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_copy_to(const PropertiesIndexer                     mult_arr_idx,
                                                                                           const SourceContext &                       source_context,
                                                                                           const typename SourceContext::ContextInfo & context_info,
                                                                                           const typename Property::Type *             ptr,
                                                                                           const size_type                             num,
                                                                                           const size_type                             offset,
                                                                                           Args &&... args)
        {
          Final * dhis = static_cast<Final *>(this);

          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_size())
            {
#if MARIONETTE_USE_ASSERTIONS
              const auto & arr = this->template get_array<Property>(mult_arr_idx);
              assert(num >= 0 && offset >= 0 && static_cast<typename VectorType<typename Property::Type>::size_type>(num + offset) <= arr.size());
#endif
            }

          MemoryContexts::memcopy_with_context(dhis->memory_context(),
                                               source_context,
                                               dhis->memory_context_info(),
                                               context_info,
                                               this->template get_array_pointer<Property>(mult_arr_idx, offset),
                                               ptr,
                                               num,
                                               std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool warn = true>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_resize(const PropertiesIndexer mult_arr_idx, const size_type new_size)
        {
          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_resize())
            {
              this->template get_array<Property>(mult_arr_idx).resize(static_cast<typename VectorType<typename Property::Type>::size_type>(new_size));
            }
          else
            {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
              unintended_behaviour_warner<warn && warn_on_functions>::warn_resize();
#endif
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool warn = true>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_reserve(const PropertiesIndexer mult_arr_idx, const size_type new_size)
        {
          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_reserve())
            {
              this->template get_array<Property>(mult_arr_idx).reserve(static_cast<typename VectorType<typename Property::Type>::size_type>(new_size));
            }
          else
            {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
              unintended_behaviour_warner<warn && warn_on_functions>::warn_reserve();
#endif
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool warn = true>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_clear(const PropertiesIndexer mult_arr_idx)
        {
          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_clear())
            {
              this->template get_array<Property>(mult_arr_idx).clear();
            }
          else
            {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
              unintended_behaviour_warner<warn && warn_on_functions>::warn_clear();
#endif
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool warn = true>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_shrink_to_fit(const PropertiesIndexer mult_arr_idx)
        {
          if constexpr (ExtraInformation::VectorLike<VectorType<typename Property::Type>>::has_shrink_to_fit())
            {
              this->template get_array<Property>(mult_arr_idx).shrink_to_fit();
            }
          else
            {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
              unintended_behaviour_warner<warn && warn_on_functions>::warn_shrink();
#endif
            }
        }
      };

#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic pop
#endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... PropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_maximum_extent_from_properties(Utility::TypeHolder<PropExs...>)
      {
        PropertiesIndexer maximizer = 0;

        ((maximizer = (maximizer < PropExs::extent ? PropExs::extent : maximizer)), ...);

        return maximizer;
      }

      ///@brief Returns the first type that corresponds to the maximum extent from among the list.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... PropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_maximum_property_and_extent(Utility::TypeHolder<PropExs...> th)
      {
        constexpr PropertiesIndexer maximizer = get_maximum_extent_from_properties(th);

        constexpr auto pre_ret =
          (Utility::TypeHolder<Utility::InvalidType> {} & ... & std::conditional_t<PropExs::extent == maximizer, PropExs, Utility::InvalidType> {});

        return Utility::get_one_type_or_invalid(pre_ret);
      }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <template <class...> class VectorLike,
                                                          template <class...>
                                                          class VectorLikeForSizes,
                                                          class SizeType,
                                                          class DifferenceType,
                                                          class... ExtraArgs>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES VectorLikePerItem
    {
      using size_type       = SizeType;
      using difference_type = DifferenceType;

     private:

      template <class T>
      using VectorType = VectorLike<T, ExtraArgs...>;

      template <class T>
      using VectorTypeForSizes = VectorLikeForSizes<T, ExtraArgs...>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return ExtraInformation::VectorLike<VectorType<Utility::InvalidType>>::memory_context();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
      interface_properties()
      {
        return InterfaceProperties {memory_context().access_properties(), ResizeProperties::Full, MutabilityProperties::Full};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_get_array_pointers()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size_tag_array()
      {
        return true;
      }

      template <class F, class Layout>
      using ExtraFunctions = typename ExtraInformation::VectorLike<VectorType<Utility::InvalidType>>::template ExtraFunctions<F, Layout>;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES layout_holder :
        public impl_VL::dynamic_vector_holder_aggregator<VectorType,
                                                         layout_holder<T>,
                                                         true,
                                                         typename InterfaceDescription::InterfaceInformation<T>::template flattened_properties_by_type<>,
                                                         size_type>,
        public std::decay_t<decltype(VectorLikePerItem::memory_context())>::ContextInfo
      {
        static_assert(ExtraInformation::VectorLike<VectorType<Utility::InvalidType>>::is_valid(), "Must be initialized with a proper vector-like template!");

        friend struct Utility::FriendshipProvider;

       private:

        using SizeTagList = typename InterfaceDescription::InterfaceInformation<T>::template necessary_size_tags<>;

        using SzTgHelper = Helpers::SizeTagHolder<SizeTagList>;

        using Base = impl_VL::dynamic_vector_holder_aggregator<VectorType,
                                                               layout_holder<T>,
                                                               true,
                                                               typename InterfaceDescription::InterfaceInformation<T>::template flattened_properties_by_type<>,
                                                               size_type>;

        using MemContextInfo = typename std::decay_t<decltype(VectorLikePerItem::memory_context())>::ContextInfo;

        friend Base;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
        {
          return VectorLikePerItem::memory_context();
        }

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const MemContextInfo &
        memory_context_info() const
        {
          return static_cast<const MemContextInfo &>(*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr MemContextInfo & memory_context_info()
        {
          return static_cast<MemContextInfo &>(*this);
        }

        VectorTypeForSizes<size_type> m_size_tag_arr;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_resize(const PropertiesIndexer, const size_type)
        {
          //Nothing to be done at the general level,
          //the array resizing works for this.
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_reserve(const PropertiesIndexer, const size_type)
        {
          //Nothing to be done at the general level,
          //the array reserving works for this.
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_clear(const PropertiesIndexer)
        {
          //Nothing to be done at the general level,
          //the array clearing works for this.
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_shrink_to_fit(const PropertiesIndexer)
        {
          //Nothing to be done at the general level,
          //the array shrinking works for this.
        }

#if defined(__clang__)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wunsafe-buffer-usage"
#endif
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const size_type * get_tagged_size_pointer(const PropertiesIndexer mult_arr_idx) const
        {
          return ExtraInformation::VectorLike<VectorTypeForSizes<size_type>>::get_start_of_data(m_size_tag_arr) +
                 SzTgHelper::template get_tag_index<Tag>(mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type * get_tagged_size_pointer(const PropertiesIndexer mult_arr_idx)
        {
          return ExtraInformation::VectorLike<VectorTypeForSizes<size_type>>::get_start_of_data(m_size_tag_arr) +
                 SzTgHelper::template get_tag_index<Tag>(mult_arr_idx);
        }
#if defined(__clang__)
  #pragma clang diagnostic pop
#endif

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type get_tagged_size(const PropertiesIndexer mult_arr_idx) const
        {
          size_type ret = 0;

          MemoryContexts::memcopy_with_context(MemoryContexts::Current {},
                                               memory_context(),
                                               MemoryContexts::Current::ContextInfo {},
                                               this->memory_context_info(),
                                               &ret,
                                               this->template get_tagged_size_pointer<Tag>(mult_arr_idx),
                                               1);

          return ret;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void set_tagged_size(const PropertiesIndexer mult_arr_idx, const size_type new_size)
        {
          MemoryContexts::memcopy_with_context(memory_context(),
                                               MemoryContexts::Current {},
                                               this->memory_context_info(),
                                               MemoryContexts::Current::ContextInfo {},
                                               this->template get_tagged_size_pointer<Tag>(mult_arr_idx),
                                               &new_size,
                                               1);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_capacity(const PropertiesIndexer mult_arr_idx) const
        {
          using PropExs = std::decay_t<decltype(SzTgHelper::template get_properties_and_extents_matching_tag<Tag>())>;

          if constexpr (PropExs::number() == 0 || !ExtraInformation::VectorLike<VectorType<Utility::InvalidType>>::has_capacity())
            {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
              impl_VL::unintended_behaviour_warner<ExtraInformation::VectorLike<VectorType<Utility::InvalidType>>::has_capacity()>::warn_tagged_capacity();
                //This would be the very unlikely case of properties that track size
                //inexplicably all being NoObject...
                //Returning the size is as good an answer as any,
                //but we can/should warn the user nonetheless.
#endif
              return get_tagged_size<Tag>(mult_arr_idx);
            }
          else
            {
              using MaxPropEx = decltype(impl_VL::get_maximum_property_and_extent(PropExs {}));
              //We get the type with the maximum extent so that
              //using any mult_arr_idx is well-defined:
              //otherwise we'd need to account for the case
              //where a global property is top-level
              //and we're trying to access a multi-array property thereof.
              //For the other cases, this still makes sense
              //since the "maximum" corresponds to the extent we want.
              //Plus, this is just the capacity,
              //even if the result is a bit "wrong",
              //it's not that serious...

              return this->template get_array<typename MaxPropEx::Property>(mult_arr_idx).capacity();
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto & get_size_tag_array() const
        {
          return m_size_tag_arr;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & get_size_tag_array()
        {
          return m_size_tag_arr;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const size_type *
        get_size_tag_array_pointer() const
        {
          return ExtraInformation::VectorLike<VectorTypeForSizes<size_type>>::get_start_of_data(m_size_tag_arr);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto)
        get_size_tag_array_pointer()
        {
          return ExtraInformation::VectorLike<VectorTypeForSizes<size_type>>::get_start_of_data(m_size_tag_arr);
        }

       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_for_array(const PropertiesIndexer mult_arr_idx) const
        {
          using MatchingTag = std::decay_t<decltype(SzTgHelper::template get_tag_matching_property<Property>())>;

          return this->template get_tagged_size<MatchingTag>(mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void initialize_global_property(PropEx)
        {
          constexpr auto p            = typename PropEx::Property {};
          constexpr auto initial_size = InterfaceDescription::initial_global_property_size(p);

          if constexpr (InterfaceDescription::is_global(p) && initial_size > 0)
            {
              for (PropertiesIndexer i = 0; i < PropEx::extent; ++i)
                {
                  this->template array_resize<typename PropEx::Property>(i, initial_size);
                  if constexpr (InterfaceDescription::use_initial_global_property_memset(p))
                    {
                      constexpr auto initial_memset = InterfaceDescription::initial_global_property_memset_value(p);

                      memory_context().memset(this->memory_context_info(),
                                              this->template get_array_pointer<typename PropEx::Property>(i),
                                              initial_memset,
                                              initial_size);
                    }
                  if constexpr (track_individual_size(p))
                    {
                      this->template set_tagged_size<Utility::TypeHolder<typename PropEx::Property>>(i, initial_size);
                    }
                }
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... PropExs>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void initialize_global_properties(Utility::TypeHolder<PropExs...>)
        {
          (initialize_global_property(PropExs {}), ...);
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder()
        {
          m_size_tag_arr.resize(SzTgHelper::total_number_of_tags());
          memory_context().memset(this->memory_context_info(),
                                  ExtraInformation::VectorLike<VectorTypeForSizes<size_type>>::get_start_of_data(m_size_tag_arr),
                                  0,
                                  SzTgHelper::total_number_of_tags());

          initialize_global_properties(typename InterfaceDescription::InterfaceInformation<T>::template flattened_per_item<false, true> {});
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
          class... Ts,
          class disabler = std::enable_if_t<Helpers::can_construct_layout_holder<MemContextInfo, Ts...>()>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(Ts &&... ts): MemContextInfo(std::forward<Ts>(ts)...)
        {
          m_size_tag_arr.resize(SzTgHelper::total_number_of_tags());
          memory_context().memset(this->memory_context_info(),
                                  ExtraInformation::VectorLike<VectorTypeForSizes<size_type>>::get_start_of_data(m_size_tag_arr),
                                  0,
                                  SzTgHelper::total_number_of_tags());

          initialize_global_properties(typename InterfaceDescription::InterfaceInformation<T>::template flattened_per_item<false, true> {});
        }
      };
    };
  }

  namespace Transfers
  {

    namespace impl_VLPI
    {

      template <class T1, class T2>
      inline constexpr bool can_copy_or_move_type = std::is_assignable_v<T1 &, const T2 &> || std::is_assignable_v<T1 &, T2 &&>;
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class PropertiesDest,
                                                          class PropertiesSrc,
                                                          class MetaInfoDest,
                                                          class MetaInfoSrc,
                                                          template <class...>
                                                          class VLDest,
                                                          template <class...>
                                                          class VLSDest,
                                                          class SzTpDest,
                                                          class DfTpDest,
                                                          class... ExtraDest,
                                                          template <class...>
                                                          class VLSrc,
                                                          template <class...>
                                                          class VLSSrc,
                                                          class SzTpSrc,
                                                          class DfTpSrc,
                                                          class... ExtraSrc>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification<
      Collections::CollectionHolder<LayoutTypes::VectorLikePerItem<VLDest, VLSDest, SzTpDest, DfTpDest, ExtraDest...>, PropertiesDest, MetaInfoDest>,
      Collections::CollectionHolder<LayoutTypes::VectorLikePerItem<VLSrc, VLSSrc, SzTpSrc, DfTpSrc, ExtraSrc...>, PropertiesSrc, MetaInfoSrc>,
      TransferPriority::UserProvided,
      std::enable_if_t<impl_VLPI::can_copy_or_move_type<VLDest<Utility::InvalidType, ExtraDest...>, VLSrc<Utility::InvalidType, ExtraSrc...>> &&
                       impl_VLPI::can_copy_or_move_type<VLSDest<SzTpDest, ExtraDest...>, VLSSrc<SzTpDest, ExtraSrc...>> &&
                       impl::properties_equivalent(
                         typename InterfaceDescription::InterfaceInformation<PropertiesDest>::template flattened_per_item<true, true, true> {},
                         typename InterfaceDescription::InterfaceInformation<PropertiesSrc>::template flattened_per_item<true, true, true> {}) &&
                       MetaInfoDest::extent == MetaInfoSrc::extent>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(41);

     private:

      using DestPropInfo = InterfaceDescription::InterfaceInformation<PropertiesDest>;
      using SrcPropInfo  = InterfaceDescription::InterfaceInformation<PropertiesSrc>;

      using DestFlatProps = typename DestPropInfo::template flattened_per_item<true, true, true>;
      using SrcFlatProps  = typename SrcPropInfo::template flattened_per_item<true, true, true>;

      using DestLayout = LayoutTypes::VectorLikePerItem<VLDest, VLSDest, SzTpDest, DfTpDest, ExtraDest...>;
      using SrcLayout  = LayoutTypes::VectorLikePerItem<VLSrc, VLSSrc, SzTpSrc, DfTpSrc, ExtraSrc...>;

      using DestType = Collections::CollectionHolder<DestLayout, PropertiesDest, MetaInfoDest>;
      using SrcType  = Collections::CollectionHolder<SrcLayout, PropertiesSrc, MetaInfoSrc>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_collection() && SrcLayout::interface_properties().can_copy_out_of_collection() &&
               std::is_assignable_v<VLDest<Utility::InvalidType, ExtraDest...> &, const VLSrc<Utility::InvalidType, ExtraSrc...> &> &&
               std::is_assignable_v<VLSDest<SzTpDest, ExtraDest...> &, const VLSSrc<SzTpSrc, ExtraSrc...> &>;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return DestLayout::interface_properties().can_move_into_collection() && SrcLayout::interface_properties().can_move_out_of_collection() &&
               std::is_assignable_v<VLDest<Utility::InvalidType, ExtraDest...> &, VLSrc<Utility::InvalidType, ExtraSrc...> &&> &&
               std::is_assignable_v<VLSDest<SzTpDest, ExtraDest...> &, VLSSrc<SzTpSrc, ExtraSrc...> &&>;
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_full_collection()
      {
        return T::interface_properties().resize >= ResizeProperties::Move;
        //This means that we are not handling the underlying object through a view or whatever.
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class PropEx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_helper_helper(DestLike & d, const SourceLike & s, PropEx)
      {
        const PropertiesIndexer d_start = Utility::FriendshipProvider::get_multi_array_index(d) * PropEx::extent * MetaInfoDest::extent;
        const PropertiesIndexer s_start = Utility::FriendshipProvider::get_multi_array_index(s) * PropEx::extent * MetaInfoDest::extent;
        for (PropertiesIndexer i = 0; i < PropEx::extent * MetaInfoDest::extent; ++i)
          {
            Utility::FriendshipProvider::get_array<typename PropEx::Property>(d, d_start + i) =
              Utility::FriendshipProvider::get_array<typename PropEx::Property>(s, s_start + i);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... PropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
      copy_helper(DestLike & d, const SourceLike & s, Utility::TypeHolder<PropExs...>)
      {
        (copy_helper_helper(d, s, PropExs {}), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        if constexpr (is_full_collection<DestLike>() && is_full_collection<SourceLike>())
          {
            Utility::FriendshipProvider::get_size_tag_array(d) = Utility::FriendshipProvider::get_size_tag_array(s);
            
            copy_helper(d, s, DestFlatProps {});
          }
        else
          {
            //Fall back to the standard strategy.
            Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>::copy(d, s);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>::special_copy(d, s, std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                           d,
                                                                                            const SourceLike &                   s,
                                                                                            const typename DestLike::size_type   num,
                                                                                            const typename DestLike::size_type   d_offset = 0,
                                                                                            const typename SourceLike::size_type s_offset = 0)
      {
        //We could also save compilation time here by explicitly iterating through the arrays...
        Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>::copy_some(d, s, num, d_offset, s_offset);
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class PropEx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move_helper_helper(DestLike & d, SourceLike && s, PropEx)
      {
        const PropertiesIndexer d_start = Utility::FriendshipProvider::get_multi_array_index(d) * PropEx::extent * MetaInfoDest::extent;
        const PropertiesIndexer s_start =
          Utility::FriendshipProvider::get_multi_array_index(static_cast<const SourceLike &>(s)) * PropEx::extent * MetaInfoDest::extent;
        for (PropertiesIndexer i = 0; i < PropEx::extent * MetaInfoDest::extent; ++i)
          {
            Utility::FriendshipProvider::get_array<typename PropEx::Property>(d, d_start + i) =
              std::move(Utility::FriendshipProvider::get_array<typename PropEx::Property>(std::move(s), s_start + i));
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... PropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move_helper(DestLike & d, SourceLike && s, Utility::TypeHolder<PropExs...>)
      {
        (move_helper_helper(d, std::move(s), PropExs {}), ...);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        if constexpr (is_full_collection<DestLike>() && is_full_collection<SourceLike>())
          {
            Utility::FriendshipProvider::get_size_tag_array(d) = std::move(Utility::FriendshipProvider::get_size_tag_array(std::move(s)));
              
            move_helper(d, std::move(s), DestFlatProps {});
          }
        else
          {
            using DefaultTransfer = Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>;

            if constexpr (DefaultTransfer::can_move())
              {
                DefaultTransfer::move(d, std::move(s));
              }
            else
              {
                copy(d, static_cast<const SourceLike &>(s));
              }
          }
      }
    };
  }
}
#endif
