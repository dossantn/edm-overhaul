//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_LAYOUTTYPES_STRUCTLIKE_H
#define MARIONETTE_LAYOUTTYPES_STRUCTLIKE_H

#include <utility>
#include <type_traits>
#include <limits>

#include "../MarionetteBase.h"
#include "../ExtraInformation.h"
#include "../MarionetteTransfersHelpers.h"

#include "LayoutHelpers.h"

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

namespace Marionette
{
  namespace LayoutTypes
  {
    namespace impl_S
    {
#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wunsafe-buffer-usage"
#endif
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class size_type, class SizeMap, class PropEx>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES static_struct_single_holder
      {
       private:

        static constexpr PropertiesIndexer extent = PropEx::extent;

        static constexpr MaximumSizeType max_size = SizeMap::size(typename PropEx::Property {});

        static_assert(max_size > 0, "All per-item properties must have a maximum size greater than 0 to be used with this layout type!");

        using Type = typename PropEx::Property::Type;

       protected:

        Type m_arrs[extent][max_size];

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const Type *
        get_array(const PropertiesIndexer mult_arr_idx) const
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(mult_arr_idx >= 0 && mult_arr_idx < extent);
#endif

          return m_arrs[mult_arr_idx];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Type *
        get_array(const PropertiesIndexer mult_arr_idx)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(mult_arr_idx >= 0 && mult_arr_idx < extent);
#endif

          return m_arrs[mult_arr_idx];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const Type &
        get_array(const PropertiesIndexer mult_arr_idx, const size_type i) const
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(i >= 0 && i < static_cast<size_type>(max_size));
          assert(mult_arr_idx >= 0 && mult_arr_idx < extent);
#endif

          return m_arrs[mult_arr_idx][i];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr Type &
        get_array(const PropertiesIndexer mult_arr_idx, const size_type i)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(i >= 0 && i < static_cast<size_type>(max_size));
          assert(mult_arr_idx >= 0 && mult_arr_idx < extent);
#endif

          return m_arrs[mult_arr_idx][i];
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr const Type *
        array_pointer_getter(const static_struct_single_holder * ptr, const PropertiesIndexer mult_arr_idx, const size_type i = 0)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(i >= 0 && i < static_cast<size_type>(max_size));
          assert(mult_arr_idx >= 0 && mult_arr_idx < extent);
#endif
          return &(ptr->m_arrs[mult_arr_idx][i]);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr Type *
        array_pointer_getter(static_struct_single_holder * ptr, const PropertiesIndexer mult_arr_idx, const size_type i = 0)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(i >= 0 && i < static_cast<size_type>(max_size));
          assert(mult_arr_idx >= 0 && mult_arr_idx < extent);
#endif
          return &(ptr->m_arrs[mult_arr_idx][i]);
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class size_type, class SizeMap, class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES static_struct_properties_holder
      {
        static_assert(Utility::always_false<T>, "Please initialize the Collection properly with a TypeHolder of properties!");
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class size_type, class SizeMap, class... PropExs>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES static_struct_properties_holder<size_type, SizeMap, Utility::TypeHolder<PropExs...>> :
        public static_struct_single_holder<size_type, SizeMap, PropExs>...
      {
       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto find_correct_property_extent()
        {
          return Utility::get_one_type_or_invalid((Utility::TypeHolder<Utility::InvalidType> {} & ... &
                                                   std::conditional_t<std::is_same_v<typename PropExs::Property, Property>,
                                                                      Utility::TypeHolder<PropExs>,
                                                                      Utility::TypeHolder<Utility::InvalidType>> {}));
        }

        template <class Property>
        using DesiredPropEx = std::decay_t<decltype(find_correct_property_extent<Property>())>;

        template <class Property>
        using DesiredType = static_struct_single_holder<size_type, SizeMap, DesiredPropEx<Property>>;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(Args &&... args) const
        {
          return static_cast<const DesiredType<Property> *>(this)->get_array(std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(Args &&... args)
        {
          return static_cast<DesiredType<Property> *>(this)->get_array(std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_pointer_getter(const static_struct_properties_holder * ptr,
                                                                                                                   Args &&... args)
        {
          return DesiredType<Property>::array_pointer_getter(static_cast<const DesiredType<Property> *>(ptr), std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_pointer_getter(static_struct_properties_holder * ptr,
                                                                                                                   Args &&... args)
        {
          return DesiredType<Property>::array_pointer_getter(static_cast<DesiredType<Property> *>(ptr), std::forward<Args>(args)...);
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... PropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_jagged_vector_size_prop_exs(Utility::TypeHolder<PropExs...>)
      {
        return Utility::TypeHolder<
          InterfaceDescription::PropertyAndExtent<InterfaceDescription::JaggedVectorSizeProperty<typename PropExs::Property>, PropExs::extent>...> {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class size_type, class SizeMap, class Types, bool has_jagged_vectors>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES static_struct_full_holder_base;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class size_type, class SizeMap, class Types>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES static_struct_full_holder_base<size_type, SizeMap, Types, false>
      {
       private:

        using Info = InterfaceDescription::InterfaceInformation<Types>;

        using SizeTagList = typename Info::template necessary_size_tags<>;

        using SzTgHelper = Helpers::SizeTagHolder<SizeTagList>;

       public:

        size_type m_sizes[SzTgHelper::total_number_of_tags()];
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class size_type, class SizeMap, class Types>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES static_struct_full_holder_base<size_type, SizeMap, Types, true>
      {
       private:

        using Info = InterfaceDescription::InterfaceInformation<Types>;

        using RealSizeMap = typename Info::template per_item_size_map<SizeMap>;

        using SizeTagList = typename Info::template necessary_size_tags<>;

        using SzTgHelper = Helpers::SizeTagHolder<SizeTagList>;

       public:

        using JVSizesType =
          static_struct_properties_holder<size_type,
                                          RealSizeMap,
                                          std::decay_t<decltype(get_jagged_vector_size_prop_exs(typename Info::template flattened_jagged_vector<> {}))>>;

        size_type m_sizes[SzTgHelper::total_number_of_tags()];

        JVSizesType m_jagged_vector_sizes;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class size_type, class SizeMap, class Types>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES static_struct_full_holder
      {
        friend struct Utility::FriendshipProvider;

       private:

        using Info = InterfaceDescription::InterfaceInformation<Types>;

        using RealSizeMap = typename Info::template per_item_size_map<SizeMap>;

        using SizeTagList = typename Info::template necessary_size_tags<>;

        using SzTgHelper = Helpers::SizeTagHolder<SizeTagList>;

        using BaseType = static_struct_full_holder_base<size_type, SizeMap, Types, (Info::template flattened_jagged_vector<>::number() > 0)>;

        using AllType = static_struct_properties_holder<size_type,
                                                        RealSizeMap,
                                                        std::decay_t<decltype(typename Info::template flattened_per_item<true, true, false> {} |
                                                                              typename Info::template flattened_jagged_vector_properties<> {})>>;

       protected:

        BaseType m_base;

        AllType m_all_properties;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(Args &&... args) const
        {
          if constexpr (InterfaceDescription::is_jagged_vector_size_property(Property {}))
            {
              return m_base.m_jagged_vector_sizes.template get_array<Property>(std::forward<Args>(args)...);
            }
          else
            {
              return m_all_properties.template get_array<Property>(std::forward<Args>(args)...);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(Args &&... args)
        {
          if constexpr (InterfaceDescription::is_jagged_vector_size_property(Property {}))
            {
              return m_base.m_jagged_vector_sizes.template get_array<Property>(std::forward<Args>(args)...);
            }
          else
            {
              return m_all_properties.template get_array<Property>(std::forward<Args>(args)...);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_pointer_getter(const static_struct_full_holder * ptr,
                                                                                                                   Args &&... args)
        {
          if constexpr (InterfaceDescription::is_jagged_vector_size_property(Property {}))
            {
              return BaseType::JVSizesType::template array_pointer_getter<Property>(&(ptr->m_base.m_jagged_vector_sizes), std::forward<Args>(args)...);
            }
          else
            {
              return AllType::template array_pointer_getter<Property>(&(ptr->m_all_properties), std::forward<Args>(args)...);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) array_pointer_getter(static_struct_full_holder * ptr,
                                                                                                                   Args &&... args)
        {
          if constexpr (InterfaceDescription::is_jagged_vector_size_property(Property {}))
            {
              return BaseType::JVSizesType::template array_pointer_getter<Property>(&(ptr->m_base.m_jagged_vector_sizes), std::forward<Args>(args)...);
            }
          else
            {
              return AllType::template array_pointer_getter<Property>(&(ptr->m_all_properties), std::forward<Args>(args)...);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr const size_type *
        size_tag_array_getter(const static_struct_full_holder * ptr, const PropertiesIndexer offset = 0)
        {
          return &(ptr->m_base.m_sizes[offset]);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr size_type *
        size_tag_array_getter(static_struct_full_holder * ptr, const PropertiesIndexer offset = 0)
        {
          return &(ptr->m_base.m_sizes[offset]);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto zeroable_arrays_size()
        {
          return sizeof(BaseType);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr const auto *
        zeroable_arrays_getter(const static_struct_full_holder * ptr)
        {
          return &(ptr->m_base);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto *
        zeroable_arrays_getter(static_struct_full_holder * ptr)
        {
          return &(ptr->m_base);
        }
      };

#if defined(__clang__) && defined(MARIONETTE_USE_ASSERTIONS) && MARIONETTE_USE_ASSERTIONS
  #pragma clang diagnostic pop
#endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
      get_property_minimum_size_helper(MaximumSizeType & value, const MaximumSizeType other_value)
      {
        if (value == 0 || other_value < value)
          {
            value = other_value;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeMap, class... PropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr MaximumSizeType get_property_minimum_size(Utility::TypeHolder<PropExs...>)
      {
        MaximumSizeType ret = 0;

        (get_property_minimum_size_helper(ret, SizeMap::size(typename PropExs::Property {})), ...);

        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class LayoutHolder,
                                                            class Types,
                                                            class MemoryContext,
                                                            class size_type,
                                                            class difference_type,
                                                            class SizeMap,
                                                            class ActualHolder>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES proto_layout_holder : public MemoryContext::ContextInfo
      {
        friend struct Utility::FriendshipProvider;

       private:

        using Info = InterfaceDescription::InterfaceInformation<Types>;

        using RealSizeMap = typename Info::template per_item_size_map<SizeMap>;

        using SizeTagList = typename Info::template necessary_size_tags<>;

        using SzTgHelper = Helpers::SizeTagHolder<SizeTagList>;

        using MemContextInfo = typename MemoryContext::ContextInfo;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto * holder_ptr() const
        {
          return static_cast<const LayoutHolder *>(this)->holder_ptr();
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto * holder_ptr()
        {
          return static_cast<LayoutHolder *>(this)->holder_ptr();
        }

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(Args &&... args) const
        {
          return holder_ptr()->template get_array<Property>(std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(Args &&... args)
        {
          return holder_ptr()->template get_array<Property>(std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array_pointer(Args &&... args) const
        {
          return ActualHolder::template array_pointer_getter<Property>(holder_ptr(), std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array_pointer(Args &&... args)
        {
          return ActualHolder::template array_pointer_getter<Property>(holder_ptr(), std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const MemContextInfo &
        memory_context_info() const
        {
          return static_cast<const MemContextInfo &>(*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr MemContextInfo & memory_context_info()
        {
          return static_cast<MemContextInfo &>(*this);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_resize(const PropertiesIndexer, const size_type)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_reserve(const PropertiesIndexer, const size_type)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_clear(const PropertiesIndexer)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_shrink_to_fit(const PropertiesIndexer)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class DestContext, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_copy_from(const PropertiesIndexer                   mult_arr_idx,
                                                                                             const DestContext &                       dest_context,
                                                                                             const typename DestContext::ContextInfo & context_info,
                                                                                             typename Property::Type *                 ptr,
                                                                                             const size_type                           num,
                                                                                             const size_type                           offset,
                                                                                             Args &&... args) const
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(num >= 0 && offset >= 0 &&
                 (num + offset <= this->template get_size_tag_for_array<Property>(mult_arr_idx) || InterfaceDescription::is_global(Property {})));
#endif

          MemoryContexts::memcopy_with_context(dest_context,
                                               MemoryContext {},
                                               context_info,
                                               this->memory_context_info(),
                                               ptr,
                                               this->template get_array_pointer<Property>(mult_arr_idx, offset),
                                               num,
                                               std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class SourceContext, class... Args>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_copy_to(const PropertiesIndexer                     mult_arr_idx,
                                                                                           const SourceContext &                       source_context,
                                                                                           const typename SourceContext::ContextInfo & context_info,
                                                                                           const typename Property::Type *             ptr,
                                                                                           const size_type                             num,
                                                                                           const size_type                             offset,
                                                                                           Args &&... args)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(num >= 0 && offset >= 0 &&
                 (num + offset <= this->template get_size_tag_for_array<Property>(mult_arr_idx) || InterfaceDescription::is_global(Property {})));
#endif

          MemoryContexts::memcopy_with_context(MemoryContext {},
                                               source_context,
                                               this->memory_context_info(),
                                               context_info,
                                               this->template get_array_pointer<Property>(mult_arr_idx, offset),
                                               ptr,
                                               num,
                                               std::forward<Args>(args)...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_resize(const PropertiesIndexer, const size_type)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_reserve(const PropertiesIndexer, const size_type)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_shrink_to_fit(const PropertiesIndexer)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_clear(const PropertiesIndexer)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const size_type * get_tagged_size_pointer(const PropertiesIndexer mult_arr_idx) const
        {
          return ActualHolder::size_tag_array_getter(holder_ptr(), SzTgHelper::template get_tag_index<Tag>(mult_arr_idx));
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type * get_tagged_size_pointer(const PropertiesIndexer mult_arr_idx)
        {
          return ActualHolder::size_tag_array_getter(holder_ptr(), SzTgHelper::template get_tag_index<Tag>(mult_arr_idx));
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type get_tagged_size(const PropertiesIndexer mult_arr_idx) const
        {
          size_type ret = 0;

          MemoryContexts::memcopy_with_context(MemoryContexts::Current {},
                                               MemoryContext {},
                                               MemoryContexts::Current::ContextInfo {},
                                               this->memory_context_info(),
                                               &ret,
                                               this->template get_tagged_size_pointer<Tag>(mult_arr_idx),
                                               1);

          return ret;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void set_tagged_size(const PropertiesIndexer mult_arr_idx, const size_type new_size)
        {
          MemoryContexts::memcopy_with_context(MemoryContext {},
                                               MemoryContexts::Current {},
                                               this->memory_context_info(),
                                               MemoryContexts::Current::ContextInfo {},
                                               this->template get_tagged_size_pointer<Tag>(mult_arr_idx),
                                               &new_size,
                                               1);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_capacity(const PropertiesIndexer) const
        {
          using PropExs = decltype(SzTgHelper::template get_properties_and_extents_matching_tag<Tag>());

          if constexpr (PropExs::number() == 0)
            {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
              Helpers::unintended_behaviour_warner<(PropExs::number() == 0)>::warn_tagged_capacity();
#endif
              return 0;
              //This would be the very unlikely case of properties that track size
              //inexplicably all being NoObject...
              //Returning 0 is as good an answer as any.
            }
          else
            {
              return static_cast<size_type>(impl_S::get_property_minimum_size<RealSizeMap>(PropExs {}));
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const size_type * get_size_tag_array() const
        {
          ActualHolder::size_tag_array_getter(holder_ptr());
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type * get_size_tag_array()
        {
          ActualHolder::size_tag_array_getter(holder_ptr());
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const size_type * get_size_tag_array_pointer() const
        {
          ActualHolder::size_tag_array_getter(holder_ptr());
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type * get_size_tag_array_pointer()
        {
          ActualHolder::size_tag_array_getter(holder_ptr());
        }

       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_for_array(const PropertiesIndexer mult_arr_idx) const
        {
          using MatchingTag = std::decay_t<decltype(SzTgHelper::template get_tag_matching_property<Property>())>;

          return this->template get_tagged_size<MatchingTag>(mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void initialize_global_property(PropEx)
        {
          constexpr auto p            = typename PropEx::Property {};
          constexpr auto initial_size = InterfaceDescription::initial_global_property_size(p);

          if constexpr (InterfaceDescription::is_global(p) && initial_size > 0)
            {
              for (PropertiesIndexer i = 0; i < PropEx::extent; ++i)
                {
                  if constexpr (track_individual_size(p))
                    {
                      this->template set_tagged_size<Utility::TypeHolder<typename PropEx::Property>>(i, initial_size);
                    }
                  if constexpr (InterfaceDescription::use_initial_global_property_memset(p))
                    {
                      constexpr auto initial_memset = InterfaceDescription::initial_global_property_memset_value(p);
                      MemoryContext::memset(this->memory_context_info(),
                                            this->template get_array_pointer<typename PropEx::Property>(i),
                                            initial_memset,
                                            initial_size);
                    }
                }
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... PropExs>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void initialize_global_properties(Utility::TypeHolder<PropExs...>)
        {
          (initialize_global_property(PropExs {}), ...);
        }

       protected:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void initialize_global_properties()
        {
          initialize_global_properties(typename Info::template flattened_per_item<false, true> {});
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void zero_out_sizes()
        {
          MemoryContext::memset(this->memory_context_info(), ActualHolder::zeroable_arrays_getter(holder_ptr()), 0, 1);
        }
      };

    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class SizeType, class DifferenceType, class SizeMap>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES LocalStruct
    {
      using size_type       = SizeType;
      using difference_type = DifferenceType;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
      interface_properties()
      {
        return InterfaceProperties {MemoryContexts::Current::access_properties(), ResizeProperties::Full, MutabilityProperties::Full};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return MemoryContexts::Current {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_get_array_pointers()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size_tag_array()
      {
        return true;
      }

      template <class F, class Layout>
      using ExtraFunctions = MemoryContexts::Current::template ExtraFunctions<F, Layout>;

     private:

      template <class LayoutHolder, class T>
      using ProtoLayout = impl_S::proto_layout_holder<LayoutHolder,
                                                      T,
                                                      MemoryContexts::Current,
                                                      size_type,
                                                      difference_type,
                                                      SizeMap,
                                                      impl_S::static_struct_full_holder<size_type, SizeMap, T>>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES layout_holder : public ProtoLayout<layout_holder<T>, T>
      {
        friend struct Utility::FriendshipProvider;

       private:

        using RealProtoLayout = ProtoLayout<layout_holder<T>, T>;

        friend RealProtoLayout;

        using MemContextInfo = MemoryContexts::Current::ContextInfo;

        using ActualHolder = impl_S::static_struct_full_holder<size_type, SizeMap, T>;

       protected:

        ActualHolder m_holder;

       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto * holder_ptr() const
        {
          return &m_holder;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto * holder_ptr()
        {
          return &m_holder;
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder()
        {
          this->initialize_global_properties();
          this->zero_out_sizes();
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
          class... Ts,
          class disabler = std::enable_if_t<Helpers::can_construct_layout_holder<MemContextInfo, Ts...>()>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(Ts &&... ts): MemContextInfo(std::forward<Ts>(ts)...)
        {
          this->initialize_global_properties();
          this->zero_out_sizes();
        }
      };
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <template <class...> class Allocator,
                                                          class SizeType,
                                                          class DifferenceType,
                                                          class SizeMap,
                                                          class... ExtraArgs>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DynamicStruct
    {
      using size_type       = SizeType;
      using difference_type = DifferenceType;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return ExtraInformation::AllocatorLike<Allocator<Utility::InvalidType, ExtraArgs...>>::memory_context();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
      interface_properties()
      {
        return InterfaceProperties {memory_context().access_properties(), ResizeProperties::Full, MutabilityProperties::Full};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_get_array_pointers()
      {
        return true;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size_tag_array()
      {
        return true;
      }

      template <class F, class Layout>
      using ExtraFunctions = typename std::decay_t<decltype(memory_context())>::template ExtraFunctions<F, Layout>;

     private:

      template <class LayoutHolder, class T>
      using ProtoLayout = impl_S::proto_layout_holder<LayoutHolder,
                                                      T,
                                                      typename std::decay_t<decltype(memory_context())>,
                                                      size_type,
                                                      difference_type,
                                                      SizeMap,
                                                      impl_S::static_struct_full_holder<size_type, SizeMap, T>>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES layout_holder : public ProtoLayout<layout_holder<T>, T>
      {
        friend struct Utility::FriendshipProvider;

       private:

        using RealProtoLayout = ProtoLayout<layout_holder<T>, T>;

        friend RealProtoLayout;

        using MemContextInfo = typename std::decay_t<decltype(memory_context())>::ContextInfo;

        using ActualHolder = impl_S::static_struct_full_holder<size_type, SizeMap, T>;

        using RealAllocatorType = Allocator<ActualHolder, ExtraArgs...>;

       protected:

        ActualHolder * m_ptr = nullptr;

       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto & holder_ptr() const
        {
          return m_ptr;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & holder_ptr()
        {
          return m_ptr;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void initialize_pointer()
        {
          RealAllocatorType alloc;

          if constexpr (ExtraInformation::AllocatorLike<RealAllocatorType>::context_info_aware())
            {
              m_ptr = alloc.allocate(this->memory_context_info(), 1);
            }
          else
            {
              m_ptr = alloc.allocate(1);
            }
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder()
        {
          this->initialize_pointer();
          this->initialize_global_properties();
          this->zero_out_sizes();
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
          class... Ts,
          class disabler = std::enable_if_t<Helpers::can_construct_layout_holder<MemContextInfo, Ts...>()>>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(Ts &&... ts): MemContextInfo(std::forward<Ts>(ts)...)
        {
          this->initialize_pointer();
          this->initialize_global_properties();
          this->zero_out_sizes();
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES
#if __cplusplus >= 202002L
        constexpr
          //C++20 added constexpr destructors,
          //invalid before...
#endif
          ~layout_holder()
        {
          RealAllocatorType alloc;

          if constexpr (ExtraInformation::AllocatorLike<RealAllocatorType>::context_info_aware())
            {
              alloc.deallocate(this->memory_context_info(), m_ptr, 1);
            }
          else
            {
              alloc.deallocate(m_ptr, 1);
            }
        }
      };
    };

    namespace impl_S
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class OriginalLayout, class ActualHolder, class SizeMap, bool constant>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES DynamicStructByValue
      {
        using OriginalLayoutType = OriginalLayout;

        using size_type       = typename OriginalLayout::size_type;
        using difference_type = typename OriginalLayout::difference_type;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
        interface_properties()
        {
          return OriginalLayout::interface_properties() & InterfaceProperties {AccessProperties::Full,
                                                                               constant ? ResizeProperties::NoResize : ResizeProperties::Full,
                                                                               constant ? MutabilityProperties::NoModify : MutabilityProperties::Full};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
        {
          return OriginalLayout::memory_context();
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_get_array_pointers()
        {
          return true;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size_tag_array()
        {
          return true;
        }

        template <class F, class Layout>
        using ExtraFunctions = typename OriginalLayout::template ExtraFunctions<F, Layout>;

       private:

        template <class LayoutHolder, class T>
        using ProtoLayout =
          impl_S::proto_layout_holder<LayoutHolder, T, typename std::decay_t<decltype(memory_context())>, size_type, difference_type, SizeMap, ActualHolder>;

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES layout_holder : public ProtoLayout<layout_holder<T>, T>
        {
          friend struct Utility::FriendshipProvider;

         private:

          using RealProtoLayout = ProtoLayout<layout_holder<T>, T>;

          friend RealProtoLayout;

          using MemContextInfo = typename std::decay_t<decltype(memory_context())>::ContextInfo;

         protected:

          std::conditional_t<constant, const ActualHolder *, ActualHolder *> m_ptr;

         private:

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto & holder_ptr() const
          {
            return m_ptr;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & holder_ptr()
          {
            return m_ptr;
          }

         public:

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
            class OtherLayout,
            class Properties,
            class MetaInfo,
            class disabler = std::enable_if_t < constant && Utility::always_true<OtherLayout> >>
                                                              MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(
                                                                const Collections::Collection<OtherLayout, Properties, MetaInfo> & c):
            m_ptr(Utility::FriendshipProvider::holder_ptr(c))
          {
            this->memory_context_info() = c.memory_context_info();
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherLayout,
                                                                   class Properties,
                                                                   class MetaInfo,
                                                                   bool valid     = OtherLayout::can_get_array_pointers(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(Collections::Collection<OtherLayout, Properties, MetaInfo> & c):
            m_ptr(Utility::FriendshipProvider::holder_ptr(c))
          {
            this->memory_context_info() = c.memory_context_info();
          }

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
          __pragma("nv_diag_suppress 20012")
  #else
          _Pragma("nv_diag_suppress 20012")
  #endif
#endif
            MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder() = default;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(const layout_holder &) = default;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(layout_holder &&) = default;

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
          __pragma("nv_diag_default 20012")
  #else
          _Pragma("nv_diag_default 20012")
  #endif
#endif
          //This means that by-value copies of this will work as intended.
        };
      };
    }
  }

  namespace Collections
  {
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Properties,
                                                             class MetaInfo,
                                                             template <class...>
                                                             class Allocator,
                                                             class SizeType,
                                                             class DifferenceType,
                                                             class SizeMap,
                                                             class... ExtraArgs>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto
    pass_by_value(Collection<LayoutTypes::DynamicStruct<Allocator, SizeType, DifferenceType, SizeMap, ExtraArgs...>, Properties, MetaInfo> & c)
    {
      using OriginalLayout = LayoutTypes::DynamicStruct<Allocator, SizeType, DifferenceType, SizeMap, ExtraArgs...>;

      using ActualHolderType = std::decay_t<decltype(*Utility::FriendshipProvider::holder_ptr(c))>;

      using ByValueLayout = LayoutTypes::impl_S::DynamicStructByValue<OriginalLayout, ActualHolderType, SizeMap, false>;

      return Collection<ByValueLayout, Properties, MetaInfo>(c);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Properties,
                                                             class MetaInfo,
                                                             template <class...>
                                                             class Allocator,
                                                             class SizeType,
                                                             class DifferenceType,
                                                             class SizeMap,
                                                             class... ExtraArgs>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto
    pass_by_value(const Collection<LayoutTypes::DynamicStruct<Allocator, SizeType, DifferenceType, SizeMap, ExtraArgs...>, Properties, MetaInfo> & c)
    {
      using OriginalLayout = LayoutTypes::DynamicStruct<Allocator, SizeType, DifferenceType, SizeMap, ExtraArgs...>;

      using ActualHolderType = std::decay_t<decltype(*Utility::FriendshipProvider::holder_ptr(c))>;

      using ByValueLayout = LayoutTypes::impl_S::DynamicStructByValue<OriginalLayout, ActualHolderType, SizeMap, true>;

      return Collection<ByValueLayout, Properties, MetaInfo>(c);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <template <class...> class Allocator,
                                                             class SizeType,
                                                             class DifferenceType,
                                                             class SizeMap,
                                                             class... ExtraArgs,
                                                             class PtrType,
                                                             ResizeProperties     resize,
                                                             MutabilityProperties mutability,
                                                             class Properties,
                                                             class MetaInfo>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto pass_by_value(
      Collection<LayoutTypes::Internal::
                   ViewLayout<LayoutTypes::DynamicStruct<Allocator, SizeType, DifferenceType, SizeMap, ExtraArgs...>, PtrType, resize, mutability, false>,
                 Properties,
                 MetaInfo> & c)
    {
      using OriginalLayout = LayoutTypes::Internal::
        ViewLayout<LayoutTypes::DynamicStruct<Allocator, SizeType, DifferenceType, SizeMap, ExtraArgs...>, PtrType, resize, mutability, false>;

      using ActualHolderType = std::decay_t<decltype(*Utility::FriendshipProvider::holder_ptr(*Utility::FriendshipProvider::get_layout_holder_pointer(c)))>;

      using ByValueLayout = LayoutTypes::impl_S::DynamicStructByValue<OriginalLayout, ActualHolderType, SizeMap, false>;

      return Collection<ByValueLayout, Properties, MetaInfo>(c);
    }

    template <template <class...> class Allocator,
              class SizeType,
              class DifferenceType,
              class SizeMap,
              class... ExtraArgs,
              class PtrType,
              ResizeProperties     resize,
              MutabilityProperties mutability,
              class Properties,
              class MetaInfo>
    auto pass_by_value(
      const Collection<LayoutTypes::Internal::
                         ViewLayout<LayoutTypes::DynamicStruct<Allocator, SizeType, DifferenceType, SizeMap, ExtraArgs...>, PtrType, resize, mutability, false>,
                       Properties,
                       MetaInfo> & c)
    {
      using OriginalLayout = LayoutTypes::Internal::
        ViewLayout<LayoutTypes::DynamicStruct<Allocator, SizeType, DifferenceType, SizeMap, ExtraArgs...>, PtrType, resize, mutability, false>;

      using ActualHolderType = std::decay_t<decltype(*Utility::FriendshipProvider::holder_ptr(*Utility::FriendshipProvider::get_layout_holder_pointer(c)))>;

      using ByValueLayout = LayoutTypes::impl_S::DynamicStructByValue<OriginalLayout, ActualHolderType, SizeMap, true>;

      return Collection<ByValueLayout, Properties, MetaInfo>(c);
    }
  }

  namespace Transfers
  {
    namespace impl_SL
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SzMp1, class SzMp2, class... PropExs>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool equivalent_size_maps(Utility::TypeHolder<PropExs...>)
      {
        return ((SzMp1::size(typename PropExs::Property {}) == SzMp2::size(typename PropExs::Property {})) && ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestInfo, class SrcInfo, class DestSizeMap, class SrcSizeMap>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool collections_equivalent()
      {
        return std::is_same_v<typename DestInfo::template necessary_size_tags<>, typename SrcInfo::template necessary_size_tags<>> &&
               std::is_same_v<typename DestInfo::template flattened_per_item<true, true, false>,
                              typename SrcInfo::template flattened_per_item<true, true, false>> &&
               std::is_same_v<typename DestInfo::template flattened_jagged_vector_properties<>,
                              typename SrcInfo::template flattened_jagged_vector_properties<>> &&
               std::is_same_v<typename DestInfo::template flattened_jagged_vector<>, typename SrcInfo::template flattened_jagged_vector<>> &&
               equivalent_size_maps<typename DestInfo::template per_item_size_map<DestSizeMap>, typename SrcInfo::template per_item_size_map<SrcSizeMap>>(
                 typename DestInfo::template flattened_per_item<true, true, true> {});
      }
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class SizeTypeDest,
                                                          class SizeTypeSrc,
                                                          class DifferenceTypeDest,
                                                          class DifferenceTypeSrc,
                                                          class SizeMapDest,
                                                          class SizeMapSrc,
                                                          class PropertiesDest,
                                                          class PropertiesSrc,
                                                          class MetaInfoDest,
                                                          class MetaInfoSrc>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES
    TransferSpecification<Collections::CollectionHolder<LayoutTypes::LocalStruct<SizeTypeDest, DifferenceTypeDest, SizeMapDest>, PropertiesDest, MetaInfoDest>,
                          Collections::CollectionHolder<LayoutTypes::LocalStruct<SizeTypeSrc, DifferenceTypeSrc, SizeMapSrc>, PropertiesSrc, MetaInfoSrc>,
                          TransferPriority::UserProvided,
                          std::enable_if_t<sizeof(SizeTypeDest) == sizeof(SizeTypeSrc) &&
                                           impl_SL::collections_equivalent<InterfaceDescription::InterfaceInformation<PropertiesDest>,
                                                                           InterfaceDescription::InterfaceInformation<PropertiesSrc>,
                                                                           SizeMapDest,
                                                                           SizeMapSrc>()>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(51);

     private:

      using DestLayout = LayoutTypes::LocalStruct<SizeTypeDest, DifferenceTypeDest, SizeMapDest>;
      using SrcLayout  = LayoutTypes::LocalStruct<SizeTypeSrc, DifferenceTypeSrc, SizeMapSrc>;

      using DestType = Collections::CollectionHolder<DestLayout, PropertiesDest, MetaInfoDest>;
      using SrcType  = Collections::CollectionHolder<SrcLayout, PropertiesSrc, MetaInfoSrc>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_collection() && SrcLayout::interface_properties().can_copy_out_of_collection();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return false;
        //No moving possible for what amounts
        //to a POD-like struct...
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_full_collection()
      {
        return T::interface_properties().resize >= ResizeProperties::Move && (T::extent() == 1);
        //This means that we are not handling the underlying object through a view or whatever,
        //so we really can memcopy in bulk, move and so on.
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_pointer_to_holder(T && t)
      {
        auto * layout_ptr = Utility::FriendshipProvider::get_layout_holder_pointer(std::forward<T>(t));
        return Utility::FriendshipProvider::holder_ptr(*layout_ptr);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        if constexpr (is_full_collection<DestLike>() && is_full_collection<SourceLike>())
          {
            static_assert(sizeof(decltype(*get_pointer_to_holder(d))) == sizeof(decltype(*get_pointer_to_holder(s))));
            MemoryContexts::memcopy_with_context(d.memory_context(),
                                                 s.memory_context(),
                                                 d.memory_context_info(),
                                                 s.memory_context_info(),
                                                 get_pointer_to_holder(d),
                                                 get_pointer_to_holder(s),
                                                 1);
          }
        else
          {
            //Fall back to the standard strategy.
            Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>::copy(d, s);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        if constexpr (is_full_collection<DestLike>() && is_full_collection<SourceLike>())
          {
            static_assert(sizeof(decltype(*get_pointer_to_holder(d))) == sizeof(decltype(*get_pointer_to_holder(s))));

            MemoryContexts::memcopy_with_context(d.memory_context(),
                                                 s.memory_context(),
                                                 d.memory_context_info(),
                                                 s.memory_context_info(),
                                                 get_pointer_to_holder(d),
                                                 get_pointer_to_holder(s),
                                                 1,
                                                 std::forward<Args>(args)...);
          }
        else
          {
            Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>::special_copy(d, s, std::forward<Args>(args)...);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                           d,
                                                                                            const SourceLike &                   s,
                                                                                            const typename DestLike::size_type   num,
                                                                                            const typename DestLike::size_type   d_offset = 0,
                                                                                            const typename SourceLike::size_type s_offset = 0)
      {
        Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>::copy_some(d, s, num, d_offset, s_offset);
        //Nothing intelligent to do for a partial copy,
        //fall back to the standard behaviour.
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s) = delete;
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <template <class...> class AllocatorDest,
                                                          template <class...>
                                                          class AllocatorSrc,
                                                          class SizeTypeDest,
                                                          class SizeTypeSrc,
                                                          class DifferenceTypeDest,
                                                          class DifferenceTypeSrc,
                                                          class SizeMapDest,
                                                          class SizeMapSrc,
                                                          class... ExtraArgsDest,
                                                          class... ExtraArgsSrc,
                                                          class PropertiesDest,
                                                          class PropertiesSrc,
                                                          class MetaInfoDest,
                                                          class MetaInfoSrc>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES TransferSpecification<
      Collections::CollectionHolder<LayoutTypes::DynamicStruct<AllocatorDest, SizeTypeDest, DifferenceTypeDest, SizeMapDest, ExtraArgsDest...>,
                                    PropertiesDest,
                                    MetaInfoDest>,
      Collections::
        CollectionHolder<LayoutTypes::DynamicStruct<AllocatorSrc, SizeTypeSrc, DifferenceTypeSrc, SizeMapSrc, ExtraArgsSrc...>, PropertiesSrc, MetaInfoSrc>,
      TransferPriority::UserProvided,
      std::enable_if_t<sizeof(SizeTypeDest) == sizeof(SizeTypeSrc) &&
                       impl_SL::collections_equivalent<InterfaceDescription::InterfaceInformation<PropertiesDest>,
                                                       InterfaceDescription::InterfaceInformation<PropertiesSrc>,
                                                       SizeMapDest,
                                                       SizeMapSrc>()>>
    {
      MARIONETTE_TRANSFER_SPECIFICATION_DEBUG(52);

     private:

      using DestLayout = LayoutTypes::DynamicStruct<AllocatorDest, SizeTypeDest, DifferenceTypeDest, SizeMapDest, ExtraArgsDest...>;
      using SrcLayout  = LayoutTypes::DynamicStruct<AllocatorSrc, SizeTypeSrc, DifferenceTypeSrc, SizeMapSrc, ExtraArgsSrc...>;

      using DestType = Collections::CollectionHolder<DestLayout, PropertiesDest, MetaInfoDest>;
      using SrcType  = Collections::CollectionHolder<SrcLayout, PropertiesSrc, MetaInfoSrc>;

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return DestLayout::interface_properties().can_copy_into_collection() && SrcLayout::interface_properties().can_copy_out_of_collection();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return DestLayout::interface_properties().can_move_into_collection() && SrcLayout::interface_properties().can_move_out_of_collection() &&
               MemoryContexts::memory_contexts_potentially_compatible(
                 ExtraInformation::AllocatorLike<AllocatorSrc<Utility::InvalidType, ExtraArgsSrc...>>::memory_context(),
                 ExtraInformation::AllocatorLike<AllocatorDest<Utility::InvalidType, ExtraArgsDest...>>::memory_context()) &&
               MemoryContexts::memory_contexts_potentially_compatible(
                 ExtraInformation::AllocatorLike<AllocatorDest<Utility::InvalidType, ExtraArgsDest...>>::memory_context(),
                 ExtraInformation::AllocatorLike<AllocatorSrc<Utility::InvalidType, ExtraArgsSrc...>>::memory_context());
        //We can consider moving if memory allocated from the source context
        //can be safely deleted in the destination context and vice-versa.
        //
        //We do not simply deallocate from the incoming object
        //given non-destructive moves and the fact that, by design,
        //the DynamicStruct layout (or any layout for that matter)
        //does not support an "unallocated" state.
      }

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_full_collection()
      {
        return T::interface_properties().resize >= ResizeProperties::Move && (T::extent() == 1);
        //This means that we are not handling the underlying object through a view or whatever,
        //so we really can memcopy in bulk, move and so on.
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_pointer_to_holder(T && t)
      {
        auto * layout_ptr = Utility::FriendshipProvider::get_layout_holder_pointer(std::forward<T>(t));
        return Utility::FriendshipProvider::holder_ptr(*layout_ptr);
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy(DestLike & d, const SourceLike & s)
      {
        if constexpr (is_full_collection<DestLike>() && is_full_collection<SourceLike>())
          {
            static_assert(sizeof(decltype(*get_pointer_to_holder(d))) == sizeof(decltype(*get_pointer_to_holder(s))));

            MemoryContexts::memcopy_with_context(d.memory_context(),
                                                 s.memory_context(),
                                                 d.memory_context_info(),
                                                 s.memory_context_info(),
                                                 get_pointer_to_holder(d),
                                                 get_pointer_to_holder(s),
                                                 1);
          }
        else
          {
            //Fall back to the standard strategy.
            Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>::copy(d, s);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void special_copy(DestLike & d, const SourceLike & s, Args &&... args)
      {
        if constexpr (is_full_collection<DestLike>() && is_full_collection<SourceLike>())
          {
            static_assert(sizeof(decltype(*get_pointer_to_holder(d))) == sizeof(decltype(*get_pointer_to_holder(s))));

            MemoryContexts::memcopy_with_context(d.memory_context(),
                                                 s.memory_context(),
                                                 d.memory_context_info(),
                                                 s.memory_context_info(),
                                                 get_pointer_to_holder(d),
                                                 get_pointer_to_holder(s),
                                                 1,
                                                 std::forward<Args>(args)...);
          }
        else
          {
            Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>::copy(d, s, std::forward<Args>(args)...);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void copy_some(DestLike &                           d,
                                                                                            const SourceLike &                   s,
                                                                                            const typename DestLike::size_type   num,
                                                                                            const typename DestLike::size_type   d_offset = 0,
                                                                                            const typename SourceLike::size_type s_offset = 0)
      {
        Transfer<DestType, SrcType, TransferPriority::UserProvidedSecondary>::copy_some(d, s, num, d_offset, s_offset);
        //Nothing intelligent to do for a partial copy,
        //fall back to the standard behaviour.
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class DestLike, class SourceLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void move(DestLike & d, SourceLike && s)
      {
        if (is_full_collection<DestLike>() && is_full_collection<SourceLike>() &&
            MemoryContexts::memory_contexts_compatible(s.memory_context(), d.memory_context(), s.memory_context_info(), d.memory_context_info()) &&
            MemoryContexts::memory_contexts_compatible(d.memory_context(), s.memory_context(), d.memory_context_info(), s.memory_context_info()))
          {
            auto * ptr_from_d = get_pointer_to_holder(d);
            auto * ptr_from_s = get_pointer_to_holder(s);

            MemoryContexts::move_with_context(s.memory_context(), d.memory_context(), s.memory_context_info(), d.memory_context_info(), ptr_from_d, 1);
            //Move d's holder to s's memory context.

            MemoryContexts::move_with_context(d.memory_context(), s.memory_context(), d.memory_context_info(), s.memory_context_info(), ptr_from_s, 1);
            //Move s's holder to d's memory context.

            get_pointer_to_holder(d) = ptr_from_s;
            get_pointer_to_holder(s) = ptr_from_d;
          }
        else
          {
            copy(d, static_cast<const SourceLike &>(s));
            //Fall back to copying,
            //as there is no way to move...
          }
      }
    };
  }
}

#endif
