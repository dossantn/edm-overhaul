//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_LAYOUTHELPERS_H
#define MARIONETTE_LAYOUTHELPERS_H

#include <utility>
#include <type_traits>

#include "../MarionetteBase.h"

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

namespace Marionette
{
  namespace LayoutTypes
  {
    namespace Helpers
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class... SzTgExtTps>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES SizeTagHolder<Utility::TypeHolder<SzTgExtTps...>>
      {
       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer get_base_offset()
        {
          PropertiesIndexer ret = 0;

          bool found = false;

          (((found = found || std::is_same_v<Tag, typename SzTgExtTps::SizeTag>), ret += SzTgExtTps::extent * (!found)), ...);

          return ret * found;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer get_tag_extent()
        {
          return ((SzTgExtTps::extent * std::is_same_v<Tag, typename SzTgExtTps::SizeTag>) +...);
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer
        total_number_of_tags()
        {
          return (SzTgExtTps::extent + ...);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer get_tag_index(const PropertiesIndexer mult_arr_idx)
        {
          constexpr PropertiesIndexer base_index = get_base_offset<Tag>();

          constexpr PropertiesIndexer tag_extent = get_tag_extent<Tag>();

          if constexpr (tag_extent == 1)
            //Global property on top of (a set of) property arrays,
            //or the general DefaultSizeTag size_tag.
            {
              return base_index;
            }
          else
            {
#if MARIONETTE_USE_ASSERTIONS
              assert(mult_arr_idx >= 0 && mult_arr_idx < tag_extent);
#endif

              return base_index + mult_arr_idx;
            }
        }

       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Holder>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_one_size(Holder && h, const PropertiesIndexer idx)
        {
#if MARIONETTE_USE_ASSERTIONS
          assert(idx >= 0 && idx < total_number_of_tags());
#endif

          return std::forward<Holder>(h)[static_cast<std::make_unsigned_t<PropertiesIndexer>>(idx)];
          //static_cast to the unsigned version to prevent warnings about narrowing casts
          //(which the assert above guarantees will not happen).
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag, class Holder>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_size_tag(Holder && h, const PropertiesIndexer mult_arr_idx)
        {
          return get_one_size(std::forward<Holder>(h), get_tag_index<Tag>(mult_arr_idx));
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto get_properties_and_extents_matching_tag()
        {
          constexpr auto pre_ret = (Utility::TypeHolder<Utility::InvalidType> {} & ... &
                                    std::conditional_t<std::is_same_v<Tag, typename SzTgExtTps::SizeTag>,
                                                       Utility::TypeHolder<SzTgExtTps>,
                                                       Utility::TypeHolder<Utility::InvalidType>> {});

          using OneTagExtentAndProperties = std::decay_t<decltype(Utility::get_one_type_or_invalid(pre_ret))>;

          if constexpr (std::is_same_v<OneTagExtentAndProperties, Utility::InvalidType>)
            {
              static_assert(Utility::always_false<Tag>, "Must provide a valid tag to be found!");

              return Utility::TypeHolder<> {};
            }
          else
            {
              return typename OneTagExtentAndProperties::PropertiesAndExtents {};
            }
        }

       private:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, class... PropExs>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool check_property_is_in_list(Utility::TypeHolder<PropExs...>)
        {
          return (std::is_same_v<Property, typename PropExs::Property> || ...);
        }

       public:

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto get_tag_matching_property()
        {
          constexpr auto pre_ret = (Utility::TypeHolder<Utility::InvalidType> {} & ... &
                                    Utility::TypeHolder<std::conditional_t<check_property_is_in_list<Property>(typename SzTgExtTps::PropertiesAndExtents {}),
                                                                           typename SzTgExtTps::SizeTag,
                                                                           Utility::InvalidType>> {});

          using MatchingTag = std::decay_t<decltype(Utility::get_one_type_or_invalid(pre_ret))>;

          static_assert(!std::is_same_v<MatchingTag, Utility::InvalidType>, "Please provide a valid per-item property to find the matching tag.");

          return MatchingTag {};
        }
      };

#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR

      template <bool b>
      struct unintended_behaviour_warner
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_tagged_capacity()
        {
        }
      };

      template <>
      struct unintended_behaviour_warner<true>
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        [[deprecated("This is not really a deprecation warning. The code is trying to access a tagged capacity for which no per item property can be found. "
                     "While technically valid, this may be the consequence of unintended errors in the property list. "
                     "Define 'MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR' to 0 to suppress this warning.")]]
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_tagged_capacity()
        {
        }
      };
#endif
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_some_form_of_collection_helper
      {
        static constexpr bool value = false;
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class Properties, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_some_form_of_collection_helper<Collections::CollectionHolder<Layout, Properties, MetaInfo>>
      {
        static constexpr bool value = true;
      };

#if MARIONETTE_TURN_VECTOR_LIKE_WRAPPER_INTO_IDEMPOTENT
#else
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class Properties, class MetaInfo>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES is_some_form_of_collection_helper<Collections::Collection<Layout, Properties, MetaInfo>>
      {
        static constexpr bool value = true;
      };
#endif

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool is_some_form_of_collection()
      {
        return is_some_form_of_collection_helper<T>::value;
      }

      ///@brief Checks if a layout_holder can be constructed from @p Ts,
      ///       with @p Ts being used to initialize the memory context info.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class MemContextInfo, class... Ts>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool can_construct_layout_holder()
      {
        return std::is_constructible_v<MemContextInfo, Ts...> && ((!is_some_form_of_collection<std::decay_t<Ts>>() && ...) || sizeof...(Ts) > 1);
      }
    }
  }
}

#endif
