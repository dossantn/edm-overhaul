//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_INTERNALLAYOUTTYPES_H
#define MARIONETTE_INTERNALLAYOUTTYPES_H

#include <utility>
#include <type_traits>

#include "../MarionetteBase.h"
#include "LayoutHelpers.h"
#include "VectorLike.h"

//IndividualPointerHolder relies on part
//of the implementation for the VectorLike layout
//(the logic for storing a pointer per item).

namespace Marionette
{
  namespace LayoutTypes
  {
    namespace Internal
    {
      namespace impl
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class PtrType, bool use_index>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ViewLayoutHolder;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class PtrType>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ViewLayoutHolder<T, PtrType, true>
        {
         protected:

          friend struct Utility::FriendshipProvider;

          PtrType m_ptr = {};

          PropertiesIndexer m_index = 0;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const PtrType &
          get_layout_holder_pointer() const
          {
            return m_ptr;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr PtrType & get_layout_holder_pointer()
          {
            return m_ptr;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const PropertiesIndexer &
          get_multi_array_index() const
          {
            return m_index;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr PropertiesIndexer &
          get_multi_array_index()
          {
            return m_index;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
          update_multi_array_index(const PropertiesIndexer new_index)
          {
            m_index = new_index;
          }

         public:

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
          __pragma("nv_diag_suppress 20012")
  #else
          _Pragma("nv_diag_suppress 20012")
  #endif
#endif
            MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder() = default;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder(const ViewLayoutHolder &) = delete;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder(ViewLayoutHolder &&) = delete;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder &
          operator=(const ViewLayoutHolder &) = delete;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder &
          operator=(ViewLayoutHolder &&) = delete;

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
          __pragma("nv_diag_default 20012")
  #else
          _Pragma("nv_diag_default 20012")
  #endif
#endif

            MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
            MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder(const PtrType ptr, const PropertiesIndexer idx = 0):
            m_ptr(ptr),
            m_index(idx)
          {
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherPtr, class disabler = std::enable_if_t<std::is_convertible_v<PtrType, OtherPtr>>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr operator ViewLayoutHolder<T, OtherPtr, true>() const
          {
            return {m_ptr, m_index};
          }
        };

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T, class PtrType>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ViewLayoutHolder<T, PtrType, false>
        {
         protected:

          PtrType m_ptr = {};

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const PtrType &
          get_layout_holder_pointer() const
          {
            return m_ptr;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr PtrType & get_layout_holder_pointer()
          {
            return m_ptr;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr PropertiesIndexer
          get_multi_array_index() const
          {
            return 0;
          }

         public:

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
          __pragma("nv_diag_suppress 20012")
  #else
          _Pragma("nv_diag_suppress 20012")
  #endif
#endif
            MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder() = default;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder(const ViewLayoutHolder &) = delete;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder(ViewLayoutHolder &&) = delete;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder &
          operator=(const ViewLayoutHolder &) = delete;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder &
          operator=(ViewLayoutHolder &&) = delete;

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
          __pragma("nv_diag_default 20012")
  #else
          _Pragma("nv_diag_default 20012")
  #endif
#endif

            MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr ViewLayoutHolder(const PtrType ptr):
            m_ptr(ptr)
          {
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherPtr, class disabler = std::enable_if_t<std::is_convertible_v<PtrType, OtherPtr>>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr operator ViewLayoutHolder<T, OtherPtr, false>() const
          {
            return {m_ptr};
          }
        };

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_can_get_array_pointers(Ts...)
        {
          return false;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class LayoutType, bool ret = LayoutType::can_get_array_pointers()>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_can_get_array_pointers(LayoutType)
        {
          return ret;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Ts>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_has_size_tag_array(Ts...)
        {
          return false;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class LayoutType, bool ret = LayoutType::has_size_tag_array()>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr bool layout_has_size_tag_array(LayoutType)
        {
          return ret;
        }

#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR

        template <bool b>
        struct unintended_behaviour_warner
        {
          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
          warn_nonconst_context_view()
          {
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_get_capacity_iph()
          {
          }
        };

        template <>
        struct unintended_behaviour_warner<true>
        {
          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          [[deprecated("This is not really a deprecation warning. Getting non-const access to the memory context info "
                       "of a collection through a Internal::ViewLayout . Changing the memory context info here may lead "
                       "to unexpected behaviour changes in the original collection. Proceed with caution. "
                       "Provide an additional 'false' template parameter when calling 'memory_context_info' to suppress this warning. "
                       "Define 'MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR' to 0 to suppress all warnings about this.")]]
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_nonconst_context_view()
          {
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          [[deprecated("This is not really a deprecation warning. Calling 'get_tagged_capacity' with an IndividualPointerHolder "
                       "simply returns the corresponding tagged size, as the IndividualPointerHolder does not keep any information "
                       "regarding the capacity of the original collection (as it cannot be resized). "
                       "Provide an additional 'false' template parameter when calling 'get_tagged_capacity' to suppress this warning. "
                       "Define 'MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR' to 0 to suppress all warnings about this.")]]
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_get_capacity_iph()
          {
          }
        };
#endif
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class OriginalLayout,
                                                            class PtrType,
                                                            ResizeProperties     max_resize,
                                                            MutabilityProperties max_mutability,
                                                            bool                 needs_index>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ViewLayout
      {
        using OriginalLayoutType = OriginalLayout;

        using size_type = typename OriginalLayout::size_type;

        using difference_type = typename OriginalLayout::difference_type;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
        interface_properties()
        {
          return OriginalLayout::interface_properties() & InterfaceProperties {AccessProperties::Full, max_resize, max_mutability};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool stores_multi_array_index()
        {
          return needs_index;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool collection_is_reference()
        {
          return true;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_get_array_pointers()
        {
          return impl::layout_can_get_array_pointers(OriginalLayout {});
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size_tag_array()
        {
          return impl::layout_has_size_tag_array(OriginalLayout {});
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
        {
          return OriginalLayout::memory_context();
        }

        template <class F, class Layout>
        using ExtraFunctions = typename OriginalLayout::template ExtraFunctions<F, Layout>;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES layout_holder : impl::ViewLayoutHolder<T, PtrType, needs_index>
        {
          template <class A, class B, class C>
          friend struct CollectionHolder;

          friend struct Utility::FriendshipProvider;

         private:

          using Base = impl::ViewLayoutHolder<T, PtrType, needs_index>;

         public:

          using Base::Base;

         protected:

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent, class... Args>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_resize(Args &&... args)
          {
            Utility::FriendshipProvider::template base_resize<SizeTag, extent>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent, class... Args>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_reserve(Args &&... args)
          {
            Utility::FriendshipProvider::template base_reserve<SizeTag, extent>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent, class... Args>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_clear(Args &&... args)
          {
            Utility::FriendshipProvider::template base_clear<SizeTag, extent>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent, class... Args>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_shrink_to_fit(Args &&... args)
          {
            Utility::FriendshipProvider::template base_shrink_to_fit<SizeTag, extent>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_refer_to_const_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(Args &&... args) const
          {
            return Utility::FriendshipProvider::template get_array<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_refer_to_modifiable_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(Args &&... args)
          {
            return Utility::FriendshipProvider::template get_array<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid = interface_properties().can_refer_to_const_array() && can_get_array_pointers(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array_pointer(Args &&... args) const
          {
            return Utility::FriendshipProvider::template get_array_pointer<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid = interface_properties().can_refer_to_modifiable_array() &&
                                                                                can_get_array_pointers(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array_pointer(Args &&... args)
          {
            return Utility::FriendshipProvider::template get_array_pointer<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_copy_from_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_copy_from(Args &&... args) const
          {
            Utility::FriendshipProvider::template array_copy_from<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_copy_to_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_copy_to(Args &&... args)
          {
            Utility::FriendshipProvider::template array_copy_to<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_resize_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_resize(Args &&... args)
          {
            Utility::FriendshipProvider::template array_resize<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_resize_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_reserve(Args &&... args)
          {
            Utility::FriendshipProvider::template array_reserve<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_resize_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_clear(Args &&... args)
          {
            Utility::FriendshipProvider::template array_clear<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_resize_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_shrink_to_fit(Args &&... args)
          {
            Utility::FriendshipProvider::template array_shrink_to_fit<Property>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_read_sizes(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_size_pointer(Args &&... args) const
          {
            return Utility::FriendshipProvider::template get_tagged_size_pointer<Tag>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_resize_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_size_pointer(Args &&... args)
          {
            return Utility::FriendshipProvider::template get_tagged_size_pointer<Tag>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_read_sizes(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_size(Args &&... args) const
          {
            return Utility::FriendshipProvider::template get_tagged_size<Tag>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_resize_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void set_tagged_size(Args &&... args)
          {
            Utility::FriendshipProvider::template set_tagged_size<Tag>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                                   class... Args,
                                                                   bool valid     = interface_properties().can_read_sizes(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_capacity(Args &&... args) const
          {
            return Utility::FriendshipProvider::template get_tagged_capacity<Tag>(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Args,
                                                                   bool valid     = interface_properties().can_refer_to_const_array() && has_size_tag_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_array(Args &&... args) const
          {
            return Utility::FriendshipProvider::template get_size_tag_array(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Args,
                                                                   bool valid = interface_properties().can_refer_to_modifiable_array() && has_size_tag_array(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_array(Args &&... args)
          {
            return Utility::FriendshipProvider::template get_size_tag_array(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Args,
                                                                   bool valid = interface_properties().can_refer_to_const_array() && has_size_tag_array() &&
                                                                                can_get_array_pointers(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_array_pointer(Args &&... args) const
          {
            return Utility::FriendshipProvider::template get_size_tag_array_pointer(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Args,
                                                                   bool valid = interface_properties().can_refer_to_modifiable_array() &&
                                                                                has_size_tag_array() && can_get_array_pointers(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_array_pointer(Args &&... args)
          {
            return Utility::FriendshipProvider::template get_size_tag_array_pointer(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... Args>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) memory_context_info(Args &&... args) const
          {
            return Utility::FriendshipProvider::memory_context_info(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool warn = true, class... Args>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) memory_context_info(Args &&... args)
          {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
            impl::unintended_behaviour_warner<warn>::warn_nonconst_context_view();
#endif

            return Utility::FriendshipProvider::memory_context_info(*(this->get_layout_holder_pointer()), std::forward<Args>(args)...);
          }
        };
      };
    }
  }

  namespace Collections
  {
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OriginalLayout,
                                                             class PtrType,
                                                             ResizeProperties     resize,
                                                             MutabilityProperties mutability,
                                                             class Properties,
                                                             class MetaInfo>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto
    pass_by_value(const Collection<LayoutTypes::Internal::ViewLayout<OriginalLayout, PtrType, resize, mutability, true>, Properties, MetaInfo> &)
    {
      static_assert(Utility::always_false<OriginalLayout>, "Pass-by-value of views that specify multi-array indices is not supported!");

      return Utility::InvalidType {};
    }
  }

  namespace LayoutTypes
  {
    namespace Internal
    {
      namespace impl
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <bool constant>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES individual_pointer_holder_vector_type_decider;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES individual_pointer_holder_vector_type_decider<true>
        {
          template <class T>
          using Type = const T *;
        };

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES individual_pointer_holder_vector_type_decider<false>
        {
          template <class T>
          using Type = T *;
        };
      }
    }
  }

  namespace ExtraInformation
  {
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES VectorLike<T *>
    {
     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class F, class Layout>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ExtraFunctions
      {
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_valid()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_capacity()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_resize()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_reserve()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_clear()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_shrink_to_fit()
      {
        return false;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class VecLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return MemoryContexts::Invalid {};
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class VecLike>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr decltype(auto) get_start_of_data(VecLike && v)
      {
        return std::forward<VecLike>(v);
      }
    };
  }

  namespace LayoutTypes
  {
    namespace Internal
    {
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class OriginalLayout, bool constant>
      struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES IndividualPointerHolder
      {
        using OriginalLayoutType = OriginalLayout;

        using size_type = typename OriginalLayout::size_type;

        using difference_type = typename OriginalLayout::difference_type;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
        interface_properties()
        {
          return OriginalLayout::interface_properties() & InterfaceProperties {AccessProperties::Full,
                                                                               ResizeProperties::NoResize,
                                                                               constant ? MutabilityProperties::NoModify : MutabilityProperties::Full};
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_get_array_pointers()
        {
          return true;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool has_size_tag_array()
        {
          return true;
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
        {
          return OriginalLayout::memory_context();
        }

        template <class F, class Layout>
        using ExtraFunctions = typename OriginalLayout::template ExtraFunctions<F, Layout>;

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class T>
        struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES layout_holder :
          public impl_VL::dynamic_vector_holder_aggregator<impl::individual_pointer_holder_vector_type_decider<constant>::template Type,
                                                           layout_holder<T>,
                                                           true,
                                                           typename InterfaceDescription::InterfaceInformation<T>::template flattened_properties_by_type<>,
                                                           size_type>,
          public std::decay_t<decltype(IndividualPointerHolder::memory_context())>::ContextInfo
        {
          friend struct Utility::FriendshipProvider;

         private:

          using SizeTagList = typename InterfaceDescription::InterfaceInformation<T>::template necessary_size_tags<>;

          using SzTgHelper = Helpers::SizeTagHolder<SizeTagList>;

          using Base =
            impl_VL::dynamic_vector_holder_aggregator<impl::individual_pointer_holder_vector_type_decider<constant>::template Type,
                                                      layout_holder<T>,
                                                      true,
                                                      typename InterfaceDescription::InterfaceInformation<T>::template flattened_properties_by_type<>,
                                                      size_type>;

          using MemContextInfo = typename std::decay_t<decltype(IndividualPointerHolder::memory_context())>::ContextInfo;

          friend Base;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
          {
            return IndividualPointerHolder::memory_context();
          }

         protected:

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const MemContextInfo &
          memory_context_info() const
          {
            return static_cast<const MemContextInfo &>(*this);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr MemContextInfo & memory_context_info()
          {
            return static_cast<MemContextInfo &>(*this);
          }

          const size_type * m_size_tag_arr;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_resize(const PropertiesIndexer, const size_type)
          {
            static_assert(Utility::always_false<SizeTag>, "IndividualPointerHolder does not allow resizing!");
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_reserve(const PropertiesIndexer, const size_type)
          {
            static_assert(Utility::always_false<SizeTag>, "IndividualPointerHolder does not allow reserving!");
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_clear(const PropertiesIndexer)
          {
            static_assert(Utility::always_false<SizeTag>, "IndividualPointerHolder does not allow clearing!");
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class SizeTag, PropertiesIndexer extent>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void base_shrink_to_fit(const PropertiesIndexer)
          {
            static_assert(Utility::always_false<SizeTag>, "IndividualPointerHolder does not allow shrinking to fit!");
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const size_type * get_tagged_size_pointer(const PropertiesIndexer mult_arr_idx) const
          {
            return m_size_tag_arr + SzTgHelper::template get_tag_index<Tag>(mult_arr_idx);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type get_tagged_size(const PropertiesIndexer mult_arr_idx) const
          {
            size_type ret = 0;

            MemoryContexts::memcopy_with_context(MemoryContexts::Current {},
                                                 memory_context(),
                                                 MemoryContexts::Current::ContextInfo {},
                                                 this->memory_context_info(),
                                                 &ret,
                                                 this->template get_tagged_size_pointer<Tag>(mult_arr_idx),
                                                 1);

            return ret;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void set_tagged_size(const PropertiesIndexer, const size_type)
          {
            static_assert(Utility::always_false<Tag>, "IndividualPointerHolder does not allow changing sizes!");
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag, bool warn = true>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_capacity(const PropertiesIndexer mult_arr_idx) const
          {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
            impl::unintended_behaviour_warner<warn>::warn_get_capacity_iph();
#endif
            return this->template get_tagged_size<Tag>(mult_arr_idx);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const auto & get_size_tag_array() const
          {
            return m_size_tag_arr;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto & get_size_tag_array()
          {
            return m_size_tag_arr;
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr const size_type *
          get_size_tag_array_pointer() const
          {
            return m_size_tag_arr;
          }

         private:

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_for_array(const PropertiesIndexer mult_arr_idx) const
          {
            using MatchingTag = std::decay_t<decltype(SzTgHelper::template get_tag_matching_property<Property>())>;

            return this->template get_tagged_size<MatchingTag>(mult_arr_idx);
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx, class C>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void initialize_from_collection_helper(C && c)
          {
            for (PropertiesIndexer i = 0; i < PropEx::extent; ++i)
              {
                this->template get_array<PropEx::Property>(i) = std::forward<C>(c).get_array_pointer(i);
              }
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class C, class... PropExs>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void initialize_from_collection(C && c, const Utility::TypeHolder<PropExs...> &)
          {
            this->memory_context_info() = std::forward<C>(c).memory_context_info();

            this->m_size_tag_arr = std::forward<C>(c).get_size_tag_array_pointer();

            (initialize_from_collection_helper<PropExs>(std::forward<C>(c)), ...);
          }

         public:

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
            class OtherLayout,
            class Properties,
            class MetaInfo,
            bool valid     = OtherLayout::can_get_array_pointers(),
            class disabler = std::enable_if_t < constant && valid >>
                                                              MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(
                                                                const Collections::Collection<OtherLayout, Properties, MetaInfo> & c)
          {
            initialize_from_collection(c, typename InterfaceDescription::InterfaceInformation<T>::template flattened_per_item<> {});
          }

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherLayout,
                                                                   class Properties,
                                                                   class MetaInfo,
                                                                   bool valid     = OtherLayout::can_get_array_pointers(),
                                                                   class disabler = std::enable_if_t<valid>>
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(Collections::Collection<OtherLayout, Properties, MetaInfo> & c)
          {
            initialize_from_collection(c, typename InterfaceDescription::InterfaceInformation<T>::template flattened_per_item<> {});
          }

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
          __pragma("nv_diag_suppress 20012")
  #else
          _Pragma("nv_diag_suppress 20012")
  #endif
#endif
            MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
            MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(const layout_holder &) = default;

          MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
          MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr layout_holder(layout_holder &&) = default;

          //This means that by-value copies of this will work as intended.

#if defined(__NVCC__) && !(defined(__clang__) && defined(__CUDA__))
  #ifdef _MSVC
          __pragma("nv_diag_default 20012")
  #else
          _Pragma("nv_diag_default 20012")
  #endif
#endif
        };
      };
    }
  }

  namespace Collections
  {
    namespace impl
    {
#if MARIONETTE_WARN_ON_DEFAULT_PASS_BY_VALUE
      template <bool b>
      struct default_pass_by_value_warner
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_pass_by_value()
        {
        }
      };

      template <>
      struct default_pass_by_value_warner<true>
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        [[deprecated("This is not really a deprecation warning. This call to 'pass_by_value' will fall back "
                     "to the default strategy of copying a pointer to the beginning of every per-item array. "
                     "Depending on the actual layout, there may be a more efficient way of doing this. "
                     "Please ask the implementer to provide a specialization for the layout type, which may "
                     "implement a more efficient alternative (or rely on this default behaviour without emitting warnings). "
                     "Provide an additional 'false' template parameter when calling 'pass_by_value' to suppress this warning. "
                     "Define 'MARIONETTE_WARN_ON_DEFAULT_PASS_BY_VALUE' to 0 to suppress all warnings about this.")]]
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void warn_pass_by_value()
        {
        }
      };
#endif
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool warn = true,
                                                             class Layout,
                                                             class Properties,
                                                             class MetaInfo,
                                                             std::enable_if_t<Layout::can_get_array_pointers() && Layout::has_size_tag_array(), bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto pass_by_value(const Collection<Layout, Properties, MetaInfo> & c)
    {
#if MARIONETTE_WARN_ON_DEFAULT_PASS_BY_VALUE
      impl::default_pass_by_value_warner<warn>::warn_pass_by_value();
#endif
      return Collection<LayoutTypes::Internal::IndividualPointerHolder<Layout, true>, Properties, MetaInfo>(c);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool warn = true,
                                                             class Layout,
                                                             class Properties,
                                                             class MetaInfo,
                                                             std::enable_if_t<Layout::can_get_array_pointers() && Layout::has_size_tag_array(), bool> = true>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto pass_by_value(Collection<Layout, Properties, MetaInfo> & c)
    {
#if MARIONETTE_WARN_ON_DEFAULT_PASS_BY_VALUE
      impl::default_pass_by_value_warner<warn>::warn_pass_by_value();
#endif
      return Collection<LayoutTypes::Internal::IndividualPointerHolder<Layout, false>, Properties, MetaInfo>(c);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <template <class...> class VectorLike,
                                                             template <class...>
                                                             class VectorLikeForSizes,
                                                             class SizeType,
                                                             class DifferenceType,
                                                             class... Extra,
                                                             class Properties,
                                                             class MetaInfo>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto pass_by_value(
      const Collection<LayoutTypes::VectorLikePerItem<VectorLike, VectorLikeForSizes, SizeType, DifferenceType, Extra...>, Properties, MetaInfo> & c)
    {
      using LayoutType = LayoutTypes::VectorLikePerItem<VectorLike, VectorLikeForSizes, SizeType, DifferenceType, Extra...>;

      return Collection<LayoutTypes::Internal::IndividualPointerHolder<LayoutType, true>, Properties, MetaInfo>(c);
    }

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <template <class...> class VectorLike,
                                                             template <class...>
                                                             class VectorLikeForSizes,
                                                             class SizeType,
                                                             class DifferenceType,
                                                             class... Extra,
                                                             class Properties,
                                                             class MetaInfo>
    MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto
    pass_by_value(Collection<LayoutTypes::VectorLikePerItem<VectorLike, VectorLikeForSizes, SizeType, DifferenceType, Extra...>, Properties, MetaInfo> & c)
    {
      using LayoutType = LayoutTypes::VectorLikePerItem<VectorLike, VectorLikeForSizes, SizeType, DifferenceType, Extra...>;

      return Collection<LayoutTypes::Internal::IndividualPointerHolder<LayoutType, false>, Properties, MetaInfo>(c);
    }

    //Written separately here to suppress the warning,
    //as there is no other logical way to store a vector like per item by value...
    //(If the user has a specific allocator that brings everything together or something,
    // they are always free to specialize for that particular VectorLike/VectorLikeForSizes.)
  }
}

#endif
