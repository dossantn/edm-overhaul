//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//
// Dear emacs, this is -*- c++ -*-
//

#ifndef MARIONETTE_COLLECTION_H
#define MARIONETTE_COLLECTION_H

#include <utility>
#include <type_traits>

#include "MarionetteBase.h"
#include "CollectionHelpers.h"
#include "LayoutTypes/InternalLayoutTypes.h"

#if MARIONETTE_USE_ASSERTIONS
  #include <cassert>
#endif

#if MARIONETTE_INCLUDE_POTENTIALLY_THROWING_FUNCTIONS
  #include <stdexcept>
#endif

namespace Marionette
{
  namespace Collections
  {
    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class T, class MetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CollectionHolder
    {
      static_assert(Utility::always_false<T>, "Please initialize the Collection properly with a TypeHolder of properties!");
    };

    MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS template <class Layout, class... Properties, class MetaInfo>
    struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES CollectionHolder<Layout, Utility::TypeHolder<Properties...>, MetaInfo> :
      public std::decay_t<decltype(impl::get_collection_base_classes<CollectionHolder<Layout, Utility::TypeHolder<Properties...>, MetaInfo>,
                                                        Layout,
                                                        Utility::TypeHolder<Properties...>,
                                                        MetaInfo>(impl::get_obtainable_collection_properties(Utility::TypeHolder<Properties...> {})))>
    {
      static_assert(sizeof...(Properties) > 0, "At least one property is required for the collection to work...");
      static_assert(MetaInfo::extent > 0, "A positive extent is assumed throughout the code.");
      static_assert(Layout::interface_properties().valid(), "The Layout must have valid memory to be instantiated!");

      friend struct Utility::FriendshipProvider;

      template <class A, class B, class C>
      friend struct OwningObject;

      template <class A, class B, bool c, bool d>
      friend struct ProxyObject;

      template <class A, class B, ResizeProperties c, MutabilityProperties d, bool e>
      friend struct LayoutTypes::Internal::ViewLayout;

      template <class A, class B, class C>
      friend struct CollectionHolder;

      template <class A, class B, class C>
      friend struct Object;

      template <class A, class B>
      friend struct impl::OwningPointerHolder;

     public:

      using LayoutType = Layout;

      using PropertyList = Utility::TypeHolder<Properties...>;

      static_assert(InterfaceDescription::check_validity(PropertyList {}), "Must be a valid set of properties!");

      using size_type       = typename Layout::size_type;
      using difference_type = typename Layout::difference_type;

     protected:

      using ObtainableProperties = std::decay_t<decltype(impl::get_obtainable_collection_properties(PropertyList {}))>;

      using Base = std::decay_t<decltype(impl::get_collection_base_classes<CollectionHolder, Layout, PropertyList, MetaInfo>(ObtainableProperties {}))>;

      using TrueBase = typename Layout::template layout_holder<PropertyList>;

     public:

      ///@brief The number of arrays this collection represents.
      ///       (For unspecified access into a PropertyArray.)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr PropertiesIndexer extent()
      {
        return MetaInfo::extent;
      }

      ///@brief The tag from which this collection derives its size.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto size_tag()
      {
        return typename MetaInfo::SizeTag {};
      }

      ///@brief The kind of interface one will provide for this collection.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr InterfaceProperties
      interface_properties()
      {
        return Layout::interface_properties();
      }

      //--------------------------------------------------------------------------------------------------

     public:

      ///@brief The memory context where this collection will store its data.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr auto memory_context()
      {
        return Layout::memory_context();
      }

      ///@brief The memory context information related to this collection.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) memory_context_info() const
      {
        return TrueBase::memory_context_info();
      }

      ///@brief The memory context information related to this collection.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) memory_context_info()
      {
        return TrueBase::memory_context_info();
      }

      //--------------------------------------------------------------------------------------------------

     protected:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = Layout::interface_properties().can_refer_to_const_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(const PropertiesIndexer mult_arr_idx) const
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call get_array with a property that corresponds to a PerItemObject.");

        return TrueBase::template get_array<Property>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = Layout::interface_properties().can_refer_to_modifiable_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(const PropertiesIndexer mult_arr_idx)
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call get_array with a property that corresponds to a PerItemObject.");

        return TrueBase::template get_array<Property>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = Layout::interface_properties().can_refer_to_const_object(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array(const PropertiesIndexer mult_arr_idx, const size_type idx) const
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call get_array with a property that corresponds to a PerItemObject.");

        return TrueBase::template get_array<Property>(mult_arr_idx, idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid = Layout::interface_properties().can_refer_to_const_array() &&
                                                                            impl::layout_can_get_array_pointers(Layout {}),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array_pointer(const PropertiesIndexer mult_arr_idx,
                                                                                                       const size_type         idx = 0) const
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call get_array_pointer with a property that corresponds to a PerItemObject.");

        return TrueBase::template get_array_pointer<Property>(mult_arr_idx, idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid = Layout::interface_properties().can_refer_to_modifiable_array() &&
                                                                            impl::layout_can_get_array_pointers(Layout {}),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_array_pointer(const PropertiesIndexer mult_arr_idx,
                                                                                                       const size_type         idx = 0)
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call get_array_pointer with a property that corresponds to a PerItemObject.");

        return TrueBase::template get_array_pointer<Property>(mult_arr_idx, idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = Layout::interface_properties().can_refer_to_const_array() &&
                                                                            impl::layout_has_size_tag_array(Layout {}),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_array() const
      {
        return TrueBase::get_size_tag_array();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = Layout::interface_properties().can_refer_to_modifiable_array() &&
                                                                            impl::layout_has_size_tag_array(Layout {}),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_array()
      {
        return TrueBase::get_size_tag_array();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = Layout::interface_properties().can_refer_to_const_array() &&
                                                                            impl::layout_can_get_array_pointers(Layout {}) &&
                                                                            impl::layout_has_size_tag_array(Layout {}),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_array_pointer() const
      {
        return TrueBase::get_size_tag_array_pointer();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = Layout::interface_properties().can_refer_to_modifiable_array() &&
                                                                            impl::layout_can_get_array_pointers(Layout {}) &&
                                                                            impl::layout_has_size_tag_array(Layout {}),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_size_tag_array_pointer()
      {
        return TrueBase::get_size_tag_array_pointer();
      }

      ///@brief Copy @p num elements from the array, at an optional @p offset,
      ///       into pointer @p ptr, with potential additional arguments
      ///       (that may not be a template parameter depending on the context).
      ///
      ///@remark We assume @p ptr holds enough memory.

      ///@remark The eligible additional arguments (and how they change the behaviour)
      ///        should be documented on a per-context basis. The case where there are no
      ///        additional arguments should always be supported (required information
      ///        is meant to be stored inside the appropriate @c ContextInfo).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               class DestContext,
                                                               class... Args,
                                                               bool valid     = Layout::interface_properties().can_copy_from_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_copy_from(const PropertiesIndexer                   mult_arr_idx,
                                                                                           const DestContext &                       dest_context,
                                                                                           const typename DestContext::ContextInfo & context_info,
                                                                                           typename Property::Type *                 ptr,
                                                                                           const size_type                           num,
                                                                                           const size_type                           offset,
                                                                                           Args &&... args) const
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call array_copy_from with a property that corresponds to a PerItemObject.");

        TrueBase::template array_copy_from<Property>(mult_arr_idx, dest_context, context_info, ptr, num, offset, std::forward<Args>(args)...);
      }

      ///@brief Copy @p num elements from pointer @p ptr into the array,
      ///       at an optional @p offset, with potential additional arguments
      ///       (that may not be a template parameter depending on the context).
      ///
      ///@remark We assume @p ptr holds enough memory.

      ///@remark The eligible additional arguments (and how they change the behaviour)
      ///        should be documented on a per-context basis. The case where there are no
      ///        additional arguments should always be supported (required information
      ///        is meant to be stored inside the appropriate @c ContextInfo).
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               class SourceContext,
                                                               class... Args,
                                                               bool valid     = Layout::interface_properties().can_copy_to_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_copy_to(const PropertiesIndexer                     mult_arr_idx,
                                                                                         const SourceContext &                       source_context,
                                                                                         const typename SourceContext::ContextInfo & context_info,
                                                                                         const typename Property::Type *             ptr,
                                                                                         const size_type                             num,
                                                                                         const size_type                             offset,
                                                                                         Args &&... args)
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call array_copy_from with a property that corresponds to a PerItemObject.");

        TrueBase::template array_copy_to<Property>(mult_arr_idx, source_context, context_info, ptr, num, offset, std::forward<Args>(args)...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = Layout::interface_properties().can_resize_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_resize(const PropertiesIndexer mult_arr_idx, const size_type new_size)
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call array_resize with a property that corresponds to a PerItemObject.");

        TrueBase::template array_resize<Property>(mult_arr_idx, new_size);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = Layout::interface_properties().can_resize_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_reserve(const PropertiesIndexer mult_arr_idx, const size_type new_size)
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call array_reserve with a property that corresponds to a PerItemObject.");

        TrueBase::template array_reserve<Property>(mult_arr_idx, new_size);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = Layout::interface_properties().can_resize_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_clear(const PropertiesIndexer mult_arr_idx)
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call array_clear with a property that corresponds to a PerItemObject.");

        TrueBase::template array_clear<Property>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool valid     = Layout::interface_properties().can_resize_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void array_shrink_to_fit(const PropertiesIndexer mult_arr_idx)
      {
        static_assert(InterfaceDescription::classify_property(Property {}) == InterfaceDescription::SupportedTypes::PerItemObject,
                      "One can only call array_shrink_to_fit with a property that corresponds to a PerItemObject.");

        TrueBase::template array_shrink_to_fit<Property>(mult_arr_idx);
      }

      //--------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                               bool valid     = Layout::interface_properties().can_read_sizes(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_size_pointer(const PropertiesIndexer mult_arr_idx) const
      {
        return TrueBase::template get_tagged_size_pointer<Tag>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                               bool valid     = Layout::interface_properties().can_resize_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_size_pointer(const PropertiesIndexer mult_arr_idx)
      {
        return TrueBase::template get_tagged_size_pointer<Tag>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                               bool valid     = Layout::interface_properties().can_read_sizes(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_size(const PropertiesIndexer mult_arr_idx) const
      {
        return TrueBase::template get_tagged_size<Tag>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                               bool valid     = Layout::interface_properties().can_read_sizes(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_tagged_capacity(const PropertiesIndexer mult_arr_idx) const
      {
        return TrueBase::template get_tagged_capacity<Tag>(mult_arr_idx);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Tag,
                                                               bool valid     = Layout::interface_properties().can_resize_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void set_tagged_size(const PropertiesIndexer mult_arr_idx, const size_type new_size)
      {
        TrueBase::template set_tagged_size<Tag>(mult_arr_idx, new_size);
      }

      //--------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool
      layout_stores_multi_array_index()
      {
        return impl::layout_has_multi_array_index(Layout {});
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array_index()
        const
      {
        if constexpr (layout_stores_multi_array_index())
          {
            return TrueBase::get_multi_array_index();
          }
        else
          {
            return PropertiesIndexer {0};
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array_index()
      {
        if constexpr (layout_stores_multi_array_index())
          {
            return TrueBase::get_multi_array_index();
          }
        else
          {
            return PropertiesIndexer {0};
          }
      }

      //--------------------------------------------------------------------------------------------------

     protected:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_read_sizes(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) size_with_index(const PropertiesIndexer mult_arr_idx) const
      {
        using MaybeGlobal = std::decay_t<decltype(InterfaceDescription::get_global_property(PropertyList {}))>;

        if constexpr (InterfaceDescription::single_global_property(PropertyList {}) && !InterfaceDescription::track_individual_size(MaybeGlobal {}))
          {
            return MaybeGlobal::template global_object_size<typename MetaInfo::SizeTag>(mult_arr_idx);
          }
        else
          {
            return this->template get_tagged_size<typename MetaInfo::SizeTag>(mult_arr_idx);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_read_sizes(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) size_with_index(const PropertiesIndexer mult_arr_idx)
      {
        using MaybeGlobal = std::decay_t<decltype(InterfaceDescription::get_global_property(PropertyList {}))>;

        if constexpr (InterfaceDescription::single_global_property(PropertyList {}) && !InterfaceDescription::track_individual_size(MaybeGlobal {}))
          {
            return MaybeGlobal::template global_object_size<typename MetaInfo::SizeTag>(mult_arr_idx);
          }
        else
          {
            return this->template get_tagged_size<typename MetaInfo::SizeTag>(mult_arr_idx);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_read_sizes(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) capacity_with_index(const PropertiesIndexer mult_arr_idx) const
      {
        using MaybeGlobal = std::decay_t<decltype(InterfaceDescription::get_global_property(PropertyList {}))>;

        if constexpr (InterfaceDescription::single_global_property(PropertyList {}) && !InterfaceDescription::track_individual_size(MaybeGlobal {}))
          {
            return MaybeGlobal::template global_object_capacity<typename MetaInfo::SizeTag>(mult_arr_idx);
          }
        else
          {
            return this->template get_tagged_capacity<typename MetaInfo::SizeTag>(mult_arr_idx);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_read_sizes(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) capacity_with_index(const PropertiesIndexer mult_arr_idx)
      {
        using MaybeGlobal = std::decay_t<decltype(InterfaceDescription::get_global_property(PropertyList {}))>;

        if constexpr (InterfaceDescription::single_global_property(PropertyList {}) && !InterfaceDescription::track_individual_size(MaybeGlobal {}))
          {
            return MaybeGlobal::template global_object_capacity<typename MetaInfo::SizeTag>(mult_arr_idx);
          }
        else
          {
            return this->template get_tagged_capacity<typename MetaInfo::SizeTag>(mult_arr_idx);
          }
      }

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_read_sizes(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) size() const
      {
        if constexpr (MetaInfo::extent == 1)
          {
            return this->size_with_index(this->get_multi_array_index());
            //We don't merge this with the general case since we may want to return a reference...
          }
        else
          {
            const PropertiesIndexer base_index = this->get_multi_array_index() * MetaInfo::extent;
            size_type               min_size   = this->size_with_index(base_index);

            for (PropertiesIndexer i = 1; i < MetaInfo::extent; ++i)
              {
                const size_type this_size = this->size_with_index(base_index + i);

                min_size = this_size < min_size ? this_size : min_size;
                //Let's not pull in <algorithm> just for this.
              }

            return min_size;
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_read_sizes(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) capacity() const
      {
        if constexpr (MetaInfo::extent == 1)
          {
            return this->capacity_with_index(this->get_multi_array_index());
            //We don't merge this with the general case since we may want to return a reference...
          }
        else
          {
            const PropertiesIndexer base_index = this->get_multi_array_index() * MetaInfo::extent;

            size_type min_capacity = this->capacity_with_index(base_index);

            for (PropertiesIndexer i = 1; i < MetaInfo::extent; ++i)
              {
                const size_type this_capacity = this->capacity_with_index(base_index + i);

                min_capacity = this_capacity < min_capacity ? this_capacity : min_capacity;
                //Let's not pull in <algorithm> just for this.
              }

            return min_capacity;
          }
      }

      //--------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool layout_is_non_owning()
      {
        return impl::layout_is_pointer_storage(Layout {});
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_layout_holder_pointer()
        const
      {
        if constexpr (layout_is_non_owning())
          {
            return TrueBase::get_layout_holder_pointer();
          }
        else
          {
            return static_cast<const TrueBase *>(this);
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_layout_holder_pointer()
      {
        if constexpr (layout_is_non_owning())
          {
            return TrueBase::get_layout_holder_pointer();
          }
        else
          {
            return static_cast<TrueBase *>(this);
          }
      }

     public:

      ///@brief Returns either a pointer or a pointer-like object
      ///       from which the underlying data can be accessed
      ///       for as long as the original, owning collection
      ///       remains in scope.
      ///       (Useful for array access through temporary non-owning views.)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_owning_pointer() const
      {
        if constexpr (layout_is_non_owning())
          {
            if constexpr (layout_stores_multi_array_index())
              {
                return impl::owning_pointer_holder<CollectionHolder, const CollectionHolder>(this->get_layout_holder_pointer(), this->get_multi_array_index());
              }
            else
              {
                return impl::owning_pointer_holder<CollectionHolder, const CollectionHolder>(this->get_layout_holder_pointer());
              }
          }
        else
          {
            return this;
          }
      }

      ///@brief Returns either a pointer or a pointer-like object
      ///       from which the underlying data can be accessed
      ///       for as long as the original, owning collection
      ///       remains in scope.
      ///       (Useful for accessing through temporary non-owning views.)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_owning_pointer()
      {
        if constexpr (layout_is_non_owning())
          {
            if constexpr (layout_stores_multi_array_index())
              {
                return impl::owning_pointer_holder<CollectionHolder>(this->get_layout_holder_pointer(), this->get_multi_array_index());
              }
            else
              {
                return impl::owning_pointer_holder<CollectionHolder>(this->get_layout_holder_pointer());
              }
          }
        else
          {
            return this;
          }
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Returns a constant view into the collection
      ///       with just a sub-set of its properties.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... SlicedProperties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_const_sliced_view() const
      {
        static_assert((PropertyList::template contains<SlicedProperties>() && ...), "All properties must be part of the collection!");

        using FinalType = Utility::TypeHolder<SlicedProperties...>;

        using PtrType = std::decay_t<decltype(this->get_layout_holder_pointer())>;

        if constexpr (layout_is_non_owning())
          {
            using WantedLayout = LayoutTypes::Internal::ViewLayout<typename Layout::OriginalLayoutType,
                                                                   PtrType,
                                                                   ResizeProperties::NoResize,
                                                                   MutabilityProperties::NoModify,
                                                                   layout_stores_multi_array_index()>;

            if constexpr (layout_stores_multi_array_index())
              {
                return Collection<WantedLayout, FinalType, MetaInfo> {this->get_layout_holder_pointer(), this->get_multi_array_index()};
              }
            else
              {
                return Collection<WantedLayout, FinalType, MetaInfo> {this->get_layout_holder_pointer()};
              }
          }
        else
          {
            static_assert(!layout_stores_multi_array_index());

            using WantedLayout =
              LayoutTypes::Internal::ViewLayout<Layout, PtrType, ResizeProperties::NoResize, MutabilityProperties::NoModify, layout_stores_multi_array_index()>;

            return Collection<WantedLayout, FinalType, MetaInfo> {this->get_layout_holder_pointer()};
          }
      }

      ///@brief Returns a view into the collection
      ///       with just a sub-set of its properties.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... SlicedProperties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_sliced_view() const
      {
        static_assert((PropertyList::template contains<SlicedProperties>() && ...), "All properties must be part of the collection!");

        return get_const_sliced_view<SlicedProperties...>();
      }

      ///@brief Returns a view into the collection
      ///       with just a sub-set of its properties.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... SlicedProperties>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_sliced_view()
      {
        static_assert((PropertyList::template contains<SlicedProperties>() && ...), "All properties must be part of the collection!");

        using FinalType = Utility::TypeHolder<SlicedProperties...>;

        constexpr ResizeProperties wanted_resize =
          (((impl::resizable_property_helper(SlicedProperties {}) && ...) || (sizeof...(SlicedProperties) == sizeof...(Properties))) ?
             ResizeProperties::Resize :
             ResizeProperties::ResizePartial);

        using PtrType = std::decay_t<decltype(this->get_layout_holder_pointer())>;

        if constexpr (layout_is_non_owning())
          {
            using WantedLayout = LayoutTypes::Internal::
              ViewLayout<typename Layout::OriginalLayoutType, PtrType, wanted_resize, MutabilityProperties::Full, layout_stores_multi_array_index()>;

            if constexpr (layout_stores_multi_array_index())
              {
                return Collection<WantedLayout, FinalType, MetaInfo> {this->get_layout_holder_pointer(), this->get_multi_array_index()};
              }
            else
              {
                return Collection<WantedLayout, FinalType, MetaInfo> {this->get_layout_holder_pointer()};
              }
          }
        else
          {
            static_assert(!layout_stores_multi_array_index());

            using WantedLayout =
              LayoutTypes::Internal::ViewLayout<Layout, PtrType, wanted_resize, MutabilityProperties::Full, layout_stores_multi_array_index()>;

            return Collection<WantedLayout, FinalType, MetaInfo> {this->get_layout_holder_pointer()};
          }
      }

      ///@brief Returns a constant view into the collection.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_const_view() const
      {
        return get_const_sliced_view<Properties...>();
      }

      ///@brief Returns a view into the collection.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_view() const
      {
        return get_sliced_view<Properties...>();
      }

      ///@brief Returns a view into the collection.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_view()
      {
        return get_sliced_view<Properties...>();
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Returns an owning copy of the collection
      ///       with just a sub-set of its properties.
      ///
      ///@remark It is not possible to create an owning copy of a
      ///        property array with unspecified access.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... CopiedProperties,
                                                               bool valid     = MetaInfo::extent == 1,
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_sliced_copy() const
      {
        static_assert((PropertyList::template contains<CopiedProperties>() && ...), "All properties must be part of the collection!");

        using FinalType = Utility::TypeHolder<CopiedProperties...>;

        if constexpr (layout_is_non_owning())
          {
            return Collection<typename Layout::OriginalLayoutType, FinalType, MetaInfo>(*this);
          }
        else
          {
            return Collection<Layout, FinalType, MetaInfo>(*this);
          }
      }

      ///@brief Returns an owning copy of the collection.
      ///
      ///@remark It is not possible to create an owning copy of a
      ///        property array with unspecified access.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = (MetaInfo::extent == 1), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_owning_copy() const
      {
        return get_sliced_copy<Properties...>();
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Returns @c true if this collection has a single (non-global) @c PerItemObject.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_single_property_collection()
      {
        return InterfaceDescription::single_per_item_property<ignore_globals>(PropertyList {});
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool allows_simple_accessor()
      {
        return is_single_property_collection<true>() && (MetaInfo::extent == 1) && static_cast<bool>(MARIONETTE_USE_SIMPLE_ACCESSORS);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid =
                                                                 (allows_simple_accessor() && Layout::interface_properties().can_refer_to_const_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) simple_accessor(const size_type i) const
      {
        using WantedType = decltype(InterfaceDescription::get_per_item_property(PropertyList {}));
        return this->template get_array<WantedType>(this->get_multi_array_index(), i);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid =
                                                                 (allows_simple_accessor() && Layout::interface_properties().can_refer_to_modifiable_object()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) simple_accessor(const size_type i)
      {
        using WantedType = decltype(InterfaceDescription::get_per_item_property(PropertyList {}));
        return this->template get_array<WantedType>(this->get_multi_array_index(), i);
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Returns @c true if this collection has a single (non-global) @c PropertyArray.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_array_property_collection()
      {
        return InterfaceDescription::single_property_array_property<ignore_globals>(PropertyList {});
      }

     private:

      using PropertyArrayType = decltype(InterfaceDescription::get_property_array_property(PropertyList {}));

     public:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool specifies_access_operator()
      {
        return (is_array_property_collection<true>() && MetaInfo::extent == 1);
        //If the extent is greater, we will get wrong indexing, so we won't even allow that.
      }

      ///@brief Access each separate entry of the @c PropertyArray.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = specifies_access_operator(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator[](const PropertiesIndexer mult_arr_idx) const
      {
        constexpr PropertiesIndexer multi_array_size = PropertyArrayType::number;

#if MARIONETTE_USE_ASSERTIONS
        assert(mult_arr_idx >= 0 && mult_arr_idx < multi_array_size);
#endif

        const PropertiesIndexer final_index = this->get_multi_array_index() * multi_array_size + mult_arr_idx;
        
        return impl::
          build_multi_array_indexed<ResizeProperties::NoResize, MutabilityProperties::NoModify, Layout, impl::properties_getter<PropertyArrayType>, MetaInfo>(
            this->get_layout_holder_pointer(),
            final_index);
      }

      ///@brief Access each separate entry of the @c PropertyArray.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = specifies_access_operator(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) operator[](const PropertiesIndexer mult_arr_idx)
      {
        constexpr PropertiesIndexer multi_array_size = PropertyArrayType::number;

#if MARIONETTE_USE_ASSERTIONS
        assert(mult_arr_idx >= 0 && mult_arr_idx < multi_array_size);
#endif

        const PropertiesIndexer final_index = this->get_multi_array_index() * multi_array_size + mult_arr_idx;
        
        return impl::
          build_multi_array_indexed<ResizeProperties::ResizePartial, MutabilityProperties::Full, Layout, impl::properties_getter<PropertyArrayType>, MetaInfo>(
            this->get_layout_holder_pointer(),
            final_index);
      }

#if MARIONETTE_INCLUDE_POTENTIALLY_THROWING_FUNCTIONS

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = specifies_access_operator(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES decltype(auto) at(const PropertiesIndexer mult_arr_idx)
      {
        constexpr PropertiesIndexer multi_array_size = PropertyArrayType::number;

        if (mult_arr_idx >= 0 && mult_arr_idx < multi_array_size)
          {
            return (*this)[mult_arr_idx];
          }
        else
          {
            throw std::out_of_range {""};
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = specifies_access_operator(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES decltype(auto) at(const PropertiesIndexer mult_arr_idx) const
      {
        constexpr PropertiesIndexer multi_array_size = PropertyArrayType::number;

        if (mult_arr_idx >= 0 && mult_arr_idx < multi_array_size)
          {
            return (*this)[mult_arr_idx];
          }
        else
          {
            throw std::out_of_range {""};
          }
      }

#endif

      //--------------------------------------------------------------------------------------------------

      ///@brief Force an unspecified access into a property array into a specific index.
      ///
      ///@remark Useful for writing the transfers, probably not in normal usage...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer mult_arr_idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array() const
      {
        static_assert(mult_arr_idx < MetaInfo::extent);

        const PropertiesIndexer final_index = this->get_multi_array_index() * MetaInfo::extent + mult_arr_idx;

        return impl::build_multi_array_indexed<ResizeProperties::NoResize, MutabilityProperties::NoModify, Layout, PropertyList, MetaInfo>(
          this->get_layout_holder_pointer(),
          final_index);
      }

      ///@brief Force an unspecified access into a property array into a specific index.
      ///
      ///@remark Useful for writing the transfers, probably not in normal usage...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer mult_arr_idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_multi_array()
      {
        static_assert(mult_arr_idx < MetaInfo::extent);

        const PropertiesIndexer final_index = this->get_multi_array_index() * MetaInfo::extent + mult_arr_idx;

        return impl::build_multi_array_indexed<(MetaInfo::extent == 1 ? ResizeProperties::Resize : ResizeProperties::ResizePartial),
                                               MutabilityProperties::Full,
                                               Layout,
                                               PropertyList,
                                               MetaInfo>(this->get_layout_holder_pointer(), final_index);
      }

      ///@brief Force an unspecified access into a property array into a specific index.
      ///       Returns a possibly resizable version of the collection for property @p Property,
      ///       mainly for use with global properties.
      ///
      ///@remark Useful for writing the transfers, probably not in normal usage...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer mult_arr_idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_actual_multi_array() const
      {
        return this->template get_multi_array<mult_arr_idx>();
      }

      ///@brief Force an unspecified access into a property array into a specific index.
      ///       Returns a possibly resizable version of the collection for property @p Property,
      ///       mainly for use with global properties.
      ///
      ///@remark Useful for writing the transfers, probably not in normal usage...
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <PropertiesIndexer mult_arr_idx>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_actual_multi_array()
      {
        static_assert(mult_arr_idx < MetaInfo::extent);

        const PropertiesIndexer final_index = this->get_multi_array_index() * MetaInfo::extent + mult_arr_idx;

        if constexpr (layout_is_non_owning())
          {
            return impl::
              build_multi_array_indexed<ResizeProperties::Resize, MutabilityProperties::Full, typename Layout::OriginalLayoutType, PropertyList, MetaInfo>(
                this->get_layout_holder_pointer(),
                final_index);
          }
        else
          {
            return impl::build_multi_array_indexed<ResizeProperties::Resize, MutabilityProperties::Full, Layout, PropertyList, MetaInfo>(this, final_index);
          }
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Returns @c true if this collection has a single (non-global) @c JaggedVector.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_jagged_vector_collection()
      {
        return InterfaceDescription::single_jagged_vector_property<ignore_globals>(PropertyList {});
      }

     private:

      using JVPropType = decltype(InterfaceDescription::get_jagged_vector_property(PropertyList {}));

      using JVCollType = decltype(impl::jagged_vector_collection_type_builder<is_jagged_vector_collection<true>(), Layout, JVPropType, MetaInfo>());

     protected:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = is_jagged_vector_collection<true>() &&
                                                                            Layout::interface_properties().can_refer_to_const_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) underlying_collection() const
      {
        return this->template get_collection<JVPropType>();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = is_jagged_vector_collection<true>() &&
                                                                            Layout::interface_properties().can_refer_to_modifiable_array(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) underlying_collection()
      {
        return this->template get_collection<JVPropType>();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = is_jagged_vector_collection<true>(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) actual_underlying_collection() const
      {
        return this->template get_actual_collection<JVPropType>();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid = is_jagged_vector_collection<true>(), class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) actual_underlying_collection()
      {
        return this->template get_actual_collection<JVPropType>();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid =
                                                                 (is_jagged_vector_collection<true>() && Layout::interface_properties().can_read_sizes()),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) total_size() const
      {
        return this->jagged_vector_underlying_size(this->get_multi_array_index());
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        bool valid     = Layout::interface_properties().can_refer_to_const_object(),
        class disabler = std::enable_if_t < valid && is_jagged_vector_collection<true>() >>
                                                       MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_item_starting_index(
                                                         const size_type item) const
      {
        return this->jagged_vector_sizes(this->get_multi_array_index(), item);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        bool valid     = Layout::interface_properties().can_refer_to_const_object(),
        class disabler = std::enable_if_t < valid && is_jagged_vector_collection<true>() >>
                                                       MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_item_past_last_index(
                                                         const size_type item) const
      {
        return this->jagged_vector_sizes(this->get_multi_array_index(), item + 1);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        bool valid     = Layout::interface_properties().can_refer_to_const_object(),
        class disabler = std::enable_if_t < valid && is_jagged_vector_collection<true>() >>
                                                       MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr size_type get_item_size(
                                                         const size_type item) const
      {
        return get_item_past_last_index(item) - get_item_starting_index(item);
      }

      ///@warning This does not change the overall size of the underlying container!
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_resize_jagged_vector_object(),
                                                               class disabler = std::enable_if_t < valid &&
                                                                                is_jagged_vector_collection<true>() >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
                                                                                  increase_item_size(const size_type item, const size_type delta)
      {
        for (size_type i = item; i < this->size(); ++i)
          {
            this->jagged_vector_sizes(this->get_multi_array_index(), i + 1) += delta;
          }
      }

      ///@warning This does not change the overall size of the underlying container!
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_resize_jagged_vector_object(),
                                                               class disabler = std::enable_if_t < valid &&
                                                                                is_jagged_vector_collection<true>() >>
                                                                                  MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void
                                                                                  decrease_item_size(const size_type item, const size_type delta)
      {
        for (size_type i = item; i < this->size(); ++i)
          {
            this->jagged_vector_sizes(this->get_multi_array_index(), i + 1) -= delta;
          }
      }

     public:

      ///@brief Updates the jagged vector size of a specific @p mult_arr_idx from an array of @c size_type that holds the prefix sum of the sizes.
      ///
      ///This array has to have `size + 1` elements, with arr[i + 1] = arr[i] + size of vector[i], starting with 0.
      ///
      ///@warning If used with an incorrect argument, this will lead to incorrect/corrupt data.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool suppress_warning = false,
                                                               class MemoryContext,
                                                               class... Args,
                                                               bool valid     = Layout::interface_properties().can_refer_to_modifiable_subcollection(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void update_jagged_vector_size(const PropertiesIndexer                     mult_arr_idx,
                                                                                                     const size_type *                           arr,
                                                                                                     const MemoryContext &                       context,
                                                                                                     const typename MemoryContext::ContextInfo & context_info,
                                                                                                     Args &&... args)
      {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
        impl::unintended_behaviour_warner<!suppress_warning>::warn_jv_update();
#endif

        this->template array_copy_to<InterfaceDescription::JaggedVectorSizeProperty<JVPropType>>(mult_arr_idx,
                                                                                                 context,
                                                                                                 context_info,
                                                                                                 arr,
                                                                                                 this->size_with_index(mult_arr_idx),
                                                                                                 0,
                                                                                                 std::forward<Args>(args)...);
      }

      ///@brief Updates the jagged vector size of a specific @p mult_arr_idx from an array of @c size_type that holds the prefix sum of the sizes.
      ///
      ///This array has to have `size + 1` elements, with arr[i + 1] = arr[i] + size of vector[i], starting with 0.
      ///
      ///@warning If used with an incorrect argument, this will lead to incorrect/corrupt data.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool suppress_warning = false,
                                                               bool valid            = Layout::interface_properties().can_refer_to_modifiable_subcollection(),
                                                               class disabler        = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void update_jagged_vector_size(const PropertiesIndexer mult_arr_idx,
                                                                                                     const size_type *       arr)
      {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
        impl::unintended_behaviour_warner<!suppress_warning>::warn_jv_update();
#endif

        this->template update_jagged_vector_size<false>(mult_arr_idx, arr, MemoryContexts::Current {}, MemoryContexts::Current::ContextInfo {});
      }

      ///@brief Updates the jagged vector sizes of all possible indices from an array of arrays of @c size_type that hold the prefix sum of the sizes.
      ///
      ///The outermost array has to have `extent` elements, one for each possible multiple array index,
      ///while the innermost array has to have `size + 1` elements, with arr[i + 1] = arr[i] + size of vector[i], starting with 0.
      ///
      ///@warning If used with an incorrect argument, this will lead to incorrect/corrupt data.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool suppress_warning = false,
                                                               class MemoryContext,
                                                               class... Args,
                                                               bool valid     = Layout::interface_properties().can_refer_to_modifiable_subcollection(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void update_jagged_vector_size(const size_type **                          arr_arr,
                                                                                                     const MemoryContext &                       context,
                                                                                                     const typename MemoryContext::ContextInfo & context_info,
                                                                                                     Args &&... args)
      {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
        impl::unintended_behaviour_warner<!suppress_warning>::warn_jv_update();
#endif

        for (PropertiesIndexer mult_arr_idx = 0; mult_arr_idx < MetaInfo::extent; ++mult_arr_idx)
          {
            this->template update_jagged_vector_size<false>(this->get_multi_array_index() + mult_arr_idx,
                                                            arr_arr[mult_arr_idx],
                                                            context,
                                                            context_info,
                                                            std::forward<Args>(args)...);
          }
      }

      ///@brief Updates the jagged vector sizes of all possible indices from an array of arrays of @c size_type that hold the prefix sum of the sizes.
      ///
      ///The outermost array has to have `extent` elements, one for each possible multiple array index,
      ///while the innermost array has to have `size + 1` elements, with arr[i + 1] = arr[i] + size of vector[i], starting with 0.
      ///
      ///@warning If used with an incorrect argument, this will lead to incorrect/corrupt data.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool suppress_warning = false,
                                                               bool valid            = Layout::interface_properties().can_refer_to_modifiable_subcollection(),
                                                               class disabler        = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void update_jagged_vector_size(const size_type ** arr_arr)
      {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
        impl::unintended_behaviour_warner<!suppress_warning>::warn_jv_update();
#endif

        for (PropertiesIndexer mult_arr_idx = 0; mult_arr_idx < MetaInfo::extent; ++mult_arr_idx)
          {
            this->template update_jagged_vector_size<false>(this->get_multi_array_index() + mult_arr_idx, arr_arr[mult_arr_idx]);
          }
      }

      ///@brief Updates the jagged vector size from an array of @c size_type that holds the prefix sum of the sizes.
      ///
      ///Valid for the specific (but common) case of a jagged vector outside of any property arrays, that is,
      ///with an extent of 1.
      ///
      ///This array has to have `size + 1` elements, with arr[i + 1] = arr[i] + size of vector[i], starting with 0.
      ///
      ///@warning If used with an incorrect argument, this will lead to incorrect/corrupt data.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool suppress_warning = false,
                                                               class MemoryContext,
                                                               class... Args,
                                                               bool valid = Layout::interface_properties().can_refer_to_modifiable_subcollection() &&
                                                                            MetaInfo::extent == 1,
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void update_jagged_vector_size(const size_type *                           arr,
                                                                                                     const MemoryContext &                       context,
                                                                                                     const typename MemoryContext::ContextInfo & context_info,
                                                                                                     Args &&... args)
      {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
        impl::unintended_behaviour_warner<!suppress_warning>::warn_jv_update();
#endif

        this->template update_jagged_vector_size<false>(this->get_multi_array_index(), arr, context, context_info, std::forward<Args>(args)...);
      }

      ///@brief Updates the jagged vector size from an array of @c size_type that holds the prefix sum of the sizes.
      ///
      ///Valid for the specific (but common) case of a jagged vector outside of any property arrays, that is,
      ///with an extent of 1.
      ///
      ///This array has to have `size + 1` elements, with arr[i + 1] = arr[i] + size of vector[i], starting with 0.
      ///
      ///@warning If used with an incorrect argument, this will lead to incorrect/corrupt data.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool suppress_warning = false,
                                                               bool valid            = Layout::interface_properties().can_refer_to_modifiable_subcollection() &&
                                                                            MetaInfo::extent == 1,
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void update_jagged_vector_size(const size_type * arr)
      {
#if MARIONETTE_WARN_ON_POTENTIALLY_UNINTENDED_BEHAVIOUR
        impl::unintended_behaviour_warner<!suppress_warning>::warn_jv_update();
#endif

        this->template update_jagged_vector_size<false>(this->get_multi_array_index(), arr);
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Returns @c true if this collection only has (non-global) @c NoObject properties.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool ignore_globals = true>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool is_no_object_collection()
      {
        return InterfaceDescription::only_no_object_properties<ignore_globals>(PropertyList {});
      }

      //--------------------------------------------------------------------------------------------------

      ///Returns array (or global object or sub-collection or jagged vector) for property @p Property.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool allow_any_property = false>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_collection() const
      {
        static_assert(allow_any_property || ObtainableProperties::template contains<Property>(), "Must get valid property!");

        if constexpr (layout_stores_multi_array_index())
          {
            return impl::get_new_collection<ResizeProperties::NoResize, MutabilityProperties::NoModify, Property, PropertyList, MetaInfo, Layout>(
              this->get_layout_holder_pointer(),
              this->get_multi_array_index());
          }
        else
          {
            return impl::get_new_collection<ResizeProperties::NoResize, MutabilityProperties::NoModify, Property, PropertyList, MetaInfo, Layout>(
              this->get_layout_holder_pointer());
          }
      }

      ///Returns array (or global object or sub-collection or jagged vector) for property @p Property.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool allow_any_property = false>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_collection()
      {
        static_assert(allow_any_property || ObtainableProperties::template contains<Property>(), "Must get valid property!");

        constexpr ResizeProperties resize_type =
          ((impl::resizable_property_helper(Property {}) || (is_jagged_vector_collection<true>() && std::is_same_v<Property, JVPropType>) ) ?
             ResizeProperties::Resize :
             ResizeProperties::ResizePartial);

        if constexpr (layout_stores_multi_array_index())
          {
            return impl::get_new_collection<resize_type, MutabilityProperties::Full, Property, PropertyList, MetaInfo, Layout>(
              this->get_layout_holder_pointer(),
              this->get_multi_array_index());
          }
        else
          {
            return impl::get_new_collection<resize_type, MutabilityProperties::Full, Property, PropertyList, MetaInfo, Layout>(
              this->get_layout_holder_pointer());
          }
      }

     protected:

      ///@brief Returns a possibly resizable version of the collection for property @p Property,
      ///mainly for use with global properties.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool allow_any_property = false>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_actual_collection() const
      {
        return this->template get_collection<Property, allow_any_property>();
        //The const version already returns a const collection.
      }

      ///@brief Returns a possibly resizable version of the collection for property @p Property,
      ///mainly for use with global properties.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property, bool allow_any_property = false>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_actual_collection()
      {
        static_assert(allow_any_property || ObtainableProperties::template contains<Property>(), "Must get valid property!");

        if constexpr (layout_stores_multi_array_index())
          {
            return impl::get_new_collection<ResizeProperties::Resize, MutabilityProperties::Full, Property, PropertyList, MetaInfo, Layout>(
              this->get_layout_holder_pointer(),
              this->get_multi_array_index());
          }
        else
          {
            return impl::get_new_collection<ResizeProperties::Resize, MutabilityProperties::Full, Property, PropertyList, MetaInfo, Layout>(
              this->get_layout_holder_pointer());
          }
      }

     public:

      ///Returns property @p Property of item at offset @p index
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool allow_any_property = false,
                                                               bool valid              = Layout::interface_properties().can_refer_to_const_object(),
                                                               class disabler          = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object(const size_type index) const
      {
        static_assert(allow_any_property || ObtainableProperties::template contains<Property>(), "Must get valid property!");

        const auto & coll = this->template get_collection<Property>();

        if constexpr (std::decay_t<decltype(coll)>::allows_simple_accessor())
          {
            return coll.simple_accessor(index);
          }
        else
          {
            return coll.make_const_read_proxy(index);
          }
      }

      ///Returns property @p Property of item at offset @p index
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Property,
                                                               bool allow_any_property = false,
                                                               bool valid              = Layout::interface_properties().can_refer_to_modifiable_object(),
                                                               class disabler          = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr decltype(auto) get_object(const size_type index)
      {
        static_assert(allow_any_property || ObtainableProperties::template contains<Property>(), "Must get valid property!");

        auto && coll = this->template get_collection<Property>();

        if constexpr (std::decay_t<decltype(coll)>::allows_simple_accessor())
          {
            return coll.simple_accessor(index);
          }
        else
          {
            return coll.make_read_proxy(index);
          }
      }

      //--------------------------------------------------------------------------------------------------

#if MARIONETTE_GENERIC_OPERATE_API_SHOULD_BE_PUBLIC

     public:

#else

     protected:

#endif

      //--------------------------------------------------------------------------------------------------

      ///@brief Apply @p op, with any additional arguments, to every element at @p index.
      ///
      ///@p op will receive as first argument the property definition (which is essentially an empty structure),
      ///then a reference to the corresponding object (or an appropriate proxy for jagged vectors and sub-collections),
      ///then any additional arguments.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Op, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void operate_on_all_objects(const size_type index, Op && op, Args &&... args) const
      {
        (std::forward<Op>(op)(Properties {}, this->template get_object<Properties>(index), std::forward<Args>(args)...), ...);
      }

      ///@brief Apply @p op, with any additional arguments, to every element at @p index.
      ///
      ///@p op will receive as first argument the property definition (which is essentially an empty structure),
      ///then a reference to the corresponding object (or an appropriate proxy for jagged vectors and sub-collections),
      ///then any additional arguments.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class Op, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void operate_on_all_objects(const size_type index, Op && op, Args &&... args)
      {
        (std::forward<Op>(op)(Properties {}, this->template get_object<Properties>(index), std::forward<Args>(args)...), ...);
      }

      //--------------------------------------------------------------------------------------------------

      ///@brief Apply @p op, with any additional arguments, to every array.
      ///
      ///@p op will receive as first argument the property definition (which should be an empty structure),
      ///then a reference to the corresponding array (or an appropriate proxy for jagged vectors and sub-collections),
      ///then any additional arguments.
      ///
      ///@p get_actual controls whether the collections should be obtained as the actual collection
      ///   (for the non-const overload), allowing resizing if the top-level collection supports it.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool get_actual = false, class Op, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void operate_on_all_collections(Op && op, Args && ... args) const
      {
        (std::forward<Op>(op)(Properties {}, this->template get_collection<Properties>(), std::forward<Args>(args)...), ...);
      }

      ///@brief Apply @p op, with any additional arguments, to every array.
      ///
      ///@p op will receive as first argument the property definition (which should be an empty structure),
      ///then a reference to the corresponding array (or an appropriate proxy for jagged vectors and sub-collections),
      ///then any additional arguments.
      ///
      ///@p get_actual controls whether the collections should be obtained as the actual collection
      ///   (for the non-const overload), allowing resizing if the top-level collection supports it.
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool get_actual = false, class Op, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void operate_on_all_collections(Op && op, Args && ... args)
      {
        if constexpr (get_actual)
          {
            (std::forward<Op>(op)(Properties {}, this->template get_actual_collection<Properties>(), std::forward<Args>(args)...), ...);
          }
        else
          {
            (std::forward<Op>(op)(Properties {}, this->template get_collection<Properties>(), std::forward<Args>(args)...), ...);
          }
      }

      //--------------------------------------------------------------------------------------------------

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class JVSizeProp>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_jagged_vector_size(const PropertiesIndexer mult_arr_idx, const size_type i) const
      {
        typename JVSizeProp::Type ret = 0;

        Utility::FriendshipProvider::template array_copy_from<JVSizeProp>(*this,
                                                                          mult_arr_idx,
                                                                          MemoryContexts::Current {},
                                                                          MemoryContexts::Current::ContextInfo {},
                                                                          &ret,
                                                                          static_cast<typename Layout::size_type>(1),
                                                                          i);
        return ret;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class JVSizeProp>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void set_jagged_vector_size(const PropertiesIndexer         mult_arr_idx,
                                                                                                  const size_type                 i,
                                                                                                  const typename JVSizeProp::Type new_size)
      {
        Utility::FriendshipProvider::template array_copy_to<JVSizeProp>(*this,
                                                                        mult_arr_idx,
                                                                        MemoryContexts::Current {},
                                                                        MemoryContexts::Current::ContextInfo {},
                                                                        &new_size,
                                                                        static_cast<typename Layout::size_type>(1),
                                                                        i);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ResizeHelper
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        base_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx, const size_type new_size, const bool /*update_jagged_vectors*/)
        {
          c.TrueBase::template base_resize<typename MetaInfo::SizeTag, MetaInfo::extent>(mult_arr_idx, new_size);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        size_tag_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx, const size_type new_size, const bool /*update_jagged_vectors*/)
        {
          const PropertiesIndexer base_index = mult_arr_idx * MetaInfo::extent;

          for (PropertiesIndexer i = 0; i < MetaInfo::extent; ++i)
            {
              c.template set_tagged_size<typename MetaInfo::SizeTag>(base_index + i, new_size);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        array_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx, PropEx, const size_type new_size, const bool /*update_jagged_vectors*/)
        {
          const PropertiesIndexer base_index = mult_arr_idx * PropEx::extent;

          for (PropertiesIndexer i = 0; i < PropEx::extent; ++i)
            {
              c.array_resize<typename PropEx::Property>(base_index + i, new_size);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        jagged_vector_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx, PropEx, const size_type new_size, const bool update_jagged_vectors)
        {
          using JVSizeProp = InterfaceDescription::JaggedVectorSizeProperty<typename PropEx::Property>;

          const PropertiesIndexer base_index = mult_arr_idx * PropEx::extent;

          for (PropertiesIndexer i = 0; i < PropEx::extent; ++i)
            {
              if (update_jagged_vectors)
                {
                  const PropertiesIndexer index = base_index + i;

                  const size_type old_size = c.template get_tagged_size<typename MetaInfo::SizeTag>(index);

                  if (new_size > old_size)
                    {
                      const auto old_end = c.template get_jagged_vector_size<JVSizeProp>(index, old_size);

                      c.template array_resize<JVSizeProp>(index, new_size + 1);

                      for (size_type j = old_size + 1; j < new_size + 1; ++j)
                        {
                          c.template set_jagged_vector_size<JVSizeProp>(index, j, old_end);
                          //Inefficient if we have to memcopy across different memory spaces,
                          //but that's why we can set update_jagged_vectors to false...
                        }
                    }
                  else if (old_size > new_size)
                    {
                      const size_type new_end = static_cast<size_type>(c.template get_jagged_vector_size<JVSizeProp>(index, new_size));

                      using VecInterInfo = InterfaceDescription::InterfaceInformation<impl::properties_getter<typename PropEx::Property>>;

                      c.size_changing_helper_arrays(index,
                                                    typename VecInterInfo::template flattened_per_item<true, false, false> {},
                                                    ResizeHelper {},
                                                    new_end,
                                                    false);

                      c.template set_tagged_size<InterfaceDescription::JaggedVectorSizeProperty<typename PropEx::Property>>(index, new_end);
                    }
                }
            }
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ReserveHelper
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        base_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx, const size_type new_size)
        {
          c.TrueBase::template base_reserve<typename MetaInfo::SizeTag, MetaInfo::extent>(mult_arr_idx, new_size);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        size_tag_change(CollectionHolder &, const PropertiesIndexer, const size_type)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        array_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx, PropEx, const size_type new_size)
        {
          const PropertiesIndexer base_index = mult_arr_idx * PropEx::extent;

          for (PropertiesIndexer i = 0; i < PropEx::extent; ++i)
            {
              c.array_reserve<typename PropEx::Property>(base_index + i, new_size);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        jagged_vector_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx, PropEx, const size_type new_size)
        {
          using JVSizeProp = InterfaceDescription::JaggedVectorSizeProperty<typename PropEx::Property>;

          const PropertiesIndexer base_index = mult_arr_idx * PropEx::extent;

          for (PropertiesIndexer i = 0; i < PropEx::extent; ++i)
            {
              c.array_reserve<JVSizeProp>(base_index + i, new_size + 1);
            }
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ClearHelper
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        base_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx)
        {
          c.TrueBase::template base_clear<typename MetaInfo::SizeTag, MetaInfo::extent>(mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        size_tag_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx)
        {
          const PropertiesIndexer base_index = mult_arr_idx * MetaInfo::extent;

          for (PropertiesIndexer i = 0; i < MetaInfo::extent; ++i)
            {
              c.template set_tagged_size<typename MetaInfo::SizeTag>(base_index + i, 0);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        array_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx, PropEx)
        {
          const PropertiesIndexer base_index = mult_arr_idx * PropEx::extent;

          for (PropertiesIndexer i = 0; i < PropEx::extent; ++i)
            {
              if constexpr (InterfaceDescription::is_jagged_vector_size_property(typename PropEx::Property {}))
                {
                  c.template array_resize<typename PropEx::Property>(base_index + i, 1);
                }
              else
                {
                  c.array_clear<typename PropEx::Property>(base_index + i);
                }
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void jagged_vector_change(CollectionHolder &, const PropertiesIndexer, PropEx)
        {
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_CLASS struct MARIONETTE_PORTABILITY_ANNOTATION_CLASS_ATTRIBUTES ShrinkHelper
      {
        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        base_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx)
        {
          c.TrueBase::template base_shrink_to_fit<typename MetaInfo::SizeTag, MetaInfo::extent>(mult_arr_idx);
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        size_tag_change(CollectionHolder &, const PropertiesIndexer)
        {
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
        template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void
        array_change(CollectionHolder & c, const PropertiesIndexer mult_arr_idx, PropEx)
        {
          const PropertiesIndexer base_index = mult_arr_idx * PropEx::extent;

          for (PropertiesIndexer i = 0; i < PropEx::extent; ++i)
            {
              c.array_shrink_to_fit<typename PropEx::Property>(base_index + i);
            }
        }

        MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class PropEx>
        MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr void jagged_vector_change(CollectionHolder &, const PropertiesIndexer, PropEx)
        {
        }
      };

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... PropExs, class F, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void size_changing_helper_arrays(const PropertiesIndexer mult_arr_idx,
                                                                                                       Utility::TypeHolder<PropExs...>,
                                                                                                       F,
                                                                                                       Args &&... args)
      {
        (F::array_change(*this, mult_arr_idx, PropExs {}, std::forward<Args>(args)...), ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... PropExs, class F, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void size_changing_helper_jagged_vectors(const PropertiesIndexer mult_arr_idx,
                                                                                                               Utility::TypeHolder<PropExs...>,
                                                                                                               F,
                                                                                                               Args &&... args)
      {
        (F::jagged_vector_change(*this, mult_arr_idx, PropExs {}, std::forward<Args>(args)...), ...);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool expand_jagged_vector_arrays, class F, class... Args>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void size_changing_helper(F && f, Args && ... args)
      {
        using Information = InterfaceDescription::InterfaceInformation<PropertyList>;

        F::base_change(*this, this->get_multi_array_index(), std::forward<Args>(args)...);

        size_changing_helper_arrays(this->get_multi_array_index(),
                                    typename Information::template flattened_per_item<true, false, expand_jagged_vector_arrays> {},
                                    std::forward<F>(f),
                                    std::forward<Args>(args)...);

        size_changing_helper_jagged_vectors(this->get_multi_array_index(),
                                            typename Information::template flattened_jagged_vector<true, false> {},
                                            std::forward<F>(f),
                                            std::forward<Args>(args)...);

        F::size_tag_change(*this, this->get_multi_array_index(), std::forward<Args>(args)...);
      }

      //--------------------------------------------------------------------------------------------------

     public:

      ///@remark If @p update_jagged_vectors is @c false, we do not change the jagged vector sizes.
      ///        (The user must call @c update_jagged_vector_size later to ensure everything is consistent.)
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_resize_fully(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void resize(const size_type new_size, const bool update_jagged_vectors = true)
      {
        size_changing_helper<false>(ResizeHelper {}, new_size, update_jagged_vectors);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_resize_fully(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void reserve(const size_type new_size)
      {
        size_changing_helper<false>(ReserveHelper {}, new_size);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_resize_fully(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void clear()
      {
        size_changing_helper<true>(ClearHelper {});
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <bool valid     = Layout::interface_properties().can_resize_fully(),
                                                               class disabler = std::enable_if_t<valid>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void shrink_to_fit()
      {
        size_changing_helper<true>(ShrinkHelper {});
      }

      //--------------------------------------------------------------------------------------------------

     private:

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_copy()
      {
        return Transfers::Transfer<CollectionHolder, CollectionHolder>::can_copy();
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES static constexpr bool can_move()
      {
        return Transfers::Transfer<CollectionHolder, CollectionHolder>::can_move();
      }

     public:

      //"Eric's trick" so copy/move constructors and assignment works as expected.
      //See: https://stackoverflow.com/a/52636474

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder & operator=(
        const std::conditional_t<!can_copy(), CollectionHolder, Utility::InvalidType> &) = delete;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder & operator=(
        const std::conditional_t<can_copy(), CollectionHolder, Utility::InvalidType> & c)
      {
        if constexpr (can_copy())
          {
            if (this != &c)
              {
                Transfers::Transfer<CollectionHolder, CollectionHolder>::copy(*this, c);
              }
          }
        return *this;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder & operator=(
        std::conditional_t<!can_move(), CollectionHolder, Utility::InvalidType> &&) = delete;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder & operator=(
        std::conditional_t<can_move(), CollectionHolder, Utility::InvalidType> && c)
      {
        if constexpr (can_move())
          {
            if (this != &c)
              {
                Transfers::Transfer<CollectionHolder, CollectionHolder>::move(*this, std::move(c));
              }
          }
        return *this;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder(
        const std::conditional_t<!can_copy() || layout_is_non_owning(), CollectionHolder, Utility::InvalidType> &) = delete;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder(
        const std::conditional_t<can_copy() && !layout_is_non_owning(), CollectionHolder, Utility::InvalidType> & c):
        Base()
      {
        Transfers::Transfer<CollectionHolder, CollectionHolder>::copy(*this, c);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder(std::conditional_t < !can_move() || layout_is_non_owning(),
                                                                                       CollectionHolder,
                                                                                       Utility::InvalidType > &&) = delete;

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder(std::conditional_t < can_move() && !layout_is_non_owning(),
                                                                                       CollectionHolder,
                                                                                       Utility::InvalidType > &&c):
        Base()
      {
        Transfers::Transfer<CollectionHolder, CollectionHolder>::move(*this, std::move(c));
      }

      //--------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherT,
                                                               class disabler =
                                                                 std::enable_if_t < Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::can_copy() ||
                                                                 std::is_assignable_v<Base &, const OtherT &> >>
                                                                   MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder & operator=(
                                                                     const OtherT & t)
      {
        if constexpr (Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::can_copy())
          {
            Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::copy(*this, t);
          }
        else /*if constexpr (std::is_assignable_v<Base &, const OtherT &>)*/
          {
            Base::operator=(t);
          }
        return *this;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherT,
                                                               class disabler =
                                                                 std::enable_if_t < Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::can_move() ||
                                                                 std::is_assignable_v<Base &, OtherT &&> >>
                                                                   MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto operator=(OtherT && t)
                                                                     ->std::enable_if_t<std::is_rvalue_reference_v<decltype(t)>, CollectionHolder &>
      {
        if constexpr (Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::can_move())
          {
            Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::move(*this, std::move(t));
          }
        else /*if constexpr (std::is_assignable_v<Base &, OtherT &&>)*/
          {
            Base::operator=(std::move(t));
          }
        return *this;
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class OtherT,
        class disabler = std::enable_if_t < Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::can_copy() && !layout_is_non_owning() &&
                         !std::is_constructible_v<Base, const OtherT &> >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder(const OtherT & t): Base()
      {
        Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::copy(*this, t);
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <
        class OtherT,
        class disabler = std::enable_if_t < Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::can_move() && !layout_is_non_owning() &&
                         !std::is_constructible_v<Base, OtherT &&> >>
                           MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder(OtherT && t): Base()
      {
        Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::move(*this, std::move(t));
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class OtherT,
                                                               class... Args,
                                                               class disabler =
                                                                 std::enable_if_t<Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::can_copy()>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr void special_copy(const OtherT & t, Args &&... args)
      {
        Transfers::Transfer<CollectionHolder, std::decay_t<OtherT>>::copy(*this, t, std::forward<Args>(args)...);
      }

      //--------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <class... OtherTs,
                                                               class disabler = std::enable_if_t<std::is_constructible_v<TrueBase, OtherTs...>>>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr CollectionHolder(OtherTs && ... ts): Base(std::forward<OtherTs>(ts)...)
      {
      }

      //--------------------------------------------------------------------------------------------------

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto make_const_read_proxy(
        const size_type index) const
      {
        using ConstProxyPointerType = std::decay_t<decltype(this->get_layout_holder_pointer())>;

        if constexpr (layout_stores_multi_array_index())
          {
            return Object<ProxyObject<Layout, ConstProxyPointerType, true, true>, PropertyList, MetaInfo> {this->get_layout_holder_pointer(),
                                                                                                           this->get_multi_array_index(),
                                                                                                           index};
          }
        else
          {
            return Object<ProxyObject<Layout, ConstProxyPointerType, true, false>, PropertyList, MetaInfo> {this->get_layout_holder_pointer(), index};
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto make_read_proxy(const size_type index)
      {
        using ProxyPointerType = std::decay_t<decltype(this->get_layout_holder_pointer())>;

        if constexpr (layout_stores_multi_array_index())
          {
            return Object<ProxyObject<Layout, ProxyPointerType, false, true>, PropertyList, MetaInfo> {this->get_layout_holder_pointer(),
                                                                                                       this->get_multi_array_index(),
                                                                                                       index};
          }
        else
          {
            return Object<ProxyObject<Layout, ProxyPointerType, false, false>, PropertyList, MetaInfo> {this->get_layout_holder_pointer(), index};
          }
      }

      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto make_write_proxy() const
      {
        return Object<OwningObject<size_type, difference_type>, PropertyList, MetaInfo> {};
      }

      //--------------------------------------------------------------------------------------------------

#if MARIONETTE_DEBUG_ASSIGNMENT_AND_CONVERSIONS
      MARIONETTE_PORTABILITY_ANNOTATION_PRE_FUNCTION template <Transfers::TransferPriority priority = Transfers::TransferPriority::Maximum, class T>
      MARIONETTE_PORTABILITY_ANNOTATION_FUNCTION_ATTRIBUTES constexpr auto get_transfer_specification(T &&, const bool trace = false)
      {

        return Transfers::impl::get_transfer_specification<CollectionHolder, std::decay_t<T>, priority>(trace);
      }
#endif
    };
  }
}

#endif
